import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { deepIndexOf } from '../../pipes/generales/utils/utils';
import { parseDate } from 'ionic-angular/util/datetime-util';

/*
  Generated class for the ProvClientesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvClientesProvider {
  items = [{
    'codigo': '1',
    'id_suc': '001',
    'titulo': 'ZZZZZZZZZZZZZZZZZZZZZZZ',
    'texto1': 'Barranquilla',
    'texto2': '1111111111',
    'texto3': 'Cali valle',
    'badge': 0,
    'fecha': '2018-02-21'
  }, {
    'codigo': '2',
    'id_suc': '002',
    'titulo': 'XXXXXXXXXXXXXXXXXXXXX',
    'texto1': 'Medellin',
    'texto2': '222222222222222',
    'texto3': 'Cali valle',
    'badge': 0,
    'fecha': '2018-02-19'
  }, {
    'codigo': '3',
    'id_suc': '003',
    'titulo': 'QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ',
    'texto1': '33333333',
    'texto2': 'Cali valle',
    'badge': '$ 30.000',
    'fecha': '2018-02-18'
  }, {
    'codigo': '4',
    'id_suc': '004',
    'titulo': 'CLIENTE VALLE',
    'texto1': 'Palmira',
    'texto2': 'NIT 9812301823-3',
    'texto3': 'Cali valle',
    'badge': '$ 38.000',
    'fecha': '2018-02-15'
  }, {
    'codigo': '5',
    'id_suc': '005',
    'titulo': 'CLIENTE ',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cali valle',
    'badge': '$ 90.000',
    'fecha': '2018-02-14'
  }, {
    'codigo': '6 ',
    'id_suc': '006',
    'titulo': 'CLIENTE SUDAMERICA',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Pradera valle',
    'badge': '$ 36.000',
    'fecha': '2016-03-22'
  }, {
    'codigo': '7 ',
    'id_suc': '007',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cerrito valle',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  }, {
    'codigo': '8 ',
    'id_suc': '008',
    'titulo': 'CLIENTE OTOYA ',
    'texto1': 'Medellin',
    'texto': 'NIT 9812301820-2',
    'texto3': 'Jamundi valle',
    'badge': '$ 100.000',
    'fecha': '2019-02-21'
  }, {
    'codigo': '9 ',
    'id_suc': '009',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Palmira valle',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  }, {
    'codigo': '10 ',
    'id_suc': '010',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Palmira valle',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  } ,{
    'codigo': '11 ',
    'id_suc': '011',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Cali',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Medellin',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  },{
    'codigo': '12 ',
    'id_suc': '012',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Cali',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Medellin',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  },{
    'codigo': '65 ',
    'id_suc': '065',
    'titulo': 'CLIENTE VALLE',
    'texto1': 'Palmira',
    'texto2': 'NIT 9812301823-3',
    'texto3': 'Cali valle',
    'badge': '$ 38.000',
    'fecha': '2018-02-15'
  }, {
    'codigo': '14 ',
    'id_suc': '014',
    'titulo': 'CLIENTE ',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cali valle',
    'badge': '$ 90.000',
    'fecha': '2018-02-14'
  },{
    'codigo': '15 ',
    'id_suc': '015',
    'titulo': 'CLENTE VALLE',
    'texto1': 'Palmira',
    'texto2': 'NIT 9812301823-3',
    'texto3': 'Cali valle',
    'badge': '$ 38.000',
    'fecha': '2018-02-15'
  }, {
    'codigo': '16 ',
    'id_suc': '016',
    'titulo': 'CLIENTE ',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cali valle',
    'badge': '$ 90.000',
    'fecha': '2018-02-14'
  },{
    'codigo': '17 ',
    'id_suc': '017',
    'titulo': 'CLIENTE VALLE',
    'texto1': 'Palmira',
    'texto2': 'NIT 9812301823-3',
    'texto3': 'Cali valle',
    'badge': '$ 38.000',
    'fecha': '2018-02-15'
  }, {
    'codigo': '18 ',
    'id_suc': '018',
    'titulo': 'CLIENTE ',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cali valle',
    'badge': '$ 90.000',
    'fecha': '2018-02-14'
  },{
    'codigo': '19 ',
    'id_suc': '019',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cerrito valle',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  }, {
    'codigo': '58 ',
    'id_suc': '058',
    'titulo': 'CLIENTE OTOYA ',
    'texto1': 'Medellin',
    'texto': 'NIT 9812301820-2',
    'texto3': 'Jamundi valle',
    'badge': '$ 100.000',
    'fecha': '2019-02-21'
  },{
    'codigo': '95 ',
    'id_suc': '095',
    'titulo': 'CLIENTE DEPORCALI',
    'texto1': 'Medellin',
    'texto2': 'NIT 9812301820-2',
    'texto3': 'Cerrito valle',
    'badge': '$ 77.000',
    'fecha': '2017-01-23'
  }, {
    'codigo': '96 ',
    'id_suc': '096',
    'titulo': 'CLIENTE OTOYA ',
    'texto1': 'Medellin',
    'texto': 'NIT 9812301820-2',
    'texto3': 'Jamundi valle',
    'badge': '$ 100.000',
    'fecha': '2019-02-21'
  },];


  constructor(/*public http: HttpClient*/) {
    console.log('Hello ProvClientesProvider Provider');
  }

  leerCliente(rowid,id_sucursal){
    console.log("leerclientes... ",rowid,'---   ',id_sucursal)
    let itemsfilter:any=this.items;
      itemsfilter = this.items.filter((item) => {
        return item.codigo.toLowerCase().indexOf(rowid.toLowerCase()) > -1 && item.id_suc.toLowerCase().indexOf(id_sucursal.toLowerCase()) > -1;
      })
    return new Promise((resolve, reject) => {
      resolve(
        itemsfilter
      )
    });
  }

  listarClientes(texto) {
    let itemsfilter:any=this.items;
    if (texto != '') {
      itemsfilter = this.items.filter((item) => {
        return item.titulo.toLowerCase().indexOf(texto.toLowerCase()) > -1 || item.texto1.toLowerCase().indexOf(texto.toLowerCase()) > -1
        || item.codigo.toLowerCase().indexOf(texto.toLowerCase()) > -1;
      })
    }
    return new Promise((resolve, reject) => {
      resolve(
        itemsfilter
      )
    });

  }

  //filtro por fecha..

  listarClientesFecha(date) {
    let itemsfilter:any=this.items;
    if (date) {
      itemsfilter = this.items.filter((item) => {
        return item.fecha.toLowerCase().indexOf(date.toLowerCase()) > -1;
      })
    }
    return new Promise((resolve, reject) => {
      resolve(
        itemsfilter
      )
    });
  }

}
