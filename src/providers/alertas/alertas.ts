import { Injectable } from '@angular/core';
import * as moment from 'moment';
import 'moment/src/locale/es';

@Injectable()
export class AlertasProvider {
	constructor() {
		moment.locale('es');
	}

}
