import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ToastController, Events } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';
import { NFC } from '@ionic-native/nfc';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { global } from '../../app/global';

@Injectable()
export class ConfiguracionesProvider {
	private theme: BehaviorSubject<String>;

	/*********************
     * Internal properties
     *********************/

	private checkTimer;

	/********************
     * Public properties
     ********************/

	public static instance: ConfiguracionesProvider;

	constructor(
		private network: Network,
		private toast: ToastController,
		private diagnostic: Diagnostic,
		private nfc: NFC,
		private events: Events,
		private platform: Platform,
	) {
		this.theme = new BehaviorSubject('online-theme');

		ConfiguracionesProvider.instance = this;

		platform.ready().then(() => {
			// Initial check and poll every 1s
			this.checkTimer = Observable.timer(0, 1000);

			global.dispositivo.plataforma = 'browser';
			if (this.platform.is('android')) {
				global.dispositivo.plataforma = 'android';
			}
			if (this.platform.is('ios')) {
				global.dispositivo.plataforma = 'ios';
			}

			if (global.dispositivo.plataforma != 'browser') {
				//this.checkTimer.subscribe(this.validacionNatives);
			}

			// Add state change listeners
			if (this.platform.is('android') || this.platform.is('ios')) {
				this.diagnostic.registerBluetoothStateChangeHandler((state) => {
					console.info('Bluetooth state changed to: ' + state);
					//this.validacionNatives();
				});
				this.diagnostic.registerLocationStateChangeHandler((state) => {
					console.info('Location state changed to: ' + state);
					//this.validacionNatives();
				});
			}

			if (this.platform.is('android')) {
				this.diagnostic.registerNFCStateChangeHandler((state) => {
					console.info('NFC state changed to: ' + state);
					//this.validacionNatives();
				});
			}
		});

		this.platform.resume.subscribe(this.validacionNatives);
	}

	eventosGenerales() {
		this.network.onDisconnect().subscribe((data) => {
			console.log('network was disconnected :-(');
			this.networkEstado(data);
		});

		this.network.onConnect().subscribe((data) => {
			console.log('network connected!');
			this.networkEstado(data);
		});
	}

	validacionNatives() {
		if (global.dispositivo.plataforma != 'browser') {
			//--- Validacion de la Ubicacion
			this.diagnostic
				.isLocationAuthorized()
				.then((autorizado) => {
					let permission = this.diagnostic.permission;
					if (!autorizado) {
						this.diagnostic.requestLocationAuthorization(permission.ACCESS_FINE_LOCATION).then(
							(success) => {
								console.log('requestLocationAuthorization, success', success);
							},
							(error) => {
								console.log('requestLocationAuthorization, error', error);
							}
						);
					}
				})
				.catch((e) => {
					console.log('isLocationAuthorized, error', e);
				});

			//--- Validacion de la Camara
			this.diagnostic
				.isCameraAuthorized()
				.then((autorizado) => {
					if (!autorizado) {
						this.diagnostic.requestCameraAuthorization().then(
							(success) => {
								console.log('reuqestCameraAuthroization, success', success);
							},
							(error) => {
								console.log('reuqestCameraAuthroization, error', error);
							}
						);
					}
				})
				.catch((e) => {
					console.log('isCameraAuthorized, error', e);
				});

			//--- Validacion de la Microfono
			this.diagnostic
				.isMicrophoneAuthorized()
				.then((autorizado) => {
					if (!autorizado) {
						this.diagnostic.requestMicrophoneAuthorization().then(
							(success) => {
								console.log('requestMicrophoneAuthorization, success', success);
							},
							(error) => {
								console.log('requestMicrophoneAuthorization, error', error);
							}
						);
					}
				})
				.catch((e) => {
					console.log('isMicrophoneAuthorized, error', e);
				});

			//--- Validacion del Calendario
			this.diagnostic
				.isCalendarAuthorized()
				.then((autorizado) => {
					if (!autorizado) {
						this.diagnostic.requestCalendarAuthorization().then(
							(success) => {
								console.log('requestCalendarAuthorization, success', success);
							},
							(error) => {
								console.log('requestCalendarAuthorization, error', error);
							}
						);
					}
				})
				.catch((e) => {
					console.log('isCalendarAuthorized, error', e);
				});

			//--- Validacion del Memoria
			this.diagnostic
				.isExternalStorageAuthorized()
				.then((autorizado) => {
					if (!autorizado) {
						this.diagnostic.requestExternalStorageAuthorization().then(
							(success) => {
								console.log('requestExternalStorageAuthorization, success', success);
							},
							(error) => {
								console.log('requestExternalStorageAuthorization, error', error);
							}
						);
					}
				})
				.catch((e) => {
					console.log('isExternalStorageAuthorized, error', e);
				});

			//--- Validacion del NFC
			this.diagnostic
				.isNFCAvailable()
				.then((status) => {
					this.nfc
						.enabled()
						.then(() => {
							alert('Se habilito el NFC');
							this.inicializarNFC();
						})
						.catch((err) => {
							console.log('No Existe NFC');
						});
				})
				.catch((e) => {});
		}
	}

	bluetoothEstado() {
		this.diagnostic
			.isBluetoothAvailable()
			.then((estado) => {
				console.log('bluetoothEstado : ' + estado);
			})
			.catch((e) => {});
	}

	networkEstado(data) {
		console.log(data, this);
		let tipoNetwork = this.network.type;
		let toastClass = 'success';
		if (data['type'] == 'offline') {
			toastClass = 'error';
		}

		this.toast
			.create({
				message: 'El dispositivo se encuentra ' + data['type'] + ' de ' + tipoNetwork,
				duration: 3000,
				cssClass: toastClass
			})
			.present();
	}

	setActiveTheme(val) {
		this.theme.next(val);
	}

	getActiveTheme() {
		return this.theme.asObservable();
	}

	inicializarNFC() {
		this.nfc
			.addNdefListener(
				() => {
					console.log('successfully attached ndef listener');
				},
				(err) => {
					console.log('error attaching ndef listener ' + err);
				}
			)
			.subscribe((event) => {
				if (event.tag) {
					let NFGMsg = this.nfc.bytesToString(event.tag.ndefMessage[0].payload);
					this.events.publish('cambioNFC', NFGMsg);
					//this.miNFC['id'] = this.nfc.bytesToHexString(event.tag.id);
					//this.miNFC['mensaje'] = this.nfc.bytesToString(event.tag.ndefMessage[0].payload);
				}
			});
	}
}
