import { Injectable } from '@angular/core';

@Injectable()
export class ProvFacturasProvider {

  constructor() {
    
  }
  getFacturas(rowid, id_sucursal) {
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'titulo': 'CLIENTE CALI',
          'texto1': '01 PV 0001342834',
          'texto2': 'no enviado',
          'texto3': '01/20/2019',
          'badge': '$ 25.000'
        },{
          'titulo': 'CLIENTE SIESA',
          'texto1': '01 PV 0001342835',
          'texto2': 'no enviado',
          'texto3': '01/20/2019',
          'badge': '$ 5.000'
        }, {
          'titulo': 'CLIENTE COLOMBIA',
          'texto1': '01 PV 0001342836',
          'texto2': 'no enviado',
          'texto3': '01/20/2019',
          'badge': '$ 30.000'
        }]
      )
    });

  }

  leerTodos(){
    
  }

  leerPorTercero(rowidTercero, idSucursal){
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'texto1': '01 PV 0001342834',
          'texto2': 'Sin enviar',
          'texto3': '01/20/2019',
          'badge': '$ 25.000'
        }, {
          'texto1': '01 PV 0001342836',
          'texto2': 'Sin enviar',
          'texto3': '01/20/2019',
          'badge': '$ 30.000'
        }]
      )
    });
  }
}
