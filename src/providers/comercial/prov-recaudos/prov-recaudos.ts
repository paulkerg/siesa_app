import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProvRecaudosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvRecaudosProvider {

  constructor(/*public http: HttpClient*/) {
    console.log('Hello ProvRecaudosProvider Provider');
  }
  getRecaudos(rowid, id_sucursal) {
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'titulo': 'CLIENTE EMPRESA',
          'texto1': '01 PV 0001342834',
          'texto2': 'no enviado',
          'texto3': '01/20/2019',
          'badge': '$ 25.000'
        }, {
          'titulo': 'CLIENTE SIESA',
          'texto1': '01 PV 0001342835',
          'texto2': 'no enviado',
          'texto3': '01/20/2019',
          'badge': '$ 5.000'
        }, {
          'titulo': 'CLIENTE EMPANADAS',
          'texto1': '01 PV 0001342836',
          'texto2': 'no enviado',
          'texto3': '01/20/2019',
          'badge': '$ 30.000'
        }]
      )
    });
  }

    getRecaudosId(rowid, id_sucursal) {
      return new Promise((resolve, reject) => {
        resolve(
          [{
            'texto1': '01 PV 0001342834',
            'texto2': 'Enviado',
            'texto3': '01/20/2019',
            'badge': '$ 25.000'
          }, {
            'texto1': '01 PV 0001342835',
            'texto2': 'Enviado',
            'texto3': '01/20/2019',
            'badge': '$ 5.000'
          }, {
            'texto1': '01 PV 0001342836',
            'texto2': 'Sin enviar',
            'texto3': '01/20/2019',
            'badge': '$ 30.000'
          }]
        )
      });

  }

}
