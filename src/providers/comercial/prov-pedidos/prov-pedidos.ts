import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProvPedidosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvPedidosProvider {

  constructor(/*public http: HttpClient*/) {
    console.log('Hello ProvPedidosProvider Provider');
  }

  getPedidos() {
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'titulo': 'luisk',
          'texto1': '01 PV 0001342834',
          'texto2': '01/20/2019',
          'badge': '$ 23000'
        }, {
          'titulo': 'exito',
          'texto1': '01 PV 0001342835',
          'texto2': '01/20/2019',
          'badge': '$ 3000'
        }, {
          'titulo': 'la 14',
          'texto1': '01 PV 0001342836',
          'texto2': '01/20/2019',
          'badge': '$ 3000'
        }]
      )
    });
  }

  getPedidosId(rowid, id_sucursal){
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'texto1': '01 PV 0001342834',
          'texto2': 'Sin enviar',
          'texto3': '01/20/2019',
          'badge': '$ 23000'
        }, {
          'texto1': '01 PV 0001342835',
          'texto2': 'Sin enviar',
          'texto3': '01/20/2019',
          'badge': '$ 3000'
        }, {
          'texto1': '01 PV 0001342836',
          'texto2': 'Sin enviar',
          'texto3': '01/20/2019',
          'badge': '$ 3000'
        }]
      )
    });
  }

}
