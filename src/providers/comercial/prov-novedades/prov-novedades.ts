import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProvNovedadesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProvNovedadesProvider {

  constructor(/*public http: HttpClient*/) {
    console.log('Hello ProvNovedadesProvider Provider');
  }
  getNovedades(rowid, id_sucursal) {
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'titulo': 'CLIENTE SIESA',
          'texto1': '01 PV 0001342834',
          'texto2': 'no enviado',
          'texto3': 'No se encuentra cliente en el lugar',
          'badge': '01/20/2019'
        },{
          'titulo': 'CLIENTE COLOMBIA',
          'texto1': '01 PV 0001342835',
          'texto2': 'no enviado',
          'texto3': 'Stock lleno',
          'badge': '01/20/2019'
        }, {
          'titulo': 'CLIENTE CALI',
          'texto1': '01 PV 0001342836',
          'texto2': 'no enviado',
          'texto3': 'Venir la otra semana',
          'badge': '01/20/2019'
        }]
      )
    });
  }

  leerNovedad(id,id_sucursal){
    return new Promise((resolve, reject) => {
      resolve(
        [{
          'texto1': '01 PV 0001342834',
          'texto2': 'no enviado',
          'texto3': 'No se encuentra cliente en el lugar',
          'badge': '01/20/2019'
        },{
          'texto1': '01 PV 0001342835',
          'texto2': 'no enviado',
          'texto3': 'Stock lleno',
          'badge': '01/20/2019'
        }]
      )
    });
  }

}
