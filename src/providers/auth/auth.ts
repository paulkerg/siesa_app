import { Injectable } from '@angular/core';
import { SqliteProvider } from '../sqlite/sqlite';
import { SincronizacionProvider } from '../sincronizacion/sincronizacion';
import { global } from '../../app/global';
import { Events } from 'ionic-angular';
import { Usuario } from './../../models/usuario';
import { SignalrServiceProvider } from '../signalr-service/signalr-service';

@Injectable()
export class AuthProvider {
	menu = global.menu;
	miUsuario = {
		f_id_usuario: '',
		f_id_funcionario: '',
		f_id_vendedor: '',
		f_identificacion: '',
		f_tercero_rowid: 0,
		f_tercero_nombre: '',
		f_email: '',
		f_tercero_foto: null,
		f_id_perfil: ''
	};
	constructor(
		private dbSql: SqliteProvider,
		private sync: SincronizacionProvider,
		public events: Events,
		private sr: SignalrServiceProvider
	) {}

	loginLocalHuella() {
		return new Promise(async (resolve, reject) => {
			let sql = 'select * from usuarios limit 1';
			await this.dbSql.query(sql).then(async (resp) => {
				if (resp.length > 0) {
					await this.loginLocal({
						password: resp[0]['clave'],
						email: resp[0]['id_usuario']
					}).then(() => {
						resolve(true);
					});
				} else {
					reject('Usuario no registrado');
				}
			});
		});
	}

	loginLocal(param) {
		return new Promise((resolve, reject) => {
			let access = false;
			let sql =
				"select * from usuarios where id_usuario = '" +
				param['email'] +
				"' and clave = '" +
				param['password'] +
				"'";
			this.dbSql.query(sql).then((resp) => {
				if (resp.length > 0) {
					access = param['password'] === resp[0]['clave'] && param['email'] === resp[0]['id_usuario'];
					this.cargarGlogals();
					resolve(true);
				} else {
					resolve(false);
				}
			});
		});
	}

	public login(credentials) {
		return new Promise((resolve, reject) => {
			if (credentials.email == null || credentials.password == null) {
				return reject(false);
			} else {
				let sql = 'select * from t002_mm_propiedades';
				this.dbSql
					.query(sql)
					.then((respuesta) => {
						let r = respuesta[0];
						global.dispositivo.idConexion = r['id_conexion'];
						global.dispositivo.idCia = r['id_cia'];

						let param = {
							ConnectionNumber: global.dispositivo.idConexion,
							Company: global.dispositivo.idCia,
							UserName: credentials.email,
							Password: credentials.password
						};
						this.validaTokenAG(param)
							.then((rp1) => {
								this.validaTokenUNOEE(param)
									.then((rp2) => {
										resolve(true);
									})
									.catch((er2) => {
										resolve(true);
									});
							})
							.catch((er1) => {
								this.validaTokenUNOEE(param)
									.then((rp2) => {
										resolve(true);
									})
									.catch((er2) => {
										this.loginLocal(param)
											.then((rp2) => {
												resolve(rp2);
											})
											.catch((e) => {
												reject(e);
											});
									});
							});
					})
					.catch((error) => {
						reject('No existe usuario');
					});
			}
		});
	}

	cargarGlogals() {
		let sql = 'select * from configuraciones';
		this.dbSql.query(sql).then((resp) => {
			if (resp.length > 0) {
				let u = new Usuario({
					f_email: null,
					f_id_funcionario: null,
					f_id_perfil: null,
					f_id_usuario: null,
					f_id_vendedor: null,
					f_identificacion: null,
					f_tercero_foto: null,
					f_tercero_nombre: null,
					f_tercero_rowid: null
				});
				for (let i = 0; i < resp.length; i++) {
					const id = resp[i]['id'];
					if (resp[i]['documento'] && resp[i]['documento'] != '') {
						const e = JSON.parse(resp[i]['documento']);
						if (id == 'usuario_siesapps') {
							u.f_id_funcionario = e['f_id_funcionario'];
							u.f_id_perfil = e['f_id_perfil'];
							u.f_id_vendedor = e['f_id_vendedor'];
							u.f_identificacion = e['f_identificacion'];
						}
						if (id == 'usuario') {
							u.f_id_usuario = e['f_id_usuario'];
							u.f_email = e['f_email'];
							u.f_tercero_rowid = e['f_tercero_rowid'];
							u.f_tercero_foto = e['f_tercero_foto'];
							u.f_tercero_nombre = e['f_tercero_nombre'];
						}
					}
				}
				global.usuario = u;
				global.dispositivo.idVendedor = u.f_id_vendedor;
				global.dispositivo.idFuncionario = u.f_id_funcionario;
				global.dispositivo.nombre = u.f_tercero_nombre;
				global.dispositivo.idUsuario = u.f_id_usuario;
			}
		});
	}

	validaTokenAG(param) {
		return new Promise((resolve, reject) => {
			this.sync
				.postParameters(global.api.GetTokenAG, param,0,'AG')
				.then((resultado) => {
					if (resultado['errores']) {
						reject(resultado['errores']);
					} else {
						global.dispositivo.idUsuario = param['UserName'];
						//--- Se almacena el token
						this.sync.setToket(resultado['token'], 'AG');
						this.sync.setToket(resultado['token'], 'UNOEE');
						//--- Actualizar usuario eb la BD Local
						let u = {
							f_id_usuario: resultado['usuarioTerceroAg']['identificacion'],
							f_nro_intentos_clave: resultado['usuarioAg']['numeroIntentos'],
							f_ind_cambiar: resultado['usuarioAg']['indCambiarPassword'],
							f_ind_estado: resultado['usuarioAg']['indEstado'],
							f_ind_bloqueado: resultado['usuarioAg']['indBloqueado'],
							f_email: resultado['usuarioAg']['email'],
							f_tercero_rowid: resultado['usuarioTerceroAg']['rowidTercero'],
							f_tercero_id: resultado['usuarioTerceroAg']['rowidUsuarioAg'],
							f_tercero_nombre: resultado['usuarioAg']['nombre'],
							f_contacto_nombre: resultado['usuarioAg']['nombre'],
							f_contacto_direccion: resultado['usuarioAg']['notas'],
							f_contacto_telefono: resultado['usuarioAg']['telefono1'],
							f_contacto_celular: resultado['usuarioAg']['telefono2'],
							f_tercero_foto: ''
						};
						let arrSql = [];
						arrSql.push(
							"update usuarios set nombre = '" +
								u['f_tercero_nombre'] +
								"', email = '" +
								u['f_email'] +
								"'"
						);

						arrSql.push(
							"insert or replace into configuraciones(id,documento) values('usuario','" +
								JSON.stringify(u) +
								"')"
						);

						this.miUsuario.f_id_usuario = u['f_id_usuario'];
						this.miUsuario.f_email = u['f_email'];
						this.miUsuario.f_tercero_rowid = u['f_tercero_rowid'];
						this.miUsuario.f_tercero_nombre = u['f_tercero_nombre'];
						this.miUsuario.f_identificacion = u['f_tercero_id'];

						global.usuario = new Usuario(this.miUsuario);

						global.dispositivo.idVendedor = u['f_id_usuario'];
						global.dispositivo.idFuncionario = u['f_id_usuario'];
						global.dispositivo.nombre = u['f_tercero_nombre'];
						global.dispositivo.idUsuario = u['f_id_usuario'];

						this.dbSql
							.queryAll(arrSql)
							.then(() => {
								this.actualizaTablaUsuario(param)
									.then((r) => {
										this.buscarMenuUNOEE().then(() => {
											this.buscarMenuAG()
												.then(() => {
													resolve(true);
												})
												.catch(() => {
													resolve(true);
												});
										});
									})
									.catch((e) => {
										reject(e);
									});
							})
							.catch((e) => {
								reject(e);
							});
					}
				})
				.catch((err) => {
					reject(err);
				});
		});
	}

	validaTokenUNOEE(param) {
		return new Promise((resolve, reject) => {
			this.sync
				.postParameters(global.api.GetToken, param)
				.then((resultado) => {
					if (resultado['errores']) {
						reject(resultado['errores']);
					} else {
						global.dispositivo.idUsuario = param['UserName'];
						//--- Se almacena el token
						this.sync.setToket(resultado['token'], 'UNOEE');
						//--- Actualizar usuario eb la BD Local
						this.actualizaTablaUsuario(param)
							.then((r) => {
								this.buscarDatosUsuario().then(() => {
									this.buscarPerfilUsuario().then(() => {
										this.buscarMenuUNOEE().then(() => {
											resolve(true);
										});
									});
								});
							})
							.catch((e) => {
								reject(e);
							});
					}
				})
				.catch((err) => {
					reject(err);
				});
		});
	}

	actualizaTablaUsuario(param) {
		return new Promise((resolve, reject) => {
			let sql =
				"update usuarios set id_usuario = '" +
				param['UserName'] +
				"', clave = '" +
				param['Password'] +
				"', estado = 'true' ";
			this.dbSql
				.query(sql)
				.then((r) => {
					resolve(true);
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	buscarDatosUsuario() {
		return new Promise((resolve, reject) => {
			//--- Guarda Vendedor y parametros de sincronizacion
			let p2 = {
				Parametros: global.dispositivo.idUsuario + '|',
				Consulta: 'BUSCAR_USUARIO_SIESAPPS'
			};
			this.sync
				.postParameters(global.api.consulta, p2)
				.then((r) => {
					let arrSql = [];
					if (r['length'] > 0) {
						arrSql.push(
							"update usuarios set nombre = '" +
								r[0]['f_tercero_nombre'] +
								"', email = '" +
								r[0]['f_email'] +
								"'"
						);

						arrSql.push(
							"insert or replace into configuraciones(id,documento) values('usuario','" +
								JSON.stringify(r[0]) +
								"')"
						);

						this.miUsuario.f_id_usuario = r[0]['f_id_usuario'];
						this.miUsuario.f_email = r[0]['f_email'];
						this.miUsuario.f_tercero_rowid = r[0]['f_tercero_rowid'];
						this.miUsuario.f_tercero_nombre = r[0]['f_tercero_nombre'];
						this.miUsuario.f_identificacion = r[0]['f_tercero_id'];

						global.usuario = new Usuario(this.miUsuario);
					} else {
						arrSql.push("update usuarios set nombre = '" + 'Sin Usuario' + "', email = ''");

						arrSql.push("insert or replace into configuraciones(id,documento) values('usuario','{}')");
					}

					this.dbSql
						.queryAll(arrSql)
						.then(() => {
							this.cargarGlogals();
							resolve(true);
						})
						.catch((e) => {
							reject(e);
						});
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	buscarPerfilUsuario() {
		return new Promise((resolve, reject) => {
			let p3 = {
				Parametros: global.dispositivo.idUsuario + '|',
				Consulta: 'BUSCAR_PERFIL_USUARIO_SIESAPPS'
			};
			this.sync
				.postParameters(global.api.consulta, p3)
				.then((rp) => {
					let arrSql2 = [];
					if (rp['length'] > 0) {
						global.dispositivo.idVendedor = rp[0]['f_id_vendedor'];
						global.dispositivo.idFuncionario = rp[0]['f_id_funcionario'];
						global.dispositivo.nombre = rp[0]['f_tercero_nombre'];

						this.miUsuario.f_id_vendedor = rp[0]['f_id_vendedor'];
						this.miUsuario.f_id_funcionario = rp[0]['f_id_funcionario'];
						this.miUsuario.f_tercero_nombre = rp[0]['f_tercero_nombre'];

						let filtrosP = {
							tInventarioSel: 'null',
							tLprecioSel: rp[0]['f_id_lista_precio_default'],
							tBodegasSel: rp[0]['f_id_bodega'],
							tGBodegasSel: rp[0]['f_id_grupo_bodega_pedido'],
							planCriterioSel: 'null'
						};
						arrSql2.push("update t002_mm_propiedades set id_vendedor = '" + rp[0]['f_id_vendedor'] + "'");

						arrSql2.push(
							"insert or replace into configuraciones(id,documento) values('usuario_siesapps','" +
								JSON.stringify(rp[0]) +
								"')"
						);

						this.miUsuario.f_id_vendedor = rp[0]['f_id_vendedor'];
						this.miUsuario.f_id_funcionario = rp[0]['f_id_funcionario'];
						this.miUsuario.f_identificacion = rp[0]['f_identificacion'];

						arrSql2.push(
							"insert or replace into configuraciones(id,documento) values('parametros_inventarios','" +
								JSON.stringify(filtrosP) +
								"')"
						);
						this.dbSql
							.queryAll(arrSql2)
							.then(() => {
								resolve(true);
							})
							.catch((e) => {
								reject(e);
							});
					} else {
						resolve(true);
					}
				})
				.catch((error) => {
					reject('No existe Perfil');
				});
		});
	}

	buscarMenuUNOEE() {
		return new Promise((resolve, reject) => {
			let menuPrin = [];
			let p4 = {
				Parametros: '|',
				Consulta: 'SIESAPPS_MENU'
			};
			this.sync
				.postParameters(global.api.consulta, p4)
				.then((r: any) => {
					if (r['length'] > 0) {
						for (let i = 0; i < r.length; i++) {
							menuPrin.push({
								title: r[i]['f_titulo'],
								theme: r[i]['f_tema'],
								icon: r[i]['f_icono'],
								icono: '',
								listView: r[i]['f_listview'],
								component: r[i]['f_componente'],
								page: r[i]['f_pagina']
							});
						}

						this.events.publish('cambioMenu', 'true');

						let sql =
							"insert or replace into configuraciones(id,documento) values('menu','" +
							JSON.stringify(menuPrin) +
							"')";

						this.dbSql
							.query(sql)
							.then(() => {
								resolve({});
							})
							.catch((e) => {
								reject(e);
							});
					}
				})
				.catch((error) => {
					reject('No existe Perfil');
				});
		});
	}

	validaUsuarioLocal() {
		return new Promise((resolve, reject) => {
			let sinUsuario = true;
			let sql = 'select * from usuarios limit 1';
			this.dbSql.query(sql).then(async (resp) => {
				if (resp.length > 0) {
					if (resp[0].id_usuario != 'siesa') {
						sinUsuario = false;
					}
				}
				resolve(sinUsuario);
			});
		});
	}

	getUsuarioLocal() {
		return new Promise((resolve, reject) => {
			let sql = 'select * from usuarios limit 1';
			this.dbSql.query(sql).then(async (resp) => {
				resolve(resp);
			});
		});
	}

	buscarMenuAG() {
		return new Promise((resolve, reject) => {
			this.sync
				.get(global.api.GetMenusAG, {})
				.then((r) => {
					resolve(true);
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	leerVersionERP() {
		return new Promise((resolve, reject) => {
			let p2 = {
				Parametros: global.dispositivo.idUsuario + '|',
				Consulta: 'SIESAPPS_VERSION_ERP'
			};
			this.sync
				.postParameters(global.api.consulta, p2)
				.then((r) => {
					resolve(r);
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	logout() {
		return new Promise((resolve, reject) => {
			this.sync.clearToken();
			this.sr.cerrarConexion();
			resolve(true);
		});
		/*
		return new Promise((resolve, reject) => {
			this.sync.postParameters(global.api.salir, {})
				.then((r) => {
					resolve(true);
				})
				.catch((error) => {
					this.sync.clearToken();
					resolve(true);
				});
		});
		*/
	}
}
