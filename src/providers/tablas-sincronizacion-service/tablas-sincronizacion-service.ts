import { Injectable } from '@angular/core';
import { SincronizacionProvider } from '../sincronizacion/sincronizacion';
import { SqliteProvider } from '../sqlite/sqlite';
import { Events } from 'ionic-angular';
import { global } from '../../app/global';

/*
  Generated class for the TablasSincronizacionServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TablasSincronizacionServiceProvider {
	constructor(private sync: SincronizacionProvider, private db: SqliteProvider, public events: Events) {
		events.subscribe('blnCancelar_sincronizacion', (data) => {
			this.blnCancelar = data;
		});
	}

	public blnCancelar: boolean = false;
	totalRegistros: number = 0;
	porcentaje_total: number = 0;
	tablaActual: number = 0;
	tablaTotal: number = 0;
	registro_totales: number = 0;
	pagina_actual: number = 0;
	pagina_total: number = 0;
	porcentaje_tabla: number = 0;
	cargando: boolean = false;
	tabla_sincronizacion: string = '';
	sincronizando: boolean = false;
	modulo: string = "";

	public iniciarSincronizacion(modulo) {
		this.modulo = modulo;
		if (!this.sincronizando) {
			let modulo_filtro = '';

			if (!modulo) {
				modulo_filtro = '';
			} else {
				modulo_filtro = ' and id_modulo =' + modulo;
			}

			let sql = `select * 
        from t003_mm_tablas_actualizacion 
        where 
        ind_porce_db = 0 
        and ind_actualizado = 0
        ${modulo_filtro}
        `;
				//console.warn('sql', sql);
			this.db.query(sql).then((resultado) => {
				this.sincronizando = true;
				//console.warn('resultado de tablas a sincronizar');
				//console.warn(resultado);
				this.totalRegistros = 0;
				this.blnCancelar = false;
				this.events.publish('totalRegistros_sincornizacion', this.totalRegistros);
				this.cargarTablas(resultado);
			});
		}
	}

	/**
   *
   * @param tablas Arreglo de nombre de Tablas de la BD a sincronizar
   * @param indice por donde debe de arrancar por defecto es 0 siempre
   */
	cargarTablas(tablas, indice: number = 0) {
		if (!this.blnCancelar) {
			if (indice === tablas.length - 1) {
				this.sincronizar(tablas[indice].tabla, tablas[indice].where_delete)
					.then((res) => {
						// this.msj_fin = "Sincronizado";
						this.porcentaje_total = Math.round((indice + 1) / tablas.length * 100);
						this.events.publish('porcentaje_total_sincronizacion', this.porcentaje_total);
						this.tablaActual = indice + 1;
						this.events.publish('tablaActual_sincronizacion', this.tablaActual);
						this.tablaTotal = tablas.length;
						this.events.publish('tablaTotal_sincronizacion', this.tablaTotal);
						this.cargando = false;
						this.events.publish('cargando_sincronizacion', this.cargando);
						this.confirmarSincronizacion();
						//Aqui llamar funcion para eliminar actividades pasadas de 5 dias de enviadas
						this.borradoActividadesEnviadas();
						this.events.publish('terminada_sincronizacion', true);
						this.sincronizando = false;
						if(this.modulo!=null&&this.modulo!=""){
							global['sincronizando'][""+this.modulo] = false;
						}
					})
					.catch((error) => {
						console.error('ERROR al sincronizar:');
						console.error(error);
					});
			} else {
				this.sincronizar(tablas[indice].tabla, tablas[indice].where_delete)
					.then((res) => {
						this.porcentaje_total = Math.round((indice + 1) / tablas.length * 100);
						this.events.publish('porcentaje_total_sincronizacion', this.porcentaje_total);
						this.tablaActual = indice + 1;
						this.events.publish('tablaActual_sincronizacion', this.tablaActual);
						this.tablaTotal = tablas.length;
						this.events.publish('tablaTotal_sincronizacion', this.tablaTotal);
						//Actualizacion de tiempo de ultima sincronizacion
						let query =
							"update t003_mm_tablas_actualizacion set fecha_ult_sync = datetime('now', 'localtime') where tabla = '" +
							tablas[indice].tabla +
							"'";
						this.db.queryExec(query).then((res) => {
							this.cargarTablas(tablas, indice + 1);
						});
					})
					.catch((error) => {
						console.error('ERROR al sincronizar:');
						console.error(error);
					});
			}
		} else {
			// console.log('Se cancelo');
			//this.dismiss();
		}
	}

	borradoActividadesEnviadas() {
		let sql = `delete from w0822_actividades_ot where (julianday('now') - julianday(c0822_fecha_enviada_app))>1`;
		this.db
			.query(sql)
			.then((res) => {
				// console.log('Se han borrado las actividades mayores a 5 dias desde la fecha de envio');
			})
			.catch((error) => {
				console.error(
					'Error al intentar eliminar las actividaedes enviadas mayores a 5 dias desde la fecha de envio'
				);
				console.error(error);
			});
	}

	/**
   * Metodo para confirmar al servidor de la recepcion de toda la sincronizacion
   */
	confirmarSincronizacion() {
		//Esta confirmacion se realiza para el modulo de mantenimiento
		//esto actualizar en el lado del servidor las actividades que se sincronizaron
		//desde aqui se envia los ROWID a actualizar
		let sql = ` 
      select
        c0822_rowid 
      from w0822_actividades_ot`;
		let actividades = [];
		this.db
			.query(sql)
			.then((res) => {
				actividades = res;
				if (actividades.length > 0) {
					let rowids = '';
					actividades.forEach((actividad) => {
						rowids += actividad.c0822_rowid + ',';
					});
					rowids = rowids.substring(0, rowids.length - 1);
					//console.log('global.usuario', global.usuario);
					if (global.usuario['f_tercero_rowid'] == null || global.usuario['f_tercero_rowid'] == '') {
						if (localStorage.getItem('usuario')) {
							global.usuario = JSON.parse(localStorage.getItem('usuario'));
						}
					}
					let p3 = {
						parametros: global.usuario['f_tercero_rowid'] + '|' + rowids,
						Consulta: 'Wp03_app_gestionar_estado',
						Vendedor: global.dispositivo.idVendedor ? global.dispositivo.idVendedor : 'ninguno',
						Tabla: 'Wp03_app_gestionar_estado'
					};
					//aqui se prepara para hacer la peticion de confirmacion
					this.sync
						.postParameters(global.api.sincronizar, p3)
						.then((res) => {
							//console.log('se logro confirmar la sincronizacion');
							this.blnCancelar = true;
							this.events.publish('blnCancelar_sincronizacion', this.blnCancelar);
						})
						.catch((error) => {
							this.blnCancelar = true;
							this.events.publish('blnCancelar_sincronizacion', this.blnCancelar);
							console.error('Erro al realizar la peticion de confirmacion de sincronizacion');
							console.error(error);
						});
				} else {
					this.blnCancelar = true;
				}
			})
			.catch((error) => {
				console.error(
					'Error al consultar la tabla de las actividades de mantenimeinto en el momento de confirmar la soncronizacion'
				);
				console.error(error);
			});
	}

	/**
   *
   * @param tabla nombre de la tabla para realizar el proceso de sincronizacion
   */
	sincronizar(tabla, where_delete = null) {
		return new Promise((resolve, reject) => {
			//console.log('SE va a limpiar ###########');
			this.limpiarTabla(tabla, where_delete)
				.then((res) => {
					this.procesar(tabla)
						.then((res) => {
							resolve(res);
						})
						.catch((error) => {
							reject(error);
						});
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
   *
   * @param tabla Tabla local que se debe limpiar antes de insertar los registros
   */
	limpiarTabla(tabla, where_delete = null) {
		return new Promise((resolve, reject) => {
			let sql = 'delete from ' + tabla + '  ';
			if (where_delete) sql += where_delete;
			//console.warn('SQL DELETE: ' + sql);
			this.db
				.query(sql)
				.then((res) => {
					resolve(res);
				})
				.catch((err) => {
					console.error('ERROR al borrar las tabla: --' + tabla + '--');
					console.error(err);
					reject(err);
				});
		});
	}

	/**
   *
   * @param tabla nombre de la tabla a sincronizar los datos
   * @param indiceActual indice que se debe consultar en la API para obtener los datos por defecto es 0
   */
	procesar(tabla, indiceActual = 0, intento = 1) {
		this.tabla_sincronizacion = tabla;
		this.events.publish('tabla_sincronizacion', this.tabla_sincronizacion);
		return new Promise((resolve, reject) => {
			let indice: string = indiceActual + '';
			let vendedor = 'ninguno';
			if (global.dispositivo.idVendedor) vendedor = global.dispositivo.idVendedor;

			if (global.usuario['f_tercero_rowid'] == null || global.usuario['f_tercero_rowid'] == '') {
				if (localStorage.getItem('usuario')) {
					global.usuario = JSON.parse(localStorage.getItem('usuario'));
				}
			}
			let p1 = {
				Vendedor: vendedor,
				Tabla: tabla,
				parametros: global.usuario['f_tercero_rowid'] + '|',
				indiceActual: indice
			};
			this.sync
				.postParameters(global.api.sincronizar, p1)
				.then((resultado: any) => {
					if (resultado.datos !== null && resultado.datos !== undefined && resultado.datos.length > 0) {
						this.registro_totales = resultado.nroTotalRegistros;
						this.events.publish('registro_totales_sincronizacion', this.registro_totales);
						this.totalRegistros += resultado.nroTotalRegistros;
						this.events.publish('totalRegistros_sincronizacion', this.totalRegistros);
						this.insertarBD(
							resultado.datos,
							tabla,
							resultado.nroTotalRegistros,
							resultado.indicePrimerRegistro,
							resultado.indiceUltimoRegistro,
							resultado.tamanoPagina
						)
							.then((res) => {
								if (resultado.indiceSiguiente > 0) {
									this.procesar(tabla, resultado.indiceSiguiente).then((res) => {
										resolve(true);
									});
								} else {
									resolve(true);
								}
							})
							.catch((error) => {
								console.error('ERROR AL INSERTAR EN LA BD');
								console.error(error.error);
								console.error(error.data);
							});
					} else {
						resolve(true);
					}
				})
				.catch((error) => {
					//se vuelve a realizar el mismo proceso si falla
					//console.warn('Reintentando tabla: ' + tabla + ' intento No:' + intento);
					if (intento > 20) {
						reject(error);
					} else {
						this.procesar(tabla, indiceActual, intento + 1);
					}
				});
		});
	}

	/**
   *
   * @param datos Arreglo de datos de la tabla a insertar
   * @param tabla nombre de la tabla donde se insertan los datos
   */
	insertarBD(datos, tabla, nroTotalRegistros, indicePrimerRegistro, indiceUltimoRegistro, tamanoPagina) {
		return new Promise((resolve, reject) => {
			var campos = Object.keys(datos[0]);
			let paginas_total = Math.ceil(nroTotalRegistros / tamanoPagina);
			let pagina_actual = Math.ceil((indiceUltimoRegistro + 1) / tamanoPagina);
			this.pagina_actual = pagina_actual;
			this.events.publish('pagina_actual_sincronizar', this.pagina_actual);
			this.pagina_total = paginas_total;
			this.events.publish('pagina_total_sincronizar', this.pagina_total);
			var campos_string = '';
			campos.forEach((element) => {
				if (element !== undefined) {
					campos_string += element + ',';
				}
			});
			campos_string = campos_string.substring(0, campos_string.length - 1);
			this.ejecutarQuery(
				datos,
				tabla,
				campos_string,
				pagina_actual,
				paginas_total,
				indicePrimerRegistro,
				indiceUltimoRegistro,
				nroTotalRegistros
			)
				.then((res) => {
					resolve(true);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
   *
   * @param datos arreglo de datos de la tabla a sincronizar
   * @param tabla nombre de la tabla a sincronizar
   * @param campos_string campos de la tabla a sincronizar
   * @param indice por donde debe de inicar por defecto siempre 0
   * @param contador vairable de control de proceso siempre arranca en 1
   */
	ejecutarQuery(
		datos,
		tabla,
		campos_string,
		pagina_actual,
		pagina_total,
		indicePrimerRegistro,
		indiceUltimoRegistro,
		nroTotalRegistros,
		indice = 0,
		contador = 1
	) {
		return new Promise((resolve, reject) => {
			if (contador < 1) contador = 1;
			let insert = 'insert into ' + tabla + ' (' + campos_string + ') values (';
			let sql = '';
			let sqlBulk = [];
			for (let i = 0; i < datos.length; i++) {
				let elemento = datos[i];
				let values_string = '';
				for (var n in elemento) {
					values_string += "'" + (elemento[n]?(elemento[n]+"").replace("'", "`"):"") + "',";
				}
				values_string = values_string.substring(0, values_string.length - 1);
				sql = insert + values_string + ')';
				sqlBulk.push(sql);
			}
			this.db
				.queryAll(sqlBulk)
				.then((res) => {
					this.porcentaje_tabla = Math.round(pagina_actual / pagina_total * 100);
					this.events.publish('porcentaje_tabla_sincronizacion', this.porcentaje_tabla);
					resolve(true);
				})
				.catch((error) => {
					reject({ error: error, data: sqlBulk });
				});
		});
	}
}
