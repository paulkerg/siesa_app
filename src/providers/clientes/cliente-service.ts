import { Injectable } from '@angular/core';
import { cliente, cp, h_mt } from './../../models/interfaces/cliente';
import { emp } from './../../models/interfaces/empresa';
import { SqliteProvider } from '../sqlite/sqlite';
@Injectable()
export class ClienteServiceProvider {
	constructor(private ldb: SqliteProvider) {}

	public leer(filtro) {
		let sql =
			"SELECT  'assets/images/avatar/accion.jpg' image, ifnull((select sum((f353_saldo + f353_total_db_aplicado) - f353_total_cr_aplicado)  from t353_co_saldo_abierto  where f353_rowid_tercero = f201_rowid_tercero and f353_id_sucursal = f201_id_sucursal),0) saldocxc, ifnull((select sum(f353_total_ch_postf_orden)  from t353_co_saldo_abierto  where f353_rowid_tercero = f201_rowid_tercero and f353_id_sucursal = f201_id_sucursal),0) saldocxcche ,t200.f200_rowid, t200.f200_id, t201.f201_id_sucursal, t200.f200_nit, t200.f200_dv_nit, t200.f200_razon_social, t200.f200_nombre_est, t201.f201_descripcion_sucursal, t015.f015_id_barrio, t015.f015_telefono, t015.f015_direccion1, t013.f013_descripcion FROM t200_mm_terceros t200 INNER JOIN t201_mm_clientes  t201 ON t200.f200_rowid = t201.f201_rowid_tercero INNER JOIN t015_mm_contactos t015 ON t201.f201_rowid_contacto = t015.f015_rowid INNER JOIN t013_mm_ciudades  t013 ON t015.f015_id_pais = t013.f013_id_pais AND t015.f015_id_depto = t013.f013_id_depto AND t015.f015_id_ciudad = t013.f013_id WHERE (f200_razon_social like '%%' or f200_razon_social like '%%')  order by f200_razon_social asc  limit 20";
		return this.ldb
			.queryExec(sql)
			.then((response) => {
				return Promise.resolve(response);
			})
			.catch((error) => {
				return Promise.reject(error);
			});
	}

	public buscarPor(p_tipo, p_dato, pg) {
		let filtro = '';
		if (p_tipo == 'C') {
			filtro = "f200_id like '" + p_dato + "%' order by f200_id asc ";
		}
		if (p_tipo == 'N') {
			filtro =
				"(t200.f200_razon_social LIKE '%" +
				p_dato +
				"%' OR t200.f200_nombre_est LIKE '%" +
				p_dato +
				"%' OR t201.f201_descripcion_sucursal LIKE '%" +
				p_dato +
				"%')  order by f200_razon_social asc ";
		}

		let sql =
			'SELECT t200.f200_rowid cliente_rowid, t200.f200_id cliente_id,t201.f201_id_sucursal cliente_id_suc,' +
			'	t200.f200_nit cliente_nit,t200.f200_dv_nit cliente_dv,t200.f200_razon_social cliente_razon_social,' +
			'	t200.f200_nombre_est cliente_nombre_est,t201.f201_descripcion_sucursal cliente_descripcion_sucursal,' +
			'	t015.f015_id_barrio cliente_barrio,t015.f015_telefono cliente_telefono,' +
			'	t015.f015_direccion1 cliete_direccion,t013.f013_descripcion cliente_ciudad,' +
			'	ifnull((SELECT sum((f353_saldo + f353_total_db_aplicado) - f353_total_cr_aplicado)' +
			'			FROM t353_co_saldo_abierto WHERE f353_rowid_tercero = f201_rowid_tercero AND f353_id_sucursal = f201_id_sucursal),0) cliente_saldocxc,' +
			'	ifnull((SELECT sum(f353_total_ch_postf_orden) FROM t353_co_saldo_abierto ' +
			'      WHERE f353_rowid_tercero = f201_rowid_tercero AND f353_id_sucursal = f201_id_sucursal),0) cliente_saldocxcche' +
			' ,(SELECT count(1) FROM t430_cm_pv_docto WHERE f430_rowid_tercero_fact = t200.f200_rowid AND f430_id_sucursal_fact = t201.f201_id_sucursal) cliente_nro_ped' +
			' ,(SELECT count(1) FROM t350_co_docto_contable WHERE f350_rowid_tercero = t200.f200_rowid AND f350_id_sucursal = t201.f201_id_sucursal) cliente_nro_rec' +
			' ,0 cliente_nro_fac' +
			' ,0 cliente_nro_nov';

		let sql2 =
			' FROM t200_mm_terceros t200' +
			' INNER JOIN t201_mm_clientes t201 ON t200.f200_rowid = t201.f201_rowid_tercero' +
			' INNER JOIN t015_mm_contactos t015 ON t201.f201_rowid_contacto = t015.f015_rowid' +
			' INNER JOIN t013_mm_ciudades t013 ON t015.f015_id_pais = t013.f013_id_pais' +
			'       AND t015.f015_id_depto = t013.f013_id_depto AND t015.f015_id_ciudad = t013.f013_id' +
			' WHERE ' +
			filtro;

		if (p_tipo == 'F') {
			sql2 =
				' FROM t5792_sm_ruta_visita AS t5792' +
				' INNER JOIN t5791_sm_ruta_frecuencia AS t5791 ON t5792.f5792_rowid_ruta_frecuencia = t5791.f5791_rowid' +
				' INNER JOIN t201_mm_clientes AS t201 ON t5791.f5791_rowid_tercero = t201.f201_rowid_tercero' +
				' 			AND t5791.f5791_id_suc_cliente = t201.f201_id_sucursal' +
				' INNER JOIN t200_mm_terceros AS t200 ON t201.f201_rowid_tercero = t200.f200_rowid' +
				' INNER JOIN t015_mm_contactos AS t015 ON t201.f201_rowid_contacto = t015.f015_rowid' +
				' INNER JOIN t013_mm_ciudades AS t013 ON t015.f015_id_pais = t013.f013_id_pais' +
				' 			AND t015.f015_id_depto = t013.f013_id_depto AND t015.f015_id_ciudad = t013.f013_id' +
				" WHERE t5792.f5792_fecha like '%" +
				p_dato +
				"%'" +
				' ORDER BY t5791.f5791_orden';
		}

		let sqlfull = sql + sql2 + 'LIMIT 10 OFFSET  10 * ' + pg;
		//console.log(sqlfull)
		return this.ldb
			.queryExec(sqlfull)
			.then((response) => {
				return Promise.resolve(response);
			})
			.catch((error) => {
				return Promise.reject(error);
			});
	}

	public burcarPorId(rowid_cliente, id_sucursal) {
		let sqlfull =
			'SELECT t201.f201_rowid_tercero cliente_rowid_tercero,t201.f201_id_sucursal cliente_id_sucursal,t200.f200_id cliente_id,' +
			' 			 t200.f200_nit cliente_nit,t200.f200_dv_nit cliente_nit_dv,t200.f200_razon_social cliente_razon_social,' +
			' 			 t200.f200_nombre_est cliente_nombre_est,t201.f201_descripcion_sucursal cliente_descripcion_sucursal,' +
			' 			 t201.f201_cupo_credito cliente_cupo_credito,t201.f201_ind_bloqueo_cupo cliente_ind_blo_cupo,' +
			' 			 t201.f201_ind_bloqueo_mora cliente_ind_blo_mora,0 cliente_ind_blo_margen,' +
			' 			 t015.f015_direccion1 cliente_direccion,t015.f015_telefono cliente_telefono,t015.f015_fax cliente_fax,' +
			' 			 t015.f015_cod_postal cliente_cod_postal,t015.f015_email cliente_email,t013.f013_descripcion cliente_ciudad,' +
			' 			 t014.f014_descripcion cliente_barrio,' +
			' 			 0 cliente_disponible,1 cliente_corriente,0 cliente_vencido,0 cliente_remisiones,0 cliente_pedidos,' +
			'        3.4693547 lat, -76.52635099999999 long ' +
			' ,(SELECT count(1) FROM t430_cm_pv_docto WHERE f430_rowid_tercero_fact = t200.f200_rowid AND f430_id_sucursal_fact = t201.f201_id_sucursal) cliente_nro_ped' +
			' ,(SELECT count(1) FROM t350_co_docto_contable WHERE f350_rowid_tercero = t200.f200_rowid AND f350_id_sucursal = t201.f201_id_sucursal) cliente_nro_rec' +
			' FROM t200_mm_terceros AS t200' +
			' INNER JOIN t201_mm_clientes AS t201 ON t201.f201_rowid_tercero = t200.f200_rowid' +
			' LEFT JOIN t015_mm_contactos AS t015 ON t201.f201_rowid_contacto = t015.f015_rowid' +
			' LEFT JOIN t013_mm_ciudades AS t013 ON t015.f015_id_pais = t013.f013_id_pais AND t015.f015_id_depto = t013.f013_id_depto AND t015.f015_id_ciudad = t013.f013_id' +
			' LEFT JOIN t014_mm_barrios AS t014 ON t015.f015_id_pais = t014.f014_id_pais AND t015.f015_id_depto = t014.f014_id_depto AND t015.f015_id_ciudad = t014.f014_id_ciudad AND t015.f015_id_barrio = t014.f014_id' +
			' WHERE t201.f201_rowid_tercero = ' +
			rowid_cliente +
			"   AND t201.f201_id_sucursal = '" +
			id_sucursal +
			"'";
		console.log(sqlfull);
		return this.ldb
			.queryExec(sqlfull)
			.then((response) => {
				let r = response['rows'][0];
				console.log(r);
				/*
        let c = new Cliente({
          rowid_tercero: r['cliente_rowid_tercero'];
          rowid_tercero_suc: number;
          id: string;
          nit: string;
          dv_nit: string;
          id_sucursal: string;
          apellido1: string;
          apellido2: string;
          nombre_est: string;
          nombres: string;
          razon_social: string;
          descripcion_sucursal: string;
        
        })
        */
				this.selCliente(rowid_cliente, id_sucursal);
				return Promise.resolve(response);
			})
			.catch((error) => {
				return Promise.reject(error);
			});
	}

	public selCliente(rowid_cliente, id_sucursal) {
		let sqlfull =
			'select t201.*, t200.* FROM t200_mm_terceros AS t200' +
			' INNER JOIN t201_mm_clientes AS t201 ON t201.f201_rowid_tercero = t200.f200_rowid' +
			' WHERE t201.f201_rowid_tercero = ' +
			rowid_cliente +
			"   AND t201.f201_id_sucursal = '" +
			id_sucursal +
			"'";
		console.log(sqlfull);
		this.ldb
			.queryExec(sqlfull)
			.then((response) => {
				let r = response.rows[0];
				cliente.f200_apellido1 = r.f200_apellido1;
				cliente.f200_apellido2 = r.f200_apellido2;
				cliente.f200_dv_nit = r.f200_dv_nit;
				cliente.f200_id = r.f200_id;
				cliente.f200_id_tipo_ident = r.f200_id_tipo_ident;
				cliente.f200_ind_tipo_tercero = r.f200_ind_tipo_tercero;
				cliente.f200_nit = r.f200_nit;
				cliente.f200_nombre_est = r.f200_nombre_est;
				cliente.f200_nombres = r.f200_nombres;
				cliente.f200_razon_social = r.f200_razon_social;
				cliente.f200_rowid = r.f200_rowid;
				cliente.f200_rowid_contacto = r.f200_rowid_contacto;
				cliente.f200_rowid_foto = r.f200_rowid_foto;
				cliente.f200_rowid_movto_entidad = r.f200_rowid_movto_entidad;
				cliente.f201_rowid_tercero = r.f201_rowid_tercero;
				cliente.f201_id_sucursal = r.f201_id_sucursal;
				cliente.f201_descripcion_sucursal = r.f201_descripcion_sucursal;
				cliente.f201_ind_estado_bloqueado = r.f201_ind_estado_bloqueado;
				cliente.f201_id_moneda = r.f201_id_moneda;
				cliente.f201_rowid_contacto = r.f201_rowid_contacto;
				cliente.f201_id_cond_pago = r.f201_id_cond_pago;
				cliente.f201_dias_gracia = r.f201_dias_gracia;
				cliente.f201_cupo_credito = r.f201_cupo_credito;
				cliente.f201_id_tipo_cli = r.f201_id_tipo_cli;
				cliente.f201_id_grupo_dscto = r.f201_id_grupo_dscto;
				cliente.f201_id_lista_precio = r.f201_id_lista_precio;
				cliente.f201_ind_pedido_backorder = r.f201_ind_pedido_backorder;
				cliente.f201_porc_exceso_venta = r.f201_porc_exceso_venta;
				cliente.f201_ind_bloqueo_cupo = r.f201_ind_bloqueo_cupo;
				cliente.f201_ind_bloqueo_mora = r.f201_ind_bloqueo_mora;
				cliente.f201_fecha_ingreso = r.f201_fecha_ingreso;
				cliente.f201_fecha_cupo = r.f201_fecha_cupo;
				cliente.f201_porc_tolerancia = r.f201_porc_tolerancia;
				cliente.f201_rowid_tercero_corp = r.f201_rowid_tercero_corp;
				cliente.f201_id_sucursal_corp = r.f201_id_sucursal_corp;
				cliente.f215_rowid = r.f215_rowid;
				cliente.f112_ind_imp_precio_venta = r.f112_ind_imp_precio_venta;
				cliente.f015_direccion1 = r.f015_direccion1;
				cliente.f201_saldo_rc = r.f201_saldo_rc;
				cliente.f201_vlr_pedidos_pen = r.f201_vlr_pedidos_pen;
				this.cargaConsecutivos();
			})
			.catch((error) => {
				console.log(error);
			});
	}

	public cargaConsecutivos(): any {
		let sql =
			'SELECT v.f2181_id_tipo_docto_pedido tipo_ped,v.f2181_rowid_terc_vend IdVendedor,v.f2181_rowid_bodega_pedido RowidBodega, v.f2181_id_tipo_docto_recibo tipo_rec,v.f2181_id_tipo_docto_fact tipo_fac,' +
			' ifnull(cpv.f022_cons_proximo,0) cons_ped,ifnull(crc.f022_cons_proximo,0) cons_rec,ifnull(cfv.f022_cons_proximo,0) cons_fac,' +
			' 0 cons_chepos,v.f2181_id_co,' +
			' ifnull((SELECT f461_consec_docto FROM t461_cm_docto_factura_venta ORDER BY f461_consec_docto DESC limit 1),0) f461_consec_docto' +
			' FROM t2181_sm_vend_siesamobile AS v' +
			' LEFT JOIN t022_mm_consecutivos AS cpv ON v.f2181_id_co = cpv.f022_id_co AND v.f2181_id_tipo_docto_pedido = cpv.f022_id_tipo_docto' +
			' LEFT JOIN t022_mm_consecutivos AS cfv ON v.f2181_id_co = cfv.f022_id_co AND v.f2181_id_tipo_docto_fact   = cfv.f022_id_tipo_docto' +
			' LEFT JOIN t022_mm_consecutivos AS crc ON v.f2181_id_co = crc.f022_id_co AND v.f2181_id_tipo_docto_recibo = crc.f022_id_tipo_docto';
		//console.log(sql)
		return this.ldb
			.queryExec(sql)
			.then((response) => {
				if (response.rows.length > 0) {
					let r = response.rows[0];
					cliente.Id_vendedor = r.IdVendedor;
					cliente.rowid_bodega = r.RowidBodega;
					cliente.TipoPed = r.tipo_ped;
					cliente.NroPed = r.cons_ped;
					cliente.TipoRec = r.tipo_rec;
					cliente.NroRec = r.cons_rec;
					cliente.TipoFac = r.tipo_fac;
					cliente.NroFac = r.cons_fac;
					cliente.NroChe = r.cons_chepos;
				}
				this.cambiaConpago(cliente.f201_id_cond_pago);
				this.monedaDeterminarTasas();
				return Promise.resolve(response);
			})
			.catch((error) => {
				return Promise.reject(error);
			});
	}

	cambiaConpago(id) {
		let sql = "SELECT * FROM t208_mm_condiciones_pago where f208_id = '" + id + "'";
		this.ldb
			.queryExec(sql)
			.then((response) => {
				let r = response.rows[0];
				cp.f208_cuotas = r.f208_cuotas;
				cp.f208_ind_modo_periodicidad = r.f208_ind_modo_periodicidad;
				cp.f208_dias_vcto = r.f208_dias_vcto;
				cp.f208_ind_pronto_pago = r.f208_ind_pronto_pago;
				cp.f208_dias_pronto_pago = r.f208_dias_pronto_pago;
				cp.f208_tasa_descto_pp = r.f208_tasa_descto_pp;
				cp.f208_porcentaje_anticipo = r.f208_porcentaje_anticipo;
			})
			.catch((error) => {
				console.log(error);
			});
	}

	private monedaDeterminarTasas() {
		let sql = 'SELECT f0171_id from t0171_mm_tipos_cambio WHERE f0171_ind_defecto = 1';
		this.ldb
			.queryExec(sql)
			.then((response) => {
				let r = response.rows[0];
				h_mt.v_id_tipo_cambio_default = r.f0171_id;

				let sql =
					"SELECT f0181_id_tipo_cambio from t0181_mm_tipos_cambio_clases WHERE f0181_id_clase_docto = '502'";
				this.ldb
					.queryExec(sql)
					.then((response) => {
						let r = response[0];
						if (r) {
							cliente.f201_id_tipo_cli = r.f0181_id_tipo_cambio;
						} else {
							let sql = "SELECT f0171_id from t0171_mm_tipos_cambio WHERE f0171_ind_defecto = '1'";
							this.ldb
								.queryExec(sql)
								.then((response) => {
									let r = response.rows[0];
									cliente.id_tipo_cambio = r.f0171_id;

									let sql =
										"select ifnull(f010_id_moneda_local, '') f010_id_moneda_local," +
										" ifnull(f010_id_moneda_conversion, '') f010_id_moneda_conversion " +
										' from t010_mm_companias';
									this.ldb
										.queryExec(sql)
										.then((response) => {
											let r = response.rows[0];
											h_mt.v_id_moneda_local = r.f010_id_moneda_local;
											h_mt.v_id_moneda_base = r.f010_id_moneda_conversion;
											h_mt.p_id_moneda_base = h_mt.v_id_moneda_base;
											h_mt.p_id_moneda_local = h_mt.v_id_moneda_local;

											let sql =
												'select ifnull(f017_ind_forma_conversion, 0) f017_ind_forma_conversion' +
												' from t017_mm_monedas' +
												" where f017_id = '" +
												cliente.f201_id_moneda +
												"'";
											this.ldb
												.queryExec(sql)
												.then((response) => {
													let r = response.rows[0];
													h_mt.p_ind_forma_conv_base = r.f017_ind_forma_conversion;

													let sql =
														'select ifnull(f017_ind_forma_conversion, 0) f017_ind_forma_conversion' +
														' from t017_mm_monedas' +
														" where f017_id = '" +
														h_mt.v_id_moneda_local +
														"'";
													this.ldb
														.queryExec(sql)
														.then((response) => {
															let r = response.rows[0];
															h_mt.p_ind_forma_conv_local = r.f017_ind_forma_conversion;

															let sql =
																'select f017_ind_control_vigencia' +
																' from t017_mm_monedas' +
																" where f017_id = '" +
																cliente.f201_id_moneda +
																"'";
															this.ldb
																.queryExec(sql)
																.then((response) => {
																	let r = response.rows[0];
																	h_mt.v_eval_tasa_en_fecha =
																		r.f017_ind_control_vigencia;
																	let v_id_tipo_cambio = h_mt.p_id_tipo_cambio;
																	let p_tasa = null;
																	if (
																		v_id_tipo_cambio == '' ||
																		v_id_tipo_cambio == null
																	) {
																		v_id_tipo_cambio =
																			h_mt.v_id_tipo_cambio_default;
																	}
																	if (
																		cliente.f201_id_moneda ===
																			h_mt.v_id_moneda_local ||
																		cliente.f201_id_moneda === ''
																	) {
																		h_mt.p_tasa_base = 1.0;
																		h_mt.p_tasa_local = 1.0;
																	} else if (
																		cliente.f201_id_moneda === h_mt.v_id_moneda_base
																	) {
																		let promise1 = new Promise(
																			(resolve, reject) => {
																				if (h_mt.v_eval_tasa_en_fecha != 0) {
																					let sql =
																						'select f018_tasa from t018_mm_tasas_cambio ' +
																						" where f018_id_moneda = '" +
																						h_mt.v_id_moneda_local +
																						"'" +
																						"   and f018_id_tipo_cambio = '" +
																						v_id_tipo_cambio +
																						"'" +
																						"   AND CAST ((julianday('now') - julianday(f018_fecha)) AS integer ) = 0";
																					this.ldb
																						.queryExec(sql)
																						.then((response) => {
																							let r = response.rows[0];
																							if (r.f018_tasa == null) {
																								h_mt.p_existe_tasa_en_fecha = 0;
																							} else {
																								h_mt.p_existe_tasa_en_fecha = 1;
																								h_mt.v_tasa =
																									r.f018_tasa;
																								p_tasa = r.f018_tasa;
																							}
																							resolve();
																						})
																						.catch((error) => {
																							reject(error);
																						});
																				} else {
																					h_mt.p_existe_tasa_en_fecha = 1;
																					resolve();
																				}
																			}
																		);

																		let promise2 = new Promise(
																			(resolve, reject) => {
																				if (p_tasa == null) {
																					var sql =
																						'select f018_tasa from t018_mm_tasas_cambio ' +
																						" where f018_id_moneda = '" +
																						h_mt.v_id_moneda_local +
																						"'" +
																						"   and f018_id_tipo_cambio = '" +
																						v_id_tipo_cambio +
																						"'" +
																						"   AND CAST ((julianday('now') - julianday(f018_fecha)) AS integer ) >= 0" +
																						' order by f018_fecha desc ' +
																						' LIMIT 1';
																					this.ldb
																						.queryExec(sql)
																						.then((response) => {
																							let r = response.rows[0];
																							if (r.f018_tasa == null) {
																								h_mt.v_tasa = 1;
																							} else {
																								h_mt.v_tasa =
																									r.f018_tasa;
																							}
																							resolve();
																						})
																						.catch((error) => {
																							reject(error);
																						});
																				}
																			}
																		);

																		Promise.race([ promise1, promise2 ])
																			.then(() => {
																				h_mt.p_tasa_base = 1;
																				h_mt.p_tasa_local = h_mt.v_tasa;
																			})
																			.catch((error) => {
																				console.log(error);
																			});
																	} else if (
																		h_mt.v_id_moneda_local === h_mt.v_id_moneda_base
																	) {
																		let promise1 = new Promise(
																			(resolve, reject) => {
																				if (h_mt.v_eval_tasa_en_fecha != 0) {
																					var sql =
																						'select f018_tasa from t018_mm_tasas_cambio ' +
																						" where f018_id_moneda = '" +
																						cliente.f201_id_moneda +
																						"'" +
																						"   and f018_id_tipo_cambio = '" +
																						v_id_tipo_cambio +
																						"'" +
																						"   AND CAST ((julianday('now') - julianday(f018_fecha)) AS integer ) = 0";
																					this.ldb
																						.queryExec(sql)
																						.then((response) => {
																							let r = response.rows[0];
																							if (r.f018_tasa == null) {
																								h_mt.p_existe_tasa_en_fecha = 0;
																							} else {
																								h_mt.p_existe_tasa_en_fecha = 1;
																								h_mt.v_tasa =
																									r.f018_tasa;
																								p_tasa = r.f018_tasa;
																							}
																							resolve();
																						})
																						.catch((error) => {
																							reject(error);
																						});
																				} else {
																					h_mt.p_existe_tasa_en_fecha = 1;
																					resolve();
																				}
																			}
																		);

																		let promise2 = new Promise(
																			(resolve, reject) => {
																				if (p_tasa == null) {
																					var sql =
																						'select f018_tasa from t018_mm_tasas_cambio ' +
																						" where f018_id_moneda = '" +
																						cliente.f201_id_moneda +
																						"'" +
																						"   and f018_id_tipo_cambio = '" +
																						v_id_tipo_cambio +
																						"'" +
																						"   AND CAST ((julianday('now') - julianday(f018_fecha)) AS integer ) >= 0" +
																						' order by f018_fecha desc ' +
																						' LIMIT 1';
																					this.ldb
																						.queryExec(sql)
																						.then((response) => {
																							let r = response.rows[0];
																							if (r.f018_tasa == null) {
																								h_mt.v_tasa = 1;
																							} else {
																								h_mt.v_tasa =
																									r.f018_tasa;
																							}
																							resolve();
																						})
																						.catch((error) => {
																							reject(error);
																						});
																				}
																			}
																		);

																		Promise.race([ promise1, promise2 ])
																			.then(() => {
																				h_mt.p_tasa_base = 1;
																				h_mt.p_tasa_local = h_mt.v_tasa;
																			})
																			.catch((error) => {
																				console.log(error);
																			});
																	} else {
																		let promise1 = new Promise(
																			(resolve, reject) => {
																				if (h_mt.v_eval_tasa_en_fecha != 0) {
																					var sql =
																						'select f018_tasa from t018_mm_tasas_cambio ' +
																						" where f018_id_moneda = '" +
																						cliente.f201_id_moneda +
																						"'" +
																						"   and f018_id_tipo_cambio = '" +
																						v_id_tipo_cambio +
																						"'" +
																						"   AND CAST ((julianday('now') - julianday(f018_fecha)) AS integer ) = 0";
																					this.ldb
																						.queryExec(sql)
																						.then((response) => {
																							let r = response.rows[0];
																							if (r.f018_tasa == null) {
																								h_mt.p_existe_tasa_en_fecha = 0;
																							} else {
																								h_mt.p_existe_tasa_en_fecha = 1;
																								h_mt.v_tasa =
																									r.f018_tasa;
																								p_tasa = r.f018_tasa;
																							}
																							resolve();
																						})
																						.catch((error) => {
																							reject(error);
																						});
																				} else {
																					h_mt.p_existe_tasa_en_fecha = 1;
																					resolve();
																				}
																			}
																		);

																		let promise2 = new Promise(
																			(resolve, reject) => {
																				if (p_tasa == null) {
																					var sql =
																						'select f018_tasa from t018_mm_tasas_cambio ' +
																						" where f018_id_moneda = '" +
																						cliente.f201_id_moneda +
																						"'" +
																						"   and f018_id_tipo_cambio = '" +
																						v_id_tipo_cambio +
																						"'" +
																						"   AND CAST ((julianday('now') - julianday(f018_fecha)) AS integer ) >= 0" +
																						' order by f018_fecha desc ' +
																						' LIMIT 1';
																					this.ldb
																						.queryExec(sql)
																						.then((response) => {
																							let r = response[0];
																							if (r.f018_tasa == null) {
																								h_mt.v_tasa = 1;
																							} else {
																								h_mt.v_tasa =
																									r.f018_tasa;
																							}
																							resolve();
																						})
																						.catch((error) => {
																							reject(error);
																						});
																				}
																			}
																		);

																		Promise.race([ promise1, promise2 ])
																			.then(() => {
																				h_mt.p_tasa_base = h_mt.v_tasa;

																				if (
																					emp.f010_id_moneda_local ==
																					cliente.f201_id_moneda
																				) {
																					h_mt.p_tasa_base = 1;
																					h_mt.p_tasa_local = 1;
																					h_mt.p_ind_forma_conv_local = 0;
																					h_mt.p_ind_forma_conv_base = 0;
																				}
																			})
																			.catch((error) => {
																				console.log(error);
																			});
																	}
																})
																.catch((error) => {
																	console.log(error);
																});
														})
														.catch((error) => {
															console.log(error);
														});
												})
												.catch((error) => {
													console.log(error);
												});
										})
										.catch((error) => {
											console.log(error);
										});
								})
								.catch((error) => {
									console.log(error);
								});
						}
					})
					.catch((error) => {
						console.log(error);
					});
			})
			.catch((error) => {
				console.log(error);
			});
	}
}
