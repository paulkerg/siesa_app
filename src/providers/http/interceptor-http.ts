import { Http, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';

export class HttpProvider extends Http {
	modulo: string = 'UNOEE';

	constructor(connectionBackend: ConnectionBackend, requestOptions: RequestOptions) {
		super(connectionBackend, requestOptions);
	}
	get(url: string, options?: RequestOptionsArgs): Observable<Response> {
		if (options['headers']) {
			if (options.headers.getAll('modulo')[0]) {
				this.modulo = options.headers.getAll('modulo')[0];
			}
		}
		return Observable.fromPromise(this.getRequestOptionArgs()).mergeMap((options) => {
			return super.get(url, options);
		});
	}
	post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
		if (options.headers.getAll('modulo')[0]) {
			this.modulo = options.headers.getAll('modulo')[0];
		}
		return Observable.fromPromise(this.getRequestOptionArgs()).mergeMap((options) => {
			return super.post(url, body, options);
		});
	}
	put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
		if (options.headers.getAll('modulo')[0]) {
			this.modulo = options.headers.getAll('modulo')[0];
		}
		return Observable.fromPromise(this.getRequestOptionArgs()).mergeMap((options) => {
			return super.put(url, body, options);
		});
	}
	getRequestOptionArgs(options?: RequestOptionsArgs) {
		let token = localStorage.getItem('token');
		if (this.modulo === 'AG') {
			token = localStorage.getItem('tokenAG');
		}
		return new Promise((resolve, reject) => {
			if (options == null) {
				options = new RequestOptions();
			}
			if (options.headers == null) {
				options.headers = new Headers();
			}
			if (token !== null) {
				options.headers.append('Authorization', 'Bearer ' + token);
			}
			options.headers.append('Content-Type', 'application/x-www-form-urlencoded');
			resolve(options);
		});
	}
}
