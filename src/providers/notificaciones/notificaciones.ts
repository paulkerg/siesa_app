import { Injectable } from '@angular/core';
import { SqliteProvider } from '../sqlite/sqlite';
import { ModalController, ToastController } from 'ionic-angular';
import { isNullOrEmpty } from '../../pipes/generales/utils/utils';
import { enumAccionNotificacion, global } from '../../app/global';

@Injectable()
export class NotificacionesProvider {
	modal: any;
	alerta: any;
	msgNotificacion: {
		'1': 'Se encontro cambios en un registro de una tabla';
		'2': 'Se encontraron cambios en centrar, se van a sincronizar tablas';
	};
	constructor(
		private db: SqliteProvider,
		private modalCtrl: ModalController,
		private toast: ToastController
	) {
	}

	actualizarNotificacion(datos) {
		if (!isNullOrEmpty(datos)) {
			if (isNullOrEmpty(this.alerta)) {
				let modulo: string = "" + datos[0]['modulo']
				console.log("global['sincronizando']['" + modulo + "']: ", global['sincronizando'][modulo+""])
				console.log("global['sincronizando_toast']['" + modulo + "']: ", global['sincronizando_toast'][modulo+""])
				
				if (!global['sincronizando_toast'][modulo+""]) {
					global['sincronizando_toast'][modulo+""] = true;
					console.log("global['sincronizando'][" + modulo + "]: ", global['sincronizando'][modulo])
					let toastClass = 'success';
					let toast = this.toast.create({
						message: datos[0]['tipoFuncionalidad'],
						duration: 5000,
						cssClass: toastClass,
						showCloseButton: true,
						closeButtonText: 'Cancelar'
					});

					toast.onDidDismiss((data, role) => {
						if (role != 'close') {
							this.actualizaCambios(datos);
						}
					});

					toast.present();
				}else{
					//Aqui lo que se debe de hacer es cancelar la sincronizacion actual y volver a sincronizar

				}
			} else {
				this.actualizaCambios(datos);
			}
		}
	}

	termino() {
		console.log('Termino');
	}

	private actualizaCambios(datos) {
		for (let t = 0; t < datos.length; t++) {
			const r = datos[t];
			if (r['indAccion'] === enumAccionNotificacion.INSERT_UPDATE) {
				this.actualizaTablas(r);
			}
			let modulo: string = "" + r['modulo'];
			if (!global['sincronizando']) {
				global['sincronizando'] = [];
			}
			
			console.log("global['sincronizando']['" + modulo + "']: ", global['sincronizando'][modulo+""])
			if (r['indAccion'] === enumAccionNotificacion.SINCRONIZAR && !global['sincronizando'][r['modulo'] + ""]) {
				global['sincronizando'][modulo+""] = true; // esta se cambia FALSE cuando se termina de sincronizar
				//el provider de Tablas Sincronizacion se encarga de cambiar este valor a FALSE
				this.modal = this.modalCtrl
					.create('SincronizacionProgresoPage', { modulo: r['modulo'] }, { cssClass: 'inset-modal' });

					this.modal.present();
			}else{
				//Aqui al llegar se debe de cancelar la sincronizacion actual y volver a iniciar la sincronizacion
				this.modal.dismiss();
				console.warn("LLego otra sincronizacion")

			}
			if (r['indAccion'] === enumAccionNotificacion.ALERTA) {
				console.log(enumAccionNotificacion.ALERTA);
			}
			if (r['indAccion'] === enumAccionNotificacion.MENSAJE) {
				console.log(enumAccionNotificacion.MENSAJE);
			}
		}
		this.alerta = null;
	}

	private actualizaTablas(r) {
		let objDatos: any = JSON.parse(r['datos']);
		for (const oRegistros in objDatos) {
			if (objDatos.hasOwnProperty(oRegistros)) {
				const regs = objDatos[oRegistros];
				let tabla = oRegistros;
				for (let p = 0; p < regs.length; p++) {
					const z = regs[p];
					let objInsertaCampos: any = [];
					let objInsertaDatos: any = [];
					for (const oRegs in z) {
						let campo = oRegs;
						let valor = z[campo];
						objInsertaCampos.push(campo);
						objInsertaDatos.push("'" + valor + "'");
					}
					let sql =
						'insert or replace into ' +
						tabla +
						'(' +
						objInsertaCampos.toString() +
						')' +
						' values ' +
						'(' +
						objInsertaDatos.toString() +
						')';
					this.db
						.query(sql)
						.then((resp) => {
							if (resp) {
								console.log('OK', resp);
							}
						})
						.catch((err) => {
							console.error('ERROR', err);
						});
				}
			}
		}
	}

}
