import { Injectable } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
@Injectable()
export class AnalyticsProvider {
	constructor(private ga: GoogleAnalytics) {}
	startTrackerWithId(id) {

	}
  
  trackView(screenName) {
		this.ga.trackView(screenName).then(() => {}).catch((e) => console.log(e));
  }
  
	trackEvent(category, action, label?, value?) {
    this.ga.trackEvent(category, action, label, value);
	}
}
