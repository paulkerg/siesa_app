import { Injectable } from '@angular/core';
import { Events, Platform } from 'ionic-angular/index';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { SqliteProvider } from '../sqlite/sqlite';
import { URLSearchParams } from '@angular/http';
import { global } from '../../app/global';
import { HttpParams } from '@angular/common/http';
import { count, isObject } from '../../pipes/generales/utils/utils';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { ManejoArchivosProvider } from '../manejo-archivos/manejo-archivos';
import { ToastController } from 'ionic-angular';
import { AnalyticsProvider } from '../analytics/analytics';

@Injectable()
export class SincronizacionProvider {
	private prvHeaders = new Headers();
	private prvOptions = new RequestOptions({ headers: this.prvHeaders });
	private prvTimeOut: number = global.timeoutPost;
	constructor(
		public http: Http,
		private platform: Platform,
		private dbSql: SqliteProvider,
		public events: Events,
		private transfer: FileTransfer,
		private mf: ManejoArchivosProvider,
		private toast: ToastController,
		private analytics: AnalyticsProvider
	) {
		this.http = http;
		global['mostrarError'] = false;
	}

	async postParameters(metodo, parametros, resultset: number = 0, modulo: string = 'UNOEE', formato: string = '') {
		let blnTocken = false;
		if (modulo === 'UNOEE') {
			blnTocken = this.getToken() != null;
		}
		if (modulo === 'AG') {
			blnTocken = this.getTokenAG() != null;
		}

		if (!blnTocken) {
			return new Promise((resolve, reject) => {
				let sql =
					'select u.id_usuario, u.clave, t002.id_conexion, t002.id_cia from t002_mm_propiedades AS t002, usuarios AS u ';
				this.dbSql.query(sql).then(async (respuesta) => {
					let r = respuesta[0];
					let UserName = r['id_usuario'];
					let Password = r['clave'];

					if (parametros['UserName']) {
						UserName = parametros['UserName'];
						Password = parametros['Password'];
					}
					let param = {
						ConnectionNumber: r['id_conexion'],
						Company: r['id_cia'],
						UserName: UserName,
						Password: Password
					};
					if (modulo === 'UNOEE') {
						this.validaTokenUNOEE(param)
							.then((r) => {
								blnTocken = true;
								this.postConToken(metodo, parametros, resultset, modulo, formato)
									.then((r) => {
										resolve(r);
									})
									.catch((e) => {
										reject(e);
									});
							})
							.catch((e) => reject(e));
					} else {
						this.validaTokenAG(param)
							.then((r) => {
								blnTocken = true;
								this.postConToken(metodo, parametros, resultset, modulo, formato)
									.then((r) => {
										resolve(r);
									})
									.catch((e) => {
										reject(e);
									});
							})
							.catch((e) => reject(e));
					}
				});
			});
		} else {
			return new Promise((resolve, reject) => {
				this.postConToken(metodo, parametros, resultset, modulo, formato)
					.then((r) => {
						resolve(r);
					})
					.catch((e) => {
						reject(e);
					});
			});
		}
	}

	async getParameters(metodo, parametros, resultset: number = 0, modulo: string = 'UNOEE') {
		let blnTocken = false;
		if (modulo === 'UNOEE') {
			blnTocken = this.getToken() != null;
		}
		if (modulo === 'AG') {
			blnTocken = this.getTokenAG() != null;
		}
		if (!blnTocken) {
			return new Promise((resolve, reject) => {
				let sql =
					'select u.id_usuario, u.clave, t002.id_conexion, t002.id_cia from t002_mm_propiedades AS t002, usuarios AS u ';
				this.dbSql.query(sql).then(async (respuesta) => {
					let r = respuesta[0];
					let UserName = r['id_usuario'];
					let Password = r['clave'];

					if (parametros['UserName']) {
						UserName = parametros['UserName'];
						Password = parametros['Password'];
					}
					let param = {
						ConnectionNumber: r['id_conexion'],
						Company: r['id_cia'],
						UserName: UserName,
						Password: Password
					};
					if (modulo === 'UNOEE') {
						this.validaTokenUNOEE(param)
							.then((r) => {
								blnTocken = true;
								this.getConToken(metodo, parametros, resultset, modulo)
									.then((r) => {
										resolve(r);
									})
									.catch((e) => {
										reject(e);
									});
							})
							.catch((e) => reject(e));
					} else {
						this.validaTokenAG(param)
							.then((r) => {
								blnTocken = true;
								this.getConToken(metodo, parametros, resultset, modulo)
									.then((r) => {
										resolve(r);
									})
									.catch((e) => {
										reject(e);
									});
							})
							.catch((e) => reject(e));
					}
				});
			});
		} else {
			return new Promise((resolve, reject) => {
				this.getConToken(metodo, parametros, resultset, modulo)
					.then((r) => {
						resolve(r);
					})
					.catch((e) => {
						reject(e);
					});
			});
		}
	}

	postConToken(metodo, parametros, resultset: number = 0, modulo: string = 'UNOEE', formato: string = '') {
		let prvHeaders = new Headers();
		prvHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		prvHeaders.append('modulo', modulo);
		this.prvOptions = new RequestOptions({ headers: prvHeaders });

		let param = new URLSearchParams();
		param.append('idCia', global.dispositivo.idCia);
		Object.keys(parametros).forEach((key) => {
			if (formato != '') {
				if (isObject(parametros[key])) {
					let obj = parametros[key];
					for (let t = 0; t < obj.length; t++) {
						let rg = obj[t];
						Object.keys(rg).forEach((key2) => {
							let dato = key + '[' + t + ']' + '[' + key2 + ']';
							param.append(dato, rg[key2]);
						});
					}
				} else {
					param.append(key, parametros[key]);
				}
			} else {
				param.append(key, parametros[key]);
			}
		});
		return new Promise((resolve, reject) => {
			let sql = 'select * from t002_mm_propiedades limit 1';
			this.dbSql
				.query(sql)
				.then((resp) => {
					if (resp) {
						let url = 'http://';
						if (resp[0]['ind_https'] == 'true') {
							url = 'https://';
						}
						url += resp[0]['url_servidor_local'];
						if (resp[0]['url_puerto'] != '') {
							url += ':' + resp[0]['url_puerto'];
						}
						url += '/';
						this.http
							.post(url + metodo, param, this.prvOptions)
							.timeout(this.prvTimeOut)
							.map((res) => res.json())
							.subscribe(
								(res) => {
									if (res['length']) {
										if (resultset != 0) {
											resolve(res);
										} else {
											resolve(res[resultset]);
										}
									} else {
										resolve(res);
									}
								},
								(err) => {
									this.mostrarErrorMensaje(err);
									const error = err['_body']
										? typeof err['_body'] == 'object' ? err['_body'] : JSON.parse(err['_body'])
										: err;
									reject(error);
								}
							);
					}
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	getConToken(metodo, parametros, resultset: number = 0, modulo: string = 'UNOEE') {
		let prvHeaders = new Headers();
		prvHeaders.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		prvHeaders.append('modulo', modulo);
		this.prvOptions = new RequestOptions({ headers: prvHeaders });

		let param = new URLSearchParams();
		param.append('idCia', global.dispositivo.idCia);
		Object.keys(parametros).forEach((key) => {
			param.append(key, parametros[key]);
		});

		let nParam = '';
		if (count(parametros) > 0) {
			nParam = '?' + param.toString();
		}

		return new Promise((resolve, reject) => {
			let sql = 'select * from t002_mm_propiedades limit 1';
			this.dbSql
				.query(sql)
				.then((resp) => {
					if (resp) {
						let url = 'http://';
						if (resp[0]['ind_https'] == 'true') {
							url = 'https://';
						}
						url += resp[0]['url_servidor_local'];
						if (resp[0]['url_puerto'] != '') {
							url += ':' + resp[0]['url_puerto'];
						}
						url += '/';
						this.http
							.get(url + metodo + nParam, this.prvOptions)
							.timeout(this.prvTimeOut)
							.map((res) => res.json())
							.subscribe(
								(res) => {
									resolve(res);
								},
								(err) => {
									//this.events.publish('cargandoOff');
									this.mostrarErrorMensaje(err);

									const error = err['_body']
										? typeof err['_body'] == 'object' ? err['_body'] : JSON.parse(err['_body'])
										: err;
									reject(error);
								}
							);
					}
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	post2(metodo, parametros, resultset: number = 0) {
		return new Promise((resolve, reject) => {
			let sql = 'select * from t002_mm_propiedades limit 1';
			this.dbSql
				.query(sql)
				.then((resp) => {
					if (resp) {
						let url = 'http://';
						if (resp[0]['ind_https'] == 'true') {
							url = 'https://';
						}
						url += resp[0]['url_servidor_local'];
						if (resp[0]['url_puerto'] != '') {
							url += ':' + resp[0]['url_puerto'];
						}
						url += '/';

						this.http
							.post(url + metodo, parametros, this.prvOptions)
							.timeout(this.prvTimeOut)
							.map((res) => res.json())
							.subscribe(
								(res) => {
									if (res['length']) {
										if (resultset == 9999) {
											resolve(res);
										} else {
											resolve(res[resultset]);
										}
									} else {
										resolve(res);
									}
								},
								(err) => {
									this.mostrarErrorMensaje(err);
									const error = err['_body']
										? typeof err['_body'] == 'object' ? err['_body'] : JSON.parse(err['_body'])
										: err;
									reject(error);
								}
							);
					}
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	get(metodo, parametros) {
		let params = new HttpParams();
		Object.keys(parametros).forEach((key) => {
			params = params.append(key, parametros[key]);
		});
		let nParam = '';

		if (count(parametros) > 0) {
			nParam = '?' + params.toString();
		}

		return new Promise((resolve, reject) => {
			let sql = 'select * from t002_mm_propiedades limit 1';
			this.dbSql
				.query(sql)
				.then((resp) => {
					if (resp) {
						let url = 'http://';
						if (resp[0]['ind_https'] == 'true') {
							url = 'https://';
						}
						url += resp[0]['url_servidor_local'];
						if (resp[0]['url_puerto'] != '') {
							url += ':' + resp[0]['url_puerto'];
						}
						url += '/';
						this.http
							.get(url + metodo + nParam, {})
							.timeout(this.prvTimeOut)
							.map((res) => res.json())
							.subscribe(
								(res) => {
									resolve(res);
								},
								(err) => {
									//this.events.publish('cargandoOff');
									this.mostrarErrorMensaje(err);

									const error = err['_body']
										? typeof err['_body'] == 'object' ? err['_body'] : JSON.parse(err['_body'])
										: err;
									reject(error);
								}
							);
					}
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	procesarDescargasAG(descargas) {
		const fileTransfer: FileTransferObject = this.transfer.create();
		let token = this.getTokenAG();
		let archivos = [];
		return new Promise((resolve, reject) => {
			//-- Procesa las descargas
			for (var index in descargas) {
				//-- para cada descarga
				let descargaData = descargas[index];
				let urlDescrga = descargaData['url'];
				archivos.push(descargaData['nombre']);

				if (this.platform.is('core') || this.platform.is('mobileweb')) {
					// create the link
					var link = document.createElement('a');
					link.href = urlDescrga;
					document.body.appendChild(link);
					link.click();
					// Remove the link
					document.body.removeChild(link);
					resolve(archivos);
				} else {
					this.mf
						.validaDirectorio('autogestion')
						.then((dir) => {
							fileTransfer.download(encodeURI(urlDescrga), dir + descargaData['nombre'], true).then(
								(r) => {
									resolve(archivos);
								},
								(error) => {
									reject(error);
								}
							);
						})
						.catch((error) => {
							reject(error);
						});
				}
			}
		});
	}

	async validaTokenAG(param) {
		return new Promise((resolve, reject) => {
			this.postConToken(global.api.GetTokenAG, param)
				.then((resultado) => {
					if (resultado['errores']) {
						reject(resultado['errores']);
					} else {
						this.setToket(resultado['token'], 'AG').then(() => {
							resolve(resultado);
						});
					}
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	validaTokenUNOEE(param) {
		return new Promise((resolve, reject) => {
			this.postConToken(global.api.GetToken, param)
				.then((resultado) => {
					if (resultado['errores']) {
						reject(resultado['errores']);
					} else {
						this.setToket(resultado['token'], 'UNOEE').then(() => {
							resolve(resultado);
						});
					}
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	setToket(token, modulo) {
		return new Promise((resolve, reject) => {
			if (modulo == 'AG') {
				localStorage.setItem('tokenAG', token);
			}
			if (modulo == 'UNOEE') {
				localStorage.setItem('token', token);
			}
			resolve(true);
		});
	}
	getToken() {
		return localStorage.getItem('token');
	}

	getTokenAG() {
		return localStorage.getItem('tokenAG');
	}

	clearToken() {
		localStorage.clear();
	}

	mostrarErrorMensaje(error) {
		let msj = '';
		if (error.status == 500) {
			msj = 'ERROR INTERNO DE SERVIDOR';
			console.error(msj);
			// alert(msj);
		} else if (error.status == 0) {
			msj = 'SERVICIO NO DISPONIBLE DEL SERVIDOR';
			console.error(msj);
			// alert(msj);
		}
		// console.log("global['mostrarError']: ", global['mostrarError'])
		if (error.status == undefined) {
			error.status = 'Tiempo de Espera';
		}
		let mensaje = msj + ' ERROR: ' + error.status;
		this.analytics.trackEvent('sincronizacion', 'error post', global.dispositivo.idFuncionario, mensaje);
		if (!global['mostrarError']) {
			global['mostrarError'] = true;
			let toast = this.toast.create({
				message: mensaje,
				duration: 5000,
				cssClass: 'error',
				showCloseButton: true,
				closeButtonText: 'Cancelar'
			});

			toast.onDidDismiss((data, role) => {
				global['mostrarError'] = false;
			});

			toast.present();
		}
	}
}
