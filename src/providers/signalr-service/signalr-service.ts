import { Injectable, EventEmitter } from "@angular/core";
import "rxjs/add/operator/map";
import { global } from "../../app/global";
import { SqliteProvider } from "../sqlite/sqlite";
import { Events } from "ionic-angular";
import {
  HubConnection,
  HubConnectionBuilder,
  HttpTransportType
} from "@aspnet/signalr";
/** represent chat message class */
export class ChatMessage {
  user: string;
  message: string;
  room: string;

  constructor(user: string = "", message: string = "", room: string = "") {
    this.user = user;
    this.message = message;
    this.room = room;
  }
}

@Injectable()
export class SignalrServiceProvider {
  // url = 'http://localhost/UNOMOB/SiesaHub';
  url = "http://localhost:60000/SiesaHub";
  messageReceived = new EventEmitter<ChatMessage>();
  connectionEstablished = new EventEmitter<Boolean>();

  private connectionIsEstablished = false;
  private _hubConnection: HubConnection;
  private intentos: number = 0;

  constructor(private dbSql: SqliteProvider, private events: Events) {}

  inicializar(urlServer) {
    this.crearConexion(urlServer);
    this.registroServidorEventos();
    this.iniciarConexion();
  }

  private crearConexion(urlServer) {
    this._hubConnection = new HubConnectionBuilder()
      .withUrl(urlServer + "SiesaHub", {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets
      })
      .build();

    // this._hubConnection = new HubConnectionBuilder().withUrl(this.url).build();
  }

  private registroServidorEventos(): void {
    this._hubConnection.on("UsuariosConectados", (data: any, users: any) => {
      this.messageReceived.emit(data);
    });

    this._hubConnection.on("notificacionCliente", (datos: any) => {
      //aqui se recorre el JSON obtenido para marcar las notificaciones recibidas
      var json_data = JSON.parse(datos);
      var rowids = "";
      json_data.forEach(data => {
        rowids += data.rowid + ",";
        if (
          data.tipoFuncionalidad &&
          data.tipoFuncionalidad.toLowerCase() == "sincronizar"
        ) {
          this.events.publish("notificacion_sincronizar", data);
        }
      });
      rowids = rowids.substr(0, rowids.length - 1);

      this._hubConnection.invoke("NovedadLeida", rowids);

      this.messageReceived.emit(json_data);
    });
  }

  public cerrarConexion(): void {
    if (this._hubConnection) {
      this._hubConnection
        .stop()
        .then(async () => {
          console.log("Cerro SR");
        })
        .catch(e => {
          console.log("Error cerrarConexionSR", e);
        });
    }
  }

  private iniciarConexion(): void {
    this.intentos++;
    this._hubConnection
      .start()
      .then(async () => {
        this.connectionIsEstablished = true;
        console.log("Hub connection started");
        this.connectionEstablished.emit(true);

        let sql = "SELECT * FROM usuarios, t002_mm_propiedades"
        + ", (select documento from configuraciones where id = 'usuario_siesapps') documento";
        await this.dbSql.query(sql).then(async resp => {
          if (resp.length > 0) {
            let r = resp[0];
            let doc = JSON.parse(r['documento'])
            global.dispositivo.idConexion = r["id_conexion"];
            this._hubConnection.invoke(
              "Conectar",
              r["id_conexion"],
              r["id_cia"],
              doc['f_id_funcionario'],
              doc['f_id_funcionario']
            );
          }
        });
      })
      .catch(err => {
        if (this.intentos < 10) {
          setTimeout(() => {
            this.iniciarConexion();
          }, 15000);
        }
      });
  }

  sendChatMessage(message: ChatMessage) {
    this._hubConnection.invoke("Send", message);
  }
}
