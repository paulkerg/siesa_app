import { AlertController } from 'ionic-angular';
import { Injectable, Inject } from '@angular/core';
import * as moment from 'moment';
import 'moment-timezone';

@Injectable()
export class UtilesProvider {
	constructor(@Inject(AlertController) private alerts: AlertController) {
		moment.locale('es');
	}

	async msgAlerta(error, callback?) {
		const mensaje = error.message ? error.message : error.toString();
		const titulo = error.titulo ? error.titulo : 'Error';

		const alert = this.alerts.create({
			cssClass: 'custom-alert-danger',
			title: titulo,
			subTitle: mensaje,
			enableBackdropDismiss: false,
			buttons: [
				{
					text: 'Aceptar',
					cssClass: 'disable-hover button button-outline button-outline custom-alert-btn-aceptar',
					handler: () => {
						// user has clicked the alert button
						// begin the alert's dismiss transition
						const navTransition = alert.dismiss();

						// start some async method
						navTransition.then(() => {
							if (callback != null && typeof callback == 'function') {
								callback();
							}
						});
						return false;
					}
				}
			]
		});
		alert.present();
	}

	async msgConfirmacion(mensaje) {
		const alert = this.alerts.create({
			cssClass: 'custom-alert-confirm',
			title: mensaje.titulo,
			subTitle: mensaje.mensaje,
			enableBackdropDismiss: false,
			buttons: [
				{
					text: 'Aceptar',
					cssClass: 'disable-hover button button-outline button-outline custom-alert-btn-aceptar',
					handler: () => {}
				}
			]
		});
		alert.present();
	}

	async fechaHoy() {
		var now = moment();
		return moment(now).tz('America/Bogota').format();
	}
}
