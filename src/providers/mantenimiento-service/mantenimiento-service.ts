import { Injectable } from "@angular/core";
import { SqliteProvider } from "../sqlite/sqlite";
import { SincronizacionProvider } from "../sincronizacion/sincronizacion";
import { global } from "../../app/global";
import { Events } from "ionic-angular";
import * as moment from 'moment';

/*
  Generated class for the MantenimientoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const CORRECTIVO = 1;
const CORRIENDO = 1;
const DETENIDA = 2;
const PAUSADA = 3;

@Injectable()
export class MantenimientoServiceProvider {
  constructor(
    private db: SqliteProvider,
    private sync: SincronizacionProvider,
    public events: Events
  ) {
    // console.log("Hello MantenimientoServiceProvider Provider");
  }

  actividad_vacia = {
    c0820_consec_docto: null,
    c0820_id_co: null,
    c0820_id_tipo_docto: null,
    id: "",
    descripcion: "",
    trabajo: "",
    detalle_trabajo: "",
    f_foto: "",
    c0822_fecha_programada: "",
    c0822_notas: "",
    CO: "",
    tipo_docto: "",
    numero_docto: "",
    notas: "",
    procedimiento: "",
    paroEnCadena: false,
    c0822_fecha_ejecucion_ini: null,
    c0819_rowid_w0822: null,
    c0822_rowid_docto_ot: null,
    c0822_rowid: null,
    lecturas: null,
    motivos: [],
    ayudantes: [{ c0827_rowid_tercero: 0 }],
    c0846_rowid_ayudante1: null,
    c0846_rowid_ayudante2: null,
    c0846_rowid_ayudante3: null,
    c0846_rowid_ayudante4: null,
    c0846_rowid_ayudante5: null,
    c0819_ind_dano: 0,
    c0819_ind_tipo_dano: 0,
    c0819_detalle_dano: " ",
    c0846_rowid_causal_falla: null,
    c0846_rowid_tipo_falla: null,
    c0846_notas: " ",
    c0846_ind_estado: 0,
    c0822_id_equipo: null,
    c0808_referencia: null,
    bordeIzquierdo: null,
    prvIdEquipo: 0,
    c0806_ind_regimen: null,
    c0808_ind_requiere_paro: null,
    c0819_ind_requiere_paro: null,
    c0819_ind_tipo_trabajo: null,
    c0822_duracion_horas_prog: 0,
    tareas: [],
    movimiento: null,
    tiempo_movimientos: 0,
    tareaSeleccionada: null,
    c0822_rowid_actividad: null,
    c0822_rowid_responsable: null,
    c0822_ind_enviado: 0,
    habilitar_error: true
  };
/**
 * Diferencia Fecha Segundos
 * @param fecha_inicio fecha inicial mas antigua
 * @param fecha_fin fecha final mas reciente si se deja vacia se toma la fecha actual (hoy)
 */
public diferenciaFechasSegundos(fecha_inicio:string,fecha_fin=moment(new Date())){
  var segundos = 0;
  var duration = moment.duration(fecha_fin.diff(fecha_inicio))
  segundos = duration.asSeconds();
  console.log("Segundos: ",segundos);
  return segundos;
}


  getSecondsAsDigitalClock(inputSeconds: number) {
    // console.log("inputSeconds: ",inputSeconds)
    var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - hours * 3600) / 60);
    var seconds = sec_num - hours * 3600 - minutes * 60;
    var days = Math.floor(hours / 24);

    var daysString = "";
    var hoursString = "";
    var minutesString = "";
    var secondsString = "";
    if (hours > 23) {
      hours %= 24;
      daysString = days < 10 ? "0" + days : days.toString();
      daysString += " D ";
    }
    hoursString = hours < 10 ? "0" + hours : hours.toString();
    minutesString = minutes < 10 ? "0" + minutes : minutes.toString();
    secondsString = seconds < 10 ? "0" + seconds : seconds.toString();
    // console.log("hoursString: ",hoursString)
    // console.log("minutesString: ",minutesString)
    // console.log("secondsString: ",secondsString)
    let text_time = daysString + hoursString + ":" + minutesString + ":" + secondsString;
    // console.warn("text_time: ",text_time)
    return text_time
  }

public consultarCamposConfiguraciones() {
    return new Promise((resolve, reject) => {
      let sql_consulta_campos_defecto = `select documento from configuraciones where id = 'PARAMETROS_MANTENIMIENTO'`;
      this.db
        .query(sql_consulta_campos_defecto)
        .then((res: any) => {
          if (res.length > 0 && res[0] && res[0].documento) {
            global["PARAMETROS_MANTENIMIENTO"] = JSON.parse(res[0].documento);
            let values = global["PARAMETROS_MANTENIMIENTO"];
            values = values.map(campo => {
              return {
                c08223_id: campo.alias,
                label: campo.label,
                c08223_class: campo.class,
                defecto: campo.defecto,
                seleccionado: false
              };
            });
            resolve(values);
          }
          resolve(res);
        })
        .catch(error => {
          console.error("ERROR al consultar los parametros de mantenimiento");
          console.error(error);
          reject(error);
        });
    });
  }

  public insertarCamposSeleccionados(campos_seleccionados) {
    return new Promise((resolve, reject) => {
      let sql_limpiar_campos = `delete from w08223_campos where c08223_rowid_tercero = ${
        global.usuario["f_tercero_rowid"]
      }`;
      this.db
        .query(sql_limpiar_campos)
        .then(res => {
          if(campos_seleccionados.length>0){

            var campos = " ";
            campos_seleccionados.forEach(value => {
              campos += `('${value.c08223_id}','${value.c08223_class}',${
                global.usuario["f_tercero_rowid"]
              }),`;
            });
            campos = campos.substring(0, campos.length - 1);
            
            let sql_campos_insert = `insert into w08223_campos (c08223_id,c08223_class, c08223_rowid_tercero )
            values ${campos}`;
            // console.log("SQL INSERT: ");
            // console.log(sql_campos_insert);
            this.db
            .query(sql_campos_insert)
            .then(res => {
              // console.log("se insertaron los campos seleccionados");
              // this.cargarCamposSeleccionados();
              resolve(res);
            })
            .catch(error => {
              console.error(
                "ERROR no se pudo insertar los campos seleccionados"
                );
                console.error(error);
                reject(error);
              });
            }else{
              resolve(true)
            }
        })
        .catch(error => {
          console.error(
            "Error al borrar los campos para dejar los por defecto"
          );
          console.error(error);
          reject(error);
        });
    });
  }

  public obtenerCamposSeleccionados() {
    return new Promise((resolve, reject) => {
      let sql_campos_seleccionados = `select c08223_id,c08223_class from w08223_campos`;
      this.db
        .query(sql_campos_seleccionados)
        .then(res => resolve(res))
        .catch(error => {
          console.error(
            "ocurrio un erro al consultar los campos seleccionados"
          );
          console.error(error);
          reject(error);
        });
    });
  }

  public cargarCamposDefectoActividades() {
    return new Promise((resolve, reject) => {
      this.consultarCamposConfiguraciones()
        .then((campos: any) => {
          let defecto = campos.filter(campo => campo.defecto == 1);
          this.insertarCamposSeleccionados(defecto).then(res => {
            resolve(defecto);
          });
        })
        .catch(error => {
          console.error("Ocurrio un ERROR al cargar los campos por defecto");
          console.error(error);
          reject(error);
        });
    });
  }

  public cargarMotivosParo(actividad_id) {
    return new Promise((resolve, reject) => {
      let sql = `select 
                distinct
                c0836_rowid,
                c0836_rowid_motivo_paro,
                c0836_rowid_actividad_ot,
                c0836_fecha_paro,
                c0836_dias_paro,
                c0836_horas_paro,
                c0836_minutos_paro,
                c0835_ind_tipo
                from w0836_motivos_paro_ot
                inner join w0835_motivos_paro w0835 on w0835.c0835_rowid = c0836_rowid_motivo_paro
                where c0836_rowid_actividad_ot = ${actividad_id}
    `;
      this.db
        .query(sql)
        .then(res => resolve(res))
        .catch(error => {
          console.error(
            "ERROR al consultar los motivos de paro de la actividad"
          );
          console.error(error);
        });
    });
  }

  public calcularTiempoMovimientos(actividad) {
    return new Promise((resolve, reject) => {
      this.obtenerTiempoMovimientos(actividad)
        .then((res: any[]) => {
          if (res.length > 0) {
            let tiempo = 0;
            res.forEach(time => {
              // console.log("time");
              // console.log(time);
              tiempo +=this.diferenciaFechasSegundos(time.c0846_hora_fin,time.c0846_hora_ini)
              // console.log("tiempo");
              // console.log(tiempo);
            });
            resolve(tiempo);
          } else {
            resolve(0);
          }
        })
        .catch(error => {
          console.error(
            "Error al intentar consultar los tiempos de los movimientos de la actividad: " +
              actividad.id
          );
          console.error(error);
          reject(error);
        });
    });
  }

  public obtenerTiempoMovimientos(actividad) {
    return new Promise((resolve, reject) => {
      let sql = `
    select 
	    c0846_hora_ini,
      c0846_hora_fin
    from w0846_movto_actividades_ot
    where c0846_rowid_actividad_ot = ${
      actividad.id
    } and c0846_ind_estado = ${PAUSADA}
        `;
      this.db
        .query(sql)
        .then(res => resolve(res))
        .catch(error => reject(error));
    });
  }

  public consultarUsuarioLocal() {
    return new Promise((resolve, reject) => {
      if (global.usuario["f_tercero_rowid"]) {
        resolve(global.usuario);
        return;
      }
      let sql_usuario = `select documento from configuraciones where id = 'usuario'`;
      this.db
        .query(sql_usuario)
        .then(res => {
          if (res.length > 0) {
            global.usuario = JSON.parse(res[0].documento);
            localStorage.setItem("usuario", res[0].documento);
            resolve(global.usuario);
          } else {
            reject("no Hay usuarios en la BD local");
          }
        })
        .catch(error => {
          console.error(
            "Error al intentar consultar el usuario en la BD local"
          );
          console.error(error);
          reject(error);
        });
    });
  }

  cargarDatosLocalStorge() {
    var datos_local = [
      "params_mnto",
      "actividadSel",
      "seguridad_mnto",
      "usuario"
    ];
    datos_local.forEach(dato => {
      if (localStorage.getItem(dato)) {
        global[dato] = JSON.parse(localStorage.getItem(dato));
      }
    });
  }

  public consultarTareasActividades(actividades, index = 0) {
    return new Promise((resolve, reject) => {
      if (actividades.length == index) {
        resolve(actividades);
        return;
      }
      var sql_tareas = `
            select
              c0847_ind_ejecucion c0847_ind_ejecucion,
              c0847_descripcion_tarea c0847_descripcion_tarea,
              c0847_observaciones c0847_observaciones,
              c0847_orden_tarea c0847_orden_tarea,
              c0847_notas_tarea c0847_notas_tarea,
              c0847_rowid c0847_rowid,
              c0847_rowid_movto_activ_ot c0847_rowid_movto_activ_ot


            from w0822_actividades_ot 
            inner join w0846_movto_actividades_ot w0846 on w0846.c0846_rowid_actividad_ot = c0822_rowid
            inner join w0847_movto_actividades_tareas w0847 on w0847.c0847_rowid_movto_activ_ot = c0846_rowid

            where c0822_ind_enviado = 0
            and c0846_ind_estado in (2,3)
            and c0846_rowid_docto_ot =`;
      let sql = sql_tareas + " " + actividades[index][0].c0846RowidDoctoOt;
      this.db
        .query(sql)
        .then(res => {
          // console.warn("sql: \n" + sql);
          // console.warn("resultado de tareas: ");
          // console.log(res);
          // console.warn("index de actvidades: " + index);
          // console.log(actividades[index]);
          if (res && res.length > 0) {
            actividades[index].forEach(movimiento => {
              movimiento.datosXML = res;
            });
          }
          this.consultarTareasActividades(actividades, ++index).then(res2 => {
            resolve(actividades);
          });
        })
        .catch(error => {
          console.error("ERROR al consultar las tareas de las actividades");
          console.error(error);
          reject(error);
        });
    });
  }

  public getTareasSinIniciar() {
    return new Promise((resolve, reject) => {
      let c0822_rowid = global.actividadSel.c0822_rowid;
      let sql = `
          select c0848_rowid
          from w0848_actividades_plan_tareas w0848
          inner join w0822_actividades_ot w0822 
          on w0822.c0822_rowid_actividad = w0848.c0848_rowid_actividad
          where w0822.c0822_rowid = ${c0822_rowid}
        `;
      this.db
        .query(sql)
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  }

  /**
   * Obtiene las actividades y las guarda en el objeto actividades
   * muestra las actividades terminadas si se envia como parametro
   * el string "terminadas", si este no se envia, se muestran
   * las actividades con estado diferente a terminadas
   * @param tipo default vacio, filtra las actividades segun su estado
   */
  public obtenerActividades(tipo = "", prvIdEquipo = 0, actividad_id = null) {
    return new Promise((resolve, reject) => {
      let filtro = "";
      if (tipo == "terminadas") {
        filtro = `where c0846_ind_estado = 2 `;
      } else if (prvIdEquipo != 0) {
        // console.log("++++++++++Se pone el FILTRO");
        filtro = ` where
       c0822_id_equipo = ${prvIdEquipo}   
        and  (c0846_ind_estado is null or c0846_ind_estado != 2)`;
        if (actividad_id != null && actividad_id != 0) {
          filtro += ` and w0822.c0822_rowid = ${actividad_id}`;
        }
      } else {
        filtro = `where (c0846_ind_estado is null or c0846_ind_estado != 2)`;
      }
      // console.log(this.camposVer);
      // console.log("se va a ejecutar la consulta");
      // let sql_campos = `select c08223_id from w08223_campos where c08223_rowid_tercero = ${global.usuario['f_tercero_rowid']}`;
      // this.db.query(sql_campos).then(res=>{

      // }).catch(error=>{

      // })

      let camposVer = "";
      let sql_consulta_campos = `select documento from configuraciones where id = 'PARAMETROS_MANTENIMIENTO'`;
      this.db
        .query(sql_consulta_campos)
        .then((res: any) => {
          if (res.length > 0 && res[0] && res[0].documento) {
            let listaCampos = JSON.parse(res[0].documento);
            listaCampos.forEach(campo => {
              camposVer += campo.constructor + ",\n";
            });
            this.consultarActividadesLista(filtro, camposVer)
              .then((res: any) => resolve(res))
              .catch(err => reject(err));
          }
        })
        .catch(error => {
          console.error("Ocurrio un error al consultar los campos adicionales");
          this.consultarActividadesLista(filtro, camposVer)
            .then((res: any) => resolve(res))
            .catch(err => reject(err));
        });
    });
  }

  /**
   *
   * @param filtro filtro armado en sql para filtrar la consulta
   * @param camposVer
   * @param tipo
   */
  private consultarActividadesLista(filtro = "", camposVer = "") {
    return new Promise((resolve, reject) => {
      this.consultarUsuarioLocal()
        .then(res => {
          let sql = `
             SELECT 
             ${camposVer}
            case 
								when w0800.c0800_descripcion is not null then w0800.c0800_descripcion  
								when w0811.c0811_rowid is not null then w0811.c0811_id 
								else "Sin nombre" 
							end desc_equipo,
							c0803_id,
							w0800.c0800_rowid_clase_equipo1,
							w0810_clase1.c0810_id clase_1,
						  w0800.c0800_rowid_clase_equipo2 clase_2,
							w0810_clase2.c0810_id clase_2,
              c5801_FOTO f_foto,
              case when c0822_ind_enviado = 1 then 1 else 0 end ind_enviado,
             case 
                when exists (select 1 from w08222_errores
                      where c08222_rowid_actividad_ot = w0822.c0822_rowid ) then 0
                      else 1
              end habilitar_error,
              c0822_rowid id,
              case  
                when c0822_rowid_responsable != ${global.usuario["f_tercero_rowid"]} then 'bordeIzquierdoNegro'
                when c0846_rowid is not null and c0846_ind_estado = 1 then 'bordeIzquierdoAmarillo'
                when c0846_rowid is not null and c0846_ind_estado = 2 then 'bordeIzquierdoVerde'
                when c0846_rowid is not null and c0846_ind_estado = 3 then 'bordeIzquierdoGris'
                when c0822_fecha_programada < date('now') then 'bordeIzquierdoRojo'
                else ''
              end bordeIzquierdo,
              case  
                when c0846_rowid is not null and c0846_ind_estado = 1 then 1
                when c0846_rowid is not null and c0846_ind_estado = 2 then 3
                else 2
              end orden
              

            FROM w0822_actividades_ot w0822
            inner join w0820_doctos_ot w0820 on w0820.c0820_rowid = w0822.c0822_rowid_docto_ot
						
            left join w0800_equipos w0800 on w0800.c0800_id = w0822.c0822_id_equipo
						-- Tipo de equipo
						left join w0803_tipos_equipos w0803 on w0803.c0803_rowid = c0800_rowid_tipo_equipo
						left join w0810_clases_equipos w0810_clase1 on w0800.c0800_rowid_clase_equipo1 = w0810_clase1.c0810_rowid 
						left join w0810_clases_equipos w0810_clase2 on w0810_clase2.c0810_rowid = w0800.c0800_rowid_clase_equipo2
            left join w0811_localidades w0811 on w0811.c0811_rowid = w0822.c0822_rowid_localidad
            left join w5801_FF_FOTOS_BAJA_CALIDAD w5801 on w5801.c5801_ROWID_FOTO = w0800.c0800_rowid_foto
            left join w0808_actividades_plan w0808 on w0808.c0808_rowid = w0822.c0822_rowid_actividad
            left join w0819_mnto_no_rutinario w0819 on w0819.c0819_rowid_w0822 = w0822.c0822_rowid 
						-- Clase 1 de actividad
						left join w0813_clases_actividades w0813_clase1 on w0813_clase1.c0813_rowid = c0808_rowid_clase_actividad1 or 
																															w0813_clase1.c0813_rowid  = c0819_rowid_clase_actividad1
						-- Clase 2 de actividad
						left join w0813_clases_actividades w0813_clase2 on w0813_clase2.c0813_rowid = c0808_rowid_clase_actividad2 or 
																														w0813_clase2.c0813_rowid  = c0819_rowid_clase_actividad2
						-- LOCALIDAD DEL EQUIPO
						left join w0811_localidades w0811_equipo on w0811_equipo.c0811_rowid = w0800.c0800_rowid_localidad
			left join (select c0846_rowid_actividad_ot, max(w846.c0846_rowid) c0846_rowid_1 from w0846_movto_actividades_ot w846 group by w846.c0846_rowid_actividad_ot) t1 
				on t1.c0846_rowid_actividad_ot = w0822.c0822_rowid
			left join w0846_movto_actividades_ot w0846 on w0846.c0846_rowid = t1.c0846_rowid_1
      left join w0806_planes_mnto w0806 on w0806.c0806_rowid = w0800.c0800_rowid_plan_mnto
            ${filtro}
            group by  w0822.c0822_rowid
            ORDER BY orden asc ,w0846.c0846_rowid desc,w0822.c0822_fecha_programada  
             
            `;
            // console.warn("SQL GENERADO consulta: ",sql)
          this.db
            .query(sql)
            .then(respuesta => resolve(respuesta))
            .catch(error => {
              console.error("Ocurrio un error al listar las actividades");
              console.error(error);
              reject(error);
            });
        })
        .catch(error => console.error(error));
    });
  }

  public deleteActividadesBloqueadas() {
    return new Promise((resolve, reject) => {
      this.consultarUsuarioLocal()
        .then(res => {
          let sql = `
      delete from w0822_actividades_ot
      where c0822_rowid in (select c0822_rowid from w0822_actividades_ot
      left join w0846_movto_actividades_ot w0846 on w0846.c0846_rowid_actividad_ot = c0822_rowid
      where
      c0822_rowid_responsable != ${global.usuario["f_tercero_rowid"]}
      and  c0846_rowid is null)
      `;
          this.db
            .queryExec(sql)
            .then(res => {
              // console.log("Se eliminaron las actividades reasignadas sin iniciar");
              resolve(res);
            })
            .catch(error => {
              console.error(
                "ERROR al intentar eliminar las actividades reasignadas"
              );
              console.error(error);
              reject(error);
            });
        })
        .catch(error => console.error(error));
    });
  }

  public consultarSeguridades() {
    return new Promise((resolve, reject) => {
      let sql = `
      SELECT
      w571.c571_id_metodo,
      w571.c571_nombre,
      w571.c571_constante
      FROM
      w571_ss_usuarios_perfil w571
      `;
      this.db
        .query(sql)
        .then(response => {
          response.forEach(seguridad => {
            global.seguridad_mnto[seguridad.c571_constante] = 1;
            // console.warn("se carga la seguridad: " + seguridad.c571_constante);
          });
          if (global.seguridad_mnto) {
            localStorage.setItem(
              "seguridad_mnto",
              JSON.stringify(global.seguridad_mnto)
            );
          }
          resolve(response);
        })

        .catch(error => {
          console.error("ERROR al consultar las seguridades");
          console.error(error);
          reject(error);
        });
    });
  }

  public validacionVariableLocal(actividad_id = null) {
    return new Promise((resolve, reject) => {
      var datos_local = [
        "PARAMETROS_MANTENIMIENTO",
        "seguridad_mnto",
        "usuario"
        // "params_mnto"
      ];
      if (!actividad_id || actividad_id == "") {
        // console.log("actividad_id");
        // console.log(actividad_id);
        datos_local.push("actividadSel");
      }

      datos_local.forEach(dato => {
        if (localStorage.getItem(dato) && localStorage.getItem(dato) != "") {
          global[dato] = JSON.parse(localStorage.getItem(dato));
        }
      });

      // console.warn("Antes de guardar el USUARIO: ");
      // console.warn(global.usuario);
      if (global.usuario && global.usuario != {}) {
        localStorage.setItem("usuario", JSON.stringify(global.usuario));
      }
      datos_local.forEach(dato => {
        if (global[dato]) {
          localStorage.setItem(dato, JSON.stringify(global[dato]));
        }
      });

      // if (global.actividadSel) {
      //   this.guardarActividadLocalStorge();
      // }

      this.consultarSeguridades()
        .then(res => resolve(res))
        .catch(err => reject(err));
    });
  }
  limpiarFiltroNFC() {
    // global.params_mnto.id_equipo = 0;
    // if(localStorage.getItem('params_mnto')){
    //   localStorage.setItem('params_mnto',JSON.stringify(global.params_mnto));
    // }
  }

  setearDatosActividad(actividad) {
    // console.log("Se setean los datos de la actividad: " + actividad.id);
    // console.log(actividad);
    //valores por defecto
    // actividad.habilitar_error = true;
    // let llaves = Object.keys(actividad);
    // llaves.forEach(llave => {
    //   if (global.actividadSel[llave]) {
    //     global.actividadSel[llave] = actividad[llave];
    //   }
    // });
    // global.actividadSel = this.actividadSel;
    global.actividadSel = this.agregarData(actividad, global.actividadSel);
    return global.actividadSel;
  }
  /**
   * Funcion para agregar valores de un objeto a otro
   * @param origen objeto a quien se le agrega los valores o se actualizan
   * @param destino objeto fuente de los valores a asignar
   */
  public agregarData(origen, destino) {
    let llaves = Object.keys(origen);
    llaves.forEach(llave => {
      destino[llave] = origen[llave];
    });
    return destino;
  }

  public guardarActividadLocalStorge(actividad = null) {
    // console.warn("Guardar Actividad");
    // console.log("global.actividadSel");
    // console.log(global.actividadSel);
    // console.log("actividad");
    // console.log(actividad);
    if (actividad != null) {
      if (!global.actividadSel) {
        global.actividadSel = actividad;
      } else {
        this.setearDatosActividad(actividad);
      }
    }
    // console.log("------Cambiado global.actividadSel");
    // console.log(global.actividadSel);
    // console.warn("Se va a emitir la actividad\n\n");
    this.events.publish("actividadSel_change", global.actividadSel);
    localStorage.setItem("actividadSel", JSON.stringify(global.actividadSel));
    return global.actividadSel;
  }
  public cargarCamposVer() {
    return new Promise((resolve, reject) => {
      this.consultarUsuarioLocal()
        .then(res => {
          //Listado de campos de actividades
          let sql_campos = `select
        c08223_id,
        c08223_class 
        from w08223_campos 
        where c08223_rowid_tercero =${global.usuario["f_tercero_rowid"]}
        `;
          this.db
            .query(sql_campos)
            .then(res => resolve(res))
            .catch(err => reject(err));
        })
        .catch(error => console.error(error));
    });
  }

  public cargarCamposVerDefecto() {
    return new Promise((resolve, reject) => {
      let sql_consulta_campos_defecto = `select documento from configuraciones where id = 'PARAMETROS_MANTENIMIENTO'`;
      this.db
        .query(sql_consulta_campos_defecto)
        .then((res: any) => {
          global["PARAMETROS_MANTENIMIENTO"] = JSON.parse(res[0].documento);
          let values = global["PARAMETROS_MANTENIMIENTO"].filter(
            campo => campo.defecto == 1
          );
          var campos_defecto = " ";
          let campos_seleccionados = [];
          values.forEach(value => {
            campos_seleccionados.push({
              c08223_id: value.alias,
              c08223_class: value.class
            });
            campos_defecto += `('${value.alias}',${
              global.usuario["f_tercero_rowid"]
            }),`;
          });
          resolve(campos_seleccionados);
          campos_defecto = campos_defecto.substring(
            0,
            campos_defecto.length - 1
          );

          let sql_campos_insert = `insert into w08223_campos (c08223_id, c08223_rowid_tercero )
          values ${campos_defecto}`;
          this.db
            .query(sql_campos_insert)
            .then(res => {
              // console.log("se insertaron los campos por defecto");
            })
            .catch(error => {
              console.error("ERROR no se pudo insertar los campos por defecto");
              console.error(error);
            });
        })
        .catch(error => {
          console.error("ERROR al consultar los parametros de mantenimiento");
          console.error(error);
          reject(error);
        });
    });
  }

  /**
   * name
   */
  public enviarMovimientosActividad(actividad) {
    return new Promise((resolve, reject) => {
      let parametros = {
        movtoActividades: JSON.stringify(actividad),
        accion: "CrearMovtoApp"
      };
      // console.log(parametros);
      this.sync
        .postParameters(global.api["EnviarActividades"], parametros)
        .then(res => resolve(res))
        .catch(error => reject(error));
    });
  }
  /**
   * Esta funcion realiza el proceso de marcar un actividad si tiene
   * errores o no para guardar los errores y ser mostrados al usuario
   * tambien es la que indica si se deben de habilitar los campos para volber a e
   * enviar la actividad despues de corregir algun error
   *
   * @param actividad actividad a marcar como enviada
   * @param enviado tipo de estado de enviado
   * @param errores errores si los hay como respuesta del servidor
   */
  public marcarActividadEnviada(
    actividad,
    actividadesTerminadas,
    enviado = 1,
    errores = null
  ) {
    return new Promise((resolve, reject) => {
      // console.warn("Marcando la actividad con enviado: " + enviado);
      var rowid_actividad_ot = actividad[0].c0846RowidActividadOt;
      var update_fecha_enviado = "";
      if (enviado == 1) {
        update_fecha_enviado = ", c0822_fecha_enviada_app = datetime('now')";
      }

      let sql = `
      update w0822_actividades_ot
      set c0822_ind_enviado = ${enviado} 
      ${update_fecha_enviado}
      where c0822_rowid = ${rowid_actividad_ot}
      `;
      let sql_limpiar_errores = `delete from w08222_errores where c08222_rowid_actividad_ot = ${rowid_actividad_ot}`;
      let sql_errores = `
      insert into w08222_errores (c08222_rowid_actividad_ot,c08222_error,c08222_error_detalle)
      values (
      `;
      this.db
        .query(sql)
        .then(res => {
          let terminada = actividadesTerminadas.find(
            actividadTerminada => actividadTerminada.id == rowid_actividad_ot
          );

          if (terminada) {
            terminada.c0822_ind_enviado = enviado;
            //Se marca que habilitar error sea true para bloquear o deshabilitar los campos
            actividad.habilitar_error = 1;
            if(enviado==1){
              actividad.ind_enviado = 1;
              terminada.ind_enviado = 1;
              this.events.publish("envioTerminado",{actividad_id:terminada.id});
            }
            this.db
              .query(sql_limpiar_errores)
              .then(res => {
                // console.log("Se limpia errores si tiene ")
              }).catch(err=>{
                console.error("Error al limpiar los errores de la actividad", terminada)
                console.error(err)
              })

            

            if (enviado > 1) {
              actividad.ind_enviado = 0;
              //Se habilita los campos para poder volver a enviar la actividad
              actividad.habilitar_error = 0;
            }

            if (enviado == 2) {
              let detalle = errores[1];
              if (detalle.substring(0, 1) != "'") {
                detalle = '"' + detalle + '"';
              }

              let sql_errores_full =
                sql_errores +
                `${rowid_actividad_ot},'${errores[0]}',${detalle} ) `;
              this.db
                .query(sql_limpiar_errores)
                .then(res => {
                  // console.warn(
                  //   "se limpiaron los errores sql:\n" + sql_limpiar_errores
                  // );
                  this.db
                    .query(sql_errores_full)
                    .then(res => {
                      actividad.habilitar_error = false;
                      // console.log("Se guardaron los errores correctamente");
                      resolve(res);
                    })
                    .catch(error => {
                      actividad.habilitar_error = false;
                      console.error("no se logro guardar los errores");
                      console.error(error);
                      resolve(res);
                    });
                })
                .catch(error => {
                  console.error(
                    "Error al intentar eliminar los errores actuales para reemplzar por los nuevos"
                  );
                  console.error(error);
                });
            } else {
              resolve(res);
            }
          } else {
            resolve(res);
          }
        })
        .catch(error => reject(error));
    });
  }
  public procesarActividadesEnviar(
    actividades_enviar,
    actividadesTerminadas,
    enviandoIndex = 0,
    index = 0
  ) {
    // console.log("actividades_enviar");
    // console.log(actividades_enviar)
    // console.warn(
    //   "Entro a procesar actividades.lenght: " +
    //     actividades_enviar.lenght +
    //     " index: " +
    //     index
    // );
    if (actividades_enviar.length == index) {
      enviandoIndex = -1;
      this.events.publish("enviandoIndex", enviandoIndex);
      return;
    }
    enviandoIndex = actividades_enviar[index][0].c0846RowidActividadOt;
    this.events.publish("enviandoIndex", enviandoIndex);

    this.enviarMovimientosActividad(actividades_enviar[index])
      .then(res => {
        //Aqui se debe de evaluar la respuesta para saber como
        //se va a marcar la actividad sin error o con errores
        let enviada = 1;

        let errores = null;
        if (res && res["error"]) {
          // console.log("res de envio:");
          // console.log(res);
          //con error
          enviada = 2;
          errores = res["error"];
          console.error("ERROR de validacion: \n" + res["error"][0]);
        }
        // console.warn("ERROR: " + JSON.stringify);
        this.marcarActividadEnviada(
          actividades_enviar[index],
          actividadesTerminadas,
          enviada,
          errores
        )
          .then(res => {
            this.procesarActividadesEnviar(
              actividades_enviar,
              actividadesTerminadas,
              enviandoIndex,
              ++index
            );
          })
          .catch(error => {
            console.error("No se marco como enviada la actividad");
            console.error(actividades_enviar[index]);
            console.error(error);
            this.procesarActividadesEnviar(
              actividades_enviar,
              actividadesTerminadas,
              enviandoIndex,
              ++index
            );
          });
      })
      .catch(error => {
        console.error("index error: " + index);
        console.error(error);
        this.marcarActividadEnviada(
          actividades_enviar[index],
          actividadesTerminadas,
          3
        )
          .then(res => {
            this.procesarActividadesEnviar(
              actividades_enviar,
              actividadesTerminadas,
              enviandoIndex,
              ++index
            );
          })
          .catch(error => {
            console.error("No se marco como enviada la actividad");
            console.error(actividades_enviar[index]);
            console.error(error);
            this.procesarActividadesEnviar(
              actividades_enviar,
              actividadesTerminadas,
              enviandoIndex,
              ++index
            );
          });
      });
  }

  public consultarActividadesTerminadasMovimientos() {
    return new Promise((resolve, reject) => {
      this.consultarUsuarioLocal()
        .then(res => {
          let sql_rowid_docto_ot = `
        select 
            c0846_rowid_docto_ot
        from w0822_actividades_ot 
        inner join w0846_movto_actividades_ot w0846 on 
        w0846.c0846_rowid_actividad_ot = c0822_rowid
        
        where c0822_ind_enviado != 1
        and c0846_ind_estado in (2,3)
        GROUP BY c0846_rowid_docto_ot`;
          // TipoMnto
          //1 preventivo
          //2 No rutinario
          let sql_2 = `select 
            ''||c0846_rowid_docto_ot            c0846RowidDoctoOt,
            ''||c0846_id_cia                    c0801IdCia, 
            c0846_fecha_ejecucion               c0846FechaEjecucion,
            ''||c0846_rowid_responsable          c0846RowidResponsable,
            ''||case when c0846_rowid_ayudante1 is null then "" else c0846_rowid_ayudante1 end            c0846RowidAyudante1, 
            ''||case when c0846_rowid_ayudante2 is null then "" else c0846_rowid_ayudante2 end           c0846RowidAyudante2,
            ''||case when c0846_rowid_ayudante3 is null then "" else c0846_rowid_ayudante3 end           c0846RowidAyudante3,
            ''||case when c0846_rowid_ayudante4 is null then "" else c0846_rowid_ayudante4 end           c0846RowidAyudante4,
            ''||case when c0846_rowid_ayudante5 is null then "" else c0846_rowid_ayudante5 end           c0846RowidAyudante5,
            ''||c0846_ultima_lectura                c0846UltimaLectura,
            ''||c0846_rowid_causal_falla            c0846RowidCausalFalla,
            ''||c0846_rowid_tipo_falla              c0846RowidTipoFalla,
            ''||c0846_ind_paro_equipo               c0846IndParoEquipo,
            ''||c0846_ind_paro_otros                c0846IndParoOtros,
            ''||c0846_ind_dano                      c0846IndDano,
            ''||c0846_ind_tipo_dano                 c0846IndTipoDano,
            c0846_detalle_dano                  c0846DetalleDano,
            c0846_notas                         c0846Notas,
            c0846_hora_ini                      c0846HoraIni,
            c0846_hora_fin                      c0846HoraFin,
            ''||c0846_rowid_actividad_ot        c0846RowidActividadOt,
            "0"                                 c0846HoraEjecucionHora,
            "0"                                 c0846HoraEjecucionMin,
            "0"                                 c0846HorasParoEqpHora,
            "0"                                 c0846HorasParoEqpMin,
            "0"                                 c0846HorasParoEquipoHora,
            "0"                                 c0846HorasParoEquipoMin,
            ''||(((strftime('%s',c0846_hora_fin) - strftime('%s',c0846_hora_ini) )/60)/60) c0846HorasInvertidasHora,
            ''||(((strftime('%s',c0846_hora_fin) - strftime('%s',c0846_hora_ini) )/60)%60) c0846HorasInvertidasMin,
            ''||c0846_dias_paro_real_eqp c0846DiasParoRealEqp,
            case when c0822_rowid_actividad IS NULL then '2'
            else '1'
            end c0846TipoMnto,
            case  
                when c0846_rowid is not null and c0846_ind_estado = 1 then 1
                when c0846_rowid is not null and c0846_ind_estado = 2 then 3
                else 2
              end orden,
              c0846_rowid,
              c0822_fecha_programada

            from w0822_actividades_ot  w0822
            inner join w0846_movto_actividades_ot w0846 on w0846.c0846_rowid_actividad_ot = c0822_rowid
            

            where c0822_ind_enviado != 1
            and c0846_ind_estado in (2,3)
            and c0822_rowid_responsable = ${global.usuario["f_tercero_rowid"]}
            ORDER BY orden asc ,w0846.c0846_rowid desc,w0822.c0822_fecha_programada  

        `;

          var actividades_id = [];
          var actividades = [];
          this.db
            .query(sql_rowid_docto_ot)
            .then(res => {
              actividades_id = res;
              this.db.query(sql_2).then(actividades_todas => {
                // console.warn("actividades_todas.");
                // console.warn(actividades_todas);
                actividades_id.forEach((actividad_id, index) => {
                  actividades[index] = actividades_todas.filter(actividad => {
                    // console.log("actividad: ");
                    // console.log(actividad);
                    return (
                      actividad.c0846RowidDoctoOt ==
                      actividad_id.c0846_rowid_docto_ot
                    );
                  });
                });
                // console.log("actividades");
                // console.log(actividades);
                this.consultarTareasActividades(actividades).then(res => {
                  this.consultarMotivosParoActividades(actividades)
                    .then(res => {
                      resolve(actividades);
                    })
                    .catch(error => reject(error));
                });
              });
            })
            .catch(error => reject(error));
        })
        .catch(error => console.error(error));
    });
  }

  public consultarMotivosParoActividades(actividades, index = 0) {
    return new Promise((resolve, reject) => {
      if (actividades.length == index) {
        resolve(actividades);
      }
      var sql = `SELECT
                            c0822_rowid_docto_ot c0846RowidDoctoOt,
                            w0836.c0836_dias_paro AS c0836DiasParo,
                            w0836.c0836_horas_paro AS c0000HhParo,
                            w0836.c0836_minutos_paro AS c0000MmParo,
                            (c0836_horas_paro || ":" || c0836_minutos_paro) AS c0836HorasParoText,
                            w0836.c0836_fecha_paro||".000" AS c0836FechaParo,
                            w0836.c0836_rowid_motivo_paro AS c0836RowidMotivoParo,
                            "C" AS operacion
                        FROM
                        w0822_actividades_ot
                        INNER JOIN w0836_motivos_paro_ot AS w0836 ON w0836.c0836_rowid_actividad_ot = w0822_actividades_ot.c0822_rowid
                        WHERE
                        w0822_actividades_ot.c0822_ind_enviado = 0
                        and c0822_rowid_docto_ot = ${
                          actividades[index][0].c0846RowidDoctoOt
                        } `;
      this.db
        .query(sql)
        .then(res => {
          actividades[index][0].MapActividadesPorOtMotivos = res;
          actividades[index][0].MapActualizaEquipoLocalidadDetalleMotivos = [];
          this.consultarMotivosParoActividades(actividades, ++index).then(
            res => {
              resolve(actividades);
            }
          );
        })
        .catch(error => reject(error));
    });
  }
  /**
   * cargarDetallesActividad
   * recibe el id de la actividad a consultar, para obtener
   * los datos necesarios de la actividad
   *
   * @param actividad_id
   */
  public cargarDetallesActividad(actividad_id) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT 
      w0820.c0820_consec_docto,
      w0820.c0820_id_co,
      w0820.c0820_id_tipo_docto,
      w0822.c0822_rowid id,  
      case 
        when w0800.c0800_descripcion is not null then w0800.c0800_descripcion 
        when w0811.c0811_rowid is not null then w0811.c0811_id
        else	'Sin nombre'
      end descripcion, 
      case
        when c0808_id is not null then c0808_id
        when w0819.c0819_descripcion_trabajo is not null then w0819.c0819_descripcion_trabajo 
      END trabajo,

      c5801_FOTO f_foto,
      w0822.c0822_fecha_programada,
      case when c0808_id is not null then w0822.c0822_notas
            else ' '  
      END c0822_notas,
      case 
          when w0808.c0808_rowid is not null then w0808.c0808_instructivo
          ELSE c0819_detalle_trabajo
      end procedimiento,
      c0819_rowid_w0822,
      c0822_rowid_docto_ot,
      c0822_rowid,
      c0822_id_equipo,
      c0808_referencia,
      c0806_ind_regimen,
      case 
        when c0808_ind_requiere_paro is null then 0
        else c0808_ind_requiere_paro
      end c0808_ind_requiere_paro,
      case 
        when c0819_ind_requiere_paro is null then 0
        else c0819_ind_requiere_paro
      end c0819_ind_requiere_paro,
      c0819_ind_tipo_trabajo,
      c0822_duracion_horas_prog,
      c0822_rowid_actividad,
      c0822_rowid_responsable,
      w0822.c0822_ind_enviado
      
			        

      FROM w0822_actividades_ot w0822
      inner join w0820_doctos_ot w0820 on w0820.c0820_rowid = w0822.c0822_rowid_docto_ot
      left join w0800_equipos w0800 on w0800.c0800_id = w0822.c0822_id_equipo
      left join w0811_localidades w0811 on w0811.c0811_rowid = w0822.c0822_rowid_localidad
      left join w5801_FF_FOTOS_BAJA_CALIDAD w5801 on w5801.c5801_ROWID_FOTO = w0800.c0800_rowid_foto
      left join w0808_actividades_plan w0808 on w0808.c0808_rowid = w0822.c0822_rowid_actividad
      left join w0819_mnto_no_rutinario w0819 on w0819.c0819_rowid_w0822 = w0822.c0822_rowid 
      left join w0806_planes_mnto w0806 on w0806.c0806_rowid = w0800.c0800_rowid_plan_mnto
      
      where c0822_rowid = ${actividad_id}
          
            `;
      this.db
        .query(sql)
        .then(res => {
          // console.warn("res")
          // console.warn(res)
          resolve(res[0]);
        })
        .catch(error => {
          console.error("ERROR al consultar los detalles de la actividad");
          console.error(error);
          reject(error);
        });
    });
  }
  public exigeNFC_equipo(actividad) {
    return new Promise((resolve, reject) => {
      if (
        actividad.c0822_id_equipo != null &&
        actividad.c0822_id_equipo != "null"
      ) {
        let sql_equipo = `
        select 
        c0800_ind_ident_inalambrico,
        c0800_id
        from w0800_equipos
        where c0800_id = ${actividad.c0822_id_equipo}
        `;
        this.db
          .query(sql_equipo)
          .then(res => resolve(res[0].c0800_ind_ident_inalambrico == 1))
          .catch(error => reject(error));
      } else {
        resolve(false);
      }
    });
  }
  public validarExigeNFC(actividad) {
    return new Promise((resolve, reject) => {
      this.exigeNFC_CIA()
        .then(ind_scan_movil => {
          if (ind_scan_movil) {
            // let msj = "Se requiere SCAN Movil segun la compañia";
            // console.log(msj);
            this.exigeNFC_equipo(actividad)
              .then(c0800_ind_ident_inalambrico =>
                resolve(c0800_ind_ident_inalambrico)
              )
              .catch(error => reject(error));
          } else {
            // console.log("No se requiere SCAN movil");
            // console.log("se puede iniciari de una vez");
            resolve(false);
          }
        })
        .catch(error => reject(error));
    });
  }
  public exigeNFC_CIA() {
    return new Promise((resolve, reject) => {
      let sql = ` select 
                c0801_ind_exige_scan_movil ind_scan_movil
                from w0801_parametros_cia
                `;
      this.db
        .query(sql)
        .then(respuesta => {
          // console.log("++++++++Respuesta de averiguar si la CIA exige NFC");
          // console.log(respuesta);
          resolve(respuesta[0].ind_scan_movil == 1);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  /**
   * crearMovimiento
   * recibe como parametro la actividad a generar el movimiento
   * esta guarda el movimiento de la actividad con los valores basicos
   * y por defecto para iniciar la actividad
   * setea el valor de la fecha inicial
   * @param actividadSel
   */
  public crearMovimiento(actividadSel) {
    return new Promise((resolve, reject) => {
      // console.warn("*******se entro a crear el movimiento para iniciar la actividad");
      let c0846_ind_paro_equipo = actividadSel.motivos.length > 0;
      let sql = `insert into w0846_movto_actividades_ot 
              (
                c0846_id_cia,
                c0846_rowid_docto_ot,
                c0846_rowid_actividad_ot,
                c0846_fecha_ejecucion,
                c0846_hora_ini,
                c0846_hora_fin,
                c0846_total_horas,
                c0846_ind_estado,
                c0846_dias_paro_real_eqp,
                c0846_horas_paro_real_eqp,
                c0846_horas_paro_act,
                c0846_ultima_lectura,
                c0846_lectura_ant,
                c0846_turno,
                c0846_rowid_responsable,
                c0846_rowid_ayudante1,
                c0846_rowid_ayudante2,
                c0846_rowid_ayudante3,
                c0846_rowid_ayudante4,
                c0846_rowid_ayudante5,
                c0846_tarifa_responsable,
                c0846_tarifa_ayudante1,
                c0846_tarifa_ayudante2,
                c0846_tarifa_ayudante3,
                c0846_tarifa_ayudante4,
                c0846_tarifa_ayudante5,
                c0846_costo_mo,
                c0846_ind_paro_equipo,
                c0846_ind_paro_otros,
                c0846_ind_dano,
                c0846_ind_tipo_dano,
                c0846_detalle_dano,
                c0846_rowid_causal_falla,
                c0846_rowid_tipo_falla,
                c0846_notas,
                c0846_fecha_creacion,
                c0846_usuario_creacion,
                c0846_fecha_actualizacion,
                c0846_usuario_actualizacion
              )
              values
              (
                ${global.dispositivo.idCia},
                ${actividadSel.c0822_rowid_docto_ot},
                ${actividadSel.c0822_rowid}, --c0846_rowid_actividad_ot
                datetime('now', 'localtime'), -- c0846_fecha_ejecucion
                datetime('now', 'localtime'), -- c0846_hora_ini
                datetime('now', 'localtime'), -- c0846_hora_fin // se debe de actualizar cuando se finalice
                0, -- c0846_total_horas
                1, -- c0846_ind_estado
                0, -- c0846_dias_paro_real_eqp
                0, -- c0846_horas_paro_real_eqp
                0, -- c0846_horas_paro_act
                ${
                  actividadSel.lecturas ? actividadSel.lecturas : 0
                }, -- c0846_ultima_lectura
                0, -- c0846_lectura_ant se carga sola en el servidor 
                0, -- c0846_turno
                ${global.usuario["f_tercero_rowid"]}, -- c0846_rowid_responsable
                ${
                  actividadSel.ayudantes[0]
                    ? actividadSel.ayudantes[0].c0827_rowid_tercero
                    : null
                },-- c0846_rowid_ayudante1
                ${
                  actividadSel.ayudantes[1]
                    ? actividadSel.ayudantes[1].c0827_rowid_tercero
                    : null
                },-- c0846_rowid_ayudante2
                ${
                  actividadSel.ayudantes[2]
                    ? actividadSel.ayudantes[2].c0827_rowid_tercero
                    : null
                },-- c0846_rowid_ayudante3
                ${
                  actividadSel.ayudantes[3]
                    ? actividadSel.ayudantes[3].c0827_rowid_tercero
                    : null
                },-- c0846_rowid_ayudante4
                ${
                  actividadSel.ayudantes[4]
                    ? actividadSel.ayudantes[4].c0827_rowid_tercero
                    : null
                },-- c0846_rowid_ayudante5
                0, -- c0846_tarifa_responsable
                0, -- c0846_tarifa_ayudante1
                0, -- c0846_tarifa_ayudante2
                0, -- c0846_tarifa_ayudante3
                0, -- c0846_tarifa_ayudante4
                0, -- c0846_tarifa_ayudante5
                0, -- c0846_costo_mo
                ${c0846_ind_paro_equipo ? 1 : 0}, -- c0846_ind_paro_equipo
                ${actividadSel.paroEnCadena ? 1 : 0}, -- c0846_ind_paro_otros
                ${
                  actividadSel.c0819_ind_dano ? actividadSel.c0819_ind_dano : 0
                }, -- c0846_ind_dano
                ${
                  actividadSel.c0819_ind_tipo_dano
                    ? actividadSel.c0819_ind_tipo_dano
                    : 0
                }, -- c0846_ind_tipo_dano
                '${
                  actividadSel.c0819_detalle_dano
                    ? actividadSel.c0819_detalle_dano
                    : 0
                }', -- c0846_detalle_dano
                ${
                  actividadSel.c0846_rowid_causal_falla
                }, -- c0846_rowid_causal_falla
                ${
                  actividadSel.c0846_rowid_tipo_falla
                }, -- c0846_rowid_tipo_falla
                '${actividadSel.c0846_notas}' , -- c0846_notas
                datetime('now', 'localtime'), --c0846_fecha_creacion
                '${
                  global.usuario["f_tercero_nombre"]
                }', -- c0846_usuario_creacion
                datetime('now', 'localtime'), -- c0846_fecha_actualizacion
                '${
                  global.usuario["f_tercero_nombre"]
                }' -- c0846_usuario_actualizacion

              )          
    `;
      this.db
        .queryExec(sql)
        .then(res => {
          // console.log("respuesta despues de crear el movimiento:");
          // console.log(res);
          actividadSel["c0822_fecha_ejecucion_ini"] = new Date();
          let c0846_rowid_anterior = actividadSel.c0846_rowid;
          actividadSel.c0846_rowid = res.insertId;
          // console.log(
          //   "Ya se creo el registo en la BD del movimiento de la actividad"
          // );
          // console.log(actividadSel);
          actividadSel.c0846_ind_estado = CORRIENDO;
          this.guardarMotivosParo(actividadSel)
            .then(res => {
              this.guardarMovimientosTareas(actividadSel, c0846_rowid_anterior)
                .then(res => {
                  resolve(res);
                })
                .catch(error => reject(error));
            })
            .catch(error => {
              console.error(error);
              console.error("Error al intentar Guardar los Motivos de Paro");
              reject(error);
            });
          // console.log("Se ejecuto la consulta de creacion del movimiento exitosamente");
        })
        .catch(error => {
          console.error(
            "Ocurrio un error al intentar insertar en la tabla de movimientos "
          );
          console.error(error);
          console.error("SQL: ");
          console.info(sql);
          reject(error);
        });
    });
  }

  public guardarMotivosParo(actividad) {
    return new Promise((resolve, reject) => {
      let sqlBorrarMotivosParo = `delete from w0836_motivos_paro_ot where c0836_rowid_actividad_ot = ${
        actividad.id
      }`;
      this.db
        .queryExec(sqlBorrarMotivosParo)
        .then(res => {
          let sql = this.getSQLMotivosParoGuardar(actividad);
          if (sql != "") {
            this.db
              .queryExec(sql)
              .then(res => resolve(res))
              .catch(error => reject(error));
          } else {
            resolve(true);
          }
        })
        .catch(error => reject(error));
    });
  }

  public getSQLMotivosParoGuardar(actividad) {
    //este retorna un SQL para realizar un sql bulk
    if (actividad.motivos == null || actividad.motivos.length == 0) return "";
    let sql = `insert into w0836_motivos_paro_ot 
              (
                c0836_rowid_motivo_paro,
                c0836_rowid_actividad_ot,
                c0836_fecha_paro,
                c0836_dias_paro,
                c0836_horas_paro,
                c0836_minutos_paro
              ) values 
              `;
    let values = "";
    actividad.motivos.forEach(motivo => {
      if (
        motivo.c0836_rowid_motivo_paro != null &&
        motivo.c0836_rowid_motivo_paro != ""
      ) {
        values += `(
              ${motivo.c0836_rowid_motivo_paro},
              ${actividad.id},
              datetime('now', 'localtime'),
              ${motivo.c0836_dias_paro == "" ? 0 : motivo.c0836_dias_paro},
              ${motivo.c0836_horas_paro == "" ? 0 : motivo.c0836_horas_paro},
              ${motivo.c0836_minutos_paro == "" ? 0 : motivo.c0836_minutos_paro}
              ),`;
      }
    });
    if (values.length == 0) return "";
    return sql + values.substr(0, values.length - 1);
  }

  public guardarMovimientosTareas(actividad, c0846_rowid_anterior = null) {
    // console.log("c0846_rowid_anterior");
    // console.log(c0846_rowid_anterior);
    // console.log("entro a guardar Movimientos tareas");
    return new Promise((resolve, reject) => {
      let sql = `
     insert into w0847_movto_actividades_tareas 
      (
        c0847_id_cia,
        c0847_rowid_movto_activ_ot,
        c0847_orden_tarea,
        c0847_descripcion_tarea,
        c0847_notas_tarea,
        c0847_ind_ejecucion,
        c0847_observaciones,
        c0847_fecha_creacion,
        c0847_usuario_creacion,
        c0847_fecha_actualizacion,
        c0847_usuario_actualizacion
      )
  
    
        select 
								${global.dispositivo.idCia},-- c0847_id_cia
								${actividad.c0846_rowid},
								c0848_orden,
								c0848_descripcion,
								c0848_notas,
								0,
								' ',
								c0848_fecha_creacion,
								c0848_usuario_creacion,
								c0848_fecha_actualizacion,
								c0848_usuario_actualizacion
								
								
				FROM	w0848_actividades_plan_tareas
				where c0848_rowid_actividad = ${actividad.c0822_rowid_actividad}

      `;

      //Se consulta si existe algun movimiento con el rowid anterior para asi actualizarlo
      if (c0846_rowid_anterior) {
        let query = `
          select
            c0847_rowid_movto_activ_ot
          from w0847_movto_actividades_tareas
          where c0847_rowid_movto_activ_ot = ${c0846_rowid_anterior}
        `;
        this.db
          .query(query)
          .then(res => {
            // console.log("res");
            // console.log(res);
            if (res) {
              let sql_update = `
              update w0847_movto_actividades_tareas 
              set c0847_rowid_movto_activ_ot = ${actividad.c0846_rowid}
              where c0847_rowid_movto_activ_ot = ${c0846_rowid_anterior};
            `;
              this.db
                .queryExec(sql_update)
                .then(res => {
                  this.actualizarMovimientosTareas(actividad)
                    .then(res => {
                      resolve(true);
                    })
                    .catch(error => {
                      console.error(error);
                      reject(error);
                    });
                  // console.log(
                  //   "Se actulizaron los movimientos de las tareas por el ultimo movimiento de actividad"
                  // );
                  // console.log(res);
                })
                .catch(error => {
                  console.error(
                    "Error al actualizar los movimientos de las tareas"
                  );
                  console.error(error);
                  reject(error);
                });
            }
          })
          .catch(error => {
            console.error("ERROR al consultar los movimientos de tareas");
            console.error(error);
            reject(error);
          });
      } else {
        // console.log("validar antes las tareas si tiene el registro de la c0847");
        // console.log(actividad.tareas);
        if (
          actividad.tareas.lenght > 0 &&
          actividad.tareas[0].c0847_rowid != null
        ) {
          this.actualizarMovimientosTareas(actividad)
            .then(res => resolve(actividad))
            .catch(error => reject(error));
        } else {
          // console.log("SQL movimeintos tareas:");
          // console.log(sql);
          this.db
            .queryExec(sql)
            .then(res => {
              // console.log("Se crearon los movimientos de las tareas");
              // console.log(res);
              // console.log("se vuelven a cargar las tareas");
              this.cargarTareas(actividad)
                .then(res => {
                  actividad.tareas = res;
                  resolve(actividad);
                })
                .catch(err => reject(err));
            })
            .catch(error => {
              console.error("error al crear los movimientos de las tareas");
              console.error(error);
              reject(error);
            });
        }
      }
    });
  }

  public actualizarMovimientosTareas(actividad) {
    return new Promise((resolve, reject) => {
      // console.log("se va a actualizar los movimientos de las tareas");
      let query = `
          select
            c0847_rowid,
            c0847_ind_ejecucion,
            c0847_rowid_movto_activ_ot
          from w0847_movto_actividades_tareas
          where c0847_rowid_movto_activ_ot = ${actividad.c0846_rowid}
        `;
      this.db
        .query(query)
        .then(res => {
          // console.log("res");
          // console.log(res);
          if (res) {
            // console.log("actividad.tareas");
            // console.log(actividad.tareas);
            let listado_true = actividad.tareas.filter(
              tarea => tarea.c0847_ind_ejecucion
            );
            listado_true = listado_true.map(tarea => tarea.c0847_rowid);
            let rowids = "";
            listado_true.forEach(tarea => {
              rowids += tarea + ",";
            });
            rowids = rowids.substring(0, rowids.length - 1);
            rowids = "(" + rowids + ")";
            let sql_update = `
              update w0847_movto_actividades_tareas 
              set c0847_ind_ejecucion = 
              case
                when c0847_rowid in ${rowids} then 1 else 0
              end 
              where c0847_rowid_movto_activ_ot = ${actividad.c0846_rowid};
            `;
            this.db
              .queryExec(sql_update)
              .then(res => {
                // console.log(
                //   "Se actulizaron los movimientos de las tareas por el ultimo movimiento de actividad"
                // );
                // console.log(res);
                resolve(actividad);
              })
              .catch(error => {
                console.error(
                  "Error al actualizar los movimientos de las tareas"
                );
                console.error(error);
                reject(error);
              });
          }
        })
        .catch(error => {
          console.error("ERROR al actualizar los movimientos de tareas");
          console.error(error);
          reject(error);
        });
    });
  }

  cargarTareas(actividad) {
    return new Promise((resolve, reject) => {
      let sql = `
     select 
				c0847_rowid,
        c0847_id_cia,
        c0847_rowid_movto_activ_ot,
        c0847_orden_tarea,
        c0847_descripcion_tarea,
        c0847_notas_tarea,
        c0847_ind_ejecucion,
        c0847_observaciones,
        c0847_fecha_creacion,
        c0847_usuario_creacion,
        c0847_fecha_actualizacion,
        c0847_usuario_actualizacion
      
      from w0847_movto_actividades_tareas 
      where c0847_rowid_movto_activ_ot = ${actividad.c0846_rowid}
    `;

      if (!actividad.c0846_ind_estado && actividad.c0846_ind_estado != 1) {
        // console.log("se entro a alterar el sql");
        sql = `
     select 
				c0848_rowid,
        c0848_rowid_actividad,
        c0848_orden,
        c0848_descripcion c0847_descripcion_tarea,
        c0848_notas,
        c0848_fecha_creacion,
        c0848_usuario_creacion,
        c0848_fecha_actualizacion,
        c0848_usuario_actualizacion,
        0 c0847_ind_ejecucion
      
      from w0848_actividades_plan_tareas 
      where c0848_rowid_actividad = ${actividad.c0822_rowid_actividad}
    `;
      }

      this.db
        .query(sql)
        .then(res => {
          // console.log("Se consultan las tareas 2s");
          // console.log(res);
          actividad.tareas = res;
          resolve(res);
          // this.actividadSel.tareas = res;
          // this.cargaDatos(actividad.id);
        })
        .catch(error => {
          console.error("ERROR ha ocurrido un error al consultar las tareas");
          console.error(error);
          reject(error);
        });
    });
  }

  public eliminarActividaReasignada(actividad) {
    return new Promise((resolve, reject) => {
      let sql_delete_tareas = `delete from w0847_movto_actividades_tareas 
        where c0847_rowid in 
          (
            select
             c0847_rowid 
            from w0847_movto_actividades_tareas w0847
            inner join w0846_movto_actividades_ot w0846 on w0846.c0846_rowid = w0847.c0847_rowid_movto_activ_ot
            inner join w0822_actividades_ot w0822 on w0846.c0846_rowid_actividad_ot = w0822.c0822_rowid
            where w0822.c0822_rowid = ${actividad.c0822_rowid}
          )`;
      let sql_delete_motivos_paros = `delete from w0836_motivos_paro_ot
      where c0836_rowid in (
          select
           c0836_rowid
          from w0836_motivos_paro_ot w0836
          inner join w0822_actividades_ot w0822 on w0822.c0822_rowid = w0836.c0836_rowid_actividad_ot
          where w0822.c0822_rowid = ${actividad.c0822_rowid}
        ) `;
      let sql_delete_movto_actividad = ` 
      delete from w0846_movto_actividades_ot 
      where c0846_rowid_actividad_ot = ${actividad.c0822_rowid} `;
      let sql_delete_actividad = ` 
      delete from w0822_actividades_ot where c0822_rowid = ${
        actividad.c0822_rowid
      } `;

      // console.log("actividad: ");
      // console.log(actividad);
      this.db
        .query(sql_delete_tareas)
        .then(res => {
          // console.log("SQL delete tareas");
          // console.log(sql_delete_tareas);
          // console.log("-------SE eliminaron las tareas correctamente");
          this.db
            .query(sql_delete_motivos_paros)
            .then(res => {
              // console.log("SQL delete motivos paros");
              // console.log(sql_delete_motivos_paros);
              // console.log("---------SE eliminaron los motivos paro correctamente");
              this.db
                .query(sql_delete_movto_actividad)
                .then(res => {
                  // console.log("sql_delete_movto_actividad");
                  // console.log(sql_delete_movto_actividad);
                  // console.log("-------Se elimino satisfactoriamente los movimientos");

                  this.db
                    .query(sql_delete_actividad)
                    .then(res => {
                      // console.log("SQL delete actividad");
                      // console.log(sql_delete_actividad);
                      // console.log("-------Se elimino satisfactoriamente");
                      resolve(res);
                    })
                    .catch(error => {
                      console.error(
                        "Ocurrio un error al intentar elimiar la actividad reasignada"
                      );
                      console.error(error);
                      reject(error);
                    });
                })
                .catch(error => {
                  console.error(
                    "Ocurrio un error al intentar elimiar los movimientos de la actividad reasignada"
                  );
                  console.error(error);
                  reject(error);
                });
            })
            .catch(error => {
              console.error(
                "Ocurrio un Error al intentar eliminar los motivos de paro relacionados con la actividad"
              );
              console.error(error);
              reject(error);
            });
        })
        .catch(error => {
          console.error(
            "Ocurrio un Error al intentar eliminar las tareas relacionadas con la actividad"
          );
          console.error(error);
          reject(error);
        });
    });
  }

  public actualizarIndicadorEnviado(actividad) {
    let sql = `
    update w0822_actividades_ot
    set c0822_ind_enviado = 0
    where c0822_rowid = ${actividad.id}
    and c0822_ind_enviado != 1
    `;
    actividad.c0822_ind_enviado = 0;
    this.db
      .query(sql)
      .then(res => {
        // console.log("se actualizo el indicador enviado correctamente");
      })
      .catch(error => {
        console.error(
          "Error al actualizar el indicador enviado de la actividad"
        );
        console.error(error);
      });
  }

  /**
   * Carga el movimiento de una actividad si esta tiene movimientos
   * @param actividad_id
   */
  public cargarMovimientoActividad(actividad_id) {
    return new Promise((resolve, reject) => {
      let sql = `
      select 
                c0846_rowid,
                c0846_id_cia,
                c0846_rowid_docto_ot c0822_rowid_docto_ot,
                c0846_rowid_actividad_ot id,
                c0846_fecha_ejecucion,
                c0846_hora_ini,
                c0846_hora_ini c0822_fecha_ejecucion_ini,
                c0846_hora_fin,
                c0846_total_horas,
                c0846_ind_estado,
                c0846_dias_paro_real_eqp,
                c0846_horas_paro_real_eqp,
                c0846_horas_paro_act,
                c0846_ultima_lectura lecturas,
                c0846_lectura_ant,
                c0846_turno,
                c0846_rowid_responsable,
                c0846_rowid_ayudante1,
                c0846_rowid_ayudante2,
                c0846_rowid_ayudante3,
                c0846_rowid_ayudante4,
                c0846_rowid_ayudante5,
                c0846_tarifa_responsable,
                c0846_tarifa_ayudante1,
                c0846_tarifa_ayudante2,
                c0846_tarifa_ayudante3,
                c0846_tarifa_ayudante4,
                c0846_tarifa_ayudante5,
                c0846_costo_mo,
                c0846_ind_paro_equipo,
                c0846_ind_paro_otros paroEnCadena,
                c0846_ind_dano c0819_ind_dano,
                c0846_ind_tipo_dano c0819_ind_tipo_dano,
                c0846_detalle_dano c0819_detalle_dano,
                c0846_rowid_causal_falla,
                c0846_rowid_tipo_falla,
                c0846_notas c0846_notas,
                max(c0846_fecha_creacion) c0846_fecha_creacion,
                c0846_usuario_creacion,
                c0846_fecha_actualizacion,
                c0846_usuario_actualizacion

            from w0846_movto_actividades_ot
            where 
            c0846_rowid_actividad_ot = ${actividad_id} 
            group by c0846_rowid_actividad_ot
            order by c0846_fecha_creacion asc
            
      `;
      // console.log("actividad.id consultada:" + actividad.id);
      this.db
        .query(sql)
        .then(res => resolve(res[0]))
        .catch(error => reject(error));
    });
  }
  public consultarActividadIniciada() {
    return new Promise((resolve, reject) => {
      let sql = ` 
              select 
                c0846_id_cia,
                c0846_rowid_docto_ot c0822_rowid_docto_ot,
                c0846_rowid_actividad_ot id,
                c0846_fecha_ejecucion,
                c0846_hora_ini c0822_fecha_ejecucion_ini,
                c0846_hora_fin,
                c0846_total_horas,
                c0846_ind_estado,
                c0846_dias_paro_real_eqp,
                c0846_horas_paro_real_eqp,
                c0846_horas_paro_act,
                c0846_ultima_lectura lecturas,
                c0846_lectura_ant,
                c0846_turno,
                c0846_rowid_responsable,
                c0846_rowid_ayudante1,
                c0846_rowid_ayudante2,
                c0846_rowid_ayudante3,
                c0846_rowid_ayudante4,
                c0846_rowid_ayudante5,
                c0846_tarifa_responsable,
                c0846_tarifa_ayudante1,
                c0846_tarifa_ayudante2,
                c0846_tarifa_ayudante3,
                c0846_tarifa_ayudante4,
                c0846_tarifa_ayudante5,
                c0846_costo_mo,
                c0846_ind_paro_equipo,
                c0846_ind_paro_otros paroEnCadena,
                c0846_ind_dano c0819_ind_dano,
                c0846_ind_tipo_dano c0819_ind_tipo_dano,
                c0846_detalle_dano c0819_detalle_dano,
                c0846_rowid_causal_falla,
                c0846_rowid_tipo_falla,
                c0846_notas c0846_notas,
                c0846_fecha_creacion,
                c0846_usuario_creacion,
                c0846_fecha_actualizacion,
                c0846_usuario_actualizacion

            from w0846_movto_actividades_ot
            where 
             c0846_ind_estado = 1
             and c0846_rowid_responsable = ${global.usuario["f_tercero_rowid"]}
      `;

      // var movto_actividad = JSON.parse(JSON.stringify(this.actividad_vacia));
      this.db
        .query(sql)
        .then(res => (res[0] ? resolve(res[0]) : resolve(this.actividad_vacia)))
        .catch(error => reject(error));
    });
  }

  /**
   * actualizarMovimiento
   * se actualiza el movimiento para guardar todos los valores ingresados
   * de los detalle de la actividad
   * @param actividadSel actividad a la cual se le va  actulizar la informacion del movimiento
   * @param cerrar flag o bandera para determinar si se va a finalizar la actividad
   * @param estado estado que debe de quedar la actividad 1 corriendo, 2 terminada, 3 pausada
   */
  public actualizarMovimiento(actividadSel, cerrar = false, estado = 2) {
    return new Promise((resolve, reject) => {
      if (cerrar && estado == DETENIDA) {
        // console.warn("Entra a hacer las validaciones");
        let validacion = this.validacionesActividad(actividadSel);
        if (validacion != true) {
          // console.log(
          //   "No se termina la actividad por error en la validacion de los datos"
          // );
          reject(validacion);
          return;
        }
      }
      // cerrar
      //   ? console.log("Se va a finalizar la actividad")
      //   : console.log("se actualiza la actividad sin cerrar");
      let c0846_ind_paro_equipo =
        actividadSel.motivos.filter(motivo => motivo.c0835_ind_tipo == 0)
          .length > 0;
      let c0846_ind_estado = cerrar ? `c0846_ind_estado =${estado},` : "";
      let c0846_hora_fin = cerrar ? "datetime('now', 'localtime')" : "null";
      let hora_fin = cerrar ? "c0846_hora_fin =" + c0846_hora_fin + "," : "";

      // console.log("se va a actulizar el motiviento de la actividad");
      // console.log(actividadSel.motivos);

      let paro_equipo_real = this.horasParoReal(actividadSel.motivos, 0);
      let paro_actividad_real = this.horasParoReal(actividadSel.motivos, 1);

      let filtroPausa =
        estado > 1 ? `and c0846_rowid >= ${actividadSel.c0846_rowid}` : "";

      let sql = ` update w0846_movto_actividades_ot
                set
                ${hora_fin}
                c0846_total_horas=0,
                ${c0846_ind_estado}                
                c0846_dias_paro_real_eqp=${paro_equipo_real["paro_dias_real"]},
                c0846_horas_paro_real_eqp=${
                  paro_equipo_real["paro_horas_real"]
                },
                c0846_horas_paro_act=${paro_actividad_real["paro_horas_real"]},
                c0846_ultima_lectura=${
                  actividadSel.lecturas ? actividadSel.lecturas : 0
                },
                c0846_rowid_ayudante1=  ${
                  actividadSel.ayudantes[0]
                    ? actividadSel.ayudantes[0].c0827_rowid_tercero
                    : null
                },
                c0846_rowid_ayudante2= ${
                  actividadSel.ayudantes[1]
                    ? actividadSel.ayudantes[1].c0827_rowid_tercero
                    : null
                },
                c0846_rowid_ayudante3=${
                  actividadSel.ayudantes[2]
                    ? actividadSel.ayudantes[2].c0827_rowid_tercero
                    : null
                },
                c0846_rowid_ayudante4=${
                  actividadSel.ayudantes[3]
                    ? actividadSel.ayudantes[3].c0827_rowid_tercero
                    : null
                },
                c0846_rowid_ayudante5=${
                  actividadSel.ayudantes[4]
                    ? actividadSel.ayudantes[4].c0827_rowid_tercero
                    : null
                },
                c0846_ind_paro_equipo=${c0846_ind_paro_equipo ? 1 : 0},
                c0846_ind_paro_otros= ${actividadSel.paroEnCadena ? 1 : 0},
                c0846_ind_dano= ${actividadSel.c0819_ind_dano},
                c0846_ind_tipo_dano= ${actividadSel.c0819_ind_tipo_dano},
                c0846_detalle_dano='${actividadSel.c0819_detalle_dano}',
                c0846_notas='${global.actividadSel.c0846_notas}' ,
                c0846_fecha_actualizacion=datetime('now', 'localtime'),
                c0846_rowid_causal_falla=${
                  actividadSel.c0846_rowid_causal_falla
                },
                c0846_rowid_tipo_falla= ${actividadSel.c0846_rowid_tipo_falla}
                where c0846_rowid_actividad_ot = ${actividadSel.id}
                ${filtroPausa}
                `;

      // console.log("SQL:");
      // console.log(sql);
      this.db
        .queryExec(sql)
        .then(res => {
          this.actualizarIndicadorEnviado(actividadSel);
          this.guardarMotivosParo(actividadSel);
          this.actualizarMovimientosTareas(actividadSel);
          // console.log("4- Se ha actualizado la actividad");
          if (cerrar) {
            if (estado == PAUSADA) {
              actividadSel.c0846_ind_estado = PAUSADA;
            }

            actividadSel["c0846_hora_fin"] = new Date();
          }
          resolve(actividadSel);
        })
        .catch(error => {
          console.error("ERROR al intentar actualizar la actividad");
          console.error(error);
        });
    });
  }
  /**
   * Esta funcion retorna true cuando si pasa las validaciones
   * de lo contrario retorna un mensaje de error
   * @param actividad
   */
  public validacionesActividad(actividad) {
    // console.log("Validaciones actividad:");
    // console.log(actividad);
    // console.log("seguridades");
    // console.log(global.seguridad_mnto);
    // console.warn("actividad.c0808_ind_requiere_paro: " + actividad.c0808_ind_requiere_paro);
    // console.warn("actividad.c0819_ind_requiere_paro: " + actividad.c0819_ind_requiere_paro);
    // console.warn(
    //   "MNTOPREV_ORDENES_ACTUALIZACION_OT_ACTIVID_REQ_PARO: " +
    //     global.seguridad_mnto.MNTOPREV_ORDENES_ACTUALIZACION_OT_ACTIVID_REQ_PARO
    // );
    if (
      (actividad.c0808_ind_requiere_paro == 1 ||
        actividad.c0819_ind_requiere_paro == 1) &&
      global.seguridad_mnto
        .MNTOPREV_ORDENES_ACTUALIZACION_OT_ACTIVID_REQ_PARO == 1
    ) {
      if (
        actividad.motivos.filter(motivo => motivo.c0835_ind_tipo == 0).length ==
        0
      ) {
        return "se require al menos un motivo de paro de Equipo";
      }
    }

    if (
      actividad.c0819_ind_tipo_trabajo == CORRECTIVO &&
      global.seguridad_mnto.MNTOPREV_ORDENES_ACT_FALLA_TRAB_CORRECTIVOS == 1
    ) {
      // console.log("entro a validar porque es de tipo correctivo");
      // console.log(actividad);
      if (
        actividad.c0846_rowid_causal_falla == null ||
        actividad.c0846_rowid_causal_falla == "" ||
        actividad.c0846_rowid_causal_falla == 0
      ) {
        return "El campo Causal de falla es obligatorio";
      }
      if (
        actividad.c0846_rowid_tipo_falla == null ||
        actividad.c0846_rowid_tipo_falla == "" ||
        actividad.c0846_rowid_tipo_falla == 0
      ) {
        if (!isNaN(Number(actividad.c0822_id_equipo))) {
          return "El campo Tipo de falla es obligatorio";
        }
      }
    }

    if (this.validacion_motivos_paro_tiempos(actividad.motivos)) {
      return "En los motivos se require como minimo un dato de tiempo mayor a 0";
    }

    if (
      actividad.tareas.length > 0 &&
      !actividad.tareas.find(tarea => tarea.c0847_ind_ejecucion == true)
    ) {
      return "se require confirmar al menos una Tarea";
    }

    return true;
  }

  public validacion_motivos_paro_tiempos(motivos) {
    // console.log("motivos que se reciben para validar los tiempos");
    // console.log(motivos);
    let invalidos = motivos.filter(
      motivo =>
        motivo.c0836_rowid_motivo_paro &&
        (motivo.c0836_dias_paro == "" ||
          motivo.c0836_dias_paro == 0 ||
          motivo.c0836_dias_paro < 0) &&
        (motivo.c0836_horas_paro == "" ||
          motivo.c0836_horas_paro == 0 ||
          motivo.c0836_horas_paro < 0) &&
        (motivo.c0836_minutos_paro == "" ||
          motivo.c0836_minutos_paro == 0 ||
          motivo.c0836_minutos_paro < 0)
    );
    // console.log("invalidos:");
    // console.log(invalidos);
    return invalidos.length > 0;
  }

  public continuarTiempoIniciada(actividad_iniciada) {
    return new Promise((resolve, reject) => {
      // console.log("actividad_iniciada: ");
      // console.log(actividad_iniciada);
      if (
        (actividad_iniciada != null && actividad_iniciada.id != "") ||
        actividad_iniciada.c0846_ind_estado != 2
      ) {
        let timeInSeconds = 0;
        if (actividad_iniciada["c0822_fecha_ejecucion_ini"]) {
          let fecha_inicio = new Date(
            actividad_iniciada["c0822_fecha_ejecucion_ini"]
          );
          let diff = new Date().getTime() - fecha_inicio.getTime();
          timeInSeconds = diff / 1000;
        }
        this.calcularTiempoMovimientos(actividad_iniciada)
          .then((res: number) => {
            // console.log("respuesta de calcularTiempo Movimientos: " + res);
            actividad_iniciada.tiempo_movimientos = res;
            timeInSeconds += res;
            resolve(timeInSeconds);
            // this.timer.displayTime = this.getSecondsAsDigitalClock(this.timeInSeconds);
          })
          .catch(error => reject(error));
      } else {
        // console.log("La actividad es NUla o algo parecido");
        resolve(0);
      }
    });
  }

  horasParoReal(motivos, c0835_ind_tipo) {
    let paro_total_dias = 0;
    let paro_total_horas = 0;
    let paro_total_min = 0;

    motivos
      .filter(motivo => motivo.c0835_ind_tipo == c0835_ind_tipo)
      .forEach(motivo => {
        paro_total_dias += Number(motivo.c0836_dias_paro);
        paro_total_horas += Number(motivo.c0836_horas_paro);
        paro_total_min += Number(motivo.c0836_minutos_paro);
      });

    let paro_timepo_min =
      paro_total_dias * 24 * 60 + paro_total_horas * 60 + paro_total_min;
    let respuesta = {};
    let paro_dias_real = 0;
    let paro_horas_real = 0;
    if (c0835_ind_tipo == 0) {
      //paro equipo en dias y horas
      paro_dias_real = Math.floor(paro_timepo_min / 60 / 24);
      paro_horas_real = paro_timepo_min / 60 - paro_dias_real * 24;
      respuesta = {
        paro_dias_real: paro_dias_real,
        paro_horas_real: paro_horas_real
      };
    } else if (c0835_ind_tipo == 1) {
      //paro actividad en horas
      paro_horas_real = paro_timepo_min / 60;
      respuesta = { paro_horas_real: paro_horas_real };
    }

    return respuesta;
  }

  public cargarAyudantes(actividad) {
    if (
      actividad.c0846_rowid_ayudante1 != null &&
      actividad.c0846_rowid_ayudante1 != 0
    ) {
      actividad.ayudantes.push({
        c0827_rowid_tercero: actividad.c0846_rowid_ayudante1
      });
    }
    if (
      actividad.c0846_rowid_ayudante2 != null &&
      actividad.c0846_rowid_ayudante2 != 0
    ) {
      actividad.ayudantes.push({
        c0827_rowid_tercero: actividad.c0846_rowid_ayudante2
      });
    }
    if (
      actividad.c0846_rowid_ayudante3 != null &&
      actividad.c0846_rowid_ayudante3 != 0
    ) {
      actividad.ayudantes.push({
        c0827_rowid_tercero: actividad.c0846_rowid_ayudante3
      });
    }
    if (
      actividad.c0846_rowid_ayudante4 != null &&
      actividad.c0846_rowid_ayudante4 != 0
    ) {
      actividad.ayudantes.push({
        c0827_rowid_tercero: actividad.c0846_rowid_ayudante4
      });
    }
    if (
      actividad.c0846_rowid_ayudante5 != null &&
      actividad.c0846_rowid_ayudante5 != 0
    ) {
      actividad.ayudantes.push({
        c0827_rowid_tercero: actividad.c0846_rowid_ayudante5
      });
    }
    this.guardarActividadLocalStorge(actividad);
    return actividad.ayudantes;
  }

  cargaDatos(actividad_id) {
    return new Promise((resolve, reject) => {
      let actividad;
      // console.log("Se van a cargar la actividad: " + actividad_id);
      if (!actividad_id) {
        if (localStorage.getItem("actividadSel")) {
          actividad = JSON.parse(localStorage.getItem("actividadSel"));
          resolve(actividad);
        }
      } else {
        this.cargarDetallesActividad(actividad_id)
          .then(res => {
            let actividad = this.guardarActividadLocalStorge(res);
            resolve(actividad);
          })
          .catch(error => {
            console.error("Error al cargar Los detalles de la actividad");
            console.error(error);
            reject(error);
          });
      }
    });
  }

  public cargarErroresActividad(actividad) {
    return new Promise((resolve, reject) => {
      let sql = `
      select 
      c08222_error,
      c08222_error_detalle
      from w08222_errores
      where c08222_rowid_actividad_ot =  ${actividad.id}
      `;
      this.db
        .query(sql)
        .then(res => {
          actividad.habilitar_error = res.length == 0;
          resolve(actividad);
          this.guardarActividadLocalStorge();
        })
        .catch(error => reject(error));
    });
  }

  public cargarTeceros() {
    return new Promise((resolve, reject) => {
      let sql = `SELECT c0827_rowid_tercero,c0827_razon_social 
              FROM "w0827_operarios" w0827 
              where w0827.c0827_rowid_tercero != ${
                global.usuario["f_tercero_rowid"]
              }
                order by c0827_razon_social asc`;
      let terceros = [];
      this.db
        .query(sql)
        .then(respuesta => {
          terceros = respuesta;
          terceros.unshift({
            c0827_rowid_tercero: 0,
            c0827_razon_social: "Ninguno"
          });
          resolve(terceros);
        })
        .catch(error => {
          console.error(
            "Ocurrio un error al ejecutar la consulta de operarios"
          );
          console.error(error);
          reject(error);
        });
    });
  }

  public limpiarActividadLocalStorage() {
    localStorage.removeItem("actividadSel");
  }

  public consultarCamposDinamicosMantenimiento() {
    return new Promise((resolve, reject) => {
      // console.log("SE va a consultar los campos de mantenimiento");
      let p4 = { Parametros: "|", Consulta: "PARAMETROS_MANTENIMIENTO" };
      this.sync
        .postParameters(global.api.consulta, p4)
        .then((res: any) => {
          if (res["length"] > 0) {
            if (typeof res == "object") {
              let camposVer = res;
              let sql = `insert or replace into configuraciones(id,documento) values('PARAMETROS_MANTENIMIENTO','${JSON.stringify(
                camposVer
              )}')`;

              this.db
                .query(sql)
                .then(r => {
                  this.actualizarCamposSeleccionados(camposVer)
                    .then(res => resolve(r))
                    .catch(err => console.error(err));
                })
                .catch(error => {
                  console.error(error);
                  reject(
                    "ERROR al insertar los campos para visualizar en las actividades de mantenimiento"
                  );
                });
            } else {
              reject(res);
            }
          } else {
            resolve(false);
          }
        })
        .catch(e => reject(e));
    });
  }

  private actualizarCamposSeleccionados(camposVer) {
    return new Promise((resolve, reject) => {
      // actualizar los estilos de los campos ya seleccionados
      let sql_campos_seleccionados = `
                select
                  c08223_id
                from w08223_campos 
                where
                c08223_rowid_tercero = ${global.usuario["f_tercero_rowid"]}`;
      this.db
        .query(sql_campos_seleccionados)
        .then(res => {
          // console.log("CAMPOS seleccionados");
          // console.log(res);
          res.forEach(campo => {
            let sql_update = `update w08223_campos set c08223_class = `;
            let campo_found = camposVer.find(
              campover => campover.alias == campo.c08223_id
            );
            if (campo_found) {
              sql_update += `'${campo_found.class}' where c08223_id = '${
                campo_found.alias
              }'`;
              // console.log("SQL: " + sql_update);
              this.db
                .query(sql_update)
                .then(res => {
                  // console.log(
                  //   "ha sido actualizado el campo: " + campo_found.alias
                  // );
                })
                .catch(err => {
                  console.error("ERROR ACTUALIZANDO el campo");
                  console.error(err);
                });
            }
          });
          resolve(true);
        })
        .catch(error => {
          console.error(
            "Ha ocurrido un error al consultar los campos seleecionados "
          );
          console.error(error);
        });
    });
  }
}
