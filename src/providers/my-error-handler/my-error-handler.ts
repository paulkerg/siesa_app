import { ErrorHandler, Inject, Injector } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { APP_CONFIG } from '../../config/app.config';

export class AppConstants {
  public static get baseURL(): string { return APP_CONFIG.srvSincronizar; }
  public static get httpError(): string { return 'Ocurrio error de HTTP.'; }
  public static get typeError(): string { return 'Ocurrio un error.'; }
  public static get generalError(): string { return 'Ocurrio un error general.'; }
  public static get somethingHappened(): string { return 'Error no controlado!'; }
}

export class MyErrorHandlerProvider implements ErrorHandler {
  msgEror = {
    titulo: "",
    mensaje: ""
  }
  blnAlerta: boolean = false;

  constructor(
    @Inject(SplashScreen) public splashScreen: SplashScreen,
    @Inject(Injector) private injector: Injector
  ) { }

  async handleError(error) {
    console.warn("HandleError error:")
    console.warn(error)
    const message = error.message ? error.message : error.toString();
    const location = this.injector.get(LocationStrategy);
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    const date = new Date().toUTCString();

    console.log('message', message, 'location', location, 'url', url);
    console.log("location['_platformLocation']['hash']", location['_platformLocation']['hash']);

    this.blnAlerta = false;
    if (error instanceof HttpErrorResponse) {
      this.blnAlerta = true;
      this.msgEror.titulo = AppConstants.httpError;
      this.msgEror.mensaje = (<HttpErrorResponse>error).status + "-" + error.message;
      console.log(date, AppConstants.httpError, error.message, 'codigo:', (<HttpErrorResponse>error).status);
    }
    else if (error instanceof TypeError) {
      this.blnAlerta = true;
      this.msgEror.titulo = AppConstants.typeError;
      this.msgEror.mensaje = error.stack + "-" + error.message;
      console.log(date, AppConstants.typeError, error.message, error.stack);
    }
    else if (error instanceof Error) {
      this.msgEror.titulo = AppConstants.generalError;
      this.msgEror.mensaje = error.stack + "-" + error.message;
      console.log(date, AppConstants.generalError, error.message, error.stack);
    }
    else if (error instanceof ErrorEvent) {
      this.msgEror.titulo = AppConstants.generalError;
      this.msgEror.mensaje = error.message;
      console.log(date, AppConstants.generalError, error.message);
    }
    else {
      this.msgEror.titulo = AppConstants.somethingHappened;
      this.msgEror.mensaje = error.stack + "-" + error.message;
      console.log(date, AppConstants.somethingHappened, error.message, error.stack);
    }
  }
}

