import { Injectable } from '@angular/core';
import { pedidoEncabezado } from './../../models/interfaces/pedidoEncabezado';
import { parVendedor } from './../../models/interfaces/parametros';
import { cliente, h_mt } from './../../models/interfaces/cliente';

import * as moment from 'moment';
import { v4 as uuid } from 'uuid';
import { SqliteProvider } from '../sqlite/sqlite';
import { pedidoDetalle } from '../../models/interfaces/pedidoDetalle';

@Injectable()
export class PedidosProvider {
  constructor(private ldb: SqliteProvider) {
    moment.locale("es");
  }

  //--- Cargar el encabezado del Pedido
  cargarEncabezado(rowidPedido) {
    if (rowidPedido === 0) {
      pedidoEncabezado.f430_guid = uuid();
      pedidoEncabezado.f430_id_co = parVendedor.f2181_id_co;
      pedidoEncabezado.f430_id_tipo_docto = parVendedor.f2181_id_tipo_docto_pedido;
      pedidoEncabezado.f430_consec_docto = cliente.NroPed;
      pedidoEncabezado.f430_id_fecha = moment().format('YYYY-MM-DD');
      pedidoEncabezado.f430_rowid_tercero_fact = cliente.f200_rowid;
      pedidoEncabezado.f430_id_sucursal_fact = cliente.f201_id_sucursal;
      pedidoEncabezado.f430_rowid_punto_envio_rem = cliente.f215_rowid;
      pedidoEncabezado.f430_fecha_entrega = moment().format('YYYY-MM-DD');
      pedidoEncabezado.f430_id_cond_pago = cliente.f201_id_cond_pago;
      pedidoEncabezado.f430_id_moneda_docto = cliente.f201_id_moneda;
      pedidoEncabezado.f430_id_moneda_conv = h_mt.p_id_moneda_base;
      pedidoEncabezado.f430_ind_forma_conv = h_mt.p_ind_forma_conv_local;
      pedidoEncabezado.f430_tasa_conv = h_mt.p_tasa_base;
      pedidoEncabezado.f430_id_moneda_local = h_mt.p_id_moneda_local;
      pedidoEncabezado.f430_ind_forma_local = h_mt.p_ind_forma_conv_base;
      pedidoEncabezado.f430_tasa_local = h_mt.p_tasa_local;
      pedidoEncabezado.f430_id_tipo_cambio = h_mt.v_id_tipo_cambio_default;

      return Promise.resolve(pedidoEncabezado);
    } else {
      let sql = "Select * FROM t430_cm_pv_docto WHERE f430_rowid = " + rowidPedido;
      return this.ldb.queryExec(sql)
        .then(response => {
          let r = response.rows[0];
          if (response.rows.length > 0) {
            pedidoEncabezado.f430_ts = r.f430_ts;
            pedidoEncabezado.f430_rowid = r.f430_rowid;
            pedidoEncabezado.f430_guid = r.f430_guid;
            pedidoEncabezado.f430_id_co = r.f430_id_co;
            pedidoEncabezado.f430_id_tipo_docto = r.f430_id_tipo_docto;
            pedidoEncabezado.f430_consec_docto = r.f430_consec_docto;
            pedidoEncabezado.f430_id_fecha = r.f430_id_fecha;
            pedidoEncabezado.f430_ind_estado_int = r.f430_ind_estado_int;
            pedidoEncabezado.f430_ind_estado = r.f430_ind_estado;
            pedidoEncabezado.f430_ind_transmitido = r.f430_ind_transmitido;
            pedidoEncabezado.f430_rowid_tercero_fact = r.f430_rowid_tercero_fact;
            pedidoEncabezado.f430_id_sucursal_fact = r.f430_id_sucursal_fact;
            pedidoEncabezado.f430_rowid_punto_envio_rem = r.f430_rowid_punto_envio_rem;
            pedidoEncabezado.f430_fecha_entrega = r.f430_fecha_entrega;
            pedidoEncabezado.f430_num_dias_entrega = r.f430_num_dias_entrega;
            pedidoEncabezado.f430_num_docto_referencia = r.f430_num_docto_referencia;
            pedidoEncabezado.f430_referencia = r.f430_referencia;
            pedidoEncabezado.f430_id_cargue = r.f430_id_cargue;
            pedidoEncabezado.f430_ind_tasa = r.f430_ind_tasa;
            pedidoEncabezado.f430_id_cond_pago = r.f430_id_cond_pago;
            pedidoEncabezado.f430_id_moneda_docto = r.f430_id_moneda_docto;
            pedidoEncabezado.f430_id_moneda_conv = r.f430_id_moneda_conv;
            pedidoEncabezado.f430_ind_forma_conv = r.f430_ind_forma_conv;
            pedidoEncabezado.f430_tasa_conv = r.f430_tasa_conv;
            pedidoEncabezado.f430_id_moneda_local = r.f430_id_moneda_local;
            pedidoEncabezado.f430_ind_forma_local = r.f430_ind_forma_local;
            pedidoEncabezado.f430_tasa_local = r.f430_tasa_local;
            pedidoEncabezado.f430_id_tipo_cambio = r.f430_id_tipo_cambio;
            pedidoEncabezado.f430_ind_impresion = r.f430_ind_impresion;
            pedidoEncabezado.f430_nro_impresiones = r.f430_nro_impresiones;
            pedidoEncabezado.f430_fecha_ts_transmitido = r.f430_fecha_ts_transmitido;
            pedidoEncabezado.f430_notas = r.f430_notas;
            pedidoEncabezado.f430_tasa_dscto_global_cap = r.f430_tasa_dscto_global_cap;
            pedidoEncabezado.f430_total_bruto = r.f430_total_bruto;
            pedidoEncabezado.f430_total_dscto_linea = r.f430_total_dscto_linea;
            pedidoEncabezado.f430_total_dscto_global = r.f430_total_dscto_global;
            pedidoEncabezado.f430_total_impuestos = r.f430_total_impuestos;
            pedidoEncabezado.f430_neto = r.f430_neto;
          }
          return Promise.resolve(pedidoEncabezado);
        })
        .catch(error => {
          return Promise.reject(error)
        });
    }
  }

  //----Pagina pedidoEncabezadoRegreso... 
  pedidosconsulta(rowidPedido) {
    let sql = "Select * FROM t430_cm_pv_docto WHERE f430_rowid = " + rowidPedido;
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  pedidoEncabezadoRegreso(t430) {
    let nro_p: any;
    if (t430.rows.lenght > 0) {
      if (t430.f430_consec_docto != 0 || t430.f430_consec_docto != null) {
        nro_p = t430.f430_consec_docto;
      }
    } else {
      nro_p = "''";
    }
    let strSQL = "select f430_neto neto, f430_ind_transmitido, (select count(1) from t431_cm_pv_movto WHERE f431_rowid_pv_docto = f430_rowid) nro_reg_det "
      + " from t430_cm_pv_docto "
      + " WHERE f430_consec_docto  = "
      + nro_p
      ;
    return this.ldb.queryExec(strSQL)
      .then(response => {

        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  //Cargar Notas y Referencia
  cargarEncabezados(nro_doc) {
    let strSQL = 'SELECT * FROM t430_cm_pv_docto WHERE f430_consec_docto = ' + nro_doc;
    return this.ldb.queryExec(strSQL)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  //--- Cargar el detalle del Pedido
  cargarDetalle(rowidPedido) {
    let sql = ' SELECT t431_cm_pv_movto.*, t121_mc_items_extensiones.f121_descripcion_toda,'
      + ' t430_cm_pv_docto.f430_neto,f121_desc_ext1_detalle,f121_desc_ext2_detalle,f120_id,f120_referencia,'
      + 'f122_id_unidad,f122_factor,'
      + " (select Group_Concat(f432_tasa ,'|')"
      + " 	from t432_cm_pv_movto_dscto "
      + " 	where t432_cm_pv_movto_dscto.f432_rowid_pv_movto = t431_cm_pv_movto.f431_rowid"
      + " 	group by t432_cm_pv_movto_dscto.f432_rowid_pv_movto) f432_tasas,"
      + " (select Group_Concat(f432_vlr_uni ,'|')"
      + " 	from t432_cm_pv_movto_dscto "
      + " 	where t432_cm_pv_movto_dscto.f432_rowid_pv_movto = t431_cm_pv_movto.f431_rowid"
      + " 	group by t432_cm_pv_movto_dscto.f432_rowid_pv_movto) f432_vlr_unis"
      + " ,t121_mc_items_extensiones.f121_id_ext1_detalle,t121_mc_items_extensiones.f121_id_ext2_detalle"
      + ' FROM t431_cm_pv_movto '
      + ' INNER JOIN t430_cm_pv_docto ON t430_cm_pv_docto.f430_rowid = t431_cm_pv_movto.f431_rowid_pv_docto'
      + ' INNER JOIN t121_mc_items_extensiones ON t431_cm_pv_movto.f431_rowid_item_ext = t121_mc_items_extensiones.f121_rowid '
      + ' INNER JOIN t120_mc_items ON t121_mc_items_extensiones.f121_rowid_item = t120_mc_items.f120_rowid '
      + ' LEFT JOIN t122_mc_items_unidades AS t122UE ON t122UE.f122_rowid_item = t120_mc_items.f120_rowid'
      + '        AND t122UE.f122_id_unidad = t120_mc_items.f120_id_unidad_empaque'
      + ' WHERE f431_rowid_pv_docto =' + rowidPedido
      + ' ORDER BY t431_cm_pv_movto.f431_ts DESC';
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });

  }
  //Consulta Lista de Precio...
  consultaLP(Cliente_id, Sucur_id) {
    let sqlfull = "select * from t201_mm_clientes" +
      " where f201_rowid_tercero='" + Cliente_id + "' and f201_id_sucursal='" + Sucur_id + "'";
    return this.ldb.queryExec(sqlfull)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //------------Funcion Creditos
  getCon_Pago(f201_rowid_tercero, f201_id_sucursal, f201_id_cond_pago) {
    let strSQL = '';
    strSQL = 'select * from t208_mm_condiciones_pago';
    if (parVendedor.f5784_ind_cli_x_uni_neg == 1) {
      strSQL += '	WHERE f208_id IN ( SELECT f2012_id_cond_pago FROM t2012_mm_clientes_un '
        + " 	WHERE f2012_rowid_tercero = '" + f201_rowid_tercero + "' "
        + " 	AND f2012_id_sucursal = '" + f201_id_sucursal + "' "
        + " 	AND f2012_id_vendedor = '" + f201_id_cond_pago + "')"
    }
    /* let sql = "select f208_id ,f208_descripcion descripcion from t208_mm_condiciones_pago	WHERE f208_id IN ( SELECT f2012_id_cond_pago FROM t2012_mm_clientes_un " +
       "	WHERE f2012_rowid_tercero = '" + f201_rowid_tercero +
       "'  	AND f2012_id_sucursal = '" + f201_id_sucursal
       + "'  	AND f2012_id_vendedor = '" + f201_id_cond_pago + " ')";*/
    return this.ldb.queryExec(strSQL)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //pedido
  pedido(cliente_rowid_tercero, cliente_id_sucursal) {
    let sql = "SELECT * FROM t201_mm_clientes" +
      " WHERE	f201_rowid_tercero = '" + cliente_rowid_tercero + "'" +
      " AND f201_id_sucursal = '" + cliente_id_sucursal + "'";
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //--_Cargar pedido encabezado editar
  consultapedidoEncabezado() {
    let sql = "SELECT v.f2181_id_tipo_docto_pedido tipo_ped,"
      + " v.f2181_id_tipo_docto_recibo tipo_rec,"
      + " v.f2181_id_tipo_docto_fact tipo_fac,"
      + " ifnull(cpv.f022_cons_proximo, 0) cons_ped,"
      + " ifnull(crc.f022_cons_proximo, 0) cons_rec,"
      + " ifnull(cfv.f022_cons_proximo, 0) cons_fac,"
      + " 0 cons_chepos, v.f2181_id_co,"
      + " ifnull((SELECT f461_consec_docto FROM"
      + " t461_cm_docto_factura_venta  ORDER BY  f461_consec_docto DESC    LIMIT 1"
      + " ),0) f461_consec_docto FROM"
      + " t2181_sm_vend_siesamobile AS v"
      + " LEFT JOIN t022_mm_consecutivos AS cpv ON v.f2181_id_co = cpv.f022_id_co"
      + " AND v.f2181_id_tipo_docto_pedido = cpv.f022_id_tipo_docto"
      + " LEFT JOIN t022_mm_consecutivos AS cfv ON v.f2181_id_co = cfv.f022_id_co"
      + " AND v.f2181_id_tipo_docto_fact = cfv.f022_id_tipo_docto"
      + " LEFT JOIN t022_mm_consecutivos AS crc ON v.f2181_id_co = crc.f022_id_co"
      + " AND v.f2181_id_tipo_docto_recibo = crc.f022_id_tipo_docto"
    console.log(sql);
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //--- Cargar el detalle del Pedido
  guardarPedidoEncabezado(item) {
    console.log(item)
    let sql = '';
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //Insertar tabla t430
  pedidosCreados(r, notas, referencia) {
    let sqlScript = '';
    let campoUpdate = "f430_notas = ''";
    console.log("r... ", r)
    /*if(r.f430_ind_transmitido==0 && r.f430_ind_estado_int != 2){
      
      this.borraItemDetalle(r.f430_rowid);
    }*/
    if (notas != '') {
      campoUpdate = "f430_notas ='" + notas + "'";
    } else if (referencia != '') {
      campoUpdate = "f430_referencia = substr('" + referencia + "', 1, 10)";
    }
    if (r.f430_rowid != 0 && r.f430_rowid != null) {
      sqlScript = 'UPDATE t430_cm_pv_docto SET ' + campoUpdate
        + " WHERE f430_rowid = " + r.f430_rowid;
    } else if (r.f430_rowid == null) {
      sqlScript = 'INSERT INTO t430_cm_pv_docto VALUES ("'
        + r.f430_ts + '"'
        + ',null'
        + ",'" + r.f430_guid + "'"
        + ",'" + r.f430_id_co + "'"
        + ",'" + r.f430_id_tipo_docto + "'"
        + "," + r.f430_consec_docto + ""
        + ",'" + r.f430_id_fecha + "'"
        + ",'" + r.f430_ind_estado + "'"
        + ",'" + r.f430_ind_estado_int + "'"
        + ",'" + r.f430_ind_transmitido + "'"
        + "," + r.f430_rowid_tercero_fact + ""
        + ",'" + r.f430_id_sucursal_fact + "'"
        + "," + r.f430_rowid_punto_envio_rem + ""
        + ",'" + r.f430_fecha_entrega + "'"
        + "," + r.f430_num_dias_entrega + ""
        + ",'" + r.f430_num_docto_referencia + "'"
        + ",substr('" + referencia.substring(0, 10) + "', 1, 10)"
        + ",'" + r.f430_id_cargue + "'"
        + "," + r.f430_ind_tasa + ""
        + ",'" + r.f430_id_cond_pago + "'"
        + ",'" + r.f430_id_moneda_docto + "'"
        + ",'" + r.f430_id_moneda_conv + "'"
        + ",'" + r.f430_ind_forma_conv + "'"
        + "," + r.f430_ind_impresion + ""
        + ",'" + r.f430_tasa_conv + "'"
        + ",'" + r.f430_id_moneda_local + "'"
        + ",'" + r.f430_ind_forma_local + "'"
        + ",'" + r.f430_tasa_local + "'"
        + ",'" + r.f430_id_tipo_cambio + "'"
        + "," + r.f430_nro_impresiones + ""
        + ",'" + r.f430_fecha_ts_transmitido + "'"
        + ",'" + notas.substring(0, 200) + "'"
        + "," + r.f430_tasa_dscto_global_cap + ""
        + "," + r.f430_total_bruto + ""
        + "," + r.f430_total_dscto_linea + ""
        + "," + r.f430_total_dscto_global + ""
        + "," + r.f430_total_impuestos + ""
        + "," + r.f430_neto + ""
        + ")"
        ;
    }
    return this.ldb.queryExec(sqlScript)
      .then(response => {
        this.updateConsecutivos();
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });

  }

  insertT431(item) {
    let fects = moment().format('YYYY-MM-DD');
    let vlr_bruto_parc = (pedidoDetalle.f431_precio_unitario_base * pedidoDetalle.f431_cant1_pedida);
    if (pedidoDetalle.f431_ind_impuesto_precio_venta == 1) {
      vlr_bruto_parc = pedidoDetalle.f431_vlr_bruto;
    }
    let sqlScript = 'INSERT INTO t431_cm_pv_movto VALUES ("'
      + fects + '"' + ',null' + ","
      + pedidoEncabezado.f430_rowid + "" + "," + "376" + "" + ","
      + cliente.rowid_bodega + "" + "," + pedidoDetalle.f431_id_concepto + ""
      + ",'" + pedidoDetalle.f431_id_motivo + "'" + "," + pedidoDetalle.f431_ind_obsequio
      + "" + ",'" + pedidoDetalle.f431_id_lista_precio + "'" + ",'"
      + pedidoEncabezado.f430_fecha_entrega + "'" + ","
      + pedidoDetalle.f431_ind_impuesto_precio_venta + "" + ","
      + pedidoDetalle.f431_precio_unitario_base + "" + ",'"
      + pedidoDetalle.f431_id_unidad_medida_captura + "'" + "," + pedidoDetalle.f431_factor
      + "" + "," + pedidoDetalle.f431_cant1_pedida + "" + ","
      + pedidoDetalle.f431_cant2_pedida + "" + ",'" + pedidoDetalle.f431_notas + "'" + ","
      + vlr_bruto_parc + "" + "," + pedidoDetalle.f431_vlr_dscto_linea + ""
      + "," + pedidoDetalle.f431_vlr_dscto_global + "" + "," + pedidoDetalle.f431_vlr_imp
      + "" + "," + pedidoDetalle.f431_vlr_imp_no_apli + "" + ","
      + pedidoDetalle.f431_vlr_neto + "" + "," + pedidoDetalle.f431_vlr_imp_margen + ""
      + ",'" + pedidoDetalle.f431_id_unidad_medida_captura + "'," + pedidoDetalle.f431_rowid_ccosto_movto
      + ",'" + pedidoDetalle.f431_id_un_movto + "','" + pedidoDetalle.f431_id_co_movto + "'"
      + ")";
    return this.ldb.queryExec(sqlScript)
      .then(response => {
        this.retornaRowidMvto();
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //Actualizar consecutivos..
  updateConsecutivos() {
    let sqlScript
    let nro = pedidoEncabezado.f430_consec_docto;
    nro++;
    sqlScript = 'UPDATE t022_mm_consecutivos SET f022_cons_proximo = ' + nro
      + ' WHERE f022_id_co = "' + pedidoEncabezado.f430_id_co + '"'
      + ' AND f022_id_tipo_docto = "' + pedidoEncabezado.f430_id_tipo_docto + '"'
      ;
    return this.ldb.queryExec(sqlScript)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  };

  //Actualizar  Totales.
  /*updateTotales() {
    let sql = 'UPDATE t430_cm_pv_docto SET '
      + ' f430_neto = (select sum(f431_vlr_neto) from t431_cm_pv_movto where f431_rowid_pv_docto = ' + pedidoEncabezado.f430_rowid + ')'
      + ' ,f430_total_bruto = (select sum((f431_vlr_bruto - f431_vlr_imp_no_apli)) from t431_cm_pv_movto where f431_rowid_pv_docto = ' + pedidoEncabezado.f430_rowid + ')'
      + ' ,f430_total_dscto_linea = (select sum(f431_vlr_dscto_linea) from t431_cm_pv_movto where f431_rowid_pv_docto = ' + pedidoEncabezado.f430_rowid + ')'
      + ' ,f430_total_impuestos = (select sum(f431_vlr_imp) from t431_cm_pv_movto where f431_rowid_pv_docto = ' + pedidoEncabezado.f430_rowid + ')'
      + ' WHERE f430_rowid = ' + pedidoEncabezado.f430_rowid
      ;
    //	console.log(sql);
  };*/

  //---- Funciones para eliminar pedidos. 
  //borrar pedido detalle
  borraItemDetalle = function (rowidItem) {
    let strSQL='';
    if (confirm("Eliminar Registro?")) {
     strSQL = 'SELECT f432_rowid_pv_movto,f432_rowid_pv_movto_obsequio FROM t432_cm_pv_movto_dscto WHERE f432_rowid_pv_movto = "'
        + rowidItem + '"';
      //		 console.log(strSQL);
    }
    return this.ldb.queryExec(strSQL)
      .then(response => {
        this.recargaRowidMvto(response.rows);
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  };

  //borrar pedido Ok
  borraItemDetalleOK = function (tx, results) {
    let len = results.rows.length;
    let arrayQuery = [];
    let pos = 0;
    let sqlScript = '';
    if (len > 0) {
      for (let i = 0; i < len; i++) {
        let r = results.rows.item(i);
        sqlScript = 'DELETE FROM t431_cm_pv_movto WHERE f431_rowid = ' + r.f432_rowid_pv_movto;
        arrayQuery[pos++] = sqlScript;
        sqlScript = 'DELETE FROM t431_cm_pv_movto WHERE f431_rowid = ' + r.f432_rowid_pv_movto_obsequio;
        arrayQuery[pos++] = sqlScript;
        sqlScript = 'DELETE FROM t432_cm_pv_movto_dscto WHERE f432_rowid_pv_movto = ' + r.f432_rowid_pv_movto;
        arrayQuery[pos++] = sqlScript;
        sqlScript = 'DELETE FROM t433_cm_pv_movto_imp   WHERE f433_rowid_pv_movto = ' + r.f432_rowid_pv_movto;
        arrayQuery[pos++] = sqlScript;
        sqlScript = 'DELETE FROM t433_cm_pv_movto_imp   WHERE f433_rowid_pv_movto = ' + r.f432_rowid_pv_movto_obsequio;
        arrayQuery[pos++] = sqlScript;
        sqlScript = 'DELETE FROM t430_cm_pv_docto WHERE f430_rowid = "' + pedidoEncabezado.f430_rowid + '"';
        arrayQuery[pos++] = sqlScript;
      }
    }
    if (pedidoEncabezado.f430_rowid != undefined) {
      sqlScript = 'DELETE FROM t431_cm_pv_movto where f431_rowid = '
        + ' (select f432_rowid_pv_movto_obsequio from t432_cm_pv_movto_dscto '
        + ' WHERE EXISTS (select 1 from t431_cm_pv_movto '
        + ' WHERE f431_rowid  = f432_rowid_pv_movto_obsequio'
        + ' and f432_rowid_pv_movto = ' + pedidoEncabezado.f430_rowid + '))';
      arrayQuery[pos++] = sqlScript;
      sqlScript = 'DELETE FROM t431_cm_pv_movto WHERE f431_rowid = ' + pedidoEncabezado.f430_rowid;
      arrayQuery[pos++] = sqlScript;
      sqlScript = 'DELETE FROM t433_cm_pv_movto_imp WHERE f433_rowid_pv_movto = ' + pedidoEncabezado.f430_rowid;
      arrayQuery[pos++] = sqlScript;
      sqlScript = 'DELETE FROM t430_cm_pv_docto WHERE f430_rowid = "' + pedidoEncabezado.f430_rowid + '"';
      arrayQuery[pos++] = sqlScript;
    }
    /* if (pedidoEncabezado.dataHandlerLocal == '' || pedidoEncabezado.dataHandlerLocal == N) {
       siesaDB.executeSqlBatch(arrayQuery, pedidoEncabezado.actualizaEstadoPed);
     } else {
       siesaDB.executeSqlBatch(arrayQuery, pedidoEncabezado.dataHandlerLocal);
     }*/
  };

  //--- Borrar el items del Pedido
  borrarPedidoItem(item) {

    let arrayQuery: any = [];
    let pos: number = 0;
    let sql = 'SELECT f432_rowid_pv_movto,f432_rowid_pv_movto_obsequio FROM t432_cm_pv_movto_dscto WHERE f432_rowid_pv_movto = "'
      + item + '"';
    return this.ldb.queryExec(sql)
      .then(response => {
        if (response.rows.length) {
          for (let t = 0; t < response.rows.length; t++) {
            let r = response.rows[t];
            arrayQuery[pos++] = 'DELETE FROM t431_cm_pv_movto WHERE f431_rowid = ' + r.f432_rowid_pv_movto;
            arrayQuery[pos++] = 'DELETE FROM t431_cm_pv_movto WHERE f431_rowid = ' + r.f432_rowid_pv_movto_obsequio;
            arrayQuery[pos++] = 'DELETE FROM t432_cm_pv_movto_dscto WHERE f432_rowid_pv_movto = ' + r.f432_rowid_pv_movto;
            arrayQuery[pos++] = 'DELETE FROM t433_cm_pv_movto_imp   WHERE f433_rowid_pv_movto = ' + r.f432_rowid_pv_movto;
            arrayQuery[pos++] = 'DELETE FROM t433_cm_pv_movto_imp   WHERE f433_rowid_pv_movto = ' + r.f432_rowid_pv_movto_obsequio;
          }
        } else {
          arrayQuery[pos++] = 'DELETE FROM t431_cm_pv_movto where f431_rowid = '
            + ' (select f432_rowid_pv_movto_obsequio from t432_cm_pv_movto_dscto '
            + ' WHERE EXISTS (select 1 from t431_cm_pv_movto '
            + ' WHERE f431_rowid  = f432_rowid_pv_movto_obsequio'
            + ' and f432_rowid_pv_movto = ' + item + '))';
          arrayQuery[pos++] = 'DELETE FROM t431_cm_pv_movto WHERE f431_rowid = ' + item;
          arrayQuery[pos++] = 'DELETE FROM t433_cm_pv_movto_imp WHERE f433_rowid_pv_movto = ' + item;
        }
        return this.ldb.queryAll(arrayQuery)
          .then(response => {
            return Promise.resolve(response);
          })
          .catch(error => {
            return Promise.reject(error)
          });
      })
      .catch(error => {
        return Promise.reject(false)
      });
  }

   //------------------------Funcion  para Eliminar item
   getDeletePedido(item) {
    let sql;
    sql = "DELETE FROM t431_cm_pv_movto WHERE f431_rowid =" + item;

    //console.log(sql);
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  
  //Inicio consulta descuentos... 
  retornaRowidMvto() {
    let sqlScript = 'select f431_rowid,f431_vlr_bruto from t431_cm_pv_movto'
      + ' ORDER BY f431_rowid DESC LIMIT 1';
    return this.ldb.queryExec(sqlScript)
      .then(response => {
        this.recargaRowidMvto(response.rows);
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  };

  recargaRowidMvto(results) {
    pedidoDetalle.f431_rowid = results[0].f431_rowid;
    pedidoEncabezado.f430_rowid = pedidoDetalle.f431_rowid;
    pedidoEncabezado.f430_neto = results[0].f431_vlr_bruto;
    // --- Si el obsequi es manual no se pasa por la rutina de descuentos
    this.armaDescuentos();

  };

  armaDescuentos = function () {
    let sqlScript = 'select f_rowid,f_orden,f_porcentaje_dscto,f_valor_dscto,f_rowid_promo_dscto_linea,f_fecha_inicial,f_fecha_final,'
      + ' f_rowid_item_ext_obsequio,f_id_concepto_obsequio,f_id_motivo_obsequio,f_id_um_obsequio,f_rowid_bodega_obsequio,'
      + ' ifnull(f_cant1_obsequio,0) f_cant1_obsequio, ifnull(f_cant2_obsequio,0) f_cant2_obsequio, f_cant_base_obsequio,'
      + pedidoEncabezado.f430_rowid
      + ' rowid_movto, '
      + " (select f431_cant1_pedida from t431_cm_pv_movto WHERE f431_rowid = " + pedidoEncabezado.f430_rowid + ") f431_cant1_pedida"
      + ' from tp1_generico_hallar_dsctos_vta' + ' order by f_orden asc';
    //siesaDB.executeSql(sqlScript, itemsEncabezado.insertarDescuentos);
    return this.ldb.queryExec(sqlScript)
      .then(response => {
        this.insertarDescuentos(response);
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });

  };

  //Fin consulta descuentos.
  UnidadesItem(sql) {
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  
  //---------------------Actualizar Descripciones
  updateDescripciones(update, value, rowid, idsuc) {
    let campoUpdate: any;
    let sqlScript: any;
    if (value == 'Bodega') {
      sqlScript = " UPDATE t2181_sm_vend_siesamobile SET f2181_rowid_bodega_pedido='" + update.F150_ROWID
        + "' WHERE f2181_id_vendedor = '" + rowid + "'"

    } else if (value == 'Lpre') {

      campoUpdate = " f201_id_lista_precio = '" + update.Id + "'";
      sqlScript = 'UPDATE t201_mm_clientes SET ' + campoUpdate
        + " WHERE f201_rowid_tercero = " + rowid + " and f201_id_sucursal = '" + idsuc + "'";

    } else if (value == 'C.Pago') {
      campoUpdate = " f201_id_cond_pago = '" + update.F150_ROWID + " '";
      sqlScript = 'UPDATE t201_mm_clientes SET ' + campoUpdate
        + " WHERE f201_rowid_tercero = " + rowid + " and f201_id_sucursal = '" + idsuc + "'";

    } else if (value == 'P.Envio') {
      sqlScript = "UPDATE t215_mm_puntos_envio_cliente SET f215_rowid = '" + update.f215_rowid + "'"
        + " WHERE f215_rowid_tercero = " + rowid + " and f215_id_sucursal = '" + idsuc + " '";
    }
    return this.ldb.queryExec(sqlScript)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //---------------------Funcion Descripciones para editar 
  getDescripciones(cond_pago, rowid_punto_envio, lista_precio, rowidbodega) {
    let sql = "select  "
      + " IFNULL((SELECT f208_descripcion FROM t208_mm_condiciones_pago WHERE f208_id = '"
      + cond_pago + "'),'') desc_cpago, "
      + " IFNULL((SELECT f150_descripcion FROM t150_mc_bodegas WHERE f150_rowid = '"
      + rowidbodega + "'),'') desc_bodega, "
      + " IFNULL(("
      + " SELECT f215_descripcion   || f015_direccion1 ||  f013_descripcion "
      + " FROM t215_mm_puntos_envio_cliente "
      + " INNER JOIN t015_mm_contactos ON f215_rowid_contacto = f015_rowid"
      + " INNER JOIN t013_mm_ciudades ON f015_id_pais = f013_id_pais "
      + " AND f015_id_depto = f013_id_depto "
      + " AND f015_id_ciudad = f013_id"
      + " WHERE f215_rowid = '"
      + rowid_punto_envio + "'),'') desc_penv, "
      + " IFNULL((SELECT f112_descripcion FROM t112_mc_listas_precios   WHERE f112_id = '"
      + lista_precio + "'),'') desc_lpre ";
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  //----------------------Funcion Descuentos.....
  getDescuentos(rowIdItem) {
    let sql = 'SELECT f432_orden,f432_tasa,f432_vlr_uni,f432_fecha_inicial_promo,f432_fecha_final_promo,f110_id,f110_descripcion,'
      + " CASE WHEN f121_id_ext1_detalle IS NULL THEN f120_descripcion"
      + " ELSE rtrim(f121_id_ext1_detalle) + ifnull(' - ' + rtrim(f121_id_ext2_detalle), '') + '  ' + rtrim(f120_descripcion) END f121_ext_detalle, "
      + " f432_id_um_obsequio,f120_id_unidad_adicional,f432_cant2_obsequio,f432_id_motivo_obsequio,f432_ind_tipo_dscto,"
      + " f432_rowid_item_ext_obsequio,f432_rowid_pv_movto_obsequio,f432_rowid_promo_dscto_linea,f432_id_concepto_obsequio,"
      + " ifnull(f110_ind_control_manual, 1) f110_ind_control_manual,"
      + " f431_cant1_pedida,f431_vlr_bruto,f431_vlr_dscto_linea,f431_vlr_dscto_global,f431_vlr_imp,f431_vlr_neto,"
      + " f431_ind_impuesto_precio_venta,f431_precio_unitario_base"
      + " FROM	t432_cm_pv_movto_dscto"
      + " INNER JOIN t431_cm_pv_movto ON t432_cm_pv_movto_dscto.f432_rowid_pv_movto = t431_cm_pv_movto.f431_rowid"
      + " LEFT JOIN t111_mc_promo_dsctos_linea ON f111_rowid = f432_rowid_promo_dscto_linea"
      + " LEFT JOIN t110_mc_promo_dsctos ON f110_rowid = f111_rowid_promo_dscto"
      + " LEFT JOIN t121_mc_items_extensiones ON f121_rowid = f432_rowid_item_ext_obsequio"
      + " LEFT JOIN t120_mc_items ON f120_rowid = f121_rowid_item"
      + ' WHERE f432_rowid_pv_movto=' + rowIdItem;
    //console.log(sql);
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //-------------------------------Consulta de pedido seleccionado  que tiene el vendedor 
  getPedidoCliente(rowid, id_sucursal) {
    let filtro = " WHERE	f430_rowid = " + rowid
      + " AND f430_id_sucursal_fact = '" + id_sucursal + "' "

    let sql = "Select t201_mm_clientes.rowid , t201_mm_clientes.f201_rowid_tercero,t201_mm_clientes.f201_id_lista_precio,"
      + " t201_mm_clientes.f201_id_sucursal cliente_id_suc, t200.f200_razon_social cliente_nombre_est,"
      + " f430_rowid,f430_id_tipo_docto tipo_doc,f430_consec_docto,f430_ind_estado_int ,1 f430_ind_transmitido, f430_rowid_tercero_fact,"
      + " f430_id_sucursal_fact,f201_descripcion_sucursal, ifnull(sum(f430_neto), 0) total_neto_pedido, f430_id_fecha fecha,f430_fecha_entrega fechaentrega,f430_ts fechahora,"
      + " f430_id_cond_pago,f430_rowid_punto_envio_rem"
      + " FROM t430_cm_pv_docto INNER JOIN t201_mm_clientes ON t430_cm_pv_docto.f430_rowid_tercero_fact = t201_mm_clientes.f201_rowid_tercero"
      + " AND t430_cm_pv_docto.f430_id_sucursal_fact = t201_mm_clientes.f201_id_sucursal "
      + " INNER JOIN t200_mm_terceros AS t200 ON t201_mm_clientes.f201_rowid_tercero = t200.f200_rowid"
      + filtro
      + " GROUP BY f430_consec_docto";

    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //Consulta de pedidos
  consultarPedidos(rowid, idsuc) {
    let sql = "SELECT f430_rowid,	f430_ts,f430_ind_estado_int,f430_ind_transmitido,	f430_consec_docto,f430_neto,f430_id_tipo_docto" +
      " from t430_cm_pv_docto WHERE f430_rowid_tercero_fact = '" + rowid + "' AND f430_id_sucursal_fact = '" + idsuc + "'  GROUP BY f430_consec_docto";
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  getpedidoTotales(rowid) {
    let sql = "SELECT t201.f201_descripcion_sucursal, t430.f430_rowid, " +
      " t430.f430_id_tipo_docto,t430.f430_consec_docto,t430.f430_id_fecha, " +
      " t430.f430_tasa_dscto_global_cap,t430.f430_total_bruto,t430.f430_total_dscto_linea, " +
      " t430.f430_total_dscto_global,t430.f430_total_impuestos,t430.f430_neto" +
      " FROM	t430_cm_pv_docto AS t430" +
      " INNER JOIN t201_mm_clientes AS t201 ON t430.f430_rowid_tercero_fact = t201.f201_rowid_tercero" +
      " AND t430.f430_id_sucursal_fact = t201.f201_id_sucursal" +
      " WHERE	t430.f430_rowid =" + rowid;
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //---------------- Funcion Bodegas
  getBodegas() {
    let sql = 'SELECT F150_ROWID, F150_DESCRIPCION descripcion FROM t150_mc_bodegas ';
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  //---------------- Funcion Condicion de pago

  getlista_precios() {
    let sql = ' SELECT t112_2.f112_descripcion descripcion, t112_2.f112_id Id'
      + ' FROM t112_mc_listas_precios AS t112_2'
      + ' WHERE EXISTS (select 1 from t5786_sm_perfil_param_listasp where f5786_id_lista_precio = f112_id)';
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //--------- Funcion Pedido envio
  getP_envio(rowid, id_suc) {

    let sql = 'select f215_rowid,f215_id,f215_id_sucursal,f215_rowid_tercero,'
      + " (f215_descripcion || f015_direccion1  || f013_descripcion || f215_descripcion) descripcion"
      + ' from t215_mm_puntos_envio_cliente'
      + " left JOIN t015_mm_contactos ON t215_mm_puntos_envio_cliente.f215_rowid_contacto = t015_mm_contactos.f015_rowid"
      + " left JOIN t013_mm_ciudades ON t015_mm_contactos.f015_id_pais = t013_mm_ciudades.f013_id_pais AND t015_mm_contactos.f015_id_depto = t013_mm_ciudades.f013_id_depto AND t015_mm_contactos.f015_id_ciudad = t013_mm_ciudades.f013_id"
      + ' WHERE f215_rowid_tercero =' + rowid
      + ' AND f215_id_sucursal ="' + id_suc + '"'

    // console.log("getP_envio... ",sql);

    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  //Consulta plan cirterio
  getCRI(id) {
    let sql
    if (id == "") {
      sql = "SELECT f105_id,f105_descripcion from t105_mc_criterios_item_planes";
    } else {
      sql = "SELECT f106_id,f106_id_plan,f106_descripcion from t106_mc_criterios_item_mayores where f106_id_plan ='" + id + "'";
    }
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //---Consulta Portafolio..
  getPortafolio() {
    let sql = "SELECT * FROM t136_mc_portafolio order by f136_descripcion";
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  //-------------------------Consultas a Items de Prueba por el momento

  //------------------- Prueba Items
  getItems(descripcion, pg, texto) {
    let sql = '';
    let filtroPlan = "";
    let filtro = "";
    let order_ext = '';
    let Sqlfull = '';
    if (descripcion == "123") {
      filtro = ' i.f120_id  LIKE "%' + texto + '%"';
      order_ext += ',ie.f121_id_ext1_detalle,ie.f121_id_ext2_detalle ';
      filtro += ' GROUP BY ie.f121_rowid ORDER BY i.f120_id ' + order_ext;
    } else if (descripcion == "ABC") {
      filtro = ' ie.f121_descripcion_toda LIKE "%' + texto + '%"';
      order_ext += ',ie.f121_desc_ext1_detalle,ie.f121_desc_ext2_detalle ';
      filtro += ' GROUP BY ie.f121_rowid ORDER BY ie.f121_descripcion_toda ' + order_ext;
    } else if (descripcion == "REF") {
      filtro = ' i.f120_referencia  LIKE "%' + texto + '%"';
      order_ext += ',ie.f121_id_ext1_detalle,ie.f121_id_ext2_detalle ';
      filtro += ' GROUP BY ie.f121_rowid ORDER BY i.f120_referencia ' + order_ext;
    } else if (descripcion == "CRI") {
      filtroPlan = " INNER JOIN t125_mc_items_criterios AS t125 ON t125.f125_rowid_item = i.f120_rowid ";
      filtro = " t125.f125_id_criterio_mayor = '" + texto + "'"
        + " GROUP BY ie.f121_descripcion_toda"
        + " ORDER BY ie.f121_descripcion_toda";
    } else if (descripcion == "PORTAFOLIO") {
      filtroPlan = " INNER JOIN t125_mc_items_criterios AS t125 ON t125.f125_rowid_item = i.f120_rowid ";
      filtro = " t125.f125_id_criterio_mayor = '" + texto + "'"
        + " GROUP BY ie.f121_descripcion_toda"
        + " ORDER BY ie.f121_descripcion_toda";
    }

    if (descripcion == "BAR") {
      sql = "SELECT ie.f121_rowid,i.f120_rowid,i.f120_id,i.f120_referencia,i.f120_descripcion,ie.f121_descripcion_toda,"
        + " i.f120_id_unidad_inventario,i.f120_id_unidad_adicional,ie.f121_id_extension1,ie.f121_id_extension2,"
        + " i.f120_ind_lista_precios_ext,ie.f121_desc_ext1_detalle,ie.f121_desc_ext2_detalle,"
        + " ie.f121_id_ext1_detalle,ie.f121_id_ext2_detalle,t131.f131_id_unidad_medida,t131.f131_cant_unidad_medida"
        + " FROM t120_mc_items AS i"
        + " INNER JOIN t121_mc_items_extensiones AS ie ON i.f120_rowid = ie.f121_rowid_item"
        + " INNER JOIN t131_mc_items_barras AS t131 ON t131.f131_rowid_item_ext = ie.f121_rowid"
        + " WHERE t131.f131_id Like '%" + texto + "%'";

    } else {
      sql = 'SELECT DISTINCT ie.f121_rowid,i.f120_rowid,i.f120_id,i.f120_referencia,i.f120_descripcion,ie.f121_descripcion_toda,'
        + " ifnull((SELECT sum(f400_cant_existencia_1) FROM t400_cm_existencia where f400_rowid_item_ext = ie.f121_rowid GROUP BY f400_rowid_item_ext),0) f400_existencias, "
        + " ifnull((SELECT sum(f431_cant1_pedida/f431_factor) FROM t431_cm_pv_movto where f431_rowid_pv_docto = 5 and f431_rowid_item_ext = ie.f121_rowid),0) cant1_pedida,  "
        + ' i.f120_id_unidad_inventario,i.f120_id_unidad_adicional,i.f120_id_unidad_empaque,ie.f121_id_extension1,ie.f121_id_extension2'
        + ' ,i.f120_ind_lista_precios_ext,ie.f121_desc_ext1_detalle,ie.f121_desc_ext2_detalle'
        + ' ,t5801_ff_fotos_baja_calidad.f5801_foto,f121_id_ext1_detalle,f121_id_ext2_detalle, t122.f122_factor, t122.f122_id_unidad,t122UE.f122_id_unidad f122_id_unidad_empaque ,t122UE.f122_factor f122_factor_empaque'
        + ' FROM t120_mc_items AS i'
        + ' INNER JOIN t121_mc_items_extensiones AS ie ON i.f120_rowid = ie.f121_rowid_item'
        + ' INNER JOIN t122_mc_items_unidades AS t122 ON t122.f122_rowid_item = i.f120_rowid'
        + '        AND t122.f122_id_unidad = f120_id_unidad_inventario'
        + ' LEFT JOIN t5801_ff_fotos_baja_calidad ON i.f120_rowid_foto = t5801_ff_fotos_baja_calidad.f5801_rowid_foto '
        + ' LEFT JOIN t122_mc_items_unidades AS t122UE ON t122UE.f122_rowid_item = i.f120_rowid'
        + '    AND t122UE.f122_id_unidad = f120_id_unidad_empaque '
        + filtroPlan
        + ' where' + filtro;
    }
    if (texto != '') {
      pg = 0;
      Sqlfull = sql;
    } else {
      Sqlfull = sql + " LIMIT 10 OFFSET  10 * " + pg;
    }
    return this.ldb.queryExec(Sqlfull)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }
  
  leerItemUni(rowid_item) {
    let filtroPrecio = '';
    let strInstalacionUM = '';
    let strInstalacionUMobs = '';
    let strInstalacionUMLimit = '';
    let barra = '';
    let f_precio = '';
    let strObsequio = '';

    let strPrecio = ', 0 precio';
    let srtRowidLipre = ', ifnull((select f126_rowid '
      + ' from t126_mc_items_precios '
      + ' where f126_id_lista_precio = "'
      + '007'//f201_id_lista_precio
      + '"'
      + ' and f126_id_unidad_medida = f122_id_unidad'
      + ' and	((f126_fecha_inactivacion is null and f126_fecha_activacion <= "'
      + '2019-01-23 17:44:43.742' + '") ' + ' or (not f126_fecha_inactivacion is null  and "'
      + '2019-01-23 17:44:43.742'
      + '" between f126_fecha_activacion and f126_fecha_inactivacion))'
      + filtroPrecio + ' order by f126_fecha_activacion desc'
      + ' limit 1),0) p_rowid_lista_prec';

    strPrecio = ', 0 precio';

    strObsequio = ' UNION ALL'
      + ' SELECT 1 ord, f122_id_unidad AS unidad, f101_descripcion AS unidadesc, f101_decimales AS decimales, '
      + ' f122_factor,f101_descripcion AS descripcion, 2 AS t_obs, 0 precio '
      + srtRowidLipre
      + ' FROM t137_mc_portafolio_items'
      + ' INNER JOIN t121_mc_items_extensiones ON t121_mc_items_extensiones.f121_rowid = t137_mc_portafolio_items.f137_rowid_item_ext'
      + ' INNER JOIN t122_mc_items_unidades ON t121_mc_items_extensiones.f121_rowid_item = t122_mc_items_unidades.f122_rowid_item'
      + ' INNER JOIN t101_mc_unidades_medida ON t122_mc_items_unidades.f122_id_unidad = t101_mc_unidades_medida.f101_id'
      + strInstalacionUMobs
      + ' WHERE t122_mc_items_unidades.f122_rowid_item = ' + rowid_item
      + ' AND t137_mc_portafolio_items.f137_id_portafolio = "10        "';


    strInstalacionUM = " INNER JOIN t121_mc_items_extensiones ON f122_rowid_item =  f121_rowid_item "
      + " INNER JOIN t132_mc_items_instalacion ON  f121_rowid =  f132_rowid_item_ext AND  f122_id_unidad =  f132_id_um_venta_suge ";
    strInstalacionUMobs = " INNER JOIN t132_mc_items_instalacion ON  f121_rowid =  f132_rowid_item_ext AND  f122_id_unidad =  f132_id_um_venta_suge ";
    //strInstalacionUMLimit = " LIMIT 1 ";
    filtroPrecio = ' and f126_rowid_item = ' + rowid_item;

    let strSQL = 'select 0 ord, f122_id_unidad unidad, f101_descripcion unidadesc, f101_decimales decimales, f122_factor, f101_descripcion descripcion, 0 t_obs'
      + strPrecio
      + srtRowidLipre
      + ' from t122_mc_items_unidades'
      + ' inner join t101_mc_unidades_medida on f122_id_unidad = f101_id'
      + ' where f122_rowid_item = ' + rowid_item
      + barra
      + f_precio
      + strObsequio + ' order by ord, f122_id_unidad'
      + strInstalacionUMLimit;

    return this.ldb.queryExec(strSQL)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  traerItems(texto) {
    let sql = "SELECT DISTINCT ie.f121_rowid,i.f120_rowid,i.f120_id,i.f120_referencia,i.f120_descripcion,ie.f121_descripcion_toda,"
      + " ifnull((SELECT sum(f400_cant_existencia_1) FROM	t400_cm_existencia WHERE f400_rowid_item_ext = ie.f121_rowid"
      + " GROUP BY f400_rowid_item_ext),0) f400_existencias,0 f400_cant_vendida,"
      + " ifnull((SELECT sum(f431_cant1_pedida * f431_factor)	FROM t431_cm_pv_movto	WHERE	f431_rowid_pv_docto = NULL"
      + " AND f431_rowid_item_ext = ie.f121_rowid),0) cant1_pedida,i.f120_id_unidad_inventario,"
      + " i.f120_id_unidad_adicional,	i.f120_id_unidad_empaque,	ie.f121_id_extension1,ie.f121_id_extension2,i.f120_ind_lista_precios_ext,"
      + " ie.f121_desc_ext1_detalle,ie.f121_desc_ext2_detalle,t5801_ff_fotos_baja_calidad.f5801_foto,f121_id_ext1_detalle,f121_id_ext2_detalle, "
      + " t122.f122_factor,	t122.f122_id_unidad,ifnull(t122UE.f122_id_unidad, '') f122_id_unidad_empaque,	ifnull(t122UE.f122_factor, 0) f122_factor_empaque "
      + " FROM	t120_mc_items AS i INNER JOIN t121_mc_items_extensiones AS ie ON i.f120_rowid = ie.f121_rowid_item "
      + " INNER JOIN t122_mc_items_unidades AS t122 ON t122.f122_rowid_item = i.f120_rowid AND t122.f122_id_unidad = f120_id_unidad_inventario"
      + " LEFT JOIN t5801_ff_fotos_baja_calidad ON i.f120_rowid_foto = t5801_ff_fotos_baja_calidad.f5801_rowid_foto "
      + " LEFT JOIN t122_mc_items_unidades AS t122UE ON t122UE.f122_rowid_item = i.f120_rowid "
      + " AND t122UE.f122_id_unidad = f120_id_unidad_empaque WHERE ie.f121_descripcion_toda LIKE '%" + texto + "%'"
      + " GROUP BY	ie.f121_rowid ORDER BY	ie.f121_descripcion_toda"
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  gettraerItem(rowid_item, f201_id_lista_precio, rowid_item_ext, ts) {
    let filtroPrecio = '';
    let strInstalacionUM = '';
    let strInstalacionUMobs = '';
    let f_precio = '';

    if (f201_id_lista_precio == 0) {
      filtroPrecio = ' and f126_rowid_item = ' + rowid_item;
    } else {
      filtroPrecio = ' and f126_rowid_item_ext = ' + rowid_item_ext;
    }

    let strPrecio = ', ifnull((select f126_precio'
      + ' from t126_mc_items_precios '
      + ' where f126_id_lista_precio = "017'
      + '"'
      + ' and f126_id_unidad_medida = f122_id_unidad'
      + ' and	((f126_fecha_inactivacion is null and f126_fecha_activacion <= "' + ts
      + '") '
      + ' or (not f126_fecha_inactivacion is null  and "'
      + ts + '" between f126_fecha_activacion and f126_fecha_inactivacion))'
      + filtroPrecio + ' order by f126_fecha_activacion desc'
      + ' limit 1),0) precio';

    // -- Encuentra rowid_lista_precios
    let srtRowidLipre = ', ifnull((select f126_rowid '
      + ' from t126_mc_items_precios '
      + ' where f126_id_lista_precio = "017'
      + '"'
      + ' and f126_id_unidad_medida = f122_id_unidad'
      + ' and	((f126_fecha_inactivacion is null and f126_fecha_activacion <= "' + ts
      + '") ' + ' or (not f126_fecha_inactivacion is null  and "' + ts
      + '" between f126_fecha_activacion and f126_fecha_inactivacion))'
      + filtroPrecio + ' order by f126_fecha_activacion desc'
      + ' limit 1),0) p_rowid_lista_prec';

    let strObsequio = '';
    strObsequio = ' UNION ALL'
      + ' SELECT 1 ord, f122_id_unidad AS unidad, f101_descripcion AS unidadesc, f101_decimales AS decimales, '
      + ' f122_factor,f101_descripcion AS descripcion, 2 AS t_obs, 0 precio '
      + srtRowidLipre
      + ' FROM t137_mc_portafolio_items'
      + ' INNER JOIN t121_mc_items_extensiones ON t121_mc_items_extensiones.f121_rowid = t137_mc_portafolio_items.f137_rowid_item_ext'
      + ' INNER JOIN t122_mc_items_unidades ON t121_mc_items_extensiones.f121_rowid_item = t122_mc_items_unidades.f122_rowid_item'
      + ' INNER JOIN t101_mc_unidades_medida ON t122_mc_items_unidades.f122_id_unidad = t101_mc_unidades_medida.f101_id'
      + strInstalacionUMobs
      + ' WHERE t122_mc_items_unidades.f122_rowid_item = ' + rowid_item
      + ' AND t137_mc_portafolio_items.f137_id_portafolio = "' + parVendedor.f5785_id_portafolio_items + '"';

    /* strInstalacionUM = " INNER JOIN t121_mc_items_extensiones ON f122_rowid_item =  f121_rowid_item "
     + " INNER JOIN t132_mc_items_instalacion ON  f121_rowid =  f132_rowid_item_ext AND  f122_id_unidad =  f132_id_um_venta_suge ";
     strInstalacionUMobs = " INNER JOIN t132_mc_items_instalacion ON  f121_rowid =  f132_rowid_item_ext AND  f122_id_unidad =  f132_id_um_venta_suge ";
     strInstalacionUMLimit = " LIMIT 1 ";*/

    f_precio = ' and precio > 0';
    // --- Falta validar obsequios
    let Sql = 'select 0 ord, f122_id_unidad unidad, f101_descripcion unidadesc, f101_decimales decimales, f122_factor, f101_descripcion descripcion, 0 t_obs'
      + strPrecio
      + srtRowidLipre
      + ' from t122_mc_items_unidades'
      + ' inner join t101_mc_unidades_medida on f122_id_unidad = f101_id'
      + strInstalacionUM
      + ' where f122_rowid_item = ' + rowid_item
      + f_precio
      + strObsequio + ' order by ord, f122_id_unidad';
    return this.ldb.queryExec(Sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  getCartera(cl) {
    let strSQL = "SELECT DISTINCT c.*,ifnull(((f353_saldo + f353_total_db_aplicado) - f353_total_cr_aplicado),0) saldo"
      + " ,CASE WHEN ((julianday(date()) - julianday(substr(c.f353_fecha_vcto,1,4) ||'-'|| substr(c.f353_fecha_vcto,6,2) ||'-'|| substr(c.f353_fecha_vcto,9,2)))) < 0 THEN 0 ELSE (julianday(date()) - julianday(substr(c.f353_fecha_vcto,1,4) ||'-'|| substr(c.f353_fecha_vcto,6,2) ||'-'|| substr(c.f353_fecha_vcto,9,2))) END AS dias_vcto"
      + " ,CASE WHEN c.f353_ind_liq_tabla_pp = 0 AND (julianday(date()) - julianday(substr(c.f353_fecha_dscto_pp, 1, 4) || '-' || substr(c.f353_fecha_dscto_pp, 6, 2) || '-' || substr(c.f353_fecha_dscto_pp, 9, 2)		)	) <= 0 THEN	f353_vlr_dscto_pp ELSE	0 END AS vlr_dscto_pp"
      + " ,ifnull((select f354_rowid_docto from t354_co_mov_saldo_abierto where c.f353_rowid = t354_co_mov_saldo_abierto.f354_rowid_sa and f354_rowid_docto = " + 0 + "), 0) f354_rowid_docto"
      + " ,ifnull((select f354_valor_cr from t354_co_mov_saldo_abierto where c.f353_rowid = t354_co_mov_saldo_abierto.f354_rowid_sa and f354_rowid_docto = " + 0 + "), 0) f354_valor_cr"
      + " ,ifnull((select f354_valor_aplicado_pp from t354_co_mov_saldo_abierto where c.f353_rowid = t354_co_mov_saldo_abierto.f354_rowid_sa AND c.f353_ind_liq_tabla_pp = 1 and f354_rowid_docto = " + 0 + "), 0) vlr_dscto_pp_ind_tabla"
      + ' ,f353_valor_origen, f281_descripcion'
      + " FROM t353_co_saldo_abierto AS c  "
      + ' LEFT JOIN t354_co_mov_saldo_abierto ON c.f353_rowid = t354_co_mov_saldo_abierto.f354_rowid_sa'
      + " LEFT JOIN t281_co_unidades_negocio ON c.f353_id_un_cruce = t281_co_unidades_negocio.f281_id"
      + ' where f353_rowid_tercero = ' + cl.f201_rowid_tercero
      + ' and f353_id_sucursal = "' + cl.f201_id_sucursal + '"'
      + " ORDER BY dias_vcto desc";

    //console.log("#comsulta cartera... ", strSQL)
    return this.ldb.queryExec(strSQL)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  insertarDescuentos = function (results) {
    let len = results.rows.length;
    let arrayQuery = [];
    let pos = 0;
    let sql = '';
    if (len > 0) {
      for (let i = 0; i < len; i++) {
        let t = results.rows[i];
        //			console.log('Registro Dscto: ', t)
        let rowid_obs: any = '0';
        // --- Si tiene item para obsequio, primero se guarda en la 431, se recupera el rowid
        if (t.f_cant1_obsequio > 0) {
          pedidoDetalle.f431_rowid_item_ext = t.f_rowid_item_ext_obsequio;
          pedidoDetalle.f431_id_motivo = t.f_id_motivo_obsequio;
          pedidoDetalle.f431_ind_obsequio = 1;
          if (t.f_valor_dscto > 0) {
            pedidoDetalle.f431_ind_obsequio = 2;
          }
          if (t.f_id_motivo_obsequio != null) {
            pedidoDetalle.f431_rowid_bodega = t.f_rowid_bodega_obsequio;
          }
          //				t431.f431_precio_unitario_base = 0;
          pedidoDetalle.f431_id_unidad_medida = t.f_id_um_obsequio;
          pedidoDetalle.f431_id_unidad_medida_captura = t.f_id_um_obsequio;
          pedidoDetalle.f431_factor = 1;
          pedidoDetalle.f431_cant1_pedida = t.f_cant1_obsequio;
          pedidoDetalle.f431_cant2_pedida = t.f_cant2_obsequio;

          sql = this.armaStrMovto();

          arrayQuery[pos++] = sql;
          // console.log(sql);
          rowid_obs = '(select f431_rowid from t431_cm_pv_movto ORDER BY f431_ts DESC LIMIT 1)';
        }
        if (t.f_fecha_inicial == 'null') {
          t.f_fecha_inicial = '';
        }
        if (t.f_fecha_final == 'null') {
          t.f_fecha_final = '';
        }
        if (t.f_id_motivo_obsequio == 'null') {
          t.f_id_motivo_obsequio = '';
        }
        if (t.f_id_um_obsequio == 'null') {
          t.f_id_um_obsequio = '';
        }
        if (t.f_id_concepto_obsequio == 'null') {
          t.f_id_concepto_obsequio = '';
        }

        // --- Se guarda el detalle del descuento
        sql = 'INSERT INTO t432_cm_pv_movto_dscto VALUES ('
          + pedidoDetalle.f431_rowid + "," + t.f_orden + "," + 1 // --- Preguntar que es esto?
          + "," + t.f_porcentaje_dscto + "," + t.f_valor_dscto
          + "," + t.f_rowid_promo_dscto_linea + ",'"
          + t.f_fecha_inicial + "','" + t.f_fecha_final
          + "'," + rowid_obs + "," + t.f_rowid_item_ext_obsequio
          + ",'" + t.f_id_concepto_obsequio + "','"
          + t.f_id_motivo_obsequio + "','" + t.f_id_um_obsequio
          + "'," + t.f_cant1_obsequio + ","
          + t.f_cant2_obsequio + ")";
        arrayQuery[pos++] = sql;
        if (rowid_obs != 0) {
          sql = 'update tp1_generico_hallar_dsctos_vta set f_rowid_movto = '
            + rowid_obs + ' where rowid = ' + t.f_rowid;
          arrayQuery[pos++] = sql;
        } else {
          if (t.f_porcentaje_dscto > 0) {
            sql = 'update t431_cm_pv_movto set '
              + ' f431_vlr_dscto_linea = f431_vlr_dscto_linea + round(((f431_vlr_bruto*'
              + parseFloat(t.f_porcentaje_dscto) + ')/100),2) '
              + ' where f431_rowid = ' + t.rowid_movto;
            arrayQuery[pos++] = sql;

            sql = 'update t431_cm_pv_movto set '
              + ' f431_vlr_bruto = round(f431_vlr_bruto - ((f431_vlr_bruto*'
              + parseFloat(t.f_porcentaje_dscto) + ')/100),2) '
              + ' where f431_rowid = ' + t.rowid_movto;
            arrayQuery[pos++] = sql;
          }
          if (t.f_valor_dscto > 0) {
            sql = 'update t431_cm_pv_movto set '
              + ' f431_vlr_dscto_linea = f431_vlr_dscto_linea + '
              + (parseFloat(t.f_valor_dscto) * parseFloat(t.f431_cant1_pedida))
              + ' where f431_rowid = ' + t.rowid_movto;
            arrayQuery[pos++] = sql;
            sql = 'update t431_cm_pv_movto set '
              + ' f431_vlr_bruto = f431_vlr_bruto - ('
              + (parseFloat(t.f_valor_dscto) * parseFloat(t.f431_cant1_pedida))
              + ') where f431_rowid = ' + t.rowid_movto;
            arrayQuery[pos++] = sql;
          }
        }
        if (rowid_obs != 0 && t.f_valor_dscto != 0) {
          sql = 'update t431_cm_pv_movto set '
            + ' f431_vlr_dscto_linea = f431_vlr_dscto_linea + '
            + (parseFloat(t.f_valor_dscto) * parseFloat(t.f431_cant1_pedida))
            + ' where f431_rowid = ' + t.rowid_movto;
          arrayQuery[pos++] = sql;

          sql = 'update t431_cm_pv_movto set '
            + ' f431_vlr_bruto = f431_vlr_bruto - ('
            + (parseFloat(t.f_valor_dscto) * parseFloat(t.f431_cant1_pedida))
            + ') where f431_rowid = ' + t.rowid_movto;
          arrayQuery[pos++] = sql;
        }
        let c = len;
        if (t.porc_dscto_linea_cap > 0) {
          c++;
          if (t.p_fecha_inicial == 'null') {
            t.p_fecha_inicial = '';
          }
          if (t.p_fecha_final == 'null') {
            t.p_fecha_final = '';
          }
          sql = 'INSERT INTO t432_cm_pv_movto_dscto VALUES (' + pedidoDetalle.f431_rowid
            + "," + c + "" + "," + 1 + ""// --- Preguntar que es esto?
            + "," + t.porc_dscto_linea_cap + "" + "," + 0 + "" + ","
            + t.p_rowid_promo_dscto_linea + "" + ",'"
            + t.p_fecha_inicial + "'" + ",'" + t.p_fecha_final
            + "'" + "," + 0 + "" + "," + 0 + "" + ",'','',''" + ","
            + 0 + "" + "," + 0 + "" + ")";
          arrayQuery[pos++] = sql;

          sql = 'update t431_cm_pv_movto set '
            + ' f431_vlr_dscto_linea = f431_vlr_dscto_linea + round(((f431_vlr_bruto*'
            + parseFloat(t.porc_dscto_linea_cap) + ')/100),2) '
            + ' where f431_rowid = ' + pedidoDetalle.f431_rowid;
          arrayQuery[pos++] = sql;

          sql = 'update t431_cm_pv_movto set '
            + ' f431_vlr_bruto = round(f431_vlr_bruto - ((f431_vlr_bruto*'
            + parseFloat(t.porc_dscto_linea_cap) + ')/100),2) '
            + ' where f431_rowid = ' + pedidoDetalle.f431_rowid;
        }
        for (let i = 0; i < pos; i++) {
          return this.ldb.queryExec(arrayQuery[i])
            .then(response => {
              this.armaImpuestos();
              return Promise.resolve(response);
            })
            .catch(error => {
              return Promise.reject(error)
            });
        }
      }
    }
  }

  armaImpuestos() {
    let sqlScript = 'select id_llave,ifnull(valor_uni,0) valor_uni,ifnull(ind_accion,0) ind_accion,'
      + ' t_imp.f037_descripcion descripcion,t_imp.f037_porcentaje_base base,t_imp.f037_tasa tasa, '
      + ' t_imp.f037_ind_calculo ind_calculo,t_imp.f037_ind_descontable ind_descontable,'
      + ' ifnull(t_imp_desc.f037_porcentaje_base,0) base_desc,ifnull(t_imp_desc.f037_tasa,0) tasa_desc, '
      + pedidoDetalle.f431_rowid
      + ' rowid_movto, '
      + pedidoDetalle.f431_vlr_bruto
      + ' bruto_proy, '
      + pedidoDetalle.f431_ind_obsequio
      + ' ind_obs'
      + ' from sp_generico_hallar_impuesto1'
      + ' inner join t037_mm_llaves_impuesto t_imp on id_llave = t_imp.f037_id'
      + ' left outer join t037_mm_llaves_impuesto t_imp_desc on id_llave_desc = t_imp_desc.f037_id'
      + ' order by sigla,id_llave';

    return this.ldb.queryExec(sqlScript)
      .then(response => {
        this.insertarImpuestos(response);
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  };

  insertarImpuestos(results) {
    let len = results.rows.length;
    let arrayQuery = [];
    let pos = 0;
    let sql = '';

    if (len > 0) {
      for (let i = 0; i < len; i++) {
        let t = results.rows.item(i);
        let signo = '+';
        if (t.ind_accion == 2) {
          signo = '-';
        }

        if (t.ind_obs == false) {
          if (t.ind_accion == 3 || t.ind_accion == 4) {
            if (t.valor_uni == 0) {
              sql = 'update t431_cm_pv_movto set '
                + ' f431_vlr_imp_no_apli = f431_vlr_imp_no_apli '
                + signo + ' round((((' + pedidoDetalle.f431_vlr_bruto
                + ' - f431_vlr_dscto_linea)*'
                + parseFloat(t.tasa) + ')/100),2) '
                + ' where f431_rowid = ' + t.rowid_movto;
            } else {
              sql = 'update t431_cm_pv_movto set '
                + ' f431_vlr_imp_no_apli = f431_vlr_imp_no_apli '
                + signo + ' round(' + parseFloat(t.valor_uni)
                + '*f431_cant1_pedida,2) '
                + ' where f431_rowid = ' + t.rowid_movto;
            }
            //					 console.log(t.ind_obs + " " + sql);
            arrayQuery[pos++] = sql;
          }
          if (t.ind_accion == 1 || t.ind_accion == 2) {
            if (t.valor_uni == 0) {
              sql = 'update t431_cm_pv_movto set '
                + ' f431_vlr_imp = f431_vlr_imp ' + signo
                + ' round((((' + pedidoDetalle.f431_vlr_bruto
                + ' - f431_vlr_dscto_linea)*'
                + parseFloat(t.tasa) + ')/100),2) '
                + ' where f431_rowid = ' + t.rowid_movto;
            } else {
              sql = 'update t431_cm_pv_movto set '
                + ' f431_vlr_imp = f431_vlr_imp ' + signo
                + ' round(' + parseFloat(t.valor_uni)
                + '*f431_cant1_pedida,2) '
                + ' where f431_rowid = ' + t.rowid_movto;
            }
            arrayQuery[pos++] = sql;
            //					 console.log(sql);
          }
          sql = 'update t431_cm_pv_movto set ' + ' f431_vlr_bruto = '
            + pedidoDetalle.f431_vlr_bruto + ' + f431_vlr_imp_no_apli '
            + ' where f431_rowid = ' + t.rowid_movto;
          arrayQuery[pos++] = sql;
        } else {
          let v_imp = ((pedidoDetalle.f431_vlr_bruto * t.tasa) / 100);

          //-- Impuesto asumido por la empresa
          if (t.ind_accion == 3) {
            v_imp = 0;
          }

          sql = 'update t431_cm_pv_movto set ' + ' f431_vlr_imp =  '
            + v_imp + ' ,f431_vlr_neto =  ' + v_imp
            + ' where f431_rowid = ' + t.rowid_movto;
          arrayQuery[pos++] = sql;
          //				 console.log(sql);

          sql = ' update tp1_generico_hallar_dsctos_vta set f_proceso_imp = 1'
            + ' where f_rowid_movto = ' + t.rowid_movto;
          //				 console.log(sql);
          arrayQuery[pos++] = sql;
        }

        sql = 'update t431_cm_pv_movto set '
          + ' f431_vlr_neto = round(((f431_vlr_bruto - f431_vlr_dscto_linea) + (f431_vlr_imp - f431_vlr_imp_no_apli)),2)'
          + ' where f431_rowid = ' + t.rowid_movto;
        arrayQuery[pos++] = sql;
        //			 console.log("este si :" + sql);
        sql = 'INSERT INTO t433_cm_pv_movto_imp VALUES (' + pedidoDetalle.f431_rowid
          + ",'" + t.id_llave + "'" + "," + t.base + "" + ","
          + t.tasa + "" + "," + t.valor_uni + "" + "," + t.ind_accion
          + "" + "," + t.ind_calculo + "" + ")";
        arrayQuery[pos++] = sql;
        //			console.log(sql);
      }

    } else {
      sql = 'update t431_cm_pv_movto set ' + ' f431_vlr_imp = 0 '
        + ' where f431_rowid = ' + pedidoDetalle.f431_rowid;
      arrayQuery[pos++] = sql;
      // console.log(sql);

      sql = 'update t431_cm_pv_movto set '
        + ' f431_vlr_bruto = f431_precio_unitario_base * f431_cant1_pedida'
        + ' where f431_rowid = ' + pedidoDetalle.f431_rowid;
      arrayQuery[pos++] = sql;
      // console.log(sql);

      sql = 'update t431_cm_pv_movto set '
        + ' f431_vlr_neto = round(((f431_vlr_bruto - f431_vlr_dscto_linea) + f431_vlr_imp),2)'
        + ' where f431_rowid = ' + pedidoDetalle.f431_rowid;
      arrayQuery[pos++] = sql;
    }
    for (let i = 0; i < pos; i++) {
      return this.ldb.queryExec(arrayQuery[i])
        .then(response => {
          return Promise.resolve(response);
        })
        .catch(error => {
          return Promise.reject(error)
        });
    }

  };


  insertCabecera() {
    /*let sql = "INSERT INTO t354_co_mov_saldo_abierto VALUES("
       + t354.f354_rowid + ","
       + "'" + t354.f354_rowid_mov_docto + "',"
       + "'" + t354.f354_rowid_docto + "',"
       + "'" + t354.f354_rowid_sa + "',"
       + "'" + t354.f354_rowid_relac_medios_pago + "',"
       + "'" + t354.f354_fecha + "',"
       + "'" + t354.f354_ind_estado + "',"
       + "'" + t354.f354_valor_db + "',"
       + "'" + t354.f354_valor_cr + "',"
       + "'" + t354.f354_valor_db_alt + "',"
       + "'" + t354.f354_valor_cr_alt + "',"
       + "'" + t354.f354_valor_aplicado_pp + "',"
       + "'" + t354.f354_valor_aplicado_pp_alt + "',"
       + "'" + t354.f354_valor_aprovecha + "',"
       + "'" + t354.f354_valor_aprovecha_alt + "',"
       + "'" + t354.f354_valor_retenciones + "',"
       + "'" + t354.f354_valor_retenciones_alt + "',"
       + "'" + t354.f354_rowid_pe_prov_cuenta + "',"
       + "'" + t354.f354_rowid_vend + "',"
       + "'" + t354.f354_id_sucursal + "',"
       + "'" + t354.f354_prefijo_cruce + "',"
       + "'" + t354.f354_id_tipo_docto_cruce + "',"
       + "'" + t354.f354_consec_docto_cruce + "',"
       + "'" + t354.f354_nro_cuota_cruce + "',"
       + "'" + t354.f354_notas + "'"
       + ")";
     arrayQuery[pos++] = sql;
     cl.f201_saldo_rc = parseFloat(cl.f201_saldo_rc) - parseFloat(recaudoEncabezado.valor);
     sql = 'update t354_co_mov_saldo_abierto set '
       + ' f354_valor_aplicado_pp = ' + recaudoCartera.totalDsctoPP
       + ' where f354_rowid_sa = ' + recaudoEncabezado.rowid_sa
       + ' and f354_rowid_docto = ' + t350.f350_rowid;
     arrayQuery[pos++] = sql;*/
  }

  /*
  let promise1 = new Promise((resolve, reject) => {
    setTimeout(resolve, 2000, 'promise 1 resolved');
  });
   
  let promise2 = new Promise((resolve, reject) => {
    setTimeout(reject, 3000, 'promise 2 rejected');
  });
   
  Promise
    .race([promise1, promise2])
    .then(console.log)
    .catch(console.log);
  */
}
