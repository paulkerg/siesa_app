import { Injectable } from "@angular/core";
import { Platform } from "ionic-angular";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { DB_LOCAL_CONFIG } from "./../../config/db.local.config";
import { AnalyticsProvider } from "../analytics/analytics";
import { global } from "../../app/global";

@Injectable()
export class SqliteProvider {
  private database: SQLiteObject;
  private dbReady = new BehaviorSubject<boolean>(false);

  constructor(
    private platform: Platform, 
    private sqlite: SQLite,
		private analytics: AnalyticsProvider
    ) {
    this.platform.ready().then(() => {
      this.sqlite
        .create({
          name: "dbDemo.db",
          location: "default",
          createFromLocation: 1
        })
        .then((db: SQLiteObject) => {
          this.database = db;
          this.createTables()
            .then(() => {
              //we loaded or created tables, so, set dbReady to true
              this.dbReady.next(true);
            })
            .then(() => {
              let tam = DB_LOCAL_CONFIG["tablasActualizacion"].length;
              var pos = tam;
              var arrayQuery = [];
              do {
                var tabla =
                  DB_LOCAL_CONFIG["tablasActualizacion"][tam - pos][0];
                var orden =
                  DB_LOCAL_CONFIG["tablasActualizacion"][tam - pos][1];
                var ind_db =
                  DB_LOCAL_CONFIG["tablasActualizacion"][tam - pos][2];
                  var where_delete = 
                  DB_LOCAL_CONFIG["tablasActualizacion"][tam - pos][3];
                  var id_modulo = 
                  DB_LOCAL_CONFIG["tablasActualizacion"][tam - pos][4];
                let query =
                  `Insert into t003_mm_tablas_actualizacion (tabla, orden, ind_actualizado, ind_porce_db, where_delete, id_modulo)
                  Select 
                  '${tabla}',${orden}, 0,${ind_db},'${where_delete}','${id_modulo}'
                  
                  Where not exists (Select 1 From t003_mm_tablas_actualizacion Where tabla = '${tabla}')`;
                arrayQuery[tam - pos] = query;
              } while (--pos);

              //console.log('arrayQuery : ' + arrayQuery);
              this.queryAll(arrayQuery);
              //siesaDB.executeSqlBatch(arrayQuery);
            });
        });
    });
  }

  private createTables() {
    return new Promise((resolve, reject) => {
      let tablas = DB_LOCAL_CONFIG["tablas"];
      let cTablas = [];
      for (var i = 0; i <= tablas.length - 1; i++) {
        cTablas.push("CREATE TABLE IF NOT EXISTS " + tablas[i]);
      }
      cTablas.push(
        "Insert into usuarios (id_usuario, clave) Select 'siesa', 'siesa' Where not exists (select 1 from usuarios)"
      );
      cTablas.push(
        "Insert into configuraciones (id) Select 'parametros' Where not exists (select 1 from configuraciones WHERE id = 'parametros')"
      );
      cTablas.push(
        "Insert into t002_mm_propiedades (ind_https, id_vendedor) Select 'false','siesa' Where not exists (select 1 from t002_mm_propiedades)"
      );
      resolve(this.queryAll(cTablas));
    });
  }

  private isReady() {
    return new Promise((resolve, reject) => {
      //if dbReady is true, resolve
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        //otherwise, wait to resolve until dbReady returns true
        this.dbReady.subscribe(ready => {
          if (ready) {
            resolve();
          }
        });
      }
    });
  }

  queryExec(sql) {
    //return this.isReady().then(() => {
    return this.database
      .executeSql(sql, [])
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => Promise.reject(error));
    //});
  }

  queryExecRegs(sql) {
    //return this.isReady().then(() => {
    return this.database
      .executeSql(sql, [])
      .then(response => {
        let registros = [];
        for (let index = 0; index < response.rows.length; index++) {
          registros.push(response.rows.item(index));
        }

        return Promise.resolve(registros);
      })
      .catch(error => Promise.reject(error));
    //});
  }

  query(sql) {
    //console.log('query antes isReady:' + sql);
    return this.isReady().then(() => {
      //console.log('isReady:' + sql);
      return this.database.executeSql(sql, []).then(data => {
        //console.log('consulto:');
        let lists = [];
        for (let i = 0; i < data.rows.length; i++) {
          lists.push(data.rows.item(i));
        }
        return lists;
      });
    });
  }

  queryAll(ArraySQL: any, raw: boolean = false) {
    console.log()
		return this.database.sqlBatch(ArraySQL).then(res => {
      Promise.resolve(true);
    });

    // return new Promise((resolve, reject) => {
    // 	let promises = [];
    // 	for (let i = 0; i < SQL.length; i++) {
    // 		//console.log(SQL[i])
    // 		promises.push(this.queryExec(SQL[i]));
    // 	}
    // 	return Promise.all(promises).then(
    // 		(success) => {
    // 			resolve(success);
    // 		},
    // 		(err) => {
    // 			reject(err);
    // 		}
    // 	);
    // });
  }

  error(error){
		this.analytics.trackEvent('database', 'Error db', global.dispositivo.idFuncionario, error);    
  }
}
