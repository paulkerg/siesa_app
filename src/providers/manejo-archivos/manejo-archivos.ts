import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Device } from '@ionic-native/device';
import { SocialSharing } from '@ionic-native/social-sharing';
import { isObject } from '../../pipes/generales/utils/utils';
import * as pdfMake from 'pdfmake/build/pdfmake';

@Injectable()
export class ManejoArchivosProvider {
	mimeTypes = {
		aac: 'audio/aac',
		abw: 'application/x-abiword',
		arc: 'application/octet-stream',
		avi: 'video/x-msvideo',
		azw: 'application/vnd.amazon.ebook',
		bin: 'application/octet-stream',
		bz: 'application/x-bzip',
		bz2: 'application/x-bzip2',
		csh: 'application/x-csh',
		css: 'text/css',
		csv: 'text/csv',
		doc: 'application/msword',
		docx: 'application/msword',
		epub: 'application/epub+zip',
		gif: 'image/gif',
		htm: 'text/html',
		html: 'text/html',
		ico: 'image/x-icon',
		ics: 'text/calendar',
		jar: 'application/java-archive',
		jpeg: 'image/jpeg',
		jpg: 'image/jpeg',
		js: 'application/javascript',
		json: 'application/json',
		mid: 'audio/midi',
		midi: 'audio/midi',
		mpeg: 'video/mpeg',
		mpkg: 'application/vnd.apple.installer+xml',
		odp: 'application/vnd.oasis.opendocument.presentation',
		ods: 'application/vnd.oasis.opendocument.spreadsheet',
		odt: 'application/vnd.oasis.opendocument.text',
		oga: 'audio/ogg',
		ogv: 'video/ogg',
		ogx: 'application/ogg',
		pdf: 'application/pdf',
		ppt: 'application/vnd.ms-powerpoint',
		pptx: 'application/vnd.ms-powerpoint',
		rar: 'application/x-rar-compressed',
		rtf: 'application/rtf',
		sh: 'application/x-sh',
		svg: 'image/svg+xml',
		swf: 'application/x-shockwave-flash',
		tar: 'application/x-tar',
		tif: 'image/tiff',
		tiff: 'image/tiff',
		ttf: 'font/ttf',
		vsd: 'application/vnd.visio',
		wav: 'audio/x-wav',
		weba: 'audio/webm',
		webm: 'video/webm',
		webp: 'image/webp',
		woff: 'font/woff',
		woff2: 'font/woff2',
		xhtml: 'application/xhtml+xml',
		xls: 'application/vnd.ms-excel',
		xlsx: 'application/vnd.ms-excel',
		xml: 'application/xml',
		xul: 'application/vnd.mozilla.xul+xml',
		zip: 'application/zip',
		'3gp': 'video/3gpp',
		'3g2': 'video/3gpp2',
		'7z': 'application/x-7z-compressed',
		txt: 'text/plain',
		png: 'image/png'
	};

	mimeTypesIcons = {
		csv: 'icon-file-delimited',
		doc: 'icon-file-word',
		docx: 'icon-file-word',
		jpeg: 'icon-file-image',
		jpg: 'icon-file-image',
		mid: 'icon-file-music',
		midi: 'icon-file-music',
		mpeg: 'icon-file-video',
		oga: 'icon-file-music',
		ogv: 'icon-file-video',
		pdf: 'icon-file-pdf',
		ppt: 'icon-file-powerpoint',
		pptx: 'icon-file-powerpoint',
		wav: 'icon-file-music',
		weba: 'icon-file-music',
		webm: 'icon-file-video',
		xls: 'icon-file-excel',
		xlsx: 'icon-file-excel',
		'3gp': 'icon-file-video',
		'3g2': 'icon-file-video',
		txt: 'icon-file-document',
		png: 'icon-file-image'
	};

	dafaultDir = 'siesappsDctos';
	path: any;

	constructor(
		private file: File,
		private fileOpener: FileOpener,
		private device: Device,
		private socialSharing: SocialSharing
	) {
		this.inicializa();
	}

	inicializa() {
		let platform = this.device.platform;
		let miPath = '';

		if (!platform) {
			platform = '';
		}
		if (platform.toUpperCase() == 'ANDROID') {
			miPath = this.file.externalRootDirectory;
		}
		if (platform.toUpperCase() == 'IOS') {
			miPath = this.file.documentsDirectory;
		}
		if (platform.toUpperCase() == 'BROWSER' || platform == 'WINDOWS') {
			miPath = this.file.dataDirectory;
		}
		//console.log(this.device, this.path, platform.toUpperCase());
		this.file
			.checkDir(miPath, this.dafaultDir)
			.then(() => {
				this.path = miPath + this.dafaultDir + '/';
			})
			.catch(() => {
				this.file
					.createDir(miPath, this.dafaultDir, true)
					.then(() => {
						this.path = miPath + this.dafaultDir + '/';
					})
					.catch((err) => {
						console.log(err);
					});
			});
	}

	listDir(directorio: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.file
				.checkDir(this.path, directorio)
				.then(() => {
					this.file
						.listDir(this.path, directorio)
						.then((lista) => {
							let lstArc = [];
							for (let i = 0; i < lista.length; i++) {
								let extensiones: any = lista[i]['name'].split('.');
								let tipoArchivo = this.mimeTypes[extensiones[extensiones.length - 1]];
								let icono = this.mimeTypesIcons[extensiones[extensiones.length - 1]];
								if (!icono) {
									icono = 'icon-file';
								}
								if (lista[i]['isDirectory'] == true || lista[i]['isDirectory']) {
									icono = 'icon-folder';
								}
								lista[i]['icono'] = icono;
								lista[i]['tipoArchivo'] = tipoArchivo;
								lstArc.push(lista[i]);
							}

							resolve(lstArc);
						})
						.catch((err) => {
							console.log('Directory doesnt exist');
							reject(err);
						});
				})
				.catch((err) => {
					this.file
						.createDir(this.path, directorio, true)
						.then((_) => {
							console.log('Creando ' + this.path + directorio);
							this.listDir(directorio);
						})
						.catch((err) => {
							console.log('Directory doesnt exist');
							reject(err);
						});
				});
		});
	}

	validaDirectorio(directorio) {
		return new Promise((resolve, reject) => {
			this.file
				.checkDir(this.path, directorio)
				.then(() => {
					resolve(this.path + directorio + '/');
				})
				.catch((err) => {
					this.file
						.createDir(this.path, directorio, true)
						.then((_) => {
							resolve(this.path + directorio + '/');
						})
						.catch((err) => {
							reject(err);
						});
				});
		});
	}

	delFileDirectory(arrPath: any = [], objArchivo: any = []): Promise<any> {
		return this.rutaMem(arrPath).then((ruta) => {
			if (objArchivo['isDirectory']) {
				return this.file
					.removeRecursively(this.path + ruta, objArchivo['name'])
					.then((_) => {
						return true;
					})
					.catch((err) => {
						alert('Archivo doesnt exist' + JSON.stringify(err));
						return err;
					});
			} else {
				return this.file
					.removeFile(this.path + ruta, objArchivo['name'])
					.then((_) => {
						return true;
					})
					.catch((err) => {
						alert('Archivo doesnt exist' + JSON.stringify(err));
						return err;
					});
			}
		});
	}

	getFile(directorio: string, archivo: string): Promise<any> {
		let extensiones: any = archivo.split('.');
		let tipoArchivo = this.mimeTypes[extensiones[extensiones.length - 1]];
		return this.file
			.checkDir(this.path, directorio)
			.then((_) => {
				this.fileOpener
					.open(this.path + directorio + '/' + archivo, tipoArchivo)
					.then(() => {
						return true;
					})
					.catch((e) => {
						return e;
					});
			})
			.catch((err) => {
				alert('Archivo doesnt exist' + JSON.stringify(err));
				return err;
			});
	}

	compartir(directorio: string, archivo: string) {
		this.socialSharing.share('', '', this.path + directorio + '/' + archivo, null);
	}


	//creacion del PDF y descarga.
	procesarDescargasPDF(descargas) {
		console.log("que llega en descargas.. ", descargas)
		let doc = descargas[0].doc;
		let pdfname = descargas[0].titulo;
		return new Promise((resolve, reject) => {
			//pdfMake.createPdf(doc).open();
			pdfMake.createPdf(doc).getBlob(buffer => {
				this.file.checkDir(this.path, 'comercial')
					.then(_ => {
						this.file.writeExistingFile(this.path + 'comercial/', pdfname, buffer)
							.then(function (success) {
								// success
							}, function (error) {
								// error
							});

					})
					.catch(err => {
						this.file.createDir(this.path, 'comercial', true)
							.then(
								(files) => {
									// do something
									// alert("success");
									this.file.writeExistingFile(this.path + 'comercial/', pdfname, buffer)
										.then(function (success) {
											// success
										}, function (error) {
											// error
										});
								}
							).catch(
								(err) => {
									// do something
									//alert("error"); // i am invoking only this part
								}
							);
					});

			});
			resolve();
		});
	}


	compartirAdjuntos(directorio: string = '', mensaje: string = '', asunto: string = '', archivos: any = []) {
		let adjuntos: any = archivos;
		if (isObject(archivos)) {
			adjuntos = [];
			for (let i = 0; i < archivos.length; i++) {
				let arc = this.path + directorio + '/' + archivos[i];
				adjuntos.push(arc);
			}
		}
		return new Promise((resolve, reject) => {
			this.socialSharing
				.share(mensaje, asunto, adjuntos, null)
				.then((r) => {
					resolve(r);
				})
				.catch((e) => {
					reject(e);
				});
		});
	}

	private rutaMem(dirPath): Promise<any> {
		return new Promise((resolve, reject) => {
			let miRuta = '';
			for (let t = 0; t < dirPath.length; t++) {
				if (t > 0) {
					miRuta += '/';
				}
				miRuta += dirPath[t];
			}
			resolve(miRuta);
		});
	}
}
