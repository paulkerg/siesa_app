import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as io from 'socket.io-client';
import { APP_CONFIG } from './../../config/app.config';

export interface ChatMessage {
	to: string;
	from: string;
	message: string;
}

@Injectable()
export class SocketServiceProvider {
  socketObserver: any; 
  socketService: any;
  socket: any;
  user: any;
  data: any = null;
  socketHost: string = APP_CONFIG.io.url;

  constructor() {
    this.socketService = Observable.create(observer => {
        this.socketObserver = observer;
      });
  }


  initialize(){
    this.socket = io.connect(this.socketHost);
    
    this.socket.on("connect", (msg) => {
      console.log('on connect');
      this.socketObserver.next({ category: 'connect', message: 'user connected'});
    });

    this.socket.on("reconnecting", (msg) => {
      console.log('on reconnecting');
    });

    this.socket.on("reconnect_error", (msg) => {
      console.log('on reconnect_error');
    });
    
    this.socket.on("reconnect_failed", (msg) => {
      console.log('on reconnect_failed');
    });

     this.socket.on('disconnect', function () {
      console.log('user disconnected');
      // io.emit('user disconnected');
    });

    this.socket.on("message", (msg) => {
      this.socketObserver.next({ category: 'message', message: msg });
    }); //end of socket.on('message')

  }

  registro(id) {
    this.socket.emit('register', id, (callback)=>{
      console.log(callback);
    });
//    this.socketObserver.next({ category: 'sendMessage', message: message });

  }

  sendMessage(message: ChatMessage) {
    // console.log('in sendMessage and socket is: ', this.socket);
    this.socket.emit('message', message);
    this.socketObserver.next({ category: 'sendMessage', message: message });

  }

}
