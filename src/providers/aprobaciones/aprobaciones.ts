import { Injectable } from '@angular/core';
import { SincronizacionProvider } from '../sincronizacion/sincronizacion';
import { global } from '../../app/global';
import * as moment from 'moment';
import 'moment/src/locale/es';
import { SqliteProvider } from '../sqlite/sqlite';

@Injectable()
export class AprobacionesProvider {

  constructor(private sync: SincronizacionProvider, private dbSql: SqliteProvider) {
    moment.locale('es');
    let sql = "select documento from configuraciones where id = 'usuario_siesapps'";
    this.dbSql.query(sql).then((resp) => {
      let jsonDcto = JSON.parse(resp[0]['documento']);
      if (resp) {
        global.dispositivo.idFuncionario = jsonDcto['f_id_funcionario'];
      }
    });
  }

  contarAprobaciones() {
    return new Promise((resolve, reject) => {
      let parametros =
        global.dispositivo.idFuncionario +
        '|';
      let p1 = {
        'Parametros': parametros,
        'Consulta': 'SIESAPPS_CONS_APRO_DCTOS_CONSEC'
      };
      this.sync.postParameters(global.api.consulta, p1)
        .then((r: any[]) => {
          resolve(r);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  leerAprobaciones(id_grupo_clase_docto, nro_consulta, estado, id_detalle, rowid) {
    return new Promise((resolve, reject) => {
      let resultset = 0;
      if (rowid != '') {
        resultset = 9999;
      }
      let parametros =
        global.dispositivo.idFuncionario +
        '|' +
        estado +
        '|0|' +
        id_grupo_clase_docto +
        '|' +
        nro_consulta +
        '|' +
        id_detalle +
        '|' +
        rowid +
        '|';
      let p1 = {
        'Parametros': parametros,
        'Consulta': 'SIESAPPS_CONS_APRO_DCTOS'
      };
      this.sync.postParameters(global.api.consulta, p1, resultset)
        .then((r: any[]) => {
          resolve(r);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
