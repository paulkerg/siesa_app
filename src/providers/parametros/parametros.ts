import { Injectable } from '@angular/core';
import { parVendedor } from './../../models/interfaces/parametros';
import { emp } from './../../models/interfaces/empresa';
import { SqliteProvider } from '../sqlite/sqlite';
import { pedidoDetalle } from '../../models/interfaces/pedidoDetalle';

@Injectable()
export class ParametrosProvider {
  constructor(private ldb: SqliteProvider) { }

  public leer() {
    let sql = 'SELECT * FROM t2181_sm_vend_siesamobile'
    return this.ldb.queryExec(sql)
      .then(response => {
        console.log("parametros.response... ", response)
        //console.log(r)
        if (response.rows.length > 0) {
          let r = response.rows[0];
          parVendedor.f2181_id_vendedor = r.f2181_id_vendedor;
          parVendedor.f2181_razon_social = r.f2181_razon_social;
          parVendedor.f2181_id_co = r.f2181_id_co;
          parVendedor.f2181_id_grupo_bodega_pedido = r.f2181_id_grupo_bodega_pedido;
          parVendedor.f2181_id_cargue_pedido = r.f2181_id_cargue_pedido;
          parVendedor.f2181_id_tipo_docto_pedido = r.f2181_id_tipo_docto_pedido;
          parVendedor.f2181_rowid_bodega_pedido = r.f2181_rowid_bodega_pedido;
          parVendedor.f2181_id_co_movto = r.f2181_id_co_movto;
          parVendedor.f2181_id_tipo_docto_recibo = r.f2181_id_tipo_docto_recibo;
          parVendedor.f2181_id_caja = r.f2181_id_caja;
          parVendedor.f2181_id_tipo_docto_fact = r.f2181_id_tipo_docto_fact;
          parVendedor.f2181_rowid_bodega_fact = r.f2181_rowid_bodega_fact;
          parVendedor.f2181_id_ubicacion_fact = r.f2181_id_ubicacion_fact;
          parVendedor.f2181_id_cargue_fact = r.f2181_id_cargue_fact;
          parVendedor.f2181_id_co_movto_fact = r.f2181_id_co_movto;
          parVendedor.f2181_rowid_ccosto_fact = r.f2181_rowid_ccosto_fact;
          parVendedor.f2181_rowid_ccosto_pedido = r.f2181_rowid_ccosto_pedido;
	      	pedidoDetalle.f431_id_co_movto=r.f2181_id_co_movto;

        }
        let sql = 'SELECT * FROM t100_pp_comerciales AS t100, t5785_sm_perfil_parametros AS t5785'
        return this.ldb.queryExec(sql)
          .then(response => {
            return Promise.resolve(response);
          })
          .catch(error => {
            return Promise.reject(error)
          });

      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

  cargaPerfilParametros(resultado: any) {
    //console.log(r)
    if (resultado.rows.length > 0) {
      let r = resultado.rows[0];
      parVendedor.f5785_ind_hist_pedidos = r.f5785_ind_hist_pedidos;
      parVendedor.f5785_ind_hist_facturas = r.f5785_ind_hist_facturas;
      parVendedor.f5785_vlr_minimo_vta = r.f5785_vlr_minimo_vta;
      parVendedor.f5785_id_portafolio_items = r.f5785_id_portafolio_items;
      parVendedor.f5785_id_portafolio_obsq = r.f5785_id_portafolio_obsq;
      parVendedor.f5785_ind_cartera_transmitir = r.f5785_ind_cartera_transmitir;
      parVendedor.f5785_id_lista_precio_default = r.f5785_id_lista_precio_default;
      parVendedor.f5785_id_concepto = r.f5785_id_concepto;
      parVendedor.f5785_id_motivo_venta = r.f5785_id_motivo_venta;
      parVendedor.f5785_id_motivo_obsequio = r.f5785_id_motivo_obsequio;
      parVendedor.f100_ind_lista_precio = r.f100_ind_lista_precio;
      parVendedor.f100_id_unidad_peso = r.f100_id_unidad_peso;
      parVendedor.f100_id_unidad_volumen = r.f100_id_unidad_volumen;
      parVendedor.f100_id_lista_precio_vta = r.f100_id_lista_precio_vta;
      parVendedor.f100_id_lista_precio_imp_asum = r.f100_id_lista_precio_imp_asum;
      parVendedor.f100_id_dscto_global1 = r.f100_id_dscto_global1;
      parVendedor.f100_id_dscto_global2 = r.f100_id_dscto_global2;
    }
    let sql = 'SELECT * FROM t5784_sm_perfiles_metodo';
    this.ldb.queryExec(sql)
      .then(response => {
        this.cargaPerfilMetodos(response);
      })
      .catch(error => {
        console.log(error);
      });

  }
  cargaPerfilMetodos(resultado: any) {
    var len = resultado.rows.length;
    if (len > 0) {
      for (var i = 0; i < len; i++) {
        var r = resultado.rows[i];
        switch (r.f5784_id_metodo) {
          case 1:
            parVendedor.f5784_ind_bloqueo_cliente = 1;
          case 10:
            parVendedor.f5784_ind_cli_x_uni_neg = 1;
            break;
          case 100://--- Precios
            parVendedor.f5784_ind_modifica_precio = 1;
            break;
          case 101://--- Descuentos
            parVendedor.f5784_ind_modifica_dscto = 1;
            break;
          case 102://--- Condicion de Pago 
            parVendedor.f5784_ind_modifica_cpgo = 1;
            break;
          case 103://--- Obsequio Items
            parVendedor.f5784_ind_obsequia_items = 1;
            break;
          case 104://--- Bodega
            parVendedor.f5784_ind_cambiar_bodega = 1;
            break;
          case 105://--- Motivo
            parVendedor.f5784_ind_cambiar_motivo = 1;
            break;
          case 108://--- Unidad de medida default
            parVendedor.f5784_ind_um_venta_instalacion = 1;
            break;
          case 109://--- Valida unidad factor unidad de empaque
            parVendedor.f5784_ind_um_empaque_ppal = 1;
            break;
          case 110://--- Unidad de medida default
            parVendedor.f5784_ind_portafolio = 1;
            break;
          default:
            '';
        }
      }
      //console.log(parVendedor)
      let sql = 'SELECT * FROM t010_mm_companias'
      this.ldb.queryExec(sql)
        .then(response => {
          r = response.rows[0];
          emp.f010_id = r.f010_id;
          emp.f010_razon_social = r.f010_razon_social;
          emp.f010_nit = r.f010_nit;
          emp.f010_rowid_contacto = r.f010_rowid_contacto;
          emp.f010_id_moneda_local = r.f010_id_moneda_local;
          emp.f010_id_moneda_conversion = r.f010_id_moneda_conversion;
          emp.f010_id_plan_cuentas = r.f010_id_plan_cuentas;
          emp.f010_rowid_tercero_contab = r.f010_rowid_tercero_contab;
          emp.f010_ciiu = r.f010_ciiu;
          emp.f010_dv_nit = r.f010_dv_nit;
        })
        .catch(error => {
          console.log(error);
        });

    }
  }

}
