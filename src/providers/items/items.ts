import { Injectable } from '@angular/core';
import { SqliteProvider } from '../sqlite/sqlite';

@Injectable()
export class ItemsProvider {

  constructor(private ldb: SqliteProvider) { }

  getItemsExt(tipoBus: string = "CODIGO", pg: number = 0, txtBuscar: string = "",
    rowidPed: number = 0, rowidFac: number = 0) {
    //moment(Date()).format('YYYY-MM-DD')
    let id_ext = txtBuscar.split("_");

    let sql = '', filtroJoin = '', filtroOrder = '', order_ext = '', cant_pedida = '', filtro_ext = '', filtro_fac = '';
    let inner_bodega = "";
    let v_existencias = " ifnull((SELECT sum(f400_cant_existencia_1) FROM t400_cm_existencia where f400_rowid_item_ext = ie.f121_rowid GROUP BY f400_rowid_item_ext),0) f400_existencias, ";
    // let v_cant_vendidas = " 0 f400_cant_vendida, ";

    if (rowidFac != 0) {
      filtro_fac = " and (f401_cant_existencia_1 - f401_cant_vendida) > 0 ";
      v_existencias = " (f401_cant_existencia_1 - f401_cant_vendida) f400_existencias, ";
      inner_bodega = " INNER JOIN t401_cm_existencia_lote ON ie.f121_rowid = t401_cm_existencia_lote.f401_rowid_item_ext ";
    }

    if (tipoBus.toUpperCase() == "CODIGO") {
      if (id_ext[1] != undefined) {
        filtro_ext += ' and ie.f121_id_ext1_detalle LIKE "%' + id_ext[1] + '%"';
        order_ext += ',ie.f121_id_ext1_detalle,ie.f121_id_ext2_detalle ';
      }
      if (id_ext[2] != undefined) {
        filtro_ext += ' and ie.f121_id_ext2_detalle LIKE "%' + id_ext[2] + '%"';
      }
      filtroOrder = ' i.f120_id LIKE "' + id_ext[0] + '%" ' + filtro_ext + filtro_fac +
        ' GROUP BY ie.f121_rowid ORDER BY i.f120_id ' + order_ext;
    } else if (tipoBus.toUpperCase() == "DESCRIPCION") {
      if (id_ext[1] != undefined) {
        filtro_ext += ' and ie.f121_desc_ext1_detalle LIKE "%' + id_ext[1] + '%"';
        order_ext += ',ie.f121_desc_ext1_detalle,ie.f121_desc_ext2_detalle ';
      }
      if (id_ext[2] != undefined) {
        filtro_ext += ' and ie.f121_desc_ext2_detalle LIKE "%' + id_ext[2] + '%"';
      }
      filtroOrder = ' ie.f121_descripcion_toda LIKE "' + id_ext[0] + '%"' + filtro_ext + filtro_fac +
        ' GROUP BY ie.f121_rowid ORDER BY ie.f121_descripcion_toda ' + order_ext;
    } else if (tipoBus.toUpperCase() == "REFERENCIA") {
      if (id_ext[1] != undefined) {
        filtro_ext += ' and ie.f121_id_ext1_detalle LIKE "%' + id_ext[1] + '%"';
        order_ext += ',ie.f121_id_ext1_detalle,ie.f121_id_ext2_detalle ';
      }
      if (id_ext[2] != undefined) {
        filtro_ext += ' and ie.f121_id_ext2_detalle LIKE "%' + id_ext[2] + '%"';
      }
      filtroOrder = ' i.f120_referencia LIKE "' + id_ext[0] + '%"' + filtro_ext + filtro_fac +
        ' GROUP BY ie.f121_rowid ORDER BY i.f120_referencia ' + order_ext;
    } else if (tipoBus.toUpperCase() == "CRI" || tipoBus.toUpperCase() == "CATALOGO") {
      filtroJoin = " INNER JOIN t125_mc_items_criterios AS t125 ON t125.f125_rowid_item = i.f120_rowid ";
      filtroOrder = " t125.f125_id_plan = '" + id_ext[0] + "' AND t125.f125_id_criterio_mayor = '" + id_ext[1] + "'" + filtro_fac +
        " GROUP BY ie.f121_descripcion_toda" +
        " ORDER BY ie.f121_descripcion_toda";
    } else if (tipoBus.toUpperCase() == "PORTAFOLIO") {
      filtroJoin = " INNER JOIN t137_mc_portafolio_items AS t125 ON f137_rowid_item_ext = ie.f121_rowid ";
      filtroOrder = " f137_id_portafolio = '" + id_ext[0] + "'" + filtro_fac +
        " ORDER BY f137_secuencia";
    }

    if (rowidPed != 0) {
      cant_pedida = " ifnull((SELECT sum(f431_cant1_pedida/f431_factor) FROM t431_cm_pv_movto where f431_rowid_pv_docto =" +
        rowidPed + " and f431_rowid_item_ext = ie.f121_rowid),0) cant1_pedida,  "
    }

    if (rowidFac != 0) {
      cant_pedida = " ifnull((SELECT sum(f470_cant_1 * f470_factor) FROM t470_cm_movto_invent where f470_rowid_docto = " +
        rowidFac + " and f470_rowid_docto = ie.f121_rowid),0) cant1_pedida, ";
    }

    sql = 'SELECT DISTINCT ie.f121_rowid,i.f120_rowid,i.f120_id,i.f120_referencia,i.f120_descripcion,ie.f121_descripcion_toda,' +
      v_existencias +
      cant_pedida +
      ' i.f120_id_unidad_inventario,i.f120_id_unidad_adicional,i.f120_id_unidad_empaque,ie.f121_id_extension1,ie.f121_id_extension2' +
      ' ,i.f120_ind_lista_precios_ext,ie.f121_desc_ext1_detalle,ie.f121_desc_ext2_detalle' +
      ' ,t5801_ff_fotos_baja_calidad.f5801_foto,f121_id_ext1_detalle,f121_id_ext2_detalle, t122.f122_factor, t122.f122_id_unidad,t122UE.f122_id_unidad f122_id_unidad_empaque ,t122UE.f122_factor f122_factor_empaque' +
      ' FROM t120_mc_items AS i' +
      ' INNER JOIN t121_mc_items_extensiones AS ie ON i.f120_rowid = ie.f121_rowid_item' +
      ' INNER JOIN t122_mc_items_unidades AS t122 ON t122.f122_rowid_item = i.f120_rowid' +
      '        AND t122.f122_id_unidad = f120_id_unidad_inventario' +
      ' LEFT JOIN t5801_ff_fotos_baja_calidad ON i.f120_rowid_foto = t5801_ff_fotos_baja_calidad.f5801_rowid_foto ' +
      ' LEFT JOIN t122_mc_items_unidades AS t122UE ON t122UE.f122_rowid_item = i.f120_rowid' +
      '    AND t122UE.f122_id_unidad = f120_id_unidad_empaque ' +
      inner_bodega + filtroJoin + ' where ' + filtroOrder + " LIMIT 10 OFFSET  10 * " + pg;
    console.log(sql)
    return this.ldb.queryExec(sql)
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error)
      });
  }

}
