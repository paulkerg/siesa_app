import { Injectable } from '@angular/core';
import { SincronizacionProvider } from '../sincronizacion/sincronizacion';
import { global } from '../../app/global';
import * as moment from 'moment';
import 'moment/src/locale/es';

@Injectable()
export class ConsultasFlexProvider {
	constructor(private sync: SincronizacionProvider) {
		moment.locale('es');
	}

	leerEncabezados() {
		let result = [];
		return new Promise((resolve, reject) => {
			let p1 = {
				'Parametros': global.dispositivo.idUsuario + '|',
				'Consulta': 'CONSULTAS_ENC'
			};
			this.sync.postParameters(global.api.consulta, p1)
				.then((r: any[]) => {
					for (let i = 0; i < r.length; i++) {
						result.push({
							tipo_cons: r[i]['f_tipo'],
							tipo_grafica: global.tipoGraficas[r[i]['f_nombre'].split('_')[1] || 'torta'],
							grafica: r[i]['f_nombre'].split('_')[1] || 'torta',
							nombre_origen: r[i]['f_nombre'],
							nombre: r[i]['f_nombre'].split('_')[2] || r[i]['f_nombre']
						});
					}
					resolve(result);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	leerEncabezadoCampos(par) {
		return new Promise((resolve, reject) => {
			let parametros =
				global.dispositivo.idUsuario +
				'|' +
				global.dispositivo.idUsuario +
				'|' +
				par['tipo_cons'] +
				'|' +
				par['consulta'] +
				'|';

			let p2 = {
				'Parametros': parametros,
				'Consulta': 'CONSULTAS_ENC_CAMPOS'
			};
			this.sync.postParameters(global.api.consulta, p2)
				.then((r: any[]) => {
					resolve(r);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	leerConsultaDatos(par) {
		return new Promise((resolve, reject) => {
			let parametros =
				global.dispositivo.idUsuario +
				'|' +
				global.dispositivo.idUsuario +
				'|' +
				par['tipo_cons'] +
				'|' +
				par['consulta'] +
				'|' +
				moment(par['fechaIni']).format('YYYY-MM-DD') +
				'|' +
				moment(par['fechaFin']).format('YYYY-MM-DD') +
				'|';
			let p3 = {
				'Parametros': parametros,
				'Consulta': 'CONSULTAS_ENC_CAMPOS'
			};
			this.sync.postParameters(global.api.consulta, p3)
				.then((r: any[]) => {
					resolve(r);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}
}
