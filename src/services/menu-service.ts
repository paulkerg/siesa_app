import { Injectable } from '@angular/core';
import { IService } from './IService';
import { SqliteProvider } from '../providers/sqlite/sqlite';

@Injectable()
export class MenuService implements IService {
	constructor(private dbSql: SqliteProvider) { }

	getId = (): string => 'menu';

	getTitle = (): any => {
		return new Promise((resolve, reject) => {
			let sql = "select documento from configuraciones where id = 'usuario'";
			this.dbSql.query(sql).then((resp) => {
				if (resp.length > 0) {
					let usuario = JSON.parse(resp[0].documento);
					resolve(usuario['f_tercero_nombre']);
				} else {
					resolve(false);
				}
			});
		});
	};

	getFoto = (): any => {
		return new Promise((resolve, reject) => {
			let sql = "select documento from configuraciones where id = 'usuario'";
			this.dbSql.query(sql).then((resp) => {
				if (resp.length > 0) {
					let usuario = JSON.parse(resp[0].documento);
					resolve(usuario['f_tercero_foto']);
				} else {
					resolve(false);
				}
			});
		});
	};

	getAllMenu = (): any => {
		return new Promise((resolve, reject) => {
			let sql = "select documento from configuraciones where id = 'menu'";
			this.dbSql.query(sql).then((resp) => {
				if (resp.length > 0) {
					let mimenu = JSON.parse(resp[0]['documento'])
					resolve(mimenu);
				} else {
					resolve(false);
				}
			});
		});
	};

	getUsuarioMenu = (): any => {
		return new Promise((resolve, reject) => {
			let sql = "select documento from configuraciones where id = 'usuario'"
				+ " UNION ALL "
				+ " select documento from configuraciones where id = 'menu'";
			this.dbSql.query(sql).then((resp) => {
				if (resp.length > 0) {
					resolve(resp);
				} else {
					resolve([]);
				}
			});
		});
	};

	getAllThemes = (): Array<any> => {
		return [];
	};

	getDataForTheme = (menuItem: any): Array<any> => {
		return [];
	};

	getEventsForTheme = (menuItem: any): any => {
		return {};
	};

	prepareParams = (item: any) => {
		return {
			title: item.title,
			data: this.getDataForTheme(item),
			events: this.getEventsForTheme(item)
		};
	};
}
