import { Device } from '@ionic-native/device';

export class DeviceMock extends Device {
	get uuid(): string {
		if (document.defaultView['chrome']) {
			return 'c81a85a5-e2a1-4f3d-9c24-94fb7865b4a1';
		}else{
			return 'c81a85a5-e2a1-4f3d-9c24-94fb7865b4a1-ios';
		}
	}
	get version(): string {
		return '1';
	}
	get platform(): string {
		return 'browser';
	}
	get serial(): string {
		return '1';
	}
	get cordova(): string {
		return '4';
	}
	get model(): string {
		return '1';
	}
	get manufacturer(): string {
		return '1';
	}
	get isVirtual(): boolean {
		return true;
	}
}
