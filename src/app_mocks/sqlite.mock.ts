import { SQLiteDatabaseConfig } from "@ionic-native/sqlite";

const win: any = window;

export class SQLiteMock {
  public create(config: SQLiteDatabaseConfig): Promise<SQLiteObject> {
    //console.log("Creando Mock SQLite Database.",win);
    var db = win.openDatabase(config.name, "1.0", "database", 8 * 1024 * 1024);

    return new Promise((resolve, reject) => {
      resolve(new SQLiteObject(db));
    });
  }
}

class SQLiteObject {
  _objectInstance: any;

  constructor(_objectInstance: any) {
    this._objectInstance = _objectInstance;
  }

  executeSql(statement: string, params: any): Promise<any> {
    //console.log("Mock SQLite executeSql: " , win);
    return new Promise((resolve, reject) => {
      this._objectInstance.transaction(
        (tx: any) => {
          tx.executeSql(
            statement,
            params,
            (tx: any, res: any) => {
              resolve(res);
            },
            (tx: any, err: any) => {
              console.log('ERROR',err, statement)
              reject(err);
            }
          );
        },
        (err: any) => reject(err)
      );
    });
  }

  sqlBatch(array: Array<string | string[] | any>): Promise<any> {
    return new Promise((resolve, reject) => {
      this._objectInstance.transaction(
        (tx: any) => {
          for (let a = 0; a < array.length; a++) {
            tx.executeSql(
              array[a],
              [],
              (tx: any, res: any) => {
                resolve(res);
              },
              (tx: any, err: any) => {
                console.log('ERROR',err,array[a])
                reject(err);
              }
            );
          }
        },
        (err: any) => reject(err)
      );
    });
  }
}
