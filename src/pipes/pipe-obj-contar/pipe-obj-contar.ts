import { Pipe, PipeTransform } from '@angular/core';
import { count } from '../generales/utils/utils';

@Pipe({
  name: 'pipeObjContar',
})
export class PipeObjContarPipe implements PipeTransform {
  transform(input: any): any {
    return count(input);
  }
}
