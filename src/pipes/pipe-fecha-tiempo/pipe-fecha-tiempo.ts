import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

const momentConstructor = moment;

@Pipe({
  name: 'pipeFechaTiempo',
})
export class PipeFechaTiempoPipe implements PipeTransform {
  constructor() {
    moment.locale("es");
  }

  transform(value: Date | moment.Moment | string | number, ...args: any[]): string {
    if (!value) { return ''; }
    return momentConstructor(value).format("HH:mm");
  }
}
