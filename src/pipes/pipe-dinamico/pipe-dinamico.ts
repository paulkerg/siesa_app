import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'pipeDinamico',
})
export class PipeDinamicoPipe implements PipeTransform {
  constructor() {
    moment.locale("es");
  }

  transform(value: string, modifier: string) {
    if (!modifier) return value;
    // Evaluate pipe string
    return eval('this.' + modifier + '(\'' + value + '\')')
  }

  // Returns 'enabled' or 'disabled' based on input value
  statusFromBoolean(value: string): string {
    switch (value) {
      case 'true':
      case '1':
        return 'habilitado';
      default:
        return 'deshabilitado';
    }
  }

  // Retorna el numero formateado 
  numero(value: number): string {
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  // Retorna el numero formateado  con el signo pesos
  moneda(value: number): string {
    return '$' + value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  // Returns a human friendly time format e.g: '14 minutes ago', 'yesterday'
  humanizeDate(value: string): string {
    // Humanize if date difference is within a week from now else returns 'December 20, 2016' format
    if (moment().diff(moment(value), 'days') < 8) return moment(value).fromNow();
    return moment(value).format('MMMM Do YYYY');
  }

}
