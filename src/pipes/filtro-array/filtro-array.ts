import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroArray',
})
export class FiltroArrayPipe implements PipeTransform {
  /**
   * Perform the filtering.
   * 
   * @param items The Archivo to compare to the filter.
   * @param filter The filter to apply.
   * @return {boolean} True if Archivo satisfies filters, false if not.
   */
  transform(items: any, filter: any): any {
    if (filter && Array.isArray(items)) {
        let filterKeys = Object.keys(filter);
        return items.filter(item =>
            filterKeys.reduce((memo, keyName) =>
                (memo && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === "", true));
    } else {
        return items;
    }
  }
}
