import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'

@Pipe({
  name: 'pipeFechaEstandar',
})
export class PipeFechaEstandarPipe implements PipeTransform {
  constructor(){
    moment.locale("es");
  }

  transform(date: any, args?: any): any {
    let d = new Date(date)
    return moment(d).format('YYYY/MM/DD')
  }

}
