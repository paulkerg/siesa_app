import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

const momentConstructor = moment;

@Pipe({
	name: 'pipeFechaDiferencia'
})
export class PipeFechaDiferenciaPipe implements PipeTransform {
	constructor() {
		moment.locale('es');
	}

	transform(
		value: Date | moment.Moment,
		otherValue: Date | moment.Moment,
		unit?: moment.unitOfTime.Diff,
		precision?: boolean
	): number {
		const date = momentConstructor(value);
		const date2 = otherValue !== null ? momentConstructor(otherValue) : momentConstructor();
		return date.diff(date2, unit, precision);
	}
}
