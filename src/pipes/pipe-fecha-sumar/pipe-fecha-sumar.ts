import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

const momentConstructor = moment;
const unidades: any = {
  anos: "years",
  meses: "month",
  dias: "day",
  horas: "hours"
};

@Pipe({
  name: 'pipeFechaSumar',
})
export class PipeFechaSumarPipe implements PipeTransform {
  constructor() {
    moment.locale("es");
  }

  transform(value: any, amount: moment.DurationInputArg1, unit?: moment.DurationInputArg2): any {
    if (typeof amount === 'undefined' || (typeof amount === 'number' && typeof unit === 'undefined')) {
      throw new Error('pipeFechaRestar: Argumentos requeridos');
    }
    return momentConstructor(value).add(amount, unidades[unit]);
  }
}
