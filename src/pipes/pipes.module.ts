import { NgModule } from '@angular/core';
import { FiltroArrayPipe } from './filtro-array/filtro-array';
import { NgPipesModule } from './generales/pipes.module';
import { PipeFechaEstandarPipe } from './pipe-fecha-estandar/pipe-fecha-estandar';
import { PipeFechaHaceUnTiempoPipe } from './pipe-fecha-hace-un-tiempo/pipe-fecha-hace-un-tiempo';
import { PipeFechaRestarPipe } from './pipe-fecha-restar/pipe-fecha-restar';
import { PipeFechaSumarPipe } from './pipe-fecha-sumar/pipe-fecha-sumar';
import { PipeFechaPeriodoPipe } from './pipe-fecha-periodo/pipe-fecha-periodo';
import { PipeFechaTiempoPipe } from './pipe-fecha-tiempo/pipe-fecha-tiempo';
import { PipeFechaFormatearAPipe } from './pipe-fecha-formatear-a/pipe-fecha-formatear-a';
import { PipeFechaDiferenciaPipe } from './pipe-fecha-diferencia/pipe-fecha-diferencia';
import { PipeObjAgruparPorPipe } from './pipe-obj-agrupar-por/pipe-obj-agrupar-por';
import { PipeObjOrdenarPorPipe } from './pipe-obj-ordenar-por/pipe-obj-ordenar-por';
import { PipeObjContarPipe } from './pipe-obj-contar/pipe-obj-contar';
import { PipeMonedaPipe } from './pipe-moneda/pipe-moneda';
import { PipeDinamicoPipe } from './pipe-dinamico/pipe-dinamico';
@NgModule({
	declarations: [
		FiltroArrayPipe,
    PipeFechaEstandarPipe,
    PipeFechaHaceUnTiempoPipe,
    PipeFechaRestarPipe,
    PipeFechaSumarPipe,
    PipeFechaPeriodoPipe,
    PipeFechaTiempoPipe,
    PipeFechaFormatearAPipe,
    PipeFechaDiferenciaPipe,
    PipeObjAgruparPorPipe,
    PipeObjOrdenarPorPipe,
    PipeObjContarPipe,
    PipeMonedaPipe,
    PipeDinamicoPipe,
	],
	imports: [
		NgPipesModule,
	],
	exports: [
		NgPipesModule,
		FiltroArrayPipe,
    PipeFechaEstandarPipe,
    PipeFechaHaceUnTiempoPipe,
    PipeFechaRestarPipe,
    PipeFechaSumarPipe,
    PipeFechaPeriodoPipe,
    PipeFechaTiempoPipe,
    PipeFechaFormatearAPipe,
    PipeFechaDiferenciaPipe,
    PipeObjAgruparPorPipe,
    PipeObjOrdenarPorPipe,
    PipeObjContarPipe,
    PipeMonedaPipe,
    PipeDinamicoPipe,
	]
})
export class PipesModule { }
