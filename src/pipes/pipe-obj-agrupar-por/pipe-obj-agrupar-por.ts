import { Pipe, PipeTransform } from '@angular/core';
import { getProperty, isArray, isUndefined } from '../generales/utils/utils';

@Pipe({
  name: 'pipeObjAgruparPor',
})
export class PipeObjAgruparPorPipe implements PipeTransform {
  transform(input: any, prop: string): Array<any> {

    if (!isArray(input)) {
      return input;
    }

    const arreglo: { [llave: string]: Array<any> } = {};

    for (const value of input) {
      const campo: any = getProperty(value, prop);

      if (isUndefined(arreglo[campo])) {
        arreglo[campo] = [];
      }

      arreglo[campo].push(value);
    }

    return Object.keys(arreglo).map(llave => ({ llave, 'value': arreglo[llave] }));
  }
}
