import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ElasticHeaderDirective } from './elastic-header/elastic-header';
@NgModule({
	declarations: [ ElasticHeaderDirective ],
	imports: [],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	exports: [ ElasticHeaderDirective ]
})
export class DirectivesModule {}
