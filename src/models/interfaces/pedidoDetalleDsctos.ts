export var pedidoDetalleDsctos = {
    f432_rowid_pv_movto  			: null,
    f432_orden						: 0,
    f432_ind_tipo_dscto				: 0,
    f432_tasa						: 0,
    f432_vlr_uni 					: 0,
    f432_rowid_promo_dscto_linea  	: null,
    f432_fecha_inicial_promo 		: null,
    f432_fecha_final_promo  		: null,
    f432_rowid_pv_movto_obsequio	: null,
    f432_rowid_item_ext_obsequio  	: null,
    f432_id_concepto_obsequio		: null,
    f432_id_motivo_obsequio			: null,
    f432_id_um_obsequio  			: 0,
    f432_cant1_obsequio				: 0
};