import { parVendedor } from './parametros';
import * as moment from 'moment';

export var pedidoDetalle = {
    f431_ts: moment().format('YYYY-MM-DD HH:mm:ss.000'),
    f431_rowid: null,
    f431_rowid_pv_docto: null,
    f431_rowid_item_ext: null,
    f431_rowid_bodega: null,
    f431_id_concepto: 501,
    f431_id_motivo: null,
    f431_ind_obsequio: 0,
    f431_id_lista_precio: '',
    f431_fecha_entrega: moment().format('YYYY-MM-DD'),
    f431_ind_impuesto_precio_venta: 0,
    f431_precio_unitario_base: 0,
    f431_id_unidad_medida: '',
    f431_factor: 0,
    f431_cant1_pedida: 0,
    f431_cant2_pedida: 0,
    f431_notas: '',
    f431_vlr_bruto: 0,
    f431_vlr_dscto_linea: 0,
    f431_vlr_dscto_global: 0,
    f431_vlr_imp: 0,
    f431_vlr_imp_no_apli: 0,
    f431_vlr_neto: 0,
    f431_vlr_imp_margen: 0,
    f431_id_unidad_medida_captura: '',
    f431_rowid_ccosto_movto: parVendedor.f2181_rowid_ccosto_pedido,
    f431_id_un_movto: '',
    f431_id_co_movto: ''
};