export var cliente = {
    f200_apellido1: null,
    f200_apellido2: null,
    f200_dv_nit: null,
    f200_id: null,
    f200_id_tipo_ident: null,
    f200_ind_tipo_tercero: 0,
    f200_nit: null,
    f200_nombre_est: null,
    f200_nombres: null,
    f200_razon_social: null,
    f200_rowid: 0,
    f200_rowid_contacto: 0,
    f200_rowid_foto: null,
    f200_rowid_movto_entidad: null,
    f201_rowid_tercero: 0,
    f201_id_sucursal: '',
    f201_descripcion_sucursal: '',
    f201_ind_estado_bloqueado: 0,
    f201_id_moneda: '',
    f201_rowid_contacto: 0,
    f201_id_cond_pago: '',
    f201_dias_gracia: 0,
    f201_cupo_credito: 0,
    f201_id_tipo_cli: '',
    f201_id_grupo_dscto: '',
    f201_id_lista_precio: '',
    f201_ind_pedido_backorder: 0,
    f201_porc_exceso_venta: 0,
    f201_ind_bloqueo_cupo: 0,
    f201_ind_bloqueo_mora: 0,
    f201_fecha_ingreso: '',
    f201_fecha_cupo: '',
    f201_porc_tolerancia: 0,
    f201_rowid_tercero_corp: null,
    f201_id_sucursal_corp: null,
    f215_rowid: 0, //--- rowid del primer punto de envio del cliente
    f112_ind_imp_precio_venta: 0, //--- Indicador Impuesto Inlcuido
    f015_direccion1: '',
    f201_saldo_rc: 0,
    f201_vlr_pedidos_pen: 0,
    Id_vendedor:'',
    rowid_bodega:'',
    //---
    TipoPed:null,
    NroPed: 0,
    TipoRec:null,
    NroRec: 0,
    TipoFac:null,
    NroFac: 0,
    NroChe: 0,

    SaldoRC: 0,
    SaldoRCChe: 0,
    cupo_credito: 0,
    bloqueadoRestrictivo: false,
    dataHandlerLocal: null,
    id_tipo_cambio: null,
    id_moneda_base: null,
    id_moneda_local: null,
    tasa_base: 0,
    tasa_local: 0,
    ind_forma_conv_base: null,
    ind_forma_conv_local: null,
    existe_tasa_en_fecha: null,
};

export var cp = {
    f208_cuotas: 0,
    f208_ind_modo_periodicidad: 0,
    f208_dias_vcto: 0,
    f208_ind_pronto_pago: 0,
    f208_dias_pronto_pago: 0,
    f208_tasa_descto_pp: 0,
    f208_porcentaje_anticipo: 0
};

export var tcli = {
    f278_id: null,
    f278_descripcion: null,
    f278_rowid_aux_cxc: 0,
    f278_rowid_aux_anticipo: 0,
    f278_rowid_ccosto: 0,
    f278_notas: null,
    f278_id_un: null
};

export var cliUni = {
    f2012_rowid: 0,
    f2012_rowid_tercero: 0,
    f2012_id_sucursal: null,
    f2012_id_co: null,
    f2012_id_un: null,
    f2012_id_vendedor: null,
    f2012_id_cond_pago: null
};

export var h_mt = {
    v_tasa : 0,
    v_id_moneda_base : '',
    v_id_moneda_local : '',
    v_eval_tasa_en_fecha : 0,
    p_id_moneda_base : '',
    p_id_moneda_local : '',
    p_tasa_base : 0,
    p_tasa_local : 0,
    p_ind_forma_conv_base : 0,
    p_ind_forma_conv_local : 0,
    p_existe_tasa_en_fecha : 1,
    p_id_tipo_cambio : '',
    //--- Varios
    v_id_tipo_cambio_default:null,
}

