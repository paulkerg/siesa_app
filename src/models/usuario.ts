interface IUsuario {
    f_id_usuario: string;
    f_id_funcionario: string;
    f_id_vendedor: string;
    f_identificacion: string;
    f_tercero_rowid: number;
    f_tercero_nombre: string;
    f_email: string;
    f_tercero_foto: Blob;
    f_id_perfil: string;
}

interface UsuarioMetodos {
    getId(): string;
    getIdFuncionario(): string;
    getIdVendedor(): string;
    getNombre(): string;
    getFoto(): Blob;
}

export class Usuario implements IUsuario, UsuarioMetodos {
    f_id_usuario: string;
    f_id_funcionario: string;
    f_id_vendedor: string;
    f_identificacion: string;
    f_tercero_rowid: number;
    f_tercero_nombre: string;
    f_email: string;
    f_tercero_foto: Blob;
    f_id_perfil: string;

    constructor(IUsuario) {
        this.f_id_usuario = IUsuario.f_id_usuario;
        this.f_id_funcionario = IUsuario.f_id_funcionario;
        this.f_id_vendedor = IUsuario.f_id_vendedor;
        this.f_identificacion = IUsuario.f_identificacion;
        this.f_tercero_rowid = IUsuario.f_tercero_rowid;
        this.f_tercero_nombre = IUsuario.f_tercero_nombre;
        this.f_email = IUsuario.f_email
        this.f_tercero_foto = IUsuario.f_tercero_foto;
        this.f_id_perfil = IUsuario.f_id_perfil;
    }

    getId(): string {
        return this.f_id_usuario;
    }
    getIdFuncionario(): string {
        return this.f_id_funcionario;
    }
    getIdVendedor(): string {
        return this.f_id_vendedor;
    }
    getNombre(): string {
        return this.f_id_usuario;
    }
    getFoto(): Blob {
        return this.f_tercero_foto;
    }
}
