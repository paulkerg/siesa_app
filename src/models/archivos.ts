export class Archivo {
    constructor(
        public isFile: boolean,
        public isDirectory: boolean,
        public name: string,
        public fullPath: string,
        public nativeURL: string,
        public icono: string,
        public tipoArchivo: string,
        ) { }
}