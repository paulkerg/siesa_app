export let APP_CONFIG = {
	//srvSincronizar: 'http://optimus.siesa.com/SiesaEnterpriseApi/',
	//srvSyncSiesa: 'http://optimus.siesa.com/SiesaEnterpriseApi/',
	srvSincronizar: 'http://190.0.4.98/SiesaEnterpriseApi/',
	srvSyncSiesa: 'http://190.0.4.98/SiesaEnterpriseApi/',
	urlDescargas: 'https://lccastillo72.ddns.net/apps/',
	io: {
		url: 'http://190.0.4.98:3000',
		options: {}
	},
	module_default: {
		scrollAssist: false,
		scrollPadding: false,
		autoFocusAssist: true,
		backButtonText: '',
		iconMode: 'ios',
		modalEnter: 'modal-slide-in',
		modalLeave: 'modal-slide-out',
		tabsPlacement: 'bottom',
		pageTransition: 'ios-transition',
		monthNames: [
			'enero',
			'febrero',
			'marzo',
			'abril',
			'mayo',
			'junio',
			'julio',
			'agsoto',
			'septiembre',
			'octubre',
			'noviembre',
			'diciembre'
		],
		monthShortNames: [ 'ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic' ],
		dayNames: [ 'domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado' ],
		dayShortNames: [ 'dom', 'lun', 'mar', 'mie', 'jue', 'vie', 'sab' ]
	}
};
