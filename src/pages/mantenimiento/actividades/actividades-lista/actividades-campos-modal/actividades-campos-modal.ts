import { MantenimientoServiceProvider } from "./../../../../../providers/mantenimiento-service/mantenimiento-service";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { UtilesProvider } from "../../../../../providers/utiles/utiles";

/**
 * Generated class for the ActividadesCamposModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-actividades-campos-modal",
  templateUrl: "actividades-campos-modal.html"
})
export class ActividadesCamposModalPage {
  campos: any = [];
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    private mantenimiento: MantenimientoServiceProvider,
    private msg: UtilesProvider
  ) {}

  ionViewDidLoad() {}
  ionViewWillEnter() {
    //Consultar los campos guardados en la tabla configuraciones
    this.mantenimiento
      .consultarCamposConfiguraciones()
      .then(res => {
        this.campos = res;
        this.cargarCamposSeleccionados();
      })
      .catch(error => {
        console.error(error);
      });
  }

  cargarCamposSeleccionados() {
    this.campos.forEach(campo => (campo.seleccionado = false));
    this.mantenimiento.obtenerCamposSeleccionados().then((res: any) => {
      // console.log("res campos_seleccionados");
      // console.log(res);
      if (res.length == 0) {
        // console.warn("No hay ningun campo seleccionado");
        this.cargarCamposVerDefecto();
      }
      res.forEach(campo_sel => {
        let campo = this.campos.find(
          campo => campo.c08223_id == campo_sel.c08223_id
        );
        if (campo) {
          campo.seleccionado = true;
        }
      });
    });
  }

  cargarCamposVerDefecto() {
    this.mantenimiento
      .cargarCamposDefectoActividades()
      .then(res => this.cargarCamposSeleccionados());
  }

  regresar() {
    //Aqui se debe de guardar todos los valores de lo que hay seleccionado
    //listar los campos seleccionados y borrar los campos existentes
    //para dejar solo los seleccionados
    let seleccionados = this.campos.filter(campo => campo.seleccionado == true);
    console.log("seleccionados", seleccionados)
    if (seleccionados.length == 0) {
      this.msg.msgAlerta("Debe seleccionar al menos un campo");
      // this.mantenimiento.cargarCamposDefectoActividades()
      //   .then(res => this.cargarCamposSeleccionados())
    } else {
      this.mantenimiento
        .insertarCamposSeleccionados(seleccionados)
        .then(res => this.cargarCamposSeleccionados());
      this.viewCtrl.dismiss().catch(error => console.error(error));
    }
  }
}
