import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActividadesCamposModalPage } from './actividades-campos-modal';

@NgModule({
  declarations: [
    ActividadesCamposModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ActividadesCamposModalPage),
  ],
})
export class ActividadesCamposModalPageModule {}
