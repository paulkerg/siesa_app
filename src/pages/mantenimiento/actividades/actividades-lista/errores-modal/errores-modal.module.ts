import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ErroresModalPage } from './errores-modal';
import { ComponentsModule } from "../../../../../components/components.module";
@NgModule({
  declarations: [
    ErroresModalPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ErroresModalPage),
  ],
})
export class ErroresModalPageModule {}
