import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ViewController } from "ionic-angular";
import { SqliteProvider } from "../../../../../providers/sqlite/sqlite";

/**
 * Generated class for the ErroresModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-errores-modal",
  templateUrl: "errores-modal.html"
})
export class ErroresModalPage {
  actividad_seleccionada: any;
  errores: any;
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    private db: SqliteProvider
  ) {
    this.errores = {
      c08222_error: "Titulo de error",
      c08222_error_detalle: "Detalle del error"
    };
  }

  ionViewDidLoad() {
    // console.log("ionViewDidLoad ErroresModalPage");
  }
  regresar() {
    this.viewCtrl.dismiss().catch(error=>console.error(error));
    // this.navCtrl
    //   .pop()
    //   .then(res => {})
    //   .catch(error => {
    //     console.error("Error al quitar una pantalla");
    //     console.error(error);
    //   });
  }

  ionViewWillEnter() {
    let actividad = this.navParams.get("data").actividad;
    this.cargarErrores(actividad.id);
  }

  cargarErrores(rowid_actividad_ot) {
    let sql = `
      select 
        c08222_error,
        c08222_error_detalle
      from w08222_errores
      where c08222_rowid_actividad_ot =  ${rowid_actividad_ot}
    `;

    this.db.query(sql).then(res => {
      this.errores = res[0];
    });
  }
}
