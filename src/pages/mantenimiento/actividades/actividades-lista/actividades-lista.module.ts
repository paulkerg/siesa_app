import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActividadesListaPage } from './actividades-lista';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [
    ActividadesListaPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ActividadesListaPage),
  ],
})
export class ActividadesListaPageModule {}
