import { TablasSincronizacionServiceProvider } from './../../../../providers/tablas-sincronizacion-service/tablas-sincronizacion-service';
import { MantenimientoServiceProvider } from "./../../../../providers/mantenimiento-service/mantenimiento-service";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  Modal,
  ModalOptions,
  ItemSliding,
  ModalController,
  Events
} from "ionic-angular";
import { global } from "../../../../app/global";
import { UtilesProvider } from "../../../../providers/utiles/utiles";
import { DomSanitizer } from "@angular/platform-browser";

@IonicPage()
@Component({
  selector: "page-actividades-lista",
  templateUrl: "actividades-lista.html"
})
export class ActividadesListaPage {
  prvIdEquipo: number = 0;
  actividad_id: any = 0;
  mostrarBtnLimpiarFiltro: boolean = false;
  actividadSel = {};
  actividades: any = [];
  actividadesTerminadas: any = [];
  actividadesTerminadasMostrar: any = [];
  mostrarBtnEnviar: boolean = false;
  enviandoIndex: number = -1;
  camposVer: any = [];
  campos_seleccionados: any = [];
  sincronizando:boolean = false;
  cuenta_sincronizacion:number=0;
  tabListado: string = "pendientes";
  ind_enviadas: boolean = false;
  constructor(
    public navCtrl: NavController,
    private menu: MenuController,
    public navParams: NavParams,
    public events: Events,
    private msg: UtilesProvider,
    public modal: ModalController,
    private _sanitizer: DomSanitizer,
    private mantenimiento: MantenimientoServiceProvider,
    private tablasSincronizacion: TablasSincronizacionServiceProvider
  ) {
    this.menu = menu;
    // this.menu.enable(true, "menuPrincipal");
    events.subscribe("enviandoIndex", dato => {
      this.enviandoIndex = dato;
    });
    events.subscribe("envioTerminado", data=>{
      console.log("se va a calcular nuevamente las actividades enviadas")
      this.marcarActividadEnviada(data.actividad_id)
    })
    events.subscribe("notificacion_sincronizar",data=>{
      console.log("Se recibe la notificacion");
      // console.log("cuenta_sincronizacion: " + this.cuenta_sincronizacion);
      this.cuenta_sincronizacion++;
      console.warn("global['sincronizando']: ",global['sincronizando'][data.modulo]);
      if (data.modulo && data.modulo.toLowerCase() == "mantenimiento" && !global['sincronizando']){
        console.log("entro a sincronizar")
        global['sincronizando'][data.modulo] = true;  
        this.tablasSincronizacion.iniciarSincronizacion("mantenimiento")
      }
    });
    events.subscribe("terminada_sincronizacion",data=>{
      if(data){
        console.log("termino de hacer la sincronizacion ahora se va a consultar las actividaes locales")
        this.sincronizando = false;
        global['sincronizando'][data.modulo] = false;  
        this.mantenimiento
          .cargarCamposVer()
          .then((res:any) => {
            if (res.length == 0) {
              this.mantenimiento.cargarCamposDefectoActividades().then(response => {
                this.mantenimiento.cargarCamposVer().then(res_defecto => (this.campos_seleccionados = res_defecto))
              })
            }else{
              this.campos_seleccionados = res
            }
          })
          .catch(err => console.error(err));
        this.mantenimiento.obtenerActividades().then(res => {
          this.actividades = res;
        });
      }
    });
  }

  marcarActividadEnviada(actividad_id){
    let terminada = this.actividadesTerminadas.find(
      actividadTerminada => actividadTerminada.id == actividad_id
    );
    terminada.ind_enviado = 1;
    terminada.habilitar_error = 1;
    this.mostrarActividadesEnviadas();
  }


  mostrarActividadesEnviadas(){
    if(this.ind_enviadas){
      this.actividadesTerminadasMostrar = this.actividadesTerminadas
    }else{
      this.actividadesTerminadasMostrar = this.actividadesTerminadas.filter(actividad => actividad.ind_enviado == 0 )
    }
  }

  /**
   * Abre un modal donde se selecccionan los campos para visualizar
   * en el listado de actividades
   */
  seleccionarCampos() {
    console.log("se va a abrir el modal");
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false
    };

    const myModalData = {
      campos: ""
    };
    const myModal: Modal = this.modal.create(
      "ActividadesCamposModalPage",
      { data: myModalData },
      myModalOptions
    );

    myModal.present();

    myModal.onDidDismiss(data => {
      this.mantenimiento
        .cargarCamposVer()
        .then(res => (this.campos_seleccionados = res));
      this.mantenimiento.obtenerActividades().then(res=>{
        this.actividades = res;
      });
      this.mantenimiento.obtenerActividades("terminadas").then(res=>{
        this.actividadesTerminadas = res;
        this.mostrarActividadesEnviadas()
      })
    });

    myModal.onWillDismiss(data => {});
    // this.undo(slidingItem);
  }

  getStyle(style_css) {
    return this._sanitizer.bypassSecurityTrustStyle(style_css);
  }

  limpiarFiltroNFC() {
    this.prvIdEquipo = 0;
    this.mostrarBtnLimpiarFiltro = false;
    // this.mantenimiento.limpiarFiltroNFC();
    this.mantenimiento.obtenerActividades(
      "",
      this.prvIdEquipo,
      this.actividad_id
    ).then(res=>{
      this.actividades = res;
    }).catch(err=>console.error(err));
    this.mantenimiento.obtenerActividades(
      "terminadas",
      this.prvIdEquipo,
      this.actividad_id
    ).then(res=>{
      this.actividadesTerminadas = res
      this.mostrarActividadesEnviadas()
    }).catch(err=>console.error(err));
  }
  mostrarIconoError(actividad) {
    return (
      this.enviandoIndex != actividad.id &&
      (actividad.c0822_ind_enviado == 2 || actividad.c0822_ind_enviado == 3 || actividad.habilitar_error == 0) 
    );
  }

  enviarActividades() {
    // console.warn("Se van a enviar las actividades");
    this.mantenimiento
      .consultarActividadesTerminadasMovimientos()
      .then((actividades_enviar: any) => {
        // console.warn("actividades_enviar");
        // console.warn(actividades_enviar);
        if (actividades_enviar.length == 0) {
          let msj =
            "Ya se han enviado todas las actividades terminadas pendientes";
          this.msg.msgConfirmacion({
            titulo: "Notificacion",
            mensaje: msj
          });
        }
        this.mantenimiento.procesarActividadesEnviar(
          actividades_enviar,
          this.actividadesTerminadas
        );
      })
      .catch(error => {
        console.error("Error al consultar las actividades terminadas");
        console.error(error);
      });
  }

  ocultarBotonEnviar() {
    // console.log("Se va a ocultar el boton de enviar las actividades");
    this.mostrarBtnEnviar = false;
  }
  mostrarBotonEnviar() {
    // console.log("Se va a mostrar el boton de enviar las actividades");
    this.mostrarBtnEnviar = true;
  }

  openMenu() {
    this.menu.open("menuPrincipal");
  }

  ionViewWillEnter() {
    
    this.mantenimiento
      .cargarCamposVer()
      .then((res:any) =>{
       if(res.length==0){
          this.mantenimiento.cargarCamposDefectoActividades().then(response=>{
            this.mantenimiento.cargarCamposVer().then(res_defecto => (this.campos_seleccionados = res_defecto))
          })
       }else{
         this.campos_seleccionados = res
       }
      })
      .catch(err => console.error(err));
    this.mantenimiento
      .consultarCamposDinamicosMantenimiento()
      .then(res => {
        this.mantenimiento
          .cargarCamposVer()
          .then(res => (this.campos_seleccionados = res))
          .catch(err => console.error(err));
      })
      .catch(err => {
        console.error("Error no se pudo obtener los campos dinamicos")
        console.error(err)
        this.mantenimiento
          .cargarCamposVer()
          .then(res => (this.campos_seleccionados = res))
          .catch(err => console.error(err));
      });
    this.mantenimiento.validacionVariableLocal();
    global.params_mnto.flag_detalle = false;

    if (global.params_mnto.id_equipo && this.prvIdEquipo == 0) {
      this.prvIdEquipo = global.params_mnto.id_equipo;
      global.params_mnto.id_equipo = null; //Se limpia el valor de una vez
      this.actividad_id = global.params_mnto.actividad_id;
    }
    this.menu.enable(true, "menuPrincipal");
    this.cargarActividades();
    return true;
  }

  cargarActividades() {
    this.mantenimiento
      .deleteActividadesBloqueadas()
      .then(res => {
        this.mantenimiento
          .obtenerActividades("", this.prvIdEquipo, this.actividad_id)
          .then((respuesta: any) => {
            this.actividades = respuesta;
            // console.warn("Actividades consultadas en la BD")
            // console.warn(this.actividades)

            if (this.prvIdEquipo != 0) {
              this.mostrarBtnLimpiarFiltro = true;
              // console.log("1. hay un id de equipo leido por NFC id_equipo: "+this.prvIdEquipo);
              //Aqui se define que viene filtradas por el equipo de lectura de NFC
              if (respuesta.length == 1) {
                // console.log("2. Tan solo hay una actividad de ese equipo")
                let actividadEncontrada = this.actividades[0];
                actividadEncontrada.prvIdEquipo = this.prvIdEquipo;
                this.detalleActividad(actividadEncontrada);
                global.params_mnto.actividad_id = null;
                this.actividad_id = null;
              } else if (respuesta.length > 1) {
                // console.log("3. Hay varias actividades de este equipo");
                /**
                 * Cuando hay varias actividades de un mismo equipo
                 * Se consulta si hay alguna actividad iniciada
                 */
                this.mantenimiento
                  .consultarActividadIniciada()
                  .then(res => {
                    if (res) {
                      // console.log("4. Existe una actividad iniciada");
                      // console.log(res)
                      // console.log("Lista de actividades");
                      // console.log(this.actividades);
                      /**
                       * Entra aqui porque si hay alguna actividad iniciada
                       * Busca entre las actividades filtradas si la actividad
                       * iniciada corresponde a una de las del equipo leido por NFC
                       */
                      let actividadEncontrada = this.actividades.find(
                        actividad => Number(actividad.id) == Number(res["id"])
                      );

                      if (actividadEncontrada) {
                        // console.log("5. la actividad iniciada corresponde a una actividad leida por el NFC");
                        // console.log(actividadEncontrada);
                        /**
                         * Aqui ha encontrado la activida iniciada del equipo leido por NFC
                         * Pasa a la vista detalle de la actividad iniciada
                         */
                        this.detalleActividad(actividadEncontrada);
                      } else {
                        // console.log("6. las actividades filtradas no corresponden a la actividad iniciada");
                      }

                      /**
                       * En caso que la actividad iniciada no corresponda a ningua de las actividades
                       * del equipo entonces se mostraran las activdades filtradas del equipo
                       * pero sin la opcion de iniciar al seleccionarlas, ya que no
                       * se les asigna el parametro del id del equipo para validar su iniciacion
                       */
                    } else {
                      // console.log("7. No hay ninguna actividad iniciada, las filtradas estas listas para iniciarcen")
                      /**
                       * En este caso no hay ninguna activida iniciada por lo tanto
                       * Se muestra la lista de las actividades de este equipo
                       * Se les agrega un parametro que es el id del equipo
                       * para habilitar que al momento de seleccionar una actividad
                       * entre e inmediatamente se inicie
                       */
                      // this.actividades = this.actividades.map(actividad => {
                      //   actividad.prvIdEquipo = this.prvIdEquipo;
                      //   return actividad;
                      // });
                    }
                  })
                  .catch(error => {
                    console.error(
                      "ERROR al consutlar alguna actividad iniciada"
                    );
                    console.error(error);
                  });
              }
            }
          });
        this.mantenimiento
          .obtenerActividades("terminadas", this.prvIdEquipo, this.actividad_id)
          .then(res => {
            console.log("Actividades TERMINADAS", res)
            this.actividadesTerminadas = res;
            this.mostrarActividadesEnviadas()
          });
      })
      .catch(error => {
        console.error(error);
      });
  }

  detalleActividad(actividad) {
    // console.log("Vamos a la otra vista");
    // console.log("este es el ID: " + actividad.id);
    // global.actividadSel = actividad;
    this.mantenimiento.setearDatosActividad(actividad);
    global.params_mnto.actividad_id = actividad.id;
    // console.log("actividad_id al entrar: " + global.params_mnto.actividad_id);
    this.prvIdEquipo = 0; //Se limpia el parametro para poder volver a regresar al listado
    // console.log("SE limpio el parametro");
    this.navCtrl.push("ActividadesDetallePage", {
      actividadSel_id: actividad.id
    });
  }

  obtenerFoto(f_foto) {
    if (f_foto) {
      return "data:image/png;base64," + f_foto;
    } else {
      return "assets/imgs/no_image.png";
    }
  }

  openModal(actividadSel, slidingItem: ItemSliding) {
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false
    };

    const myModalData = { actividad: actividadSel };

    const myModal: Modal = this.modal.create(
      "ErroresModalPage",
      { data: myModalData },
      myModalOptions
    );

    myModal.present();

    myModal.onDidDismiss(data => {});

    myModal.onWillDismiss(data => {});
    this.undo(slidingItem);
  }

  undo = (slidingItem: ItemSliding) => {
    slidingItem.close();
  };
}
