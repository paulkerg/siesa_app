import { MantenimientoServiceProvider } from "./../../../../providers/mantenimiento-service/mantenimiento-service";
import { UtilesProvider } from "./../../../../providers/utiles/utiles";
import { global } from "./../../../../app/global";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  FabButton,
  Content
} from "ionic-angular";
// import { ActividadesDetalleTabDatosPage } from '../actividades-detalle-tab-datos/actividades-detalle-tab-datos';
/**
 * Generated class for the ActividadesDetallePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// const CORRECTIVO = 1;
const CORRIENDO = 1;
const DETENIDA = 2;
const PAUSADA = 3;

@IonicPage()
@Component({
  selector: "page-actividades-detalle",
  templateUrl: "actividades-detalle.html"
})
export class ActividadesDetallePage {
  @ViewChild(Content)
  content: Content;
  @ViewChild(FabButton)
  fabButton: FabButton;

  params: any = {
    data: [{ page: "ActividadesDetalleTabDatosPage", title: "Datos" }]
  };
  iconIniciar: "play";
  tiempo: "00:03:38";
  actividadSel = {
    c0820_consec_docto: null,
    c0820_id_co: null,
    c0820_id_tipo_docto: null,
    id: "",
    descripcion: "",
    trabajo: "",
    detalle_trabajo: "",
    f_foto: "",
    c0822_fecha_programada: "",
    c0822_notas: "",
    CO: "",
    tipo_docto: "",
    numero_docto: "",
    notas: "",
    procedimiento: "",
    paroEnCadena: false,
    c0822_fecha_ejecucion_ini: null,
    c0819_rowid_w0822: null,
    c0822_rowid_docto_ot: null,
    c0822_rowid: null,
    lecturas: null,
    motivos: [],
    ayudantes: [{ c0827_rowid_tercero: 0 }],
    c0846_rowid_ayudante1: null,
    c0846_rowid_ayudante2: null,
    c0846_rowid_ayudante3: null,
    c0846_rowid_ayudante4: null,
    c0846_rowid_ayudante5: null,
    c0819_ind_dano: 0,
    c0819_ind_tipo_dano: 0,
    c0819_detalle_dano: " ",
    c0846_rowid_causal_falla: null,
    c0846_rowid_tipo_falla: null,
    c0846_notas: " ",
    c0846_ind_estado: 0,
    c0822_id_equipo: null,
    c0808_referencia: null,
    bordeIzquierdo: null,
    prvIdEquipo: 0,
    c0806_ind_regimen: null,
    c0808_ind_requiere_paro: null,
    c0819_ind_requiere_paro: null,
    c0819_ind_tipo_trabajo: null,
    c0822_duracion_horas_prog: 0,
    tareas: [],
    movimiento: null,
    tiempo_movimientos: 0,
    tareaSeleccionada: null,
    c0822_rowid_actividad: null,
    c0822_rowid_responsable: null,
    c0822_ind_enviado: 0,
    habilitar_error: true
  };
  actividad_vacia: any;
  ind_scan_movil: any;
  /**Cronometro */
  timeInSeconds: any;
  timer: any = {
    displayTime: ""
  };
  /**Cronometro */

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private msg: UtilesProvider,
    private mantenimiento: MantenimientoServiceProvider
  ) {
    this.actividad_vacia = JSON.parse(JSON.stringify(this.actividadSel));
    this.params.data = [
      { page: "ActividadesDetalleTabActividadPage", title: "Actividad" },
      { page: "ActividadesDetalleTabDatosPage", title: "Datos" }
    ];
    this.timer.displayTime = "00:00:00";
  }
  rangosColores() {
    return [
      { porcentaje: value => value <= 50, color: "green" },
      { porcentaje: value => value <= 75, color: "#cbc918" },
      { porcentaje: value => value > 75, color: "red" }
    ];
  }

  porcentajeTiempoProgreso() {
    let time_total = this.actividadSel.c0822_duracion_horas_prog * 60 * 60;
    // console.log("this.timer.timeRemaining: " + this.timer.timeRemaining + ' time_total: ' + time_total);
    let avance = 0;
    if (this.timer.timeRemaining && this.timer.timeRemaining > 0) {
      avance = this.timer.timeRemaining;
    } else {
    }
    let percent = (avance * 100) / time_total;
    if (percent > 100) {
      percent = 100;
    }
    return percent;
  }

  mostrarBotonIniciar() {
    return (
      (global.actividadSel.c0822_fecha_ejecucion_ini == null ||
        (global.actividadSel.c0846_ind_estado != CORRIENDO &&
          global.actividadSel.c0846_ind_estado != DETENIDA)) &&
      global.actividadSel.c0822_rowid_responsable ==
        global.usuario["f_tercero_rowid"]
    );
  }
  mostrarBotonPausa() {
    return (
      global.actividadSel.c0822_fecha_ejecucion_ini != null &&
      global.actividadSel.c0846_ind_estado == CORRIENDO &&
      global.actividadSel.c0822_rowid_responsable ==
        global.usuario["f_tercero_rowid"]
    );
  }

  mostrarBotonDetener() {
    return (
      this.actividadSel.c0822_fecha_ejecucion_ini != null &&
      this.actividadSel.c0846_ind_estado == CORRIENDO &&
      this.actividadSel.c0822_rowid_responsable ==
        global.usuario["f_tercero_rowid"]
    );
  }

  //Metodo se carga apenas entra a la Page
  //carga la actividad seleccionada en la lista de actividades
  ionViewWillEnter() {
    // console.log("entro a la vista de detalle");
    let actividad_id = this.navParams.get("actividadSel_id");
    // console.log("Entrando a la apliacion Detalles");
    // console.log("global.actividadSel");
    // console.log(global.actividadSel);
    // this.actividadSel = null;
    this.mantenimiento
      .validacionVariableLocal(actividad_id)
      .then(res => {
        // console.log("Se entro ");
        global.params_mnto.flag_detalle = true;
        if (!actividad_id) {
          if (global.actividadSel) {
            actividad_id = global.actividadSel.id;
          }
        }
        //aqui vamnos a recibir el RowId de la actividad
        console.log("actividad_id: --" + actividad_id + "--");
        this.cargarDataActividad(actividad_id);
      })
      .catch(error => {
        console.error("ERROR al validar la actividad_id");
        console.error(error);
      });
  }

  cargarDataActividad(actividad_id) {
    this.mantenimiento.cargaDatos(actividad_id).then((res: any) => {
      this.actividadSel = res;
      this.mantenimiento
        .cargarMovimientoActividad(actividad_id)
        .then((res: any) => {
          if (res) {
            // console.log("se encontro un movimiento de actividad: " + res.id);
            // console.log(res);
            this.actividadSel = this.mantenimiento.agregarData(
              res,
              this.actividadSel
            );
            this.actividadSel.motivos = [];
            this.actividadSel.ayudantes = [];
            this.actividadSel.tareas = [];
            this.actividadSel.ayudantes = this.mantenimiento.cargarAyudantes(
              this.actividadSel
            );
            this.mantenimiento
              .cargarMotivosParo(this.actividadSel.id)
              .then((res: any) => (this.actividadSel.motivos = res));
            this.mantenimiento.cargarTareas(this.actividadSel);
            this.actividadSel.paroEnCadena = this.actividadSel.paroEnCadena
              ? true
              : false;

            // this.actividadSel = res
            if (this.actividadSel.c0846_ind_estado == CORRIENDO) {
              this.inicioCuentaCronometro();
              if (
                this.actividadSel.c0822_rowid_responsable !=
                global.usuario["f_tercero_rowid"]
              ) {
                setTimeout(() => {
                  let eliminar = confirm(
                    "La actividad ha sido reasignada, desea eliminarla"
                  );
                  if (eliminar) {
                    // console.log("Se va a eliminar y todo los movimientos tambien");
                    this.mantenimiento
                      .eliminarActividaReasignada(this.actividadSel)
                      .then(res => {
                        this.navCtrl.pop();
                      });
                  } else {
                    // console.info("NO se hace nada en la activida sigue corriendo normalmente");
                  }
                }, 500);
              }
              // console.log("Existe la actividad como en ejecucion");
            } else if (this.actividadSel.c0846_ind_estado == DETENIDA) {
              this.mostrarDuracionFinal(this.actividadSel);
              this.mantenimiento
                .cargarErroresActividad(this.actividadSel)
                .then((res: any) => {
                  this.actividadSel = res;
                });
              // console.log("Existe la actividad como finalizada o terminada");
            } else if (this.actividadSel.c0846_ind_estado == PAUSADA) {
              // console.log(
              //   "la actividad esta pausada, se va cargar el cronometro"
              // );
              this.mostrarDuracionPausa(this.actividadSel);

              // console.log("Existes como indocador 0");
            }
          } else {
            // console.log("No existe el registro del movimiento");
            // console.log("this.actividadSel.prvIdEquipo");
            // console.log(this.actividadSel.prvIdEquipo);
            this.mantenimiento.cargarTareas(this.actividadSel);
            // console.log("this.actividadSel.prvIdEquipo: " + this.actividadSel.prvIdEquipo);
            if (
              this.actividadSel.prvIdEquipo != 0 &&
              this.actividadSel.prvIdEquipo != null
            ) {
              //En el caso de que venga con el valor para iniciar la actividad
              // console.warn("#####Se inicio la actividad");
              this.validarActividad(this.actividadSel);

              //Se inicia la actividad porque no hay ninguna otra iniciada
            }
          }
        })
        .catch(error => {
          console.error("Error al consultar el movimiento de la actividad");
          console.error(error);
        });
    });
  }

  ionViewCanLeave() {
    if (
      (this.actividadSel.c0822_fecha_ejecucion_ini != null &&
        Number(this.actividadSel.c0846_ind_estado) < 2) ||
      !this.actividadSel.habilitar_error
    ) {
      this.guardarActividad(this.actividadSel)
        .then(res => {
          this.actividadSel = this.actividad_vacia;
          global.actividadSel = this.actividad_vacia;
          return true;
        })
        .catch(err => {
          console.error("Ocurri un error al guardar la actividad");
          console.error(err);
        });
    }else{
      this.actividadSel = this.actividad_vacia;
      global.actividadSel = this.actividad_vacia;
    }

    this.mantenimiento.limpiarActividadLocalStorage();
    // return true;
  }

  disabledActividad() {
    if (this.actividadSel.c0846_ind_estado != null) {
      return this.actividadSel.c0846_ind_estado == 2;
    }
    return false;
  }

  /**
   * validarActividad
   * recibe una actividad a iniciar
   * esta valida que no haya otra actividad iniciada para luego proceder a validar
   * seguridades y/o exigencias de escaner NFC
   * @param actividad
   */
  validarActividad(actividad) {
    this.mantenimiento
      .consultarActividadIniciada()
      .then((res: any) => {
        // console.log("res");
        // console.log(res);
        if (res.id != "" && res.id != null && res.id != actividad.id) {
          let msj =
            "No se puede inicar la actividad porque ya hay otra en curso";
          console.error(msj);
          // alert(msj);
          this.msg.msgAlerta({ titulo: "Error", message: msj });
        } else {
          this.iniciarActividad(actividad);
        }
      })
      .catch(error => {
        console.error("ERROR al consultar la actividad iniciada");
        console.error(error);
      });
  }

  /**
   * iniciarActividad
   * recibe una actividad a iniciar
   * cuando llega a este metodo es porque ha sido llamado desde
   * validarActividad quien se encarga de validar que no haya otra
   * actividad en curso
   *
   * en esta se valida las exigencias de escaner NFC
   * donde si la CIA y el equipo los exigen no permite iniciar
   * a menos que se lea la info del tag NFC del equipo
   * @param actividad
   */
  iniciarActividad(actividad) {
    this.mantenimiento
      .validarExigeNFC(actividad)
      .then(exige => {
        if (exige) {
          if (actividad.prvIdEquipo == actividad.c0822_id_equipo) {
            this.iniciarActividadUpdate(actividad);
          } else {
            let msj_equipo =
              "Se require lectura de NFC para validar la cercania al equipo";
            // console.log(msj_equipo);
            this.msg.msgAlerta({
              titulo: "Validacion",
              message: msj_equipo
            });
          }
        } else {
          // console.log("El equipo o la CIA no exige la lectura NFC");
          this.iniciarActividadUpdate(actividad);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }
  /**
   * iniciarActividadUpdate
   * recibe una actividad a iniciar,
   * llama el metodo para guardar la actividad  luego inicia el cronometro
   * @param actividad
   */
  iniciarActividadUpdate(actividad) {
    this.guardarActividad(actividad)
      .then(res => {
        this.inicioCuentaCronometro();
      })
      .catch(error => {
        console.error(error);
      });
  }
  /**
   * guardarActividad
   * recibe como parametro una actividad para ser guardada
   * este metodo define si se debe de crear el movimiento o actualizarlo
   * basado en la fecha de c0822_fecha_ejecucion_ini
   * @param actividad
   */
  guardarActividad(actividad) {
    return new Promise((resolve, reject) => {
      if (!actividad) {
        console.error("Error la actividad que se intenta guardar es nula");
        return;
      }
      // console.warn("Actividad a guardar: ");
      // console.warn(actividad);
      if (actividad.c0822_fecha_ejecucion_ini) {
        if (actividad.c0846_ind_estado == PAUSADA) {
          //Primero se debe de actualizar el movimeinto actual para guardarlo como detenido
          //luego se debe de crear otro movimiento como si apenas se estuviese iniciando
          // this.actualizarMovimiento(actividad,true,3);
          this.mantenimiento.crearMovimiento(actividad);
        }
        // console.log("1- Se va a actualizar la actividad");
        this.mantenimiento
          .actualizarMovimiento(actividad)
          .then(res => {
            this.stopTimer();
            resolve(res);
          })
          .catch(error => reject(error));
      } else {
        // console.log("se crea el movimiento del actividad");
        // console.log(actividad);
        this.mantenimiento
          .crearMovimiento(actividad)
          .then(res => resolve(res))
          .catch(error => reject(error));
      }
    });
  }

  mostrarDuracionPausa(actividad) {
    // this.continuarTiempoIniciada(this.actividadSel).then(res => {
    //   this.timeInSeconds = res;
    //   console.log("el tiempo que lleva es: " + this.timeInSeconds);
    //   this.iniciarCronometro();
    // }).catch(error => {
    //   console.error("ERROR al continuar con el tiempo de la actividad");
    //   console.error(error);
    // })
    // let time = 0;
    this.mantenimiento
      .calcularTiempoMovimientos(actividad)
      .then((res: number) => {
        // console.log("respuesta del tiempo de los movimientos: " + res);

        this.timeInSeconds = res;
        this.timer.displayTime = this.mantenimiento.getSecondsAsDigitalClock(res);
      })
      .catch(error => console.error(error));
  }
  mostrarDuracionFinal(actividad) {
    let time = this.mantenimiento.diferenciaFechasSegundos(actividad.c0846_hora_fin,actividad.c0846_hora_ini);
    this.mantenimiento
      .calcularTiempoMovimientos(actividad)
      .then((res: number) => {
        time += res;
        this.timeInSeconds = time;
        this.timer.displayTime = this.mantenimiento.getSecondsAsDigitalClock(time);
      })
      .catch(error => console.error(error));
  }

  pausarActividad(actividad) {
    //Aqui se pausa la actividad
    this.guardarDetenerActividad(actividad, PAUSADA);
  }

  guardarDetenerActividad(actividad, estado = 2) {
    if (!actividad) {
      console.error("Error la actividad que se intenta guardar es nula");
      return;
    }
    if (actividad.c0846_ind_estado == 2) {
      // console.log("La actividad ya esta terminada");
      return;
    }
    if (actividad.c0822_fecha_ejecucion_ini) {
      this.mantenimiento
        .validarExigeNFC(actividad)
        .then(exige => {
          var save = true;
          //TODO: pendiente confirmacion con las alertas de UtilesProvider
          if (!global.params_mnto.finalizar) {
            let msj = "Esta seguro que desea Detener la actividad?";
            if (estado == PAUSADA) {
              msj = "Esta seguro que desea PAUSAR la actividad?";
            }
            save = confirm(msj);
            if (exige && save && estado == DETENIDA) {
              //Se agrega vairable global para detener la actividad
              global.params_mnto.finalizar = true;
              //Si el mensaje de la alerta se cancela entonces se debe de limpiar el parametro
              // para no permitir la lectura de NFC y que sea cancelada
              this.msg.msgAlerta(
                "Para terminar la actividad, por favor acerque el dispositivo al TAG del equipo",
                () => {
                  global.params_mnto.finalizar = false;
                }
              );
              return;
            }
          }
          this.mantenimiento
            .actualizarMovimiento(actividad, save, estado)
            .then(res => {
              this.stopTimer();
              if (save) {
                actividad.c0846_ind_estado = estado;
                let msj = "Se guarda y detiene la actividad";
                if (estado == PAUSADA) {
                  msj = "Se guarda y se PAUSA la actividad";
                }
                // alert(msj);
                this.msg.msgConfirmacion({
                  titulo: "Notificacion",
                  mensaje: msj
                });
              }
            })
            .catch(error => {
              this.msg.msgAlerta({
                titulo: "Error en la validacion",
                message: error
              });
              console.error(error);
            });
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      alert("La actividad debe de estar iniciada para guardar y Detenerla");
    }
  }

  /**CRONOMETRO */
  inicioCuentaCronometro() {
    this.mantenimiento
      .continuarTiempoIniciada(this.actividadSel)
      .then(res => {
        this.timeInSeconds = res;
        // console.log("el tiempo que lleva es: " + this.timeInSeconds);
        this.iniciarCronometro();
        this.startTimer();
      })
      .catch(error => {
        console.error("ERROR al continuar con el tiempo de la actividad");
        console.error(error);
      });
  }
  stopTimer() {
    this.timer.runTimer = false;
  }
  startTimer() {
    this.timer.hasStarted = true;
    this.timer.runTimer = true;
    this.timerTick();
  }
  timerTick() {
    setTimeout(() => {
      if (!this.timer.runTimer) {
        return;
      }
      // //calculo del tiempo con la fercha de inicio de la actividad con el movimiento actual
      // //y la fecha actual

      var segundos = this.mantenimiento.diferenciaFechasSegundos(this.actividadSel.c0822_fecha_ejecucion_ini);
      this.timer.timeRemaining = segundos;
    
      this.timer.displayTime = this.mantenimiento.getSecondsAsDigitalClock(
        this.timer.timeRemaining
      );
      if (this.timer.timeRemaining > 0) {
        this.timerTick();
      } else {
        this.timer.hasFinished = true;
      }
    }, 1000);
  }
  iniciarCronometro() {
    // console.log(
    //   "validar que cuando entra por primera vez esto esta nulo o vacio"
    // );
    // console.log(this.timeInSeconds);
    if (!this.timeInSeconds) {
      this.timeInSeconds = 0;
    }

    this.timer = <PTimer>{
      time: this.timeInSeconds,
      runTimer: false,
      hasStarted: false,
      hasFinished: false,
      timeRemaining: this.timeInSeconds
    };
    this.timer.displayTime = this.mantenimiento.getSecondsAsDigitalClock(
      this.timer.timeRemaining
    );
    
  }

  
}
export interface PTimer {
  time: number;
  timeRemaining: number;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  displayTime: string;
}
