import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ActividadesDetallePage } from "./actividades-detalle";
import { ComponentsModule } from "../../../../components/components.module";

@NgModule({
  declarations: [
    ActividadesDetallePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ActividadesDetallePage)
  ]
})
export class ActividadesDetallePageModule {}
