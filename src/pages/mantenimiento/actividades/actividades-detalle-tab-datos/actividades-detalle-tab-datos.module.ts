import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActividadesDetalleTabDatosPage } from './actividades-detalle-tab-datos';
import { ComponentsModule } from '../../../../components/components.module';


@NgModule({
  declarations: [
    ActividadesDetalleTabDatosPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ActividadesDetalleTabDatosPage),
  ],
})
export class ActividadesDetalleTabDatosPageModule {}
