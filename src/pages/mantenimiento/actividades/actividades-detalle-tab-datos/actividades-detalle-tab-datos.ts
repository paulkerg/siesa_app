import { MantenimientoServiceProvider } from './../../../../providers/mantenimiento-service/mantenimiento-service';
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Content,
  FabButton
} from "ionic-angular";
import { global } from "../../../../app/global";

/**
 * Generated class for the ActividadesDetalleTabDatosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-actividades-detalle-tab-datos",
  templateUrl: "actividades-detalle-tab-datos.html"
})
export class ActividadesDetalleTabDatosPage {
  params: any = { data: [{ page: "GeneralesPage", title: "Generales" }] };
  @ViewChild(Content)
  content: Content;
  @ViewChild(FabButton)
  fabButton: FabButton;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private mantenimiento: MantenimientoServiceProvider
  ) {
    this.params.data = [
      { page: "GeneralesPage", title: "GENERALES" },
      { page: "ParosPage", title: "PAROS" }
      //{ page: "NoRutinarioPage", title: "NO RUTINARIO" }
      // { page: TabPage2, title: "tab 2" },
      // { page: TabPage3, title: "tab 3" },
      // { page: TabPage4, title: "tab 4" }
    ];
    if (global.actividadSel.c0819_rowid_w0822) {
      this.params.data.push({
        page: "NoRutinarioPage",
        title: "NO RUTINARIO"
      });
    } else {
      // console.log("global.actividadSel.tareas");
      if (!global.actividadSel.tareas) {
       this.mantenimiento.getTareasSinIniciar().then((res:any)=>{
         if(res.length>0){
           this.params.data.push({
             page: "TareasPage",
             title: "TAREAS"
           });   
         }
       })
      }else if(global.actividadSel.tareas.length>0){
        this.params.data.push({
          page: "TareasPage",
          title: "TAREAS"
        });
      }
    }
  }

  ionViewDidLoad() {
    // console.log("ionViewDidLoad ActividadesDetalleTabDatosPage");
  }
}
