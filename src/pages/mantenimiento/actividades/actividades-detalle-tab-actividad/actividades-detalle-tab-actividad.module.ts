import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActividadesDetalleTabActividadPage } from './actividades-detalle-tab-actividad';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [
    ActividadesDetalleTabActividadPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ActividadesDetalleTabActividadPage)
  ]
})
export class ActividadesDetalleTabActividadPageModule {}
