import { Component } from "@angular/core";
import { Events, IonicPage, NavController, NavParams } from "ionic-angular";
import { global } from "../../../../app/global";

/**
 * Generated class for the ActividadesDetalleTabActividadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-actividades-detalle-tab-actividad",
  templateUrl: "actividades-detalle-tab-actividad.html"
})
export class ActividadesDetalleTabActividadPage {
  opDatos: string = "generales";
  opPrincipal = "actividad";
  opActividad = "procedimiento";

  actividadSel: any = {
    procedimiento: ""
  };
  ind_scan_movil: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private events: Events
  ) {
    this.actividadSel = global.actividadSel;
    this.events.subscribe("actividadSel_change", actividad => {
      // console.log("Se recibe la nueva actividad que cambio");
      this.actividadSel = actividad;
    });
    // console.log("Cargando el tab de actividad")  ;
    // console.log(this.actividadSel);
  }
  ionViewWillEnter() {
    if (!this.actividadSel) {
      if (global.actividadSel) {
        this.actividadSel = global.actividadSel;
      } else {
        // console.warn("la actividad en el global es nula");
        global["actividadSel"] = JSON.parse(
          localStorage.getItem("actividadSel")
        );
        this.actividadSel = global.actividadSel;
      }
    } else if (!this.actividadSel.procedimiento) {
      if (global.actividadSel) {
        if (global.actividadSel.procedimiento) {
          this.actividadSel.procedimiento = global.actividadSel.procedimiento;
        } else {
          global.actividadSel.procedimiento = "";
          this.actividadSel = global.actividadSel;
        }
      } else {
        global["actividadSel"] = JSON.parse(
          localStorage.getItem("actividadSel")
        );
        this.actividadSel = global.actividadSel;
      }
    }
  }

  obtenerFoto(f_foto) {
    if (f_foto) {
      return "data:image/png;base64," + f_foto;
    } else {
      if (global.actividadSel.f_foto) {
        return "data:image/png;base64," + global.actividadSel.f_foto;
      }
      return "assets/imgs/no_image.png";
    }
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ActividadesDetalleTabActividadPage');
  }
}
