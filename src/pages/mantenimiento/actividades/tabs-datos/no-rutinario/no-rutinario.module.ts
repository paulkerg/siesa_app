import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoRutinarioPage } from './no-rutinario';
import { ComponentsModule } from '../../../../../components/components.module';

@NgModule({
  declarations: [
    NoRutinarioPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(NoRutinarioPage),
  ],
})
export class NoRutinarioPageModule {}
