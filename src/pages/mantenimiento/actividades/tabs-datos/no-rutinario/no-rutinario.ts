import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { global } from '../../../../../app/global';

/**
 * Generated class for the NoRutinarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-no-rutinario',
  templateUrl: 'no-rutinario.html',
})
export class NoRutinarioPage {
actividadSel:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.actividadSel = global.actividadSel;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad NoRutinarioPage');
  }

}
