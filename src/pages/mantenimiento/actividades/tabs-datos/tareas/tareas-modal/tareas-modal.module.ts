import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TareasModalPage } from './tareas-modal';
import { ComponentsModule } from "../../../../../../components/components.module";

@NgModule({
  declarations: [
    TareasModalPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TareasModalPage),
  ],
})
export class TareasModalPageModule {}
