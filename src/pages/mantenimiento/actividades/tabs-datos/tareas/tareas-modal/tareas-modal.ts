import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SqliteProvider } from "../../../../../../providers/sqlite/sqlite";

/**
 * Generated class for the TareasModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-tareas-modal",
  templateUrl: "tareas-modal.html"
})
export class TareasModalPage {
  tareaSeleccionada: any = {
    c0847_observaciones: ""
  };
  observaciones: any = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private db: SqliteProvider,
  ) {}

  ionViewWillEnter() {
    // console.log('this.navParams.get("data")');
    // console.log(this.navParams.get("data"));
    let tarea = this.navParams.get("data").tarea;
    // console.log("tarea");
    // console.log(tarea);
    this.tareaSeleccionada = JSON.parse(JSON.stringify(tarea));
  }
  ionViewDidLoad() {
    // console.log("ionViewDidLoad TareasModalPage");
  }

  guardarObservacion() {
    // console.log("ahora vamos a guardar la observaciones");
    let sql = `
    update w0847_movto_actividades_tareas
    set c0847_observaciones = '${this.tareaSeleccionada.c0847_observaciones.trim()}'
    where c0847_rowid = ${this.tareaSeleccionada.c0847_rowid}
    `;
    this.db
      .queryExec(sql)
      .then(res => {
        // console.log("Se guardo las observaciones de la tarea");
        // console.log(res);
      })
      .catch(error => {
        console.error(
          "ERROR al intentar guardar las observaciones de la tarea"
        );
        console.error(sql);
        console.error(error);
      });
  }

  limpiarCampo() {
    // this.tareaSeleccionada.c0847_observaciones = "";
  }
}
