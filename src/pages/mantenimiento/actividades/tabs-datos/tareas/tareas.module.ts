import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TareasPage } from './tareas';
import { ComponentsModule } from "../../../../../components/components.module";

@NgModule({
  declarations: [
    TareasPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TareasPage),
  ],
})
export class TareasPageModule {}
