import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, FabButton } from 'ionic-angular';
import { global } from '../../../../../app/global';

/**
 * Generated class for the ParosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paros',
  templateUrl: 'paros.html',
})
export class ParosPage {
actividadSel:any={};
  @ViewChild(Content) content: Content;
  @ViewChild(FabButton) fabButton: FabButton;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.actividadSel = global.actividadSel;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ParosPage');
  }

}
