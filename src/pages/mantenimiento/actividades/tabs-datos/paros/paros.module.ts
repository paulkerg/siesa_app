import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParosPage } from './paros';
import { ComponentsModule } from '../../../../../components/components.module';

@NgModule({
  declarations: [
    ParosPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ParosPage),
  ],
})
export class ParosPageModule {}
