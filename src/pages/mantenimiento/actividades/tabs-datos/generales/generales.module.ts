import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralesPage } from './generales';
import { ComponentsModule } from '../../../../../components/components.module';

@NgModule({
  declarations: [
    GeneralesPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(GeneralesPage),
  ],
})
export class GeneralesPageModule {}
