import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the GeneralesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-generales',
  templateUrl: 'generales.html',
})
export class GeneralesPage {

  actividadSel:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events) {
    this.events.subscribe('actividadSel_change', actividad => {
      // console.log("Se recibe la nueva actividad que cambio")
      this.actividadSel = actividad
    })  
    // this.actividadSel = global.actividadSel;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad GeneralesPage');
  }

}
