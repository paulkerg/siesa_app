import { Component } from '@angular/core';
import {
	IonicPage,
	NavController,
	NavParams,
	Platform,
	Loading,
	AlertController,
	MenuController,
	ModalController,
	Modal,
	ModalOptions,
	Events
} from 'ionic-angular';

import { Subscription } from 'rxjs/Subscription';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio';
import { Http } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';
import { AppVersion } from '@ionic-native/app-version';
import { AuthProvider } from '../../../providers/auth/auth';
import { SignalrServiceProvider } from '../../../providers/signalr-service/signalr-service';
import { UtilesProvider } from '../../../providers/utiles/utiles';
import { SqliteProvider } from '../../../providers/sqlite/sqlite';
import { NotificacionesProvider } from '../../../providers/notificaciones/notificaciones';
import { APP_CONFIG } from '../../../config/app.config';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {
	fingerprintoptions: FingerprintOptions;
	blnHuella: boolean = false;
	loadProgress: number = 0;
	subscription: Subscription;
	miDevice: any;
	siesappsVersion: string = 'Version 1.19.0100';
	data: any = {};
	demo_vigencia: boolean = false;
	loading: Loading;
	usuario = {
		valido: false
	};
	registerCredentials = {
		email: '',
		password: ''
	};
	uuid: any;

	constructor(
		public http: Http,
		public navCtrl: NavController,
		public navParams: NavParams,
		private platform: Platform,
		public alertCtrl: AlertController,
		private menu: MenuController,
		private auth: AuthProvider,
		public modal: ModalController,
		private sr: SignalrServiceProvider,
		private msg: UtilesProvider,
		public events: Events,
		private appVersion: AppVersion,
		private faio: FingerprintAIO,
		private db: SqliteProvider,
		private notificacion: NotificacionesProvider,
		private keyboard: Keyboard
	) {
		this.platform.ready().then(() => {
			this.auth.getUsuarioLocal().then((r) => {
				this.fingerprintoptions = {
					clientId: r[0]['id_usuario'],
					clientSecret: r[0]['clave'], //Only necessary for Android
					disableBackup: true //Only for Android(optional)
					//localizedFallbackTitle: 'Use Pin', //Only for iOS
					//localizedReason: 'Please authenticate' //Only for iOS
				};
				if (r[0]['id_usuario'] != 'siesa') {
					this.validaHuella();
				}
			});
			this.appVersion.getVersionNumber().then((version) => {
				if (version) {
					this.siesappsVersion = 'Version ' + version;
				}
			});

			this.data = {
				logo: 'assets/images/logo/logoSiesa.png'
			};
			this.usuario.valido = true;
			this.menu = menu;
			this.menu.enable(false, 'menuPrincipal');
			this.auth.logout();
			if (this.platform.is('core')) {
				return this.keyboard.isVisible;
			}
			if(this.navParams.get('accion') && this.navParams.get('accion') === 'demo'){
				setTimeout(() => {
					this.demo();
				}, 300);
			}
		});
	}

	ionViewWillEnter() {
		//  this.validarVigenciaDemo();
	}

	consultarVigencia() {
		return new Promise((resolve, reject) => {
			this.http
				.get('http://190.0.4.98:8020/validar?cliente=unicentro')
				.timeout(9000)
				.map((res) => res.json())
				.subscribe(
					(res) => {
						// console.log("res", res)
						if (res.demo_vigencia > 0) {
							this.msg.msgAlerta({
								titulo: 'Alerta',
								message: 'Tiempo restante de prueba: ' + res.demo_vigencia + ' dias'
							});
							this.demo_vigencia = false;

							let guardar = JSON.stringify(res);
							// console.log("guardar")
							let sql_guardar = `insert or replace into configuraciones (id,documento)values('demo_vigencia','${guardar}')`;
							this.db
								.query(sql_guardar)
								.then((res) => {
									// console.log("se guardo el registro");
									resolve(this.demo_vigencia);
								})
								.catch((err) => {
									// console.error("Algo paso que no pudo insertar")
									console.error(err);
									reject(err);
								});
						} else {
							this.msg.msgAlerta({ titulo: 'Error', message: 'El tiempo de prueba expiro' });
							resolve(this.demo_vigencia);
						}
					},
					(err) => {
						let msj = 'No se ha podio consultar la vigencia del demo';
						console.error(msj);
						this.msg.msgAlerta({ titulo: 'Error', message: msj });
					}
				);
		});
	}

	validarVigenciaDemo() {
		return new Promise((resolve, reject) => {
			//consultar localmente si este dia ya se valido la licencia
			let sql = `select documento from configuraciones where id = 'demo_vigencia'`;
			this.db
				.query(sql)
				.then((res: any) => {
					// console.log("resultado bd local: ",res)
					if (res.length > 0) {
						let demo = JSON.parse(res[0].documento);
						let fecha_fin: any = new Date(demo.fecha_fin);
						let hoy: any = new Date();
						let dias_restantes = Math.floor((fecha_fin - hoy) / (1000 * 60 * 60 * 24));
						// console.warn("dias_restantes: " + dias_restantes + "\n demo.demo_vigencia: " + demo.demo_vigencia);

						if (demo.demo_vigencia - dias_restantes != 0) {
							this.consultarVigencia();
						} else {
							this.demo_vigencia = false;
							resolve(this.demo_vigencia);
						}
					} else {
						this.consultarVigencia();
					}
				})
				.catch((err) => {
					// console.error("error al consultar localmente los datos de demo vigencia")
					console.error(err);
					reject(err);
				});
		});
	}

	keyboardisOpen() {
		if (this.platform.is('core')) {
			return this.keyboard.isVisible;
		} else {
			return false;
		}
	}
	ionViewWillLoad() {
		this.auth.logout().then(() => {}).catch(() => {});
	}

	public login() {
		this.events.publish('cargandoOn');
		this.auth.loginLocal(this.registerCredentials).then(
			(allowed) => {
				if (allowed) {
					this.accesoValido();
				} else {
					this.auth.login(this.registerCredentials).then(
						(allowed) => {
							if (allowed) {
								this.accesoValido();
							} else {
								this.showError({
									titulo: 'Error',
									message: 'Acceso denegado'
								});
							}
						},
						(error) => {
							this.showError(error);
						}
					);
				}
			},
			(error) => {
				this.auth.login(this.registerCredentials).then(
					(allowed) => {
						if (allowed) {
							this.accesoValido();
						} else {
							this.showError({
								error: 'Error',
								error_description: 'Acceso denegado'
							});
						}
					},
					(error) => {
						this.showError(error);
					}
				);
			}
		);
	}

	async validaHuella() {
		this.blnHuella = false;
		const aviable = await this.faio.isAvailable();
		if (aviable === 'finger') {
			this.blnHuella = true;
			this.dialogoHuellaLogin();
		}
	}

	accesoValido() {
		let self = this;

		this.auth
			.leerVersionERP()
			.then((r) => {
				this.events.publish('validaVersionERP', r);
			})
			.catch((e) => {
				console.log(e);
			});

		setTimeout(() => {
			let sql = 'select * from t002_mm_propiedades limit 1';
			this.db
				.query(sql)
				.then((resp) => {
					if (resp) {
						let url = 'http://';
						if (resp[0]['ind_https'] == 'true') {
							url = 'https://';
						}
						url += resp[0]['url_servidor_local'];
						if (resp[0]['url_puerto'] != '') {
							url += ':' + resp[0]['url_puerto'];
						}
						url += '/';

						self.sr.inicializar(url);
						self.sr.messageReceived.subscribe((datos) => {
							//alert('SignalR Service... ' + JSON.stringify(datos));
							this.notificacion.actualizarNotificacion(datos);
						});
					}
				})
				.catch((err) => {
					console.error('ERROR');
					console.error(err);
				});

			self.navCtrl.setRoot('PrincipalPage');
		}, 600);
	}

	createAccount() {}

	showError(error) {
		this.events.publish('cargandoOff');
		this.msg.msgAlerta(error);
	}

	public cambioDisp(obj) {}

	openModal() {
		const myModalOptions: ModalOptions = {
			enableBackdropDismiss: false
		};

		const myModalData = {
			url: APP_CONFIG.srvSincronizar
		};

		const myModal: Modal = this.modal.create('LoginConfigPage', { data: myModalData }, myModalOptions);

		myModal.present();

		myModal.onDidDismiss((data) => {
			console.log('I have dismissed.', data);
			if (data && data['bnlDemo'] === true) {
				this.demo();
			}
		});

		myModal.onWillDismiss((data) => {
			console.log("I'm about to dismiss", data);
		});
	}

	demo() {
		this.registerCredentials.email = 'unoee';
		this.registerCredentials.password = 'unoee';
		let sql =
			'update t002_mm_propiedades set ' +
			"url_servidor_internet='" +
			'190.0.4.98' +
			"', url_servidor_local='" +
			'190.0.4.98' +
			"', ind_https='" +
			'false' +
			"', url_puerto='" +
			'7720' +
			"', id_conexion='" +
			'1' +
			"'";
		this.db
			.queryExec(sql)
			.then((_) => {
				setTimeout(() => {
					this.login();
				}, 100);
			})
			.catch((error) => {
				console.log(error);
			});
	}
	pruebaHuella() {
		this.auth
			.loginLocalHuella()
			.then((allowed) => {
				if (allowed) {
					this.accesoValido();
				} else {
					this.showError({
						error: 'Error',
						error_description: 'Acceso denegado'
					});
				}
			})
			.catch((e) => {});
	}

	async dialogoHuellaLogin() {
		try {
			await this.platform.ready();
			const aviable = await this.faio.isAvailable();
			if (aviable === 'finger') {
				await this.faio
					.show(this.fingerprintoptions)
					.then((result: any) => {
						this.auth
							.loginLocalHuella()
							.then((allowed) => {
								if (allowed) {
									this.accesoValido();
								} else {
									this.showError({
										error: 'Error',
										error_description: 'Acceso denegado'
									});
								}
							})
							.catch((e) => {
								console.error('Ocurrio algun error en el loginLocalHuella');
								console.error(e);
							});
					})
					.catch((error: any) => {
						console.log(error);
					});
			}
		} catch (error) {
			this.showError({
				error: 'Error',
				error_description: JSON.stringify(error)
			});
		}
	}
}
