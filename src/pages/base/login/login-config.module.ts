import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginConfigPage } from './login-config';
import { ComponentsModule } from '../../../components/components.module';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [
    LoginConfigPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(LoginConfigPage),
  ],
})
export class LoginConfigPageModule {}
