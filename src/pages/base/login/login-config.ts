import { Component, ViewChild } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SqliteProvider } from '../../../providers/sqlite/sqlite';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../app/global';

@IonicPage()
@Component({
	selector: 'page-login-config',
	templateUrl: 'login-config.html'
})
export class LoginConfigPage {
	@ViewChild('inputUrl') inputUrl;
	@ViewChild('inputPuerto') inputPuerto;
	validations_form: FormGroup;
	parametros = {
		https: false,
		puerto: '',
		idconexion: '',
		idcia: '',
		url: ''
	};
	arrConexiones: any = [];
	data: any = {};
	validation_messages = {
		https: [],
		puerto: [],
		url: [ { type: 'required', message: 'URL es obligatorio.' } ],
		idcia: [ { type: 'required', message: 'ID Cia. es obligatorio' } ],
		idconexion: [ { type: 'required', message: 'Conexion es obligatorio' } ]
	};

	constructor(
		private view: ViewController,
		private db: SqliteProvider,
		public formBuilder: FormBuilder,
		private sync: SincronizacionProvider
	) {}

	ionViewWillLoad() {
		this.data = {
			logo: 'assets/images/logo/logoSiesa.png'
		};
		this.validations_form = this.formBuilder.group({
			https: new FormControl('', Validators.required),
			puerto: new FormControl(''),
			url: new FormControl('', Validators.required),
			idcia: new FormControl('', Validators.required),
			idconexion: new FormControl('', Validators.required)
		});

		let sql = 'select * from t002_mm_propiedades';
		this.db
			.query(sql)
			.then((respuesta) => {
				let r = respuesta[0];
				this.parametros.https = r['ind_https'];
				this.parametros.puerto = r['url_puerto'];
				this.parametros.idconexion = r['id_conexion'];
				this.parametros.idcia = r['id_cia'];
				this.parametros.url = r['url_servidor_internet'];
				/*
				setTimeout(() => {
					this.inputPuerto.setFocus();
				}, 50);
				*/
				setTimeout(() => {
					this.inputUrl.setFocus();
				}, 50);

				this.cargarConexiones();
			})
			.catch((error) => {
				console.log(error);
			});
	}

	closeModal(blnDemo) {
		const data = {
			url: this.parametros.url,
			idcia: this.parametros.idcia,
			idconexion: this.parametros.idconexion,
			bnlDemo: blnDemo
		};
		this.view.dismiss(data).catch(() => {});
	}

	cargarConexiones() {
		this.arrConexiones = [];
		this.sync
			.get(global.api.GetConexiones, {})
			.then((r: any) => {
				for (let i = 0; i < r.length; i++) {
					const e = r[i];
					this.arrConexiones.push({
						id: e['nroConexion'],
						titulo: e['nombre']
					});
				}
			})
			.catch((e) => {
				console.log(e);
			});
	}

	demo() {
		this.parametros.url = '190.0.4.98';
		this.parametros.idconexion = '1';
		this.parametros.idcia = '1';
		this.parametros.puerto = '7720';
		this.closeModal(true);
	}

	grabaURL() {
		setTimeout(() => {
			let sql =
				'update t002_mm_propiedades set ' +
				"url_servidor_internet='" +
				this.parametros.url +
				"', url_servidor_local='" +
				this.parametros.url +
				"', ind_https='" +
				this.parametros.https +
				"', url_puerto='" +
				this.parametros.puerto +
				"', id_conexion='" +
				this.parametros.idconexion +
				"'";
			this.db
				.queryExec(sql)
				.then((respuesta) => {
					setTimeout(() => {
						this.cargarConexiones();
					}, 300);
				})
				.catch((error) => {
					console.log(error);
				});
		}, 300);
	}

	onSubmit(value: any): void {
		if (this.validations_form.valid) {
			let sql =
				'update t002_mm_propiedades set ' +
				"url_servidor_internet='" +
				value.url +
				"', url_servidor_local='" +
				value.url +
				"', ind_https='" +
				value.https +
				"', url_puerto='" +
				value.puerto +
				"', id_conexion='" +
				value.idconexion +
				"'";
			this.db
				.queryExec(sql)
				.then((respuesta) => {
					setTimeout(() => {
						this.closeModal(false);
					}, 300);
				})
				.catch((error) => {
					console.log(error);
				});
		}
	}
}
