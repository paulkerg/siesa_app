import { Component, ViewChild } from '@angular/core';
import { IonicPage, Platform, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard';
import { SqliteProvider } from '../../../providers/sqlite/sqlite';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../app/global';

@IonicPage()
@Component({
	selector: 'page-onboarding',
	templateUrl: 'onboarding.html'
})
export class OnboardingPage {
	@ViewChild('inputUrl') inputUrl;
	@ViewChild('inputPuerto') inputPuerto;
	titulo: string;
	blnFinalizar: boolean = false;
	validations_form: FormGroup;
	data: any = {};
	parametros = {
		https: false,
		puerto: 50010,
		idconexion: '',
		idcia: '',
		url: ''
	};
	arrConexiones: any = [];

	validation_messages = {
		https: [],
		puerto: [],
		url: [ { type: 'required', message: 'URL es obligatorio.' } ],
		idcia: [ { type: 'required', message: 'ID Cia. es obligatorio' } ],
		idconexion: [ { type: 'required', message: 'Conexion es obligatorio' } ]
	};

	constructor(
		public navCtrl: NavController,
		private db: SqliteProvider,
		public formBuilder: FormBuilder,
		private sync: SincronizacionProvider,
		private keyboard: Keyboard,
		private platform: Platform
	) {
		this.titulo = 'Permisos';
	}

	keyboardisOpen() {
		if (this.platform.is('core')) {
			return false;
		} else {
			return this.keyboard.isVisible;
		}
	}

	ionViewWillLoad() {
		this.data = {
			logo: 'assets/images/logo/logoSiesa.png'
		};
		this.validations_form = this.formBuilder.group({
			https: new FormControl('', Validators.required),
			puerto: new FormControl(''),
			url: new FormControl('', Validators.required),
			idcia: new FormControl('', Validators.required),
			idconexion: new FormControl('', Validators.required)
		});

		let sql = 'select * from t002_mm_propiedades';
		this.db
			.query(sql)
			.then((respuesta) => {
				let r = respuesta[0];
				this.parametros.https = r['ind_https'];
				this.parametros.puerto = r['url_puerto'];
				this.parametros.idconexion = r['id_conexion'];
				this.parametros.idcia = r['id_cia'];
				this.parametros.url = r['url_servidor_internet'];
				/*
				setTimeout(() => {
					this.inputPuerto.setFocus();
				}, 50);
				*/
				setTimeout(() => {
					this.inputUrl.setFocus();
				}, 50);
				//this.validateAllFormFields(this.validations_form);
				this.cargarConexiones();
			})
			.catch((error) => {
				console.log(error);
			});
	}

	cargarConexiones() {
		this.blnFinalizar = false;
		if (this.validations_form.valid) {
			this.arrConexiones = [];
			this.sync
				.get(global.api.GetConexiones, {})
				.then((r: any) => {
					for (let i = 0; i < r.length; i++) {
						const e = r[i];
						this.arrConexiones.push({
							id: e['nroConexion'],
							titulo: e['nombre']
						});
					}
					this.blnFinalizar = true;
				})
				.catch((e) => {
					console.log(e);
				});
		}
	}

	grabaURL() {
		if (this.validations_form.valid) {
			setTimeout(() => {
				let sql =
					'update t002_mm_propiedades set ' +
					"url_servidor_internet='" +
					this.parametros.url +
					"', url_servidor_local='" +
					this.parametros.url +
					"', ind_https='" +
					this.parametros.https +
					"', url_puerto='" +
					this.parametros.puerto +
					"', id_conexion='" +
					this.parametros.idconexion +
					"'";
				this.db
					.queryExec(sql)
					.then((respuesta) => {
						setTimeout(() => {
							this.cargarConexiones();
						}, 300);
					})
					.catch((error) => {
						console.log(error);
					});
			}, 300);
		}
	}

	onSubmit(value: any): void {
		console.log('onSubmit', value);
		if (this.validations_form.valid) {
			let sql =
				'update t002_mm_propiedades set ' +
				"url_servidor_internet='" +
				value.url +
				"', url_servidor_local='" +
				value.url +
				"', ind_https='" +
				value.https +
				"', url_puerto='" +
				value.puerto +
				"', id_conexion='" +
				value.idconexion +
				"'";
			this.db
				.queryExec(sql)
				.then((respuesta) => {
					setTimeout(() => {
						this.finalizar('');
					}, 300);
				})
				.catch((error) => {
					console.log(error);
				});
		}
	}

	demo() {
		this.parametros.url = '190.0.4.98';
		this.parametros.idconexion = '1';
		this.parametros.idcia = '1';
		this.parametros.puerto = 7720;
		this.finalizar('demo');
		//this.closeModal(true);
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach((field) => {
			const control = formGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control);
			}
		});
	}

	finalizar(accion) {
		this.navCtrl.setRoot('LoginPage', { accion: accion });
	}
}
