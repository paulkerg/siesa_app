import { Component } from '@angular/core';
import { IonicPage, MenuController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-sincronizar',
	templateUrl: 'sincronizar.html'
})
export class SincronizarPage {
	regLista = [
		{
			titulo: 'Actualizar Maestras',
			imagen: 'md-cloud-download',
			accion: 'recibir'
		},
		{
			titulo: 'Enviar Documentos',
			imagen: 'md-cloud-upload',
			accion: 'enviar'
		}
	];
	constructor(public modalCtrl: ModalController, private menu: MenuController) {
		this.menu = menu;
		this.menu.enable(true, 'menuPrincipal');
	}

	openMenu() {
		this.menu.open('menuPrincipal');
	}

	accion(e) {
		if (e['accion'] == 'recibir') {
			this.modalCtrl.create('SincronizacionProgresoPage', null, { cssClass: 'inset-modal' }).present();
		}
	}
}
