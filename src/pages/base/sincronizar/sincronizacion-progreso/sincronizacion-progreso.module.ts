import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SincronizacionProgresoPage } from './sincronizacion-progreso';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [
    SincronizacionProgresoPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SincronizacionProgresoPage),
  ],
})
export class SincronizacionProgresoPageModule {}
