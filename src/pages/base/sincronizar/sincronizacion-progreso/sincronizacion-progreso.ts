import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, Events } from 'ionic-angular';
import { TablasSincronizacionServiceProvider } from '../../../../providers/tablas-sincronizacion-service/tablas-sincronizacion-service';

@IonicPage()
@Component({
	selector: 'page-sincronizacion-progreso',
	templateUrl: 'sincronizacion-progreso.html'
})
export class SincronizacionProgresoPage {
	resultado: any;
	tablas: any = [];
	cargando: boolean = false;
	avance: string = '';
	msj_fin: string = '';
	porcentaje_total: number = 0;
	porcentaje_tabla: number = 0;
	registro_totales: number = 0;
	registro_numero: number = 0;
	indice: number;
	tabla_sincronizacion: string = '';
	pagina_actual: number = 1;
	pagina_total: number = 1;

	modulo: string;
	tablaActual: number;
	tablaTotal: number;
	totalRegistros: number;
	blnCancelar: boolean = false;

	constructor(
		public viewCtrl: ViewController,
		private params: NavParams,
		private tablasSincronizacion: TablasSincronizacionServiceProvider,
		public events: Events
	) {
		this.modulo = this.params.get('modulo');
		events.subscribe('totalRegistros_sincronizacion', (data) => {
			this.totalRegistros = data;
		});
		events.subscribe('porcentaje_total_sincronizacion', (data) => {
			// console.log("porcentaje_total: "+data);
			this.porcentaje_total = data;
		});
		events.subscribe('tablaActual_sincronizacion', (data) => {
			this.tablaActual = data;
		});
		events.subscribe('tablaTotal_sincronizacion', (data) => {
			this.tablaTotal = data;
		});
		events.subscribe('cargando_sincronizacion', (data) => {
			this.cargando = data;
		});
		events.subscribe('tabla_sincronizacion', (data) => {
			this.tabla_sincronizacion = data;
		});
		events.subscribe('registro_totales_sincronizacion', (data) => {
			this.registro_totales = data;
		});
		events.subscribe('pagina_actual_sincronizar', (data) => {
			this.pagina_actual = data;
		});
		events.subscribe('pagina_total_sincronizar', (data) => {
			this.pagina_total = data;
		});
		events.subscribe('porcentaje_tabla_sincronizacion', (data) => {
			this.porcentaje_tabla = data;
		});
		events.subscribe('blnCancelar_sincronizacion', (data) => {
			this.blnCancelar = data;
		});
		events.subscribe('terminada_sincronizacion', (data) => {
			this.blnCancelar = data;
		});

		this.actualizar();
	}

	cancelar() {
		this.blnCancelar = true;
		this.events.publish('blnCancelar_sincronizar', true);
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

	actualizar() {
		this.cargando = true;
		this.tablasSincronizacion.iniciarSincronizacion(this.modulo);
	}
}
