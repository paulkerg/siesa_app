import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SincronizarPage } from './sincronizar';
import { ComponentsModule } from '../../../components/components.module';


@NgModule({
  declarations: [
    SincronizarPage, 
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SincronizarPage)]
})
export class SincronizarPageModule {}
