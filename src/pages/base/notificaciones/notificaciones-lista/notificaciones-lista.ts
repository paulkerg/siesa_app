import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, Events } from 'ionic-angular';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';

@IonicPage()
@Component({
  selector: 'page-notificaciones-lista',
  templateUrl: 'notificaciones-lista.html',
})
export class NotificacionesListaPage {
  notificaciones: any = [];
  blnLeidos: boolean = false;
  tipoNotificacion: any = {
    "T430": "Pedidos",
    "T350": "Recaudos",
    "T75957": "Aprobaciones",
    "W822": "mantenimiento"
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    public events: Events,
    private dbSql: SqliteProvider,
  ) {
    this.menu = menu;
    this.menu.enable(true, 'menuPrincipal');

    events.subscribe('cambioNotificacion', (n) => {
      this.llenarNotificaciones();
    });
  }

  ionViewDidLoad() {
    this.llenarNotificaciones();
  }

  openMenu() {
    this.menu.open('menuPrincipal');
  }

  llenarNotificaciones() {
    let sql = "select * from t231_mm_notificaciones where f231_ind_lectura  = 0";
    this.dbSql.query(sql)
      .then((resp: any) => {
        this.notificaciones = [];
        if (resp.length > 0) {
          /*
            {
              "tipo": "T75957",
              "accion": "alerta",
              "rowid": 122,
              "documento": "001-DR-229",
              "cliente": "Luis Castillo"
              "ind_lectura" : 0
            }          
          */
          let GrupoNotificaciones = [];
          for (let i = 0; i < resp.length; i++) {
            let registro = JSON.parse(resp[i]['f231_novedad']);
            registro['ind_lectura'] = resp[i]['f231_ind_lectura'];
            if (!GrupoNotificaciones[this.tipoNotificacion[registro['tipo']]]) {
              GrupoNotificaciones[this.tipoNotificacion[registro['tipo']]] = [];
            }
            GrupoNotificaciones[this.tipoNotificacion[registro['tipo']]].push(registro);
          }

          for (var i in GrupoNotificaciones) {
            this.notificaciones.push({
              grupo: i,
              notificaciones: GrupoNotificaciones[i]
            })
          }

        }
      })
      .catch((error) => {
      });
  }

  itemSelected(item) {

  }
}
