import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificacionesListaPage } from './notificaciones-lista';

@NgModule({
  declarations: [
    NotificacionesListaPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificacionesListaPage),
  ],
})
export class NotificacionesListaPageModule {}
