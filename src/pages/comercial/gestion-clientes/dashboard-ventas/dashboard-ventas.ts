import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { ProvPedidosProvider } from '../../../../providers/comercial/prov-pedidos/prov-pedidos';
import { ProvNovedadesProvider } from '../../../../providers/comercial/prov-novedades/prov-novedades';
import { ProvFacturasProvider } from '../../../../providers/comercial/prov-facturas/prov-facturas';
import { ProvRecaudosProvider } from '../../../../providers/comercial/prov-recaudos/prov-recaudos';

@IonicPage()
@Component({
  selector: 'page-dashboard-ventas',
  templateUrl: 'dashboard-ventas.html',
})
export class DashboardVentasPage {
  datos: {};
  tab: any = [];
  items: {};
  itemSeleccionado: string = 'PEDIDOS';

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController,
    private Cped: ProvPedidosProvider, private CNov: ProvNovedadesProvider,
    private CRec: ProvRecaudosProvider, private CFac: ProvFacturasProvider) {
    this.menu = menu;
    this.menu.enable(true, 'menuPrincipal');
  }

  ionViewDidLoad() {
    //llenar baldosas
    this.datos = [{
      'id':'1',
      'imagen': 'assets/icon/compras.png',
      'badge': '',
      'titulo': 'Clientes'
    }, {
      'id':'2',
      'imagen': 'assets/icon/compras.png',
      'badge': '',
      'titulo': 'Items'
    }, {
      'id':'3',
      'imagen': 'assets/icon/compras.png',
      'badge': '',
      'titulo': 'Consultas'
    }];
    //llenar tabs
    this.tab = [{
      'id': '1',
      'titulo': 'PEDIDOS',
      'badge': '2'
    }, {
      'id': '2',
      'titulo': 'RECAUDOS',
      'badge': '3'
    }, {
      'id': '3',
      'titulo': 'FACTURAS',
      'badge': '1'
    }, {
      'id': '4',
      'titulo': 'NOVEDAD',
      'badge': '5'
    }];

    //llamado metodo llena lista de items
    this.fnLlenarLista(null);

  }

  //abrir menu principal.. 
  openMenu() {
    this.menu.open('menuPrincipal');
  }

  //seleccion de baldosa.
  selbaldosa(event) {
    if (event.item.id == '1') {
      this.navCtrl.push('ClientesListaPage');
    } else if (event.item.id == '2') {
      this.navCtrl.push('ItemCapturaPage');
    } else if (event.item.id == '3') {
      this.navCtrl.push('DemosPage');
    }

  }

  //llenar lista de items
  fnLlenarLista(evento) {
    if (evento) {
      this.itemSeleccionado = evento.seleccionado.titulo;
    }

    if (this.itemSeleccionado == "PEDIDOS") {
      this.Cped.getPedidos().then(result => {
        this.items = result;
      });
    } else if (evento.seleccionado.titulo == "RECAUDOS") {
      this.CRec.getRecaudos(1, 2).then(result => {
        this.items = result;
      });
    } else if (this.itemSeleccionado == "FACTURAS") {
      this.CFac.getFacturas(1, 2).then(result => {
        this.items = result;
      });
    } else if (this.itemSeleccionado == "NOVEDAD") {
      this.CNov.getNovedades(1, 2).then(result => {
        this.items = result;
      });
    }

  }

}
