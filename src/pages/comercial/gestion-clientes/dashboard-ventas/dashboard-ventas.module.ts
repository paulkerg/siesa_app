import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardVentasPage } from './dashboard-ventas';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [
    DashboardVentasPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(DashboardVentasPage),
  ],
})
export class DashboardVentasPageModule {}
