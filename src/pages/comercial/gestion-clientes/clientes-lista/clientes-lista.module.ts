import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientesListaPage } from './clientes-lista';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [
    ClientesListaPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ClientesListaPage),
  ],
})
export class ClientesListaPageModule {}
