import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProvClientesProvider } from '../../../../providers/prov-clientes/prov-clientes';

@IonicPage()
@Component({
  selector: 'page-clientes-lista',
  templateUrl: 'clientes-lista.html',
})
export class ClientesListaPage {
  items: any = [];
  texto: any;
  myDate: any;
  prvBotonesTitulo = [
    {
      imagen: "calendar",
      estado: true
    }
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, private cCli: ProvClientesProvider) {
  }

  ionViewDidLoad() {
    this.listarClientes('');
  }

  listarClientes(ev: any) {
    if (ev) {
      this.texto = ev.value;
    }
    if (ev.opt == 'texto' || ev == '') {
      if (this.texto == undefined) {
        this.texto = '';
      } else {
        this.items = [''];
      }
      if (ev.cancelable == true) {
        this.items = [''];
      }
      this.cCli.listarClientes(this.texto).then(respuesta => {
        this.items = respuesta;
      });
    } else if (ev.opt == 'fecha') {
      this.texto = '';
      this.cCli.listarClientesFecha(ev.value).then(respuesta => {
        this.items = respuesta;
      });
    }
  }

  clickItems(evento) {
    this.navCtrl.push('ClientePage', { item: evento });
  }

  //scroll infinito.
  doInfinite(infiniteScroll) {
    setTimeout(() => {
      if (this.texto != '') {
        this.listarClientes(this.texto.toLowerCase());
      }
      infiniteScroll.complete();
    }, 500);
  }

}
