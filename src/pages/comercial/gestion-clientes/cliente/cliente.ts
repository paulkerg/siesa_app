import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProvPedidosProvider } from '../../../../providers/comercial/prov-pedidos/prov-pedidos';
import { ProvNovedadesProvider } from '../../../../providers/comercial/prov-novedades/prov-novedades';
import { ProvRecaudosProvider } from '../../../../providers/comercial/prov-recaudos/prov-recaudos';
import { ProvFacturasProvider } from '../../../../providers/comercial/prov-facturas/prov-facturas';
import { ProvClientesProvider } from '../../../../providers/prov-clientes/prov-clientes';


@IonicPage()
@Component({
  selector: 'page-cliente',
  templateUrl: 'cliente.html',
})
export class ClientePage {
  tab: any = [];
  items: any = [];
  itemSeleccionado: string = 'INFO';
  datacliente: any;
  cliente: any = []
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private Cped: ProvPedidosProvider, private CNov: ProvNovedadesProvider,
    private CRec: ProvRecaudosProvider, private CFac: ProvFacturasProvider,
    private Ccli: ProvClientesProvider) {
  }

  ionViewDidLoad() {
    //Informacion del cliente
    this.datacliente = this.navParams.get('item');
    this.Ccli.leerCliente(this.datacliente.codigo, this.datacliente.id_suc).then(result => {
      this.cliente = result;
    })

    //Tabs.
    this.tab = [{
      'id': '1',
      'titulo': 'INFO',
      'badge': ''
    }, {
      'id': '2',
      'titulo': 'PEDIDOS',
      'badge': ''
    }, {
      'id': '3',
      'titulo': 'RECAUDOS',
      'badge': ''
    }, {
      'id': '4',
      'titulo': 'FACTURAS',
      'badge': ''
    }, {
      'id': '5',
      'titulo': 'NOVEDAD',
      'badge': ''
    }];
    this.fnLlenarLista(null);

  }

  //llenar lista de items
  fnLlenarLista(evento) {
    if (evento) {
      this.itemSeleccionado = evento.seleccionado.titulo;
    }

    if (this.itemSeleccionado == "INFO") {
      this.items = [{
        'id': '1',
        'titulo': 'DIRECCION',
        'texto1': 'CALLE 289 1021345'
      }, {
        'id': '2',
        'titulo': 'TELEFONO',
        'texto1': 'CALLE 2- 245'
      }, {
        'id': '3',
        'titulo': 'CONTACTO',
        'texto1': 'CALLE 5-243'
      }, {
        'id': '4',
        'titulo': 'CIUDAD',
        'texto1': 'CARRERA 2, CLL 23'
      }, {
        'id': '5',
        'titulo': 'CARTERA',
        'texto1': 'AVENIDA 5-a OESTE #2-245'
      }];
    } if (this.itemSeleccionado == "PEDIDOS") {
      this.Cped.getPedidosId(1, 2).then(result => {
        this.items = result;
      });
    } else if (this.itemSeleccionado == "RECAUDOS") {
      this.CRec.getRecaudosId(1, 2).then(result => {
        this.items = result;
      });
    } else if (this.itemSeleccionado == "FACTURAS") {
      this.CFac.leerPorTercero(1, 2).then(result => {
        this.items = result;
      });
    } else if (this.itemSeleccionado == "NOVEDAD") {
      this.CNov.leerNovedad(1, 2).then(result => {
        this.items = result;
      });
    }
  }

  fabClick() {
    if (this.itemSeleccionado == "PEDIDOS") {
      this.navCtrl.push('PedidoPage');
    } else if (this.itemSeleccionado == "RECAUDOS") {
      this.navCtrl.push('RecaudoPage');
    } else if (this.itemSeleccionado == "FACTURAS") {
      this.navCtrl.push('FacturaPage');
    } else if (this.itemSeleccionado == "NOVEDAD") {
      this.navCtrl.push('NovedadPage');
    }
  }

}
