import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosPreferenciasPage } from './inventarios-preferencias';

@NgModule({
  declarations: [
    InventariosPreferenciasPage,
  ],
  imports: [
    IonicPageModule.forChild(InventariosPreferenciasPage),
  ],
})
export class InventariosPreferenciasPageModule {}
