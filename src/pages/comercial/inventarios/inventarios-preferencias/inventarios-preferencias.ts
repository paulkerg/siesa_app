import { Component } from '@angular/core';
import { IonicPage, Loading, Events } from 'ionic-angular';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../../app/global';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';

@IonicPage()
@Component({
	selector: 'page-inventarios-preferencias',
	templateUrl: 'inventarios-preferencias.html'
})
export class InventariosPreferenciasPage {
	loading: Loading;
	tInventarioSel: string = 'null';
	tInventarios: any = [];
	tLprecioSel: string = 'null';
	tLprecios: any = [];
	tBodegasSel: string = 'null';
	tBodegas: any = [];
	tGBodegasSel: string = 'null';
	tGBodegas: any = [];
	planCriterioSel: string = 'null';
	blnVerImagenes: boolean = false;

	constructor(
		private sync: SincronizacionProvider,
		private db: SqliteProvider,
		public events: Events,
	) { }

	ngOnInit() {
		this.tInventarioSel = 'null';
		this.tLprecioSel = 'null';
		this.tBodegasSel = 'null';
		this.tGBodegasSel = 'null';
		this.planCriterioSel = 'null';
		this.blnVerImagenes = false;
		this.events.publish('cargandoOn');

		this.cargaTInv();
	}

	ionViewWillEnter() {
		let sql = "select * from configuraciones where id = 'parametros_inventarios'";
		this.db
			.query(sql)
			.then((respuesta) => {
				if (respuesta.length > 0) {
					let jsonObj = JSON.parse(respuesta[0]['documento']);
					this.tInventarioSel = jsonObj['tInventarioSel'];
					this.tLprecioSel = jsonObj['tLprecioSel'];
					this.tBodegasSel = jsonObj['tBodegasSel'];
					this.tGBodegasSel = jsonObj['tGBodegasSel'];
					this.planCriterioSel = jsonObj['planCriterioSel'];
					this.blnVerImagenes = jsonObj['blnVerImagenes'];
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}

	cargaTInv() {
		let p1 = {
			'Parametros': '|',
			'Consulta': 'LEER_TIPO_INVENTARIO_TODOS'
		};
		this.sync.postParameters(global.api.consulta, p1)
			.then((r) => {
				if (r['length'] > 0) {
					this.tInventarios = [
						{
							f_id: 'null',
							f_descripcion: 'Ninguno'
						}
					];
					for (let i = 0; i < r['length']; i++) {
						this.tInventarios.push(r[i]);
					}
				}
				this.cargaLPrecios();
			});
	}

	cargaLPrecios() {
		let p2 = {
			'Parametros': '|',
			'Consulta': 'LEER_LISTA_PRECIOS_TODAS'
		};
		this.sync.postParameters(global.api.consulta, p2)
			.then((r) => {
				this.tLprecios = [
					{
						f_id: 'null',
						f_descripcion: 'Ninguno'
					}
				];
				for (let i = 0; i < r['length']; i++) {
					this.tLprecios.push(r[i]);
				}

				this.cargaBodegas();
			});
	}

	cargaBodegas() {
		let p3 = {
			'Parametros': '|',
			'Consulta': 'LEER_BODEGAS_TODAS'
		};
		this.sync.postParameters(global.api.consulta, p3)
			.then((r) => {
				if (r['length'] > 0) {
					this.tBodegas = [
						{
							f_id: 'null',
							f_descripcion: 'Ninguno'
						}
					];
					for (let i = 0; i < r['length']; i++) {
						this.tBodegas.push(r[i]);
					}
				}
				this.cargaGrupoBodegas();
			});
	}

	cargaGrupoBodegas() {
		let p4 = {
			'Parametros': '|',
			'Consulta': 'LEER_GRUPO_BODEGAS_TODAS'
		};
		this.sync.postParameters(global.api.consulta, p4)
			.then((r) => {
				if (r['length'] > 0) {
					this.tGBodegas = [
						{
							f_id: 'null',
							f_descripcion: 'Ninguno'
						}
					];
					for (let i = 0; i < r['length']; i++) {
						this.tGBodegas.push(r[i]);
					}
				}
				this.events.publish('cargandoOff');
			});
	}

	cambiaDatos() {
		if (this.tGBodegasSel != 'null') {
			this.tBodegasSel = 'null';
		}
		if (this.tBodegasSel != 'null') {
			this.tGBodegasSel = 'null';
		}
		let filtrosP = {
			tInventarioSel: this.tInventarioSel,
			tLprecioSel: this.tLprecioSel,
			tBodegasSel: this.tBodegasSel,
			tGBodegasSel: this.tGBodegasSel,
			planCriterioSel: this.planCriterioSel,
			blnVerImagenes: this.blnVerImagenes
		};
		let sql =
			"insert or replace into configuraciones(id,documento) values('parametros_inventarios','" +
			JSON.stringify(filtrosP) +
			"')";
		this.db
			.query(sql)
			.then((r) => {
			})
			.catch((error) => {
				console.log(error);
			});
	}

}
