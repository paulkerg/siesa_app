import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosPrincipalPage } from './inventarios-principal';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    InventariosPrincipalPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(InventariosPrincipalPage),
  ],
})
export class InventariosPrincipalPageModule { }
