import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, PopoverController, Loading, Events } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { global } from '../../../../app/global';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';

@IonicPage()
@Component({
	selector: 'page-inventarios-principal',
	templateUrl: 'inventarios-principal.html',
})
export class InventariosPrincipalPage {
	@ViewChild('popoverContent', { read: ElementRef })
	content: ElementRef;
	@ViewChild('popoverText', { read: ElementRef })
	text: ElementRef;
	loading: Loading;
	blnScroll: boolean = true;
	strBuscar: string = '';
	items = [];
	registros: any = [];
	parametros = {
		tipoInventario: '',
		planCriterio: {
			plan: '',
			criterio: ''
		},
		bodega: '',
		grupoBodega: ''
	};
	pagina: number = 1;
	regPagina: number = 10;

	tInventarioSel: string = 'null';
	tLprecioSel: string = 'null';
	tBodegasSel: string = 'null';
	tGBodegasSel: string = 'null';
	planCriterioSel: string = 'null';
	blnVerImagenes: boolean = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private menu: MenuController,
		private barcodeScanner: BarcodeScanner,
		private sync: SincronizacionProvider,
		private popoverCtrl: PopoverController,
		private db: SqliteProvider,
		public events: Events,
	) {
		this.menu = menu;
	}

	ionViewWillEnter() {
		this.menu.enable(true, 'menuPrincipal');
	}

	buscarQR() {
		this.barcodeScanner.scan().then(
			(barcodeData) => {
				this.strBuscar = barcodeData['text']; //JSON.stringify(barcodeData);
				this.buscarItem();
			},
			(err) => {
				console.log(err);
			}
		);
	}

	buscarItem() {
		this.blnScroll = true;
		this.pagina = 1;
		this.items = [];
		this.registros = [];
		if (this.strBuscar.length > 0) {
			let sql = "select * from configuraciones where id = 'parametros_inventarios'";
			this.db
				.query(sql)
				.then((respuesta) => {
					if (respuesta.length > 0) {
						let jsonObj = JSON.parse(respuesta[0]['documento']);
						this.tInventarioSel = jsonObj['tInventarioSel'];
						this.tLprecioSel = jsonObj['tLprecioSel'];
						this.tBodegasSel = jsonObj['tBodegasSel'];
						this.tGBodegasSel = jsonObj['tGBodegasSel'];
						this.planCriterioSel = jsonObj['planCriterioSel'];
						this.blnVerImagenes = jsonObj['blnVerImagenes'];
					}
					this.llamarPost().then((e) => { });
				})
				.catch((error) => {
					console.log(error);
				});
		}
	}

	llamarPost() {
		this.events.publish('cargandoOn');
		return new Promise((resolve) => {
			let pfin: number = this.pagina + this.regPagina;
			let parametros =
				this.strBuscar +
				'|' +
				this.pagina +
				'|' +
				pfin +
				'|' +
				this.tInventarioSel +
				'|' +
				this.planCriterioSel +
				'|' +
				this.tLprecioSel +
				'|' +
				this.tBodegasSel +
				'|' +
				this.tGBodegasSel +
				'|' +
				this.blnVerImagenes +
				'|';

			let p1 = {
				Parametros: parametros,
				Consulta: 'BUSCAR_ITEM_SIESAPPS'
			};
			this.sync
				.postParameters(global.api.consulta, p1)
				.then((r) => {
					if (r['length'] > 0) {
						for (let i = 0; i < r['length']; i++) {
							if ((r[i]['f_foto'] === '')) {
								r[i]['f_foto'] = 'assets/imgs/no_image.png';
							} else {
								r[i]['f_foto'] = 'data:image/png;base64,' + r[i]['f_foto'];
							}
							this.items.push(r[i]);
							this.registros.push({
								foto: {
									tipo: 'avatar',
									imagen: r[i]['f_foto']
								},
								titulo: {
									texto: r[i]['f_descripcion']
								},
								texto1: {
									texto: "Item: " + r[i]['f_item_resumen'] + ' Ref.: ' + r[i]['f_referencia']
								},
								derecha1: {
									tipo: 'moneda',
									posicion: 'arriba',
									texto: r[i]['f_precio_vta']
								}//this.cp.transform(r[i]['f_precio_vta'], '$'),
							})
						}
						this.pagina = pfin;
					}
					this.events.publish('cargandoOff');
					resolve(r);
				})
				.catch((e) => {
					this.events.publish('cargandoOff');
					console.log(e);
				});
		});
	}

	filtros(ev) {
		let popover = this.popoverCtrl.create('InventariosPreferenciasPage', {
			contentEle: this.content.nativeElement,
			textEle: this.text.nativeElement
		});

		popover.present({
			ev: ev
		});

		popover.onDidDismiss(() => {
			this.buscarItem();
		});
	}

	onInput(ev) {
		if (ev.keyCode === 13) {
			this.buscarItem();
		}
	}

	doInfinite(infiniteScroll) {
		this.llamarPost().then((d: any) => {
			infiniteScroll.complete();
			if (d.length < this.regPagina) {
				this.blnScroll = false;
			}
		});
	}

	itemSelected(item) {
		this.navCtrl.push('InventariosDetallePage', { itemSel: this.items[item['indice']] });
	}

	openMenu() {
		this.menu.open('menuPrincipal');
	}
}
