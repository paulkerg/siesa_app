import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, Events } from 'ionic-angular';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';
import { ImageViewerController } from 'ionic-img-viewer';
import { global } from '../../../../app/global';
import { List } from 'linqts';

@IonicPage()
@Component({
  selector: 'page-inventarios-lote-serial',
  templateUrl: 'inventarios-lote-serial.html',
})
export class InventariosLoteSerialPage {
  loading: Loading;

  itemSel = {
    item_id: "",
    item_rowid: 0,
    item_referencia: "",
    item_descripcion: "",
    item_descripcion_corta: "",
    item_resumen: "",
    item_id_grupo_impositivo: "",
    item_id_tipo_inv_serv: "",
    item_id_grupo_dscto: "",
    item_ind_tipo_item: 0,
    item_ind_lista_precios_ext: 0,
    item_id_unidad_inventario: "",
    item_id_unidad_adicional: "",
    item_id_unidad_orden: "",
    item_id_unidad_empaque: "",
    item_id_extension1: "",
    item_id_extension2: "",
    item_rowid_foto: 0,
    item_ind_lote: 0,
    item_ind_serial: 0,
    item_ind_paquete: 0,
    item_ext_rowid: 0,
    item_ext_id_ext1_detalle: "",
    item_ext_id_ext2_detalle: "",
    item_ext_rowid_foto: 0,
    item_ext_id_extension1: "",
    item_ext_id_extension2: "",
    item_ext_desc_ext1_detalle: "",
    item_ext_desc_ext2_detalle: "",
    item_ext_descripcion_toda: "",
    item_foto: null,
    item_precio: 0
  };
  items: any = [];
  bodegaSel: any = [];
  tInventarioSel: string = 'null';
  tLprecioSel: string = 'null';
  tBodegasSel: string = 'null';
  tGBodegasSel: string = 'null';
  planCriterioSel: string = 'null';
  indLoteSerial: number;
  origen: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sync: SincronizacionProvider,
    public imageViewerCtrl: ImageViewerController,
    private db: SqliteProvider,
    public events: Events,
  ) {
  }

  ionViewDidLoad() {
    this.events.publish('cargandoOn');
    this.origen = this.navParams.get('origen');
    this.itemSel = this.navParams.get('itemSel');
    this.bodegaSel = this.navParams.get('bodegaSel');
    this.indLoteSerial = 0;
    if (this.itemSel.item_ind_lote != 0) {
      this.indLoteSerial = 1;
    }
    if (this.itemSel.item_ind_serial != 0) {
      this.indLoteSerial = 2;
    }
    if (this.indLoteSerial == 0) {
      this.indLoteSerial = 3;
    }

    console.log(this.itemSel, this.bodegaSel);
    this.cargaDatos();
  }

  cargaDatos() {
    let sql = "select * from configuraciones where id = 'parametros_inventarios'";
    this.db
      .query(sql)
      .then((respuesta) => {
        if (respuesta.length > 0) {
          let reg = JSON.parse(respuesta[0]['documento']);
          this.tInventarioSel = reg['tInventarioSel'];
          this.tLprecioSel = reg['tLprecioSel'];
          this.tBodegasSel = this.bodegaSel['bodega'];
          this.tGBodegasSel = null;
          this.planCriterioSel = reg['planCriterioSel'];

          let parametros =
            this.itemSel.item_rowid +
            '|' +
            this.itemSel.item_ext_rowid +
            '|' +
            this.tInventarioSel +
            '|' +
            this.planCriterioSel +
            '|' +
            this.tLprecioSel +
            '|' +
            this.tBodegasSel +
            '|' +
            this.tGBodegasSel +
            '|' +
            this.indLoteSerial +
            '|' +
            this.origen +
            '|';
            let p1 = {
              'Parametros': parametros,
              'Consulta': 'BUSCAR_EXIST_BODEGA_SERIA_LOTE'
            };
            this.sync.postParameters(global.api.consulta, p1)      
            .then((rf: any) => {
              if (rf['length'] > 0) {

                console.log(rf);
                if (rf.length > 0) {
                  if (this.origen == "D" || this.origen == "E") {
                    //--- Lotes
                    if (this.indLoteSerial == 1) {
                      for (let i = 0; i < rf.length; i++) {
                        const e = rf[i];
                        this.items.push({
                          titulo: "Ubicacion : " + e['ubicacion'],
                          subTitulo: "Lote : " + e['lote'],
                          badge: e['cant_existencia']
                        })
                      }
                    }
                    //--- Seriales
                    if (this.indLoteSerial == 2) {
                      let array = new List<any>(rf)
                        .Where((x) => x['f_id_bodega'] == this.bodegaSel['bodega'])
                        .GroupBy((key) => key['f_id'], (element) => element);
                      //console.log(array);
                      let labels = Object.keys(array);
                      for (let p = 0; p < labels.length; p++) {
                        let e = array[labels[p]][0];
                        this.items.push({
                          titulo: "Docto. creacion : " + e['f_docto_creacion1'],
                          subTitulo: "Serial : " + e['f_id'],
                          texto1: "Estado : " + e['f_ind_estado']
                        })
                      }
                    }
                    //--- Normales
                    if (this.indLoteSerial == 3) {
                      for (let i = 0; i < rf.length; i++) {
                        const e = rf[i];
                        this.items.push({
                          titulo: "Ubicacion : " + e['ubicacion'],
                          subTitulo: "Lote : " + e['lote'],
                          badge: e['cant_existencia']
                        })
                      }
                    }
                  }
                  if (this.origen == "C") {
                    for (let i = 0; i < rf.length; i++) {
                      const e = rf[i];
                      this.items.push({
                        titulo: "Dcto. : " + e['f_co'] + "-" + e['f_nrodocto'],
                        subTitulo: "Fecha : " + e['f_fecha'],
                        badge: e['f_cant_comprom_base']
                      });
                    }
                  }
                  if (this.origen == "O") {
                    for (let i = 0; i < rf.length; i++) {
                      const e = rf[i];
                      this.items.push({
                        titulo: "Comprador : " + e['f_comprador'],
                        subTitulo: "Orden : " + e['f_nroorden'],
                        texto1: "Vlr. Neto : " + e['f_valor_neto_docto'],
                        badge: e['f_cant_orden_base']
                      });
                    }
                  }
                  if (this.origen == "T") {
                    let array = new List<any>(rf)
                    .Where((x) => x['f_um'] == this.itemSel.item_id_unidad_inventario)
                    .ToArray();
                    let items = [];
                    for (let t = 0; t < array.length; t++) {
                      items.push({
                        titulo: "Docto. : " + array[t]['f_nrodocto'], //e['f_nrodocto'],
                        texto1: "Bodega salida : " + array[t]['f_bodega_sal'], //e['f_bodega_sal'],
                        texto2: "Bodega entrada : " + array[t]['f_bodega_ent'], //e['f_bodega_ent'],
                        badge: array[t]['f_cant_1'] //e['f_cant_1']
                      });
                    }
                    this.items = items;
                  }

                  this.events.publish('cargandoOff');

                }
              }
            })
            .then(() => {
            });
        }

      })
      .catch((error) => {
        console.log(error);
      });
  }

  onClick(imageToView) {
    console.log('onClick');
    const viewer = this.imageViewerCtrl.create(imageToView);
    viewer.present();
  }

}
