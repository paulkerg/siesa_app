import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosLoteSerialPage } from './inventarios-lote-serial';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    InventariosLoteSerialPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(InventariosLoteSerialPage),
  ],
})
export class InventariosLoteSerialPageModule {}
