import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,  Loading } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';


@IonicPage()
@Component({
	selector: 'page-inventarios-bodegas-detalle',
	templateUrl: 'inventarios-bodegas-detalle.html',
})
export class InventariosBodegasDetallePage {
	loading: Loading;
	bodegaSel: any = [];

	itemSel = {
		item_id: "",
		item_rowid: 0,
		item_referencia: "",
		item_descripcion: "",
		item_descripcion_corta: "",
		item_resumen: "",
		item_id_grupo_impositivo: "",
		item_id_tipo_inv_serv: "",
		item_id_grupo_dscto: "",
		item_ind_tipo_item: 0,
		item_ind_lista_precios_ext: 0,
		item_id_unidad_inventario: "",
		item_id_unidad_adicional: "",
		item_id_unidad_orden: "",
		item_id_unidad_empaque: "",
		item_id_extension1: "",
		item_id_extension2: "",
		item_rowid_foto: 0,
		item_ind_lote: 0,
		item_ind_serial: 0,
		item_ind_paquete: 0,
		item_ext_rowid: 0,
		item_ext_id_ext1_detalle: "",
		item_ext_id_ext2_detalle: "",
		item_ext_rowid_foto: 0,
		item_ext_id_extension1: "",
		item_ext_id_extension2: "",
		item_ext_desc_ext1_detalle: "",
		item_ext_desc_ext2_detalle: "",
		item_ext_descripcion_toda: "",
		item_foto: null,
		item_precio: 0
	};
	itemResumen: any = {};
	tInventarioSel: string = 'null';
	tLprecioSel: string = 'null';
	tBodegasSel: string = 'null';
	tGBodegasSel: string = 'null';
	planCriterioSel: string = 'null';

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public imageViewerCtrl: ImageViewerController,
	) { }

	ionViewWillEnter() {
		this.itemSel = this.navParams.get('itemSel');
		this.bodegaSel = this.navParams.get('item');
	}

	onClick(imageToView) {
		const viewer = this.imageViewerCtrl.create(imageToView);
		viewer.present();
	}

	verLoteSerialPG(origen) {
		this.navCtrl.push('InventariosLoteSerialPage', {
			origen: origen,
			itemSel: this.itemSel,
			bodegaSel: this.bodegaSel
		});
	}
}

