import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosBodegasDetallePage } from './inventarios-bodegas-detalle';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    InventariosBodegasDetallePage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(InventariosBodegasDetallePage),
  ],
})
export class InventariosBodegasDetallePageModule {}
