import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosPreciosPage } from './inventarios-precios';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    InventariosPreciosPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(InventariosPreciosPage),
  ],
})
export class InventariosPreciosPageModule {}
