import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, Events } from 'ionic-angular';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../../app/global';
import { ImageViewerController } from 'ionic-img-viewer';
import { List } from 'linqts';

@IonicPage()
@Component({
	selector: 'page-inventarios-precios',
	templateUrl: 'inventarios-precios.html'
})
export class InventariosPreciosPage {
	loading: Loading;
	items: any = [];

	itemSel = {
		item_id: "",
		item_rowid: 0,
		item_referencia: "",
		item_descripcion: "",
		item_descripcion_corta: "",
		item_resumen: "",
		item_id_grupo_impositivo: "",
		item_id_tipo_inv_serv: "",
		item_id_grupo_dscto: "",
		item_ind_tipo_item: 0,
		item_ind_lista_precios_ext: 0,
		item_id_unidad_inventario: "",
		item_id_unidad_adicional: "",
		item_id_unidad_orden: "",
		item_id_unidad_empaque: "",
		item_id_extension1: "",
		item_id_extension2: "",
		item_rowid_foto: 0,
		item_ind_lote: 0,
		item_ind_serial: 0,
		item_ind_paquete: 0,
		item_ext_rowid: 0,
		item_ext_id_ext1_detalle: "",
		item_ext_id_ext2_detalle: "",
		item_ext_rowid_foto: 0,
		item_ext_id_extension1: "",
		item_ext_id_extension2: "",
		item_ext_desc_ext1_detalle: "",
		item_ext_desc_ext2_detalle: "",
		item_ext_descripcion_toda: "",
		item_foto: null,
		item_precio: 0
	}

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private sync: SincronizacionProvider,
		public imageViewerCtrl: ImageViewerController,
		public events: Events,
	) { }

	ionViewWillEnter() {
		this.itemSel = this.navParams.get('itemSel');
		this.cargaDatos();
	}

	cargaDatos() {
		this.events.publish('cargandoOn');
		let items = [];
		let p1 = {
			'Parametros': this.itemSel.item_rowid + '|',
			'Consulta': 'LEER_ITEM_LISTAS_PRECIOS'
		};
		this.sync.postParameters(global.api.consulta, p1,0)
			.then((rf: any) => {
				if (rf['length'] > 0) {
					let array: any = [];
					if (this.itemSel['item_ind_lista_precios_ext'] === 0) {
						array = new List<any>(rf)
							.Where((x) => x['f_rowid_item'] == this.itemSel.item_rowid)
							.Where((x) => x['f_um'] == this.itemSel.item_id_unidad_inventario)
							.ToArray();
					} else {
						array = new List<any>(rf)
							.Where((x) => x['f_rowid_item_ext'] == this.itemSel.item_ext_rowid)
							.Where((x) => x['f_um'] == this.itemSel.item_id_unidad_inventario)
							.ToArray();
					}
					for (let t = 0; t < array.length; t++) {
						let precio = array[t]['f_precio'];

						if (parseFloat(array[t]['f_precio']) == 0) {
							precio = array[t]['f_precio_sug'];
						}
						if (precio == 0) {
							precio = array[t]['f_precio_max'];
						}
						items.push({
							f_lista_desc: array[t]['f_lista'] + ' ' + array[t]['f_lista_desc'],
							f_precio: precio,
							f_unidad: array[t]['f_um']
						});
					}
					this.items = items;
				}
			}).then(() => {
				this.events.publish('cargandoOff');
			});
	}

	onClick(imageToView) {
		const viewer = this.imageViewerCtrl.create(imageToView);
		viewer.present();
	}

}
