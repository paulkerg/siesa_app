import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, Events } from 'ionic-angular';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../../app/global';
import { ImageViewerController } from 'ionic-img-viewer';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';
import { itemExt } from '../../../../models/interfaces/items';

@IonicPage()
@Component({
	selector: 'page-inventarios-detalle',
	templateUrl: 'inventarios-detalle.html'
})
export class InventariosDetallePage {
	loading: Loading;
	itemResumen: any = {};
	itemResumenDefa: any = {};
	tInventarioSel: string = 'null';
	tLprecioSel: string = 'null';
	tBodegasSel: string = 'null';
	tGBodegasSel: string = 'null';
	planCriterioSel: string = 'null';
	itemSel = {
		item_id: "",
		item_rowid: 0,
		item_referencia: "",
		item_descripcion: "",
		item_descripcion_corta: "",
		item_resumen: "",
		item_id_grupo_impositivo: "",
		item_id_tipo_inv_serv: "",
		item_id_grupo_dscto: "",
		item_ind_tipo_item: 0,
		item_ind_lista_precios_ext: 0,
		item_id_unidad_inventario: "",
		item_id_unidad_adicional: "",
		item_id_unidad_orden: "",
		item_id_unidad_empaque: "",
		item_id_extension1: "",
		item_id_extension2: "",
		item_rowid_foto: 0,
		item_ind_lote: 0,
		item_ind_serial: 0,
		item_ind_paquete: 0,
		item_ext_rowid: 0,
		item_ext_id_ext1_detalle: "",
		item_ext_id_ext2_detalle: "",
		item_ext_rowid_foto: 0,
		item_ext_id_extension1: "",
		item_ext_id_extension2: "",
		item_ext_desc_ext1_detalle: "",
		item_ext_desc_ext2_detalle: "",
		item_ext_descripcion_toda: "",
		item_foto: null,
		item_precio: 0
	}
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private sync: SincronizacionProvider,
		public imageViewerCtrl: ImageViewerController,
		private db: SqliteProvider,
		public events: Events,
	) { }

	ionViewWillEnter() {
		let i = this.navParams.get('itemSel');
		let sql = "select * from configuraciones where id = 'parametros_inventarios'";
		this.db
			.query(sql)
			.then((respuesta) => {
				if (respuesta.length > 0) {
					let jsonObj = JSON.parse(respuesta[0]['documento']);
					this.tInventarioSel = jsonObj['tInventarioSel'];
					this.tLprecioSel = jsonObj['tLprecioSel'];
					this.tBodegasSel = jsonObj['tBodegasSel'];
					this.tGBodegasSel = jsonObj['tGBodegasSel'];
					this.planCriterioSel = jsonObj['planCriterioSel'];
				}
			})
			.catch((error) => {
				console.log(error);
			});

		if (i) {
			this.events.publish('cargandoOn');
			this.itemSel = {
				item_id: i.f_id,
				item_rowid: i.f_rowid_item,
				item_referencia: i.f_referencia,
				item_descripcion: i.f_descripcion,
				item_descripcion_corta: i.descripcion_corta,
				item_resumen: i.f_item_resumen,
				item_id_grupo_impositivo: i.f_id_grupo_impositivo,
				item_id_tipo_inv_serv: i.f_id_tipo_inv_serv,
				item_id_grupo_dscto: i.f_id_grupo_dscto,
				item_ind_tipo_item: i.f_ind_tipo_item,
				item_ind_lista_precios_ext: i.f_ind_lista_precios_ext,
				item_id_unidad_inventario: i.f_id_unidad_inventario,
				item_id_unidad_adicional: i.f_id_unidad_adicional,
				item_id_unidad_orden: i.f_id_unidad_orden,
				item_id_unidad_empaque: i.f_id_unidad_empaque,
				item_id_extension1: i.f_id_extension1,
				item_id_extension2: i.f_id_extension2,
				item_rowid_foto: i.f_rowid_foto,
				item_ind_lote: i.f_ind_lote,
				item_ind_serial: i.f_ind_serial,
				item_ind_paquete: i.f_ind_paquete,
				item_ext_rowid: i.f_rowid_item_ext,
				item_ext_id_ext1_detalle: i.f_id_ext1_detalle,
				item_ext_id_ext2_detalle: i.f_id_ext2_detalle,
				item_ext_rowid_foto: i.f_ext_rowid_foto,
				item_ext_id_extension1: i.f_id_extension1,
				item_ext_id_extension2: i.f_id_extension2,
				item_ext_desc_ext1_detalle: "",
				item_ext_desc_ext2_detalle: "",
				item_ext_descripcion_toda: "",
				item_foto: 'assets/imgs/no_image.png',
				item_precio: i.f_precio_vta
			}
			this.cargaDatos(i);
		}
	}

	cargaDatos(i) {
		let p1 = {
			'Parametros': this.itemSel.item_ext_rowid + '|',
			'Consulta': 'BUSCAR_ITEM_SOLO_FOTO'
		};
		this.sync.postParameters(global.api.consulta, p1)
			.then((rf) => {
				if (rf['length'] > 0) {
					if (rf[0]['f_foto'] != '') {
						this.itemSel.item_foto = 'data:image/png;base64,' + rf[0]['f_foto'];
					}
				}

				itemExt.itemSel = this.itemSel;

				let parametros =
					this.itemSel.item_rowid +
					'|' +
					this.itemSel.item_ext_rowid +
					'|' +
					this.tInventarioSel +
					'|' +
					this.planCriterioSel +
					'|' +
					this.tLprecioSel +
					'|' +
					'null' + //this.tBodegasSel +
					'|' +
					this.tGBodegasSel +
					'|';

				let p2 = {
					'Parametros': parametros,
					'Consulta': 'LEER_ITEM_MONITOR'
				};
				this.sync.postParameters(global.api.consulta, p2)
					.then((r) => {
						if (r['length'] > 0) {
							this.itemResumen = r[0];
						}
					}).then(() => {
						this.events.publish('cargandoOff');
					});
			});
	}

	verPrecios() {
		this.navCtrl.push('InventariosPreciosPage', { itemSel: this.itemSel });
	}

	verExistencias() {
		this.navCtrl.push('InventariosBodegasPage', { itemSel: this.itemSel });
	}

	onClick(imageToView) {
		const viewer = this.imageViewerCtrl.create(imageToView);
		viewer.present();
	}

}
