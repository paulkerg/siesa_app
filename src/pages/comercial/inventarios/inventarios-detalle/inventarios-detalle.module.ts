import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosDetallePage } from './inventarios-detalle';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    InventariosDetallePage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(InventariosDetallePage),
  ],
})
export class InventariosDetallePageModule {}
