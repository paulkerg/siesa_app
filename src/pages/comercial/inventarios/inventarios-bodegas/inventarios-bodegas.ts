import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, Events } from 'ionic-angular';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../../app/global';
import { ImageViewerController } from 'ionic-img-viewer';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';

@IonicPage()
@Component({
	selector: 'page-inventarios-bodegas',
	templateUrl: 'inventarios-bodegas.html'
})
export class InventariosBodegasPage {
	loading: Loading;
	items: any = [];

	itemSel = {
		item_id: "",
		item_rowid: 0,
		item_referencia: "",
		item_descripcion: "",
		item_descripcion_corta: "",
		item_resumen: "",
		item_id_grupo_impositivo: "",
		item_id_tipo_inv_serv: "",
		item_id_grupo_dscto: "",
		item_ind_tipo_item: 0,
		item_ind_lista_precios_ext: 0,
		item_id_unidad_inventario: "",
		item_id_unidad_adicional: "",
		item_id_unidad_orden: "",
		item_id_unidad_empaque: "",
		item_id_extension1: "",
		item_id_extension2: "",
		item_rowid_foto: 0,
		item_ind_lote: 0,
		item_ind_serial: 0,
		item_ind_paquete: 0,
		item_ext_rowid: 0,
		item_ext_id_ext1_detalle: "",
		item_ext_id_ext2_detalle: "",
		item_ext_rowid_foto: 0,
		item_ext_id_extension1: "",
		item_ext_id_extension2: "",
		item_ext_desc_ext1_detalle: "",
		item_ext_desc_ext2_detalle: "",
		item_ext_descripcion_toda: "",
		item_foto: null,
		item_precio: 0
	}
	itemResumen: any = {};
	tInventarioSel: string = 'null';
	tLprecioSel: string = 'null';
	tBodegasSel: string = 'null';
	tGBodegasSel: string = 'null';
	planCriterioSel: string = 'null';

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private sync: SincronizacionProvider,
		public imageViewerCtrl: ImageViewerController,
		private db: SqliteProvider,
		public events: Events,
	) { }

	ionViewWillEnter() {
		this.itemSel = this.navParams.get('itemSel');
		this.cargaDatos();
	}

	cargaDatos() {
		this.events.publish('cargandoOn');

		let sql = "select * from configuraciones where id = 'parametros_inventarios'";
		this.db
			.query(sql)
			.then((respuesta) => {
				if (respuesta.length > 0) {
					let jsonObj = JSON.parse(respuesta[0]['documento']);
					this.tInventarioSel = jsonObj['tInventarioSel'];
					this.tLprecioSel = jsonObj['tLprecioSel'];
					this.tBodegasSel = 'null'; //jsonObj['tBodegasSel'];
					this.tGBodegasSel = jsonObj['tGBodegasSel'];
					this.planCriterioSel = jsonObj['planCriterioSel'];

					let parametros =
						this.itemSel.item_rowid +
						'|' +
						this.itemSel.item_ext_rowid +
						'|' +
						this.tInventarioSel +
						'|' +
						this.planCriterioSel +
						'|' +
						this.tLprecioSel +
						'|' +
						this.tBodegasSel +
						'|' +
						this.tGBodegasSel +
						'|';
					let p1 = {
						'Parametros': parametros,
						'Consulta': 'BUSCAR_ITEM_EXIST_BODEGA_APPS'
					};
					this.sync.postParameters(global.api.consulta, p1)
						.then((rf: any) => {
							if (rf['length'] > 0) {
								this.items = rf;
								this.events.publish('cargandoOff');
							}
						});
				}
			})
			.catch((error) => {
				this.events.publish('cargandoOff');
			});

	}

	onClick(imageToView) {
		const viewer = this.imageViewerCtrl.create(imageToView);
		viewer.present();
	}

	verBodegasDetalle(item) {
		this.navCtrl.push('InventariosBodegasDetallePage', { itemSel: this.itemSel, item: item });
	}
}
