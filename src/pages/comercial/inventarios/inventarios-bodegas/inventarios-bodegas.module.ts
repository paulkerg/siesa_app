import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariosBodegasPage } from './inventarios-bodegas';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    InventariosBodegasPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(InventariosBodegasPage),
  ],
})
export class InventariosBodegasPageModule {}
