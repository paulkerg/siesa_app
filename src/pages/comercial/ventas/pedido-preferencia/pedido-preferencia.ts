import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PedidoPreferenciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pedido-preferencia',
  templateUrl: 'pedido-preferencia.html',
})
export class PedidoPreferenciaPage {
  
  items:any=[{titulo:'Compartir',estado:'compartir'},{titulo:'Imprimir',estado:'compartir'}];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidoPreferenciaPage');
  }
  guardar() {
			this.salir();
	}
	salir() {
		this.navCtrl.pop();
	}

}
