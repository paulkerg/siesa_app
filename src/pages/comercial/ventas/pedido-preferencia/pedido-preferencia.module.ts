import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoPreferenciaPage } from './pedido-preferencia';

@NgModule({
  declarations: [
    PedidoPreferenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(PedidoPreferenciaPage),
  ],
})
export class PedidoPreferenciaPageModule {}
