import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovedadPage } from './novedad';

@NgModule({
  declarations: [
    NovedadPage,
  ],
  imports: [
    IonicPageModule.forChild(NovedadPage),
  ],
})
export class NovedadPageModule {}
