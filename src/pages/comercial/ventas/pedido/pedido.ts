import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ActionSheetController, Platform } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ManejoArchivosProvider } from '../../../../providers/manejo-archivos/manejo-archivos';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { UtilesProvider } from '../../../../providers/utiles/utiles';

@IonicPage()
@Component({
  selector: 'page-pedido',
  templateUrl: 'pedido.html',
})
export class PedidoPage {
  items: any = [];
  pedido: any = [];
  num: any = 0;
  sheet: any = 'false';
  pCompBlnImagen: any = 'assets/images/logo/login-3.png';
  letterObj = {
    to: '',
    from: '',
    text: ''
  };
  registrosheet: any = [];
  pdf:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private popoverCtrl: PopoverController, private socialSharing: SocialSharing,
    private msg: UtilesProvider,
    public actionSheetCtrl: ActionSheetController, private platform: Platform, 
    private sync: SincronizacionProvider, private mf: ManejoArchivosProvider) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidoPage');
    this.items = [{
      "titulo": 'Empresa',
      "texto1": 'Lista de precios : 1245878',
      'texto2': 'Condicion de pago 2018/03/12'
    }]

    //registros que iran en el componente actionsheet.

    this.registrosheet = [{
      titulo: 'TOTAL $ 30800',
      subtitulo: 'TOTAL',
      col1text1: '5 items',
      col2text1: 'BRUTO',
      col2text2: 'IMPUESTOS',
      col2text3: 'DESCUENTOS',
      col3text1: ' $ 25800',
      col3text2: ' $ 2500',
      col3text3: ' $ 2500',
    }]
  }

  nuevoItem(ev) {
    if (ev == 'cero' || this.pedido == '') {
      this.pedido = [{
        'titulo': 'Azucar',
        'texto1': '1qqqqqqq',
        'texto2': '222222',
        'badge': '50 und',
        'imagen': this.pCompBlnImagen
      }];
    }
    else if (ev == 'uno' && this.pedido != '') {
      this.pedido.push(this.pedido[0])
    }
    console.log("pedido... ", this.pedido)
  }

  //Eliminar un item .
  eliminarItem(ev) {
    const index: number = this.pedido.lenght;
    if (index !== -1) {
      this.pedido.splice(ev, 1);
    }

  }

  //Editar un item .
  editarItem() {
  }

  abrirPreferencias(ev) {
    let popover = this.popoverCtrl.create('PedidoPreferenciaPage', {}, { enableBackdropDismiss: false });

    popover.onDidDismiss(() => {
      this.compartir();
    });

    popover.present({
      ev: ev
    });
  }

  //crear el pdf de pedidos..
  crearPDF() {
    let fileName = 'pedidoSiesaMobile.pdf';
    var doc = {
      header: {
        columns: [
          {
            stack: [
              //{ text: 'SIESA...', alignment: 'center' },

            ],
            fontSize: 15
          }
        ]
      },
      styles: {
        title: {
          fontSize: 14,
          bold: true,
          alignment: 'center',
        },
        piepagina: {
          fontSize: 14,
          bold: true,
        }
      },
      footer: function (page, pages) {
        return {
          columns: [{
            stack: [
              {
                text: [
                  { text: page.toString(), italics: true, alignment: 'center' },
                  ' de ',
                  { text: pages.toString(), italics: true, alignment: 'center' }
                ]
              }
            ],
          }
          ],
          margin: [10, 0]
        };
      },
      content: [
        { text: 'SIESA', style: 'title' },
        { text: 'NIT :' + this.pedido[0].titulo, style: 'title' },
        { text: 'Diego Hoyos', style: 'title' },
        { text: 'TEL : ' + 3146118525, style: 'title', margin: [0, 0, 0, 35] },
        {
          columns: [
            [
              // First column consists of paragraphs
              'Factura ',
              'Fecha',
              'Vendedor',
              'Cliente',
              'N.I.T-CC'
            ],
            [
              // second column consists of paragraphs
              ' AR010698',
              new Date().getUTCDate(),
              this.pedido[0].titulo,
              'Diego Hoyos',
              this.pedido[0].texto2
            ],
          ]
        },

        {
          margin: [0, 28, 0, 28], alignment: 'right',
          table: {
            headerRows: 1,
            widths: ['*', 'auto', 100, '*'],
            body: this.items,

          }
        },
        {
          columns: [
            [
              // First column consists of paragraphs
              { text: 'Valor Bruto ', style: 'piepagina' },
              { text: 'Valor Descuento', style: 'piepagina' },
              { text: 'Valor IVA', style: 'piepagina' },
              { text: 'Valor Neto', style: 'piepagina' },
            ],
            {
              // auto-sized columns have their widths based on their content
              width: 'auto',
              text: '      '
            },
            {
              width: 'auto',
              text: '       '
            },
            [
              // second column consists of paragraphs
              { text: '$ ' + 5000, style: 'piepagina', alignment: 'right', },
              { text: '$ ' + 500, style: 'piepagina', alignment: 'right', },
              { text: '$ ' + 800, style: 'piepagina', alignment: 'right', },
              { text: '$ ' + 600, style: 'piepagina', alignment: 'right', },
            ]
          ]
        }
      ]
    };

    this.pdf = [{
      'titulo': fileName,
      'tipo': 'pdf',
      doc: doc
    }];
    
    this.mf.procesarDescargasPDF(this.pdf)
      .then((lista: any) => {
        console.log("que llega despues de la descarga.. ", lista)
      });

  }

  compartir() {
    if (this.pedido != '') {
      this.crearPDF();
      //Compartir pdf creado..

      let archivos = [];
      console.log("pdfcompartir.. ",this.pdf)
      archivos.push(this.pdf[0].titulo);

      this.mf
        .compartirAdjuntos('comercial', '', '', archivos)
        .then((r) => {
          console.log(r);
        })
        .catch((e) => {
          console.log(e);
        });

      //this.socialSharing.share("Ducumento de pedidos", 'Documento de pedidos', path + 'Download/' + this.namepdf, null)
    } else {
      this.msg.msgAlerta("Para compartir debe tener por lo menos un pedido creado!");
    }
  }

  // action sheet ...
  presentActionSheet(evento) {
    this.msg.msgAlerta("crear pedido...");
  }



}
