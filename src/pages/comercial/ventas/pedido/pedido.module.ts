import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoPage } from './pedido';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [
    PedidoPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(PedidoPage),
  ],
})
export class PedidoPageModule {}
