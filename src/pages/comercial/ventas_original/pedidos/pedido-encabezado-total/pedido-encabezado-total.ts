import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PedidosProvider } from '../../../../../providers/pedidos/pedidos';


@IonicPage()
@Component({
  selector: 'page-pedido-encabezado-total',
  templateUrl: 'pedido-encabezado-total.html',
})
export class PedidoEncabezadoTotalPage {
  evento: any;
  items:any=[];
  cliente:any;
  titulo:any;
  datos_cliente:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private sPed: PedidosProvider) {
    this.evento = this.navParams.get("evento");
    this.cliente = this.navParams.get("cliente");
    this.datos_cliente = this.navParams.get("item");

  }

  ionViewDidLoad() {
    this.titulo = this.datos_cliente.cliente_descripcion_sucursal;
    this.sPed.getpedidoTotales(this.evento.item.f430_rowid).then(result=>{
      this.items=result.rows;      
    });
  }

}
