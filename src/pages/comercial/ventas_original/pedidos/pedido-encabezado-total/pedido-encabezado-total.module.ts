import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoEncabezadoTotalPage } from './pedido-encabezado-total';

@NgModule({
  declarations: [
    PedidoEncabezadoTotalPage,
  ],
  imports: [
    IonicPageModule.forChild(PedidoEncabezadoTotalPage),
  ],
})
export class PedidoEncabezadoTotalPageModule {}
