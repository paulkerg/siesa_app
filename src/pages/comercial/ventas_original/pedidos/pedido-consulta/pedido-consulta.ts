import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PedidosProvider } from '../../../../../providers/pedidos/pedidos';
import { ClienteServiceProvider } from '../../../../../providers/clientes/cliente-service';

@IonicPage()
@Component({
  selector: 'page-pedido-consulta',
  templateUrl: 'pedido-consulta.html',
})
export class PedidoConsultaPage {
  evento: any;
  data: any;
  titulo: string;
  rowid_terc: number;
  fecha: any = new Date().toISOString();
  item: any;
  items: any=[];
  cliente: any;
  listaprecio: any;
  totales: any;
  total: any = 0;
  totalCant: number = 0;
  Pedido: any;
  datas: any = [];
  pedidos: any;
  itemspedido: any;
  F430_rowid:any;
  Rowid_borrar:any;
  prvBotonesTitulo = [
    {
      imagen: "ios-add-circle-outline"
    }
  ];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sPed: PedidosProvider,
    private datacliente: ClienteServiceProvider ) {
    this.data = {
      items: []
    }
    this.item = this.navParams.get("item");
    this.data = this.navParams.get("items");
    this.cliente = this.navParams.get("cliente");
    this.listaprecio = this.navParams.get("lp");
    this.Pedido = this.navParams.get("Pedido");
    this.itemspedido = this.navParams.get("itemspedido")
    this.evento = this.navParams.get("evento");
    this.Pedido = this.navParams.get("Pedido");
    this.titulo = "Pedidos Hoy";
    this.data.items = this.data;
  }

  ionViewDidLoad() {    
    if (this.evento) {
      this.F430_rowid = this.evento.item.f430_rowid;
    } else {
      this.F430_rowid = "''";
    }
    this.Cargarpedido();
    this.datacliente.burcarPorId(this.item.cliente_rowid_tercero, this.item.cliente_id_sucursal).then(result => {

    });
    this.titulo = this.item.cliente_descripcion_sucursal;
    // this.cargarpedidodetalle();

  }

  //consulta de pedidos
  Cargarpedido() {
    this.sPed.cargarDetalle(this.F430_rowid)
      .then(result => {
        let items: any = [];
        for (let i = 0; i < result.rows.length; i++) {
          let r = result.rows[i];
          this.total += r.f431_vlr_neto;
          this.totalCant += 1;
          let v_ext1 = "", v_ext2 = "";

          if (r.f121_id_ext1_detalle != "null") {
            v_ext1 = "_" + r.f121_id_ext1_detalle.replace(" ", "");
          }

          if (r.f121_id_ext2_detalle != "null") {
            v_ext2 = "_" + r.f121_id_ext2_detalle.replace(" ", "");
          }
          let itemRef = "Itm: " + r.f120_id + v_ext1 + v_ext2 + " Ref: " + r.f120_referencia.replace(" ", "");
          items.push({
            "item_ts": r.f431_ts,
            "item_rowid": r.f431_rowid,
            "item_rowid_pv_docto": r.f431_rowid_pv_docto,
            "item_rowid_item_ext": r.f431_rowid_item_ext,
            "item_rowid_bodega": r.f431_rowid_bodega,
            "item_id_concepto": r.f431_id_concepto,
            "item_id_motivo": r.f431_id_motivo,
            "item_ind_obsequio": r.f431_ind_obsequio,
            "item_id_lista_precio": r.f431_id_lista_precio,
            "item_fecha_entrega": r.f431_fecha_entrega,
            "item_ind_impuesto_precio_venta": r.f431_ind_impuesto_precio_venta,
            "item_precio_unitario_base": r.f431_precio_unitario_base,
            "item_id_unidad_medida": r.f431_id_unidad_medida,
            "item_factor": r.f431_factor,
            "item_cant1_pedida": r.f431_cant1_pedida,
            "item_cant2_pedida": r.f431_cant2_pedida,
            "item_notas": r.f431_notas,
            "item_vlr_bruto": r.f431_vlr_bruto,
            "item_vlr_dscto_linea": r.f431_vlr_dscto_linea,
            "item_vlr_dscto_global": r.f431_vlr_dscto_global,
            "item_vlr_imp": r.f431_vlr_imp,
            "item_vlr_imp_no_apli": r.f431_vlr_imp_no_apli,
            "item_vlr_neto": r.f431_vlr_neto,
            "item_vlr_imp_margen": r.f431_vlr_imp_margen,
            "item_id_unidad_medida_captura": r.f431_id_unidad_medida_captura,
            "item_rowid_ccosto_movto": r.f431_rowid_ccosto_movto,
            "item_id_un_movto": r.f431_id_un_movto,
            "item_id_co_movto": r.f431_id_co_movto,
            "item_descripcion_toda": r.f121_descripcion_toda,
            "item_neto": r.f430_neto,
            "item_desc_ext1_detalle": r.f121_desc_ext1_detalle,
            "item_desc_ext2_detalle": r.f121_desc_ext2_detalle,
            "item_id": r.f120_id,
            "item_referencia": r.f120_referencia,
            "item_id_unidad": r.f122_id_unidad,
            "item_tasas": r.f432_tasas,
            "item_vlr_unis": r.f432_vlr_unis,
            "item_id_ext1_detalle": r.f121_id_ext1_detalle,
            "item_id_ext2_detalle": r.f121_id_ext2_detalle,
            "total": this.total,
            "totalcant": this.totalCant,
            "itemRef": itemRef
          });
          this.datas.push({
            "item_rowid":items[i].item_rowid,
            "titulo": items[i].item_descripcion_toda,
            "texto1": items[i].itemRef,
            "indice": 1,
            "badge": 0,
            "texto2": items[i].item_cant1_pedida + " X " + items[i].item_id_unidad_medida + " X $" + items[i].item_precio_unitario_base,
            "texto3": "Neto:$ " + items[i].item_vlr_neto
          });
          if (this.Pedido == 'pedidoLista') {
            this.items = this.itemspedido
            this.totales = [{
              "texto1": "Total: " + items[i].totalcant + " $ " + items[i].total
            }]
          }
          else {
            this.items = this.itemspedido
          }
        }

      });

  }

  //--- Crear nuevo pedido
  nuevo() {
    this.navCtrl.push('ItemEncabezadoPage', { item: this.item, cliente: this.cliente,evento:this.evento });
  }

  // Seleccionar pedido.
  clickItems(item, value) {
    this.Rowid_borrar=item.item_rowid;
    if (value == 'listaprecio') {
      this.navCtrl.push('PedidoEncabezadoListaPage', { item: this.item, items: this.itemspedido, cliente: this.cliente, listaprecio: this.listaprecio,evento:this.evento});
    } else if (value == 'listatotal') {
      this.navCtrl.push('PedidoEncabezadoTotalPage',
        { item: this.item,evento:this.evento,cliente: this.cliente }
      );
    }
  }

  //Opcion de borrado , provisional
  OpcionBorrar(){
    this.datas=[];
    this.sPed.borrarPedidoItem(this.Rowid_borrar).then(response=>{
      alert("Borro correctamente!");
      this.Cargarpedido();
    });
  
  }

  //--- Funcion para borrar un Item del pedido.
  /*public borrarPedidoItem(item) {
    this.total = 0;
    this.sPed.borrarPedidoItem(item)
      .then(result => {
        this.cargarpedidodetalle();
      })
      .catch(error => {
        console.error(error);
      });
  }*/
}

