import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoConsultaPage } from './pedido-consulta';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PedidoConsultaPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(PedidoConsultaPage),
  ],
})
export class PedidoConsultaPageModule {}
