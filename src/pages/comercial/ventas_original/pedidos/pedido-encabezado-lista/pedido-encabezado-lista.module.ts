import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoEncabezadoListaPage } from './pedido-encabezado-lista';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PedidoEncabezadoListaPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(PedidoEncabezadoListaPage),
  ],
})
export class PedidoEncabezadoListaPageModule {}
