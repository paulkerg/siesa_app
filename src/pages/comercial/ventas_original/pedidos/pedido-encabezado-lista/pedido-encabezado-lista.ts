import { cliente } from '../../../../../models/interfaces/cliente';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PedidosProvider } from '../../../../../providers/pedidos/pedidos';
import { ActionSheetController } from "ionic-angular";


@IonicPage()
@Component({
  selector: 'page-pedido-encabezado-lista',
  templateUrl: 'pedido-encabezado-lista.html',
})
export class PedidoEncabezadoListaPage {
  datos: any[];
  datos1: any[];
  items: any;
  datos_cliente: any;
  LPitems: any;
  data: any;
  rowid: any;
  titulo: string;
  cliente: any;
  listaprecio: any;
  fecha = new Date().toISOString();
  pedidoencabezado: any;
  evento: any;
  F430_rowid: any;
  Cpag: any; Lpre: any; Bodega: any; PEnvio: any; notas: any = ''; referencia: any = '';
  controlInput: any =
    {
      fecha: {
        modelo: new Date(),
        nombre: "fecha",
        label: "Fecha",
        placeholder: "Ingrese fecha"
      }
    };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sPed: PedidosProvider,
    public actionSheetCtrl: ActionSheetController,
    public actionAlert: AlertController) {

    this.cliente = this.navParams.get("cliente");
    this.datos_cliente = this.navParams.get("item");
    this.listaprecio = this.navParams.get("listaprecio");
    this.evento = this.navParams.get("evento");
  }

  ionViewDidLoad() {
    if (this.evento) {
      this.F430_rowid = this.evento.item.f430_rowid;
    } else {
      this.F430_rowid = "''";
    }
    this.titulo = this.datos_cliente.cliente_descripcion_sucursal;
    this.LPitems = this.navParams.get("items");
    //regresar pagina ...
    this.sPed.pedidosconsulta(this.LPitems[0].f201_rowid_tercero).then(respuesta => {
      this.sPed.pedidoEncabezadoRegreso(respuesta).then(result => {
        let len = result.rows.length;
        let swOK = true;
        let swOKBorrar = false;
        let swRetornar = false;
        // let strSQL = '';
        if (len == 0) {
          // --- Si el Documento no tiene Registros
          swRetornar = true;
          swOK = false;
        } else {
          let i = respuesta.rows.item(0);
          if (i.f430_ind_transmitido == 0 && i.neto == 0 && i.nro_reg_det == 0) {
            swOKBorrar = true;
            swRetornar = true;
            swOK = false;
          }
          if (i.nro_reg_det > 0 && i.f430_ind_transmitido == 0) {
            if (i.neto == 0) {
              swOKBorrar = true;
              swOK = false;
            }
            if (swOK == true && i.neto > 0) {
              // --- Si esta bien envia el documento
              swRetornar = true;
            }
          }
          if (swOKBorrar == true) {
            // --- Borrar el documento con detalle si lo tiene y restablece los
            // cambios
            if (confirm("Documento en ceros, borrarlo?")) {
              // --- Si el Documento tiene detalle pero esta en ceros
              swRetornar = true;
              if (this.LPitems[0].f201_rowid_tercero != undefined) {
                //this.sPed.borrarPedidoCompleto(this.LPitems[0].f201_rowid_tercero)
              }
            }
          }
          if (i.f430_ind_transmitido != 0) {
            swRetornar = true;
            swOK = false;
          }
        }
      });
    });
    this.LPitems = this.navParams.get("items");
    this.sPed.cargarEncabezado(this.F430_rowid).then(respuesta => {
      this.pedidoencabezado = respuesta;
      this.sPed.cargarEncabezados(respuesta.f430_consec_docto).then(result => {
        if (result.rows.length > 0) {
          this.notas = result.rows[0].f430_notas;
          this.referencia = result.rows[0].f430_referencia;
        }
        this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
      });
    });

    this.Edicion_lista();
  }


  //fecha
  clickCombo(e) {
    this.fecha = e;
    console.log(JSON.stringify(e), this.fecha);
    console.log("fecha...", e)
  }
  //Pedido encabezado lista...
  Edicion_lista() {
    this.sPed.getP_envio(this.LPitems[0].f201_rowid_tercero, this.LPitems[0].f201_id_sucursal)
      .then(result => {
        this.sPed.getDescripciones(this.LPitems[0].f201_id_cond_pago, result.rows[0].f215_rowid, this.LPitems[0].f201_id_lista_precio, cliente.rowid_bodega)
          .then(result => {
            //--- actualiza datos al modelo
            this.items = result.rows;
            this.Cpag = this.items[0].desc_cpago;
            this.Lpre = this.items[0].desc_lpre;
            this.Bodega = this.items[0].desc_bodega;
            this.PEnvio = this.items[0].desc_penv;
            this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
          })
          .catch(error => {
            console.error(error);
          });
      })
      .catch(error => {
        console.error(error);
      });

  }
  //click item seleccionado ....
  clickItems(event) {
    let actionsheet;
    let botones = [];
    if (event.value == 'C.pago') {
      //------Opcion para editar los creditos.
      if (this.rowid == null) {
        // this.navCtrl.push('PedidoEncabezadoEditarPage', { item });
        this.sPed.getCon_Pago(this.LPitems[0].f201_rowid_tercero, this.LPitems[0].f201_id_sucursal, this.LPitems[0].f201_id_cond_pago)
          .then(result => {
            this.data = result.rows;
            for (let i = 0; i < this.data.length; i++) {
              botones.push({
                text: this.data[i].descripcion,
                handler: () => {
                  this.Cpag = this.data[i].descripcion
                  this.sPed.updateDescripciones(this.data[i], 'C.pago', this.LPitems[0].f201_rowid_tercero, this.LPitems[0].f201_id_sucursal).then(result => {
                  });
                  this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
                }
              },
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                });
              actionsheet = this.actionSheetCtrl.create({
                title: "Seleccionar",
                buttons: botones
              });
              return actionsheet.present();
            }
          }
          ).catch(Error => {
            console.log("Error ", Error);
          });
      }
    } else if (event.value == 'L.pre') {
      //------Opcion para editar las condiciones de pago .
      if (this.rowid == null) {
        this.sPed.getlista_precios()
          .then(result => {
            this.data = result.rows;
            for (let i = 0; i < this.data.length; i++) {
              botones.push({
                text: this.data[i].descripcion,
                handler: () => {
                  this.Lpre = this.data[i].descripcion;
                  this.sPed.updateDescripciones(this.data[i], 'Lpre', this.LPitems[0].f201_rowid_tercero, this.LPitems[0].f201_id_sucursal).then(result => {
                  });
                  this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
                },
              },
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                }
              );
              actionsheet = this.actionSheetCtrl.create({
                title: "Seleccionar",
                buttons: botones,
              });
            }
            return actionsheet.present();
          }
          ).catch(Error => {
            console.log("Error ", Error);
          });
      }
    } else if (event.value == 'Bodega') {
      //------Opcion para editar las Bodegas.
      if (this.rowid == null) {
        this.sPed.getBodegas()
          .then(result => {
            this.data = result.rows;
            for (let i = 0; i < this.data.length; i++) {
              botones.push({
                text: this.data[i].descripcion,
                handler: () => {
                  this.Bodega = this.data[i].descripcion;
                  this.sPed.updateDescripciones(this.data[i], 'Bodega', cliente.Id_vendedor, this.LPitems[0].f201_id_sucursal).then(result => {
                  });
                  this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
                }
              },
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                });
              actionsheet = this.actionSheetCtrl.create({
                title: "Seleccionar",
                buttons: botones
              });
            }
            return actionsheet.present();
          }
          ).catch(Error => {
            console.log("Error ", Error);
          });
      }

    } else if (event.value == 'P.Envio') {
      if (this.rowid == null) {
        this.sPed.getP_envio(this.LPitems[0].f201_rowid_tercero, this.LPitems[0].f201_id_sucursal)
          .then(result => {
            this.data = result.rows;
            for (let i = 0; i < this.data.length; i++) {
              botones.push({
                text: this.data[i].descripcion,
                handler: () => {
                  this.PEnvio = this.data[i].descripcion;
                  this.sPed.updateDescripciones(this.data[i], 'P.Envio', this.LPitems[0].f201_rowid_tercero, this.LPitems[0].f201_id_sucursal).then(result => {
                  });
                  this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
                }
              },
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  handler: () => {
                  }
                });
              actionsheet = this.actionSheetCtrl.create({
                title: "Seleccionar",
                buttons: botones
              });
            }
            return actionsheet.present();
          }
          ).catch(Error => {
            console.log("Error ", Error);
          });
      }
    }
  }


  NuevosDatos(Cpago, Lpre, Bodega, PEnvio, notas, referencia) {
    this.datos = [
      { "texto1": "C.pag: " + Cpago, "value": 'C.pago' },
      { "texto1": "L.pre: " + Lpre, "value": 'L.pre' },
      { "texto1": "Bodega: " + Bodega, "value": 'Bodega' },
      { "texto1": "P.Envio: " + PEnvio, "value": 'P.Envio' }
    ]

    //Datos 1 para enviar a items.
    this.datos1 = [
      { "texto1": "Notas", "value": "  " + notas },
      { "texto1": "Referencia", "value": "  " + referencia }
    ]

  }

  addSelectCourse(title, texto) {
    let select = {
      title: title,
      message: "Digite " + title,
      inputs: [{ value: texto }],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            if (title == 'Notas') {
              this.notas = data[0]
              this.sPed.pedidosCreados(this.pedidoencabezado, this.notas, '')
                .then(result => {
                })
                .catch(error => {
                  console.error(error);
                });
            } else if (title == 'Referencia') {
              this.referencia = data[0];
              this.sPed.pedidosCreados(this.pedidoencabezado, '', this.referencia)
                .then(result => {
                })
                .catch(error => {
                  console.error(error);
                });
            }
            this.NuevosDatos(this.Cpag, this.Lpre, this.Bodega, this.PEnvio, this.notas, this.referencia);
          }
        }]
    }

    let newAlert = this.actionAlert.create(select);
    newAlert.present();
  }


  clickItems1(event) {
    if (event.texto1 == "Notas") {
      // let item = {
      //   consulta: 'notas',
      //   data_cli: this.LPitems,
      // };
      // this.navCtrl.push('PedidoEncabezadoEditarPage', { item });
      this.addSelectCourse(event.texto1, this.notas);
    } else if (event.texto1 == 'Referencia') {
      this.addSelectCourse(event.texto1, this.referencia);
      // let item = {
      //   consulta: 'referencia',
      //   data_cli: this.LPitems,
      // };
      //this.navCtrl.push('PedidoEncabezadoEditarPage', { item });
    } else if (event.texto1 == 'Fecha') {
      this.fecha = 'fecha'
      this.controlInput =
        {
          fecha: {
            modelo: 123,
            nombre: "fecha",
            label: "Fecha",
            placeholder: "Ingerse fecha"
          }
        };
      //console.log("this.vontrolinput.... ",this.controlInput)
      // let item = {
      //   consulta: 'fecha',
      //   data_cli: this.LPitems,
      // };
      // this.navCtrl.push('PedidoEncabezadoEditarPage', { item });
    }
  }


}
