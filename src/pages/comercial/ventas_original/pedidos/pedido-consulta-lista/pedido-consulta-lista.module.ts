import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidoConsultaListaPage } from './pedido-consulta-lista';
import { ComponentsModule } from '../../../../../components/components.module';

@NgModule({
  declarations: [
    PedidoConsultaListaPage,
  ],
  imports: [  
    ComponentsModule,
    IonicPageModule.forChild(PedidoConsultaListaPage),
  ],
})
export class PedidoConsultaListaPageModule {}
