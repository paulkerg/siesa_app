import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pedido-consulta-lista',
  templateUrl: 'pedido-consulta-lista.html',
})
export class PedidoConsultaListaPage {
  totales: any;
  totalCant: any;
  total: any;
  data: any = [];
  itemspedido: any;
  items: any; item: any; Pedido: any; cliente: any;
  notas: any; referencia: any;
  titulo:any;
  pedidoencabezado: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.items = this.navParams.get("items");
    this.item = this.navParams.get("item");
    this.cliente = this.navParams.get("cliente");
    this.Pedido = this.navParams.get("Pedido");
    this.itemspedido = this.navParams.get("itemspedido");
    this.titulo = this.item.cliente_descripcion_sucursal;
    this.listarPedidos(this.items);
  }

  //Listar los pedidos existentes
  listarPedidos(items) {
    for (let i = 0; i < items.length; i++) {
      
      this.data.push({
        "item": items[i],
        "titulo":'',
        "indice": 1,
        "badge": items[i].f430_neto,
        "texto1": items[i].f430_id_tipo_docto +"-"+ items[i].f430_consec_docto +"- "+this.items[i].f430_ts
      });
    }
  }

  clickItems(evento) {
    this.navCtrl.push("PedidoConsultaPage",
      { item: this.item, items: this.items, itemspedido: this.itemspedido, Pedido: this.Pedido, cliente:this.cliente,evento }
    );
  }

  nuevo(){
    let evento='';
    this.navCtrl.push("PedidoConsultaPage",
      { item: this.item, items: this.items, itemspedido: this.itemspedido, Pedido: this.Pedido, cliente:this.cliente,evento }
    );
  }

}
