import { cliente } from '../../../../../models/interfaces/cliente';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PedidosProvider } from '../../../../../providers/pedidos/pedidos';

@IonicPage()
@Component({
  selector: 'page-recaudo-cartera',
  templateUrl: 'recaudo-cartera.html',
})
export class RecaudoCarteraPage {
  item: any = 'combo';
  obj = {
    'titulo': 'Cartera prueba',
    'texto1': 'cartera',
    'texto2': 'Aplicado'
  }
  items: any = [];
  cartera: any=0;
  aplicado:any=0;
  constructor(public navCtrl: NavController, public navParams: NavParams, private sPed: PedidosProvider) {
    this.item = navParams.get('item');
    if (this.item == 'otros') {
      this.obj = {
        'titulo': 'Otros',
        'texto1': 'Neto',
        'texto2': 'Aplicado'
      }
    } else if (this.item == 'aldia') {
      this.obj = {
        'titulo': 'F.Pago',
        'texto1': 'Por aplicar',
        'texto2': ''
      }
    }

    this.sPed.getCartera(cliente).then(respuesta => {
      let r = respuesta.rows;
      for (let i = 0; i < r.length; i++) {
        if (r[i].f353_saldo > 0) {
          this.cartera = this.cartera+r[i].f353_saldo;
        }
        this.items.push({
          'item':r[i],
          'titulo': "Valor : $" + r[i].f353_valor_origen,
          'texto1': "Saldo : $" + r[i].f353_saldo,
          'texto2': r[i].f353_docto_cruce,
          'texto3': r[i].f353_fecha,
          'texto4': r[i].f281_descripcion,
          'badge': r[i].dias_vcto
        });
      }
      console.log("saldddddooooo...", this.cartera)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecaudoCarteraPage');
  }
  clickItems(item){
    console.log("que trae cartera... ",item)
  }

}
