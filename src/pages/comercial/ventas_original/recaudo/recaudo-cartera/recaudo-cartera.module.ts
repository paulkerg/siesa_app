import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecaudoCarteraPage } from './recaudo-cartera';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    RecaudoCarteraPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(RecaudoCarteraPage),
  ],
})
export class RecaudoCarteraPageModule {}
