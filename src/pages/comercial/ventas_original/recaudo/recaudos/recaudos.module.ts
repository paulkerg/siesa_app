import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecaudosPage } from './recaudos';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    RecaudosPage,
  ],
  imports:[
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(RecaudosPage),
  ],
})
export class RecaudosPageModule {}
