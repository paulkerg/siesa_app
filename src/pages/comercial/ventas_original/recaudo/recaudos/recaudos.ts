
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { cliente } from '../../../../../models/interfaces/cliente';


@IonicPage()
@Component({
  selector: 'page-recaudos',
  templateUrl: 'recaudos.html',
})
export class RecaudosPage {
  cartera = [{
    "texto1": 'Cartera :'
  }];
  otros = [{
    "texto1": 'Otros :'
  }];
  aldia = [{
    "texto1": 'Al dia'
  }];
  postfechado = [{
    "texto1": 'Postfechado'
  }];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log("itemspedido. ", this.cartera)
    console.log("#que trae cliente... ",cliente);
  }

  clickitem(valor) {
    this.navCtrl.push('RecaudoCarteraPage', { item: valor });
  }

}
