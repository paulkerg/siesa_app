import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { cliente } from '../../../../../models/interfaces/cliente';
import { SignaturePage } from '../../signature/signature';

@IonicPage()
@Component({
  selector: 'page-facturas',
  templateUrl: 'facturas.html',
})
export class FacturasPage {
  total=[{
    "texto1":'Total'
  }];
  recaudo=[{
    "texto1":"Recaudo"
  }]
  cp=[{
    "texto1":"CP"
  }];
  item:any;
  public signatureImage : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalController:ModalController) {
    this.signatureImage = navParams.get('signatureImage');;
    this.item = this.navParams.get("items");
  }

  ionViewDidLoad() {
    console.log("item..,, .." ,this.item)
  }
  
  openSignatureModel(){
    
   this.navCtrl.push(SignaturePage);
   /* setTimeout(() => {
    let modal = this.modalController.create(SignaturePage);
    modal.present();
    }, 300);*/

  }

  cpfactura(){
    this.navCtrl.push("FacturaEncabezadoPage");
  }
  ejemplo(){
   this.navCtrl.push("SignaturePage");
  }
  imprimir(){
    alert("imprimir...")
  }
  nuevo(){
    this.navCtrl.push("ItemEncabezadoPage",
      { item: cliente, cliente:cliente}
    );
  }
}
