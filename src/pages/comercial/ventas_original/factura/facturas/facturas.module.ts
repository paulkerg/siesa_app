import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacturasPage } from './facturas';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    FacturasPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(FacturasPage),
  ],
})
export class FacturasPageModule {}
