import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacturaEncabezadoPage } from './factura-encabezado';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    FacturaEncabezadoPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(FacturaEncabezadoPage),
  ],
})
export class FacturaEncabezadoPageModule {}
