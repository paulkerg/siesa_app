import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { cliente } from '../../../../../models/interfaces/cliente';

@IonicPage()
@Component({
  selector: 'page-factura-encabezado',
  templateUrl: 'factura-encabezado.html',
})
export class FacturaEncabezadoPage {
  Penvio=[{
    "texto1":'envio'
  }];
  lp=[{
    "texto1":"Libre"
  }]
  cp=[{
    "texto1":"Descuento"
  }]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacturaEncabezadoPage',cliente);
  }

}
