import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemEncabezadoPage } from './item-encabezado';
import { ComponentsModule } from '../../../../components/components.module';
import { PipesModule } from '../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    ItemEncabezadoPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(ItemEncabezadoPage),
  ],
})
export class ItemEncabezadoPageModule {}
