import { Component, ChangeDetectorRef } from '@angular/core';
import {
	IonicPage,
	NavController,
	NavParams,
	ModalOptions,
	Modal,
	ModalController
} from 'ionic-angular';
import { PedidosProvider } from '../../../../providers/pedidos/pedidos';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { pedidoEncabezado } from '../../../../models/interfaces/pedidoEncabezado';
import { pedidoDetalle } from '../../../../models/interfaces/pedidoDetalle';
import { ParametrosProvider } from '../../../../providers/parametros/parametros';
import { cliente } from '../../../../models/interfaces/cliente';

@IonicPage()
@Component({
	selector: 'page-item-encabezado',
	templateUrl: 'item-encabezado.html'
})
export class ItemEncabezadoPage {
	today: any;
	cant: any = 0;
	cantdesc: any = 'Unidad';
	precio: any = 0;
	items: any = [];
	texto: any = '';
	pg: any;
	sel: any = 'ABC';
	input: any = false;
	evento: any;
	F430_rowid: any;
	pedidoencabezado: any;
	data: any = [];
	inputselect: any = false;
	objEjemplos: any = [
		{ id: 'ABC', prop: 'Descripcion', titulo: 'ABC' },
		{ id: '123', prop: 'Codigo', titulo: '123' },
		{ id: 'REF', prop: 'Referencia', titulo: 'REF' },
		{ id: 'CRI', prop: 'Criterio', titulo: 'CRI' },
		{ id: 'BAR', prop: 'Cod. Barras', titulo: 'BAR' },
		{ id: 'PORTAFOLIO', prop: 'Portafolio', titulo: 'PORTAFOLIO' }
	];
	condicional: any = 'Descripcion';
	matches: String[];
	isRecording = false;
	referencia: any = ''
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private sPed: PedidosProvider,
		private speechRecognition: SpeechRecognition,
		private cd: ChangeDetectorRef,
		private modal: ModalController,
		private barcodeScanner: BarcodeScanner,
		private parametro: ParametrosProvider
	) {
		this.today = new Date().toISOString();
		this.evento = this.navParams.get('evento');
		this.speechRecognition.hasPermission().then((hasPermission: boolean) => {

			if (!hasPermission) {
				this.speechRecognition.requestPermission();
			}
		});
	}

	ionViewDidLoad() {
		this.parametro.leer();
		this.pg = 0;
		if (this.evento) {
			this.F430_rowid = this.evento.item.f430_rowid;
		} else {
			this.F430_rowid = "''";
		}
		this.setItems(this.sel, this.texto, this.pg);
		this.sPed.cargarEncabezado(this.F430_rowid).then((respuesta) => {
			this.pedidoencabezado = respuesta;
		});
	}

	//Comenzar reconocimiento de voz
	startListening() {
		this.speechRecognition.startListening().subscribe(matches => {
			this.matches = matches;
			this.cd.detectChanges();
		});
		this.isRecording = true;
	}
	
	//Detener reconocimiento de voz
	stopListening() {
		this.speechRecognition.stopListening().then(() => {
			this.isRecording = false;
		});
	}

	//Setear para listar Items
	setItems(sel, texto, pg) {
		let exist: any = 0;
		this.sPed.getItems(sel, pg, texto).then((result) => {
			let r = result.rows;
			if (r.length == 1) {
				this.input = true;
			}
			for (let i = 0; i < r.length; i++) {
				if (r[i].f400_existencias != undefined) {
					exist = r[i].f400_existencias;
				}
				if (r[i].f122_id_unidad_empaque != null) {
					exist += ' ' + r[i].f122_id_unidad_empaque;
				}

				this.items.push({
					item: r[i],
					titulo: r[i].f120_descripcion,
					badge: exist,
					texto1: 'Itm ' + r[i].f121_rowid + ' Ref. ' + r[i].f120_referencia,
					texto2: 'unidad: ' + r[i].f122_id_unidad + ' factor: ' + r[i].f122_factor
				});
			}
		});
	}

	//Listar items con accion Enter
	filterItems(ev: any) {
		this.pg = 0;
		this.inputselect = false;
		this.input = false;
		this.cant = 0;
		this.precio = 0;
		this.texto = ev.target.value;
		if (this.texto == undefined) {
			this.texto = '';
		} else {
			this.items = [];
		}
		if (ev.cancelable == true) {
			this.items = [];
		}
		this.setItems(this.sel, this.texto.toLowerCase(), this.pg);
	}

	//Item seleccionado 
	clickItems(item) {
		this.data = [];
		this.sPed.leerItemUni(item.item.f120_rowid).then(result => {
			let r = result.rows;
			if (r.length > 1) {
				this.inputselect = true;
				this.texto = 'unidades';
				this.input = false;
				this.cant = 0;
				this.precio = 0;
				let exist = 0;
				if (item.item.f400_existencias != undefined) {
					exist = item.item.f400_existencias;
				}
				this.items = [
					{
						item: item.item,
						titulo: item.item.f120_descripcion,
						badge: exist,
						texto1: 'unidad: ' + item.item.f122_id_unidad + ' factor: ' + item.item.f122_factor,
						texto2: 'Itm ' + item.item.f121_rowid + ' Ref. ' + item.item.f120_referencia
					}
				];
				for (let i = 0; i < r.length; i++) {
					this.data.push({
						texto1: r[i].unidad,
						badge: r[i].ord
					});
				}
			} else {
				this.input = true;
				this.cant = 0;
				this.precio = 0;
				let exist = 0;
				if (item.item.f400_existencias != undefined) {
					exist = item.item.f400_existencias;
				}
				this.items = [
					{
						item: item.item,
						titulo: item.item.f120_descripcion,
						badge: exist,
						texto1: 'unidad: ' + item.item.f122_id_unidad + ' factor: ' + item.item.f122_factor,
						texto2: 'Itm ' + item.item.f121_rowid + ' Ref. ' + item.item.f120_referencia
					}
				];
			}
		})
	}

	//seleccion de items por opcion criterio
	clickItemselect(event) {
		this.inputselect = false;
		this.input = true;
		this.cantdesc = event.texto1;
		this.items[0].item.f122_id_unidad = event.texto1;
	}

	//Filtro de consulta de items
	clickCombo(evento) {
		this.input = false;
		this.cant = 0;
		this.precio = 0;
		this.pg = 0;
		this.items = [];
		this.sel = evento;
		if (evento == 'CRI') {
			this.condicional = 'Criterio';
		} else {
			this.condicional = 'Descripcion';
			this.setItems(this.sel, this.texto.toLowerCase(), this.pg);
		}
	}

	//---- guardar item..
	save() {
		let valorbruto: number = (this.cant * this.precio);
		let obj = { cantidad: this.cant, precio: this.precio, items: this.items[0].item }
		pedidoDetalle.f431_cant1_pedida = this.cant;
		pedidoDetalle.f431_vlr_bruto = valorbruto;
		pedidoDetalle.f431_precio_unitario_base = this.precio;
		pedidoDetalle.f431_id_unidad_medida_captura = this.items[0].item.f122_id_unidad;

		if (this.cant != 0 && this.precio != 0) {
			pedidoEncabezado.f430_neto = valorbruto;
			this.sPed.pedidosCreados(pedidoEncabezado, '', '')
				.then(result => {

				})
				.catch(error => {
					console.error(error);
				});
			this.sPed.consultarPedidos(cliente.f201_rowid_tercero, cliente.f201_id_sucursal).then(result => {
				this.sPed.cargarEncabezado(result.rows[0].f430_rowid).then(result => {
					this.sPed.insertT431(obj);
				})
			});
			this.cant = 0;
			this.precio = 0;
		} else if (this.cant == 0) {
			alert('Por favor digite una cantidad');
		} else if (this.precio == 0) {
			alert('Por favor digite una precio');
		}
	}

	// Filtro de consulta...
	openModal() {
		this.pg = 0;
		const myModalOptions: ModalOptions = {
			enableBackdropDismiss: false
		};
		const myModalData = {
			name: 'Paul Halliday',
			occupation: 'Developer'
		};
		const myModal: Modal = this.modal.create('ItemsMdlFiltrosPage', { data: myModalData }, myModalOptions);
		myModal.present();
		myModal.onDidDismiss((data) => {
			console.log(data);
		});

		myModal.onWillDismiss((data) => {
			this.input = false;
			this.sel = data.filtrar;
			this.items = [];
			if (data.filtrar == 'CRI' || data.filtrar == 'PORTAFOLIO') {
				this.setItems(data.filtrar, data.occupation, this.pg);
			} else {
				this.setItems(data.filtrar, this.texto.toLowerCase(), this.pg);
			}
			console.log(data);
		});
	}

	// Scanear codigo de barras 
	Scaner() {
		//this.navCtrl.push('SLstclientesPage');
		this.barcodeScanner.scan().then(barcodeData => {
			alert('Barcode data ' + barcodeData.text);
		}).catch(err => {
			console.log('Error', err);
		})
	}

	//---------- Refrescar con scroll
	doInfinite(infiniteScroll) {
		this.pg++;
		setTimeout(() => {
			this.setItems(this.sel, this.texto.toLowerCase(), this.pg);
			infiniteScroll.complete();
		}, 500);
		return true;
	}
}
