import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClienteServiceProvider } from '../../../../../providers/clientes/cliente-service';
import { cliente } from '../../../../../models/interfaces/cliente';

@IonicPage()
@Component({
	selector: 'page-clientes-seleccionado',
	templateUrl: 'clientes-seleccionado.html'
})
export class ClientesSeleccionadoPage {
	clienteParametros: any;
	clienteSel: any;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private clienteSrv: ClienteServiceProvider
	) {
		this.clienteParametros = this.navParams.get('item');
    this.clienteSel = cliente;
		console.log(this.clienteParametros);
	}

	ionViewDidLoad() {
		this.clienteSrv
			.burcarPorId(this.clienteParametros.cliente_rowid, this.clienteParametros.cliente_id_suc)
			.then((respuesta) => {
				console.log(respuesta);
				console.log(cliente);
				this.clienteSel = cliente;
			});
	}
}
