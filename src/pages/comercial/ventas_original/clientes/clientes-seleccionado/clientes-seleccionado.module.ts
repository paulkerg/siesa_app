import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientesSeleccionadoPage } from './clientes-seleccionado';

@NgModule({
  declarations: [
    ClientesSeleccionadoPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientesSeleccionadoPage),
  ],
})
export class ClientesSeleccionadoPageModule {}
