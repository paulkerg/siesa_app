import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-clientes-mdl-filtros',
	templateUrl: 'clientes-mdl-filtros.html'
})
export class ClientesMdlFiltrosPage {
  fechaVisita:Date=new Date();
  filtro:any='abc';
	constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController) {}

	ionViewDidLoad() {
  }
  getDate(){
    this.filtro=this.fechaVisita;
  }
  fnAceptar() {
    const data = {
      name: 'Diego',
      occupation: 'Hoyos',
      filtrar:this.filtro,
    }; 
    this.view.dismiss(data);
  }
}
