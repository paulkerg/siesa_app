import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientesMdlFiltrosPage } from './clientes-mdl-filtros';

@NgModule({
  declarations: [
    ClientesMdlFiltrosPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientesMdlFiltrosPage),
  ],
})
export class ClientesMdlFiltrosPageModule {}
