import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PedidosProvider } from '../../../../../providers/pedidos/pedidos';
import { ClienteServiceProvider } from '../../../../../providers/clientes/cliente-service';
import { pedidoEncabezado } from '../../../../../models/interfaces/pedidoEncabezado';
import { pedidoDetalle } from '../../../../../models/interfaces/pedidoDetalle';

@IonicPage()
@Component({
	selector: 'page-s-lstclientes',
	templateUrl: 's-lstclientes.html'
})
export class SLstclientesPage {
	ubicacion: any = { lat: '', lon: '' };
	data: {};
	itemspedido: any;
	valor: any;
	Cliente_id: any;
	Sucur_id: any;
	lp: any;
	cliente: any;
	consecutivo: any;
	total: any = 0;
	totalCant: any = 0;
	totales: any;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		private sPed: PedidosProvider,
		private datacliente: ClienteServiceProvider
	) {
		this.valor = this.navParams.get('item');
		//---enviamos los datos que necesitamos en la consulta..
		this.Cliente_id = this.valor.cliente_rowid;
		this.Sucur_id = this.valor.cliente_id_suc;
		this.Consulta_LP();
	}

	ionViewDidLoad() {
		this.datacliente.burcarPorId(this.Cliente_id, this.Sucur_id).then((respuesta) => {
			this.data = respuesta.rows;
			this.datacliente.cargaConsecutivos().then((result) => {
				this.sPed.cargarEncabezados(result.rows[0].cons_ped).then((result) => {
					if (result.rows.length > 0) {
						this.consecutivo = result.rows[0].f430_rowid;
					}
				});
			});
		});
		this.sPed.getPedidoCliente(this.Cliente_id, this.Sucur_id).then((respuesta) => {});
	}

	Consulta_LP() {
		this.sPed
			.consultaLP(this.Cliente_id, this.Sucur_id)
			.then((respuesta) => {
				this.lp = respuesta.rows;
			})
			.catch((error) => {
				console.log(error);
			});

		this.sPed.consultapedidoEncabezado().then((resultado) => {
			this.cliente = resultado.rows;
		});
	}

	//----------------------------Funcion de pedidos
	pedidosBtn(item) {
		//---Registros Base de Datos
		this.sPed
			.pedido(item.cliente_rowid_tercero, item.cliente_id_sucursal)
			.then((response) => {
				this.itemspedido = response.rows;
				pedidoEncabezado.f430_rowid_tercero_fact=this.itemspedido[0].f201_rowid_tercero;
				pedidoEncabezado.f430_id_sucursal_fact=this.itemspedido[0].f201_id_sucursal;
				pedidoEncabezado.id_lista_precio=this.itemspedido[0].f201_id_lista_precio;
				pedidoEncabezado.f430_id_cond_pago=this.itemspedido[0].f201_id_cond_pago;
				pedidoEncabezado.f430_id_moneda_docto=this.itemspedido[0].f201_id_moneda;
				pedidoDetalle.f431_id_lista_precio=this.itemspedido[0].f201_id_lista_precio;	
				this.itemspedido = [
					{
						texto1:
							'C.P: ' +
							this.itemspedido[0].f201_id_cond_pago +
							' L.P: ' +
							this.itemspedido[0].f201_id_lista_precio,
						f201_rowid_tercero: this.itemspedido[0].f201_rowid_tercero,
						f201_id_sucursal: this.itemspedido[0].f201_id_sucursal,
						f201_id_cond_pago: this.itemspedido[0].f201_id_cond_pago,
						f201_id_lista_precio: this.itemspedido[0].f201_id_lista_precio,
						f201_descripcion_sucursal: this.itemspedido[0].f201_descripcion_sucursal
					}
				];
			})
			.catch((error) => {
				return Promise.reject(error);
			});

		this.cargarpedidodetalle(item);
	}
	
	//----------------------------Funcion de recaudos
	recaudos() {
		this.navCtrl.push('RecaudosPage');
		/* let alert = this.alertCtrl.create({
      title: 'Recaudos',
      subTitle: 'Recaudos',
      buttons: ['Ok']
    });
    alert.present();*/
	}
	//----------------------------Funcion de factura
	factura() {
		this.sPed.consultarPedidos(this.Cliente_id, this.Sucur_id).then((result) => {
			let items: any = [];
			for (var i = 0; i < result.rows.length; i++) {
				let r = result.rows[i];
				items.push({
					f430_consec_docto: r.f430_consec_docto,
					f430_id_tipo_docto: r.f430_id_tipo_docto,
					f430_ind_estado_int: r.f430_ind_estado_int,
					f430_ind_transmitido: r.f430_ind_transmitido,
					f430_neto: r.f430_neto,
					f430_rowid: r.f430_rowid,
					f430_ts: r.f430_ts
				});
			}
			this.navCtrl.push('FacturasPage', {
				data: this.data,
				lp: this.lp,
				cliente: this.cliente,
				items: items,
				itemspedido: this.itemspedido,
				Pedido: 'pedidoconsulta'
			});
		});
		/*let alert = this.alertCtrl.create({
      title: 'Factura',
      subTitle: 'Factura',
      buttons: ['Ok']
    });*/
		//alert.present();
	}
	//----------------------------Funcion de novedades
	novedades() {
		this.navCtrl.push('NovedadesPage');
		let alert = this.alertCtrl.create({
			title: 'Novedades',
			subTitle: 'Novedades',
			buttons: [ 'Ok' ]
		});
		alert.present();
	}

	//Cargar pedido detalle
	cargarpedidodetalle(item) {
		this.sPed.consultarPedidos(this.Cliente_id, this.Sucur_id).then((result) => {
			let items: any = [];
			for (var i = 0; i < result.rows.length; i++) {
				let r = result.rows[i];

				items.push({
					f430_consec_docto: r.f430_consec_docto,
					f430_id_tipo_docto: r.f430_id_tipo_docto,
					f430_ind_estado_int: r.f430_ind_estado_int,
					f430_ind_transmitido: r.f430_ind_transmitido,
					f430_neto: r.f430_neto,
					f430_rowid: r.f430_rowid,
					f430_ts: r.f430_ts
				});
			}
			if (item.cliente_nro_ped > 0) {
				this.navCtrl.push('PedidoConsultaListaPage', {
					items,
					item,
					data: this.data,
					lp: this.lp,
					cliente: this.cliente,
					itemspedido: this.itemspedido,
					Pedido: 'pedidoLista'
				});
			} else {
				item.rowid_terc = item.cliente_rowid_tercero;
				item.terc_suc = item.cliente_id_sucursal;
				item.cliente_rowid_dcto = 0;
				item.cliente_envio = 0;
				this.navCtrl.push('PedidoConsultaPage', {
					item,
					data: this.data,
					lp: this.lp,
					cliente: this.cliente,
					items: items,
					itemspedido: this.itemspedido,
					Pedido: 'pedidoconsulta'
				});
			}
		});
	}

	//----------------------------Obtener coordenadas
	public gps(item) {
		this.navCtrl.push('PedidoConsultaPage', { item });
	}
}
