import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SLstclientesPage } from './s-lstclientes';

@NgModule({
  declarations: [
    SLstclientesPage,
  ],
  imports: [
    IonicPageModule.forChild(SLstclientesPage),
  ],
})
export class SLstclientesPageModule {}
