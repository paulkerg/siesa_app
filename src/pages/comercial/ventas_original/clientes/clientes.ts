import { Component } from '@angular/core';
import {
	IonicPage,
	NavController,
	NavParams,
	MenuController,
	ModalController,
	ModalOptions,
	Modal
} from 'ionic-angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';

@IonicPage()
@Component({
	selector: 'page-clientes',
	templateUrl: 'clientes.html'
})
export class ClientesPage {
	items: any = [];
	pag: any = 0;
	respuesta: any;
	texto: any = '';
	order: any = 'f200_razon_social';
	value: any;
	filtrar: any = 'letra';

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private db: SqliteProvider,
		private menu: MenuController,
		private modal: ModalController,
		private speechRecognition: SpeechRecognition,
		private barcodeScanner: BarcodeScanner
	) {
		this.menu = menu;
		this.menu.enable(true, 'menuPrincipal');
	}

	ionViewDidLoad() {
		this.speechRecognition.hasPermission()
			.then((hasPermission: boolean) => {
				if (!hasPermission) {
					this.speechRecognition.requestPermission()
						.then(
							() => console.log('Granted'),
							() => console.log('Denied')
						)
				}
			});		
	}

	//Inicio de reconocimiento de voz , consulta clientes.
	start() {
		this.items = [];
		this.speechRecognition.startListening()
			.subscribe(
				(matches: Array<string>) => {
					this.value = matches[0];
					this.setItems(matches[0], this.pag, this.order);
				},
				(onerror) => console.log('error:', onerror)
			)
	}

	openModal() {
		const myModalOptions: ModalOptions = {
			enableBackdropDismiss: false
		};
		const myModalData = {
			name: 'Paul Halliday',
			occupation: 'Developer'
		};

		const myModal: Modal = this.modal.create('ClientesMdlFiltrosPage', { data: myModalData }, myModalOptions);

		myModal.present();

		myModal.onDidDismiss((data) => {
			console.log('I have dismissed.');
			console.log(data);
			this.filtro(data.filtrar);
		});

		myModal.onWillDismiss((data) => {
			console.log("I'm about to dismiss");
			console.log(data);
		});
	}

	ngOnInit() {
		this.setItems(this.texto.toLowerCase(), this.pag, this.order);
	}

	//---- Metodo para buscar clientes
	setItems(p_dato, pg, order) {
		let Sqlfull;
		let sql;
		let saldoRC = " ifnull((select sum((f353_saldo + f353_total_db_aplicado) - f353_total_cr_aplicado) "
			+ ' from t353_co_saldo_abierto '
			+ ' where f353_rowid_tercero = f201_rowid_tercero'
			+ ' and f353_id_sucursal = f201_id_sucursal),0) saldocxc';
		if (this.filtrar != 'fecha') {
			let filtro =
				"( t200.f200_razon_social LIKE '%" +
				p_dato +
				"%' OR t200.f200_id  LIKE '%" +
				p_dato +
				"%')  order by " +
				order +
				' asc ';
			sql =
				'SELECT t200.f200_rowid cliente_rowid, t200.f200_id cliente_id,t201.f201_id_sucursal cliente_id_suc,' +
				'	t200.f200_nit cliente_nit,t200.f200_dv_nit cliente_dv,t200.f200_razon_social cliente_razon_social,' +
				'	t200.f200_nombre_est cliente_nombre_est,t201.f201_descripcion_sucursal cliente_descripcion_sucursal,' +
				'	t015.f015_id_barrio cliente_barrio,t015.f015_telefono cliente_telefono,' +
				'	t015.f015_direccion1 cliete_direccion,t013.f013_descripcion cliente_ciudad,' +
				'	ifnull((SELECT sum((f353_saldo + f353_total_db_aplicado) - f353_total_cr_aplicado)' +
				'			FROM t353_co_saldo_abierto WHERE f353_rowid_tercero = f201_rowid_tercero AND f353_id_sucursal = f201_id_sucursal),0) cliente_saldocxc,' +
				'	ifnull((SELECT sum(f353_total_ch_postf_orden) FROM t353_co_saldo_abierto ' +
				'      WHERE f353_rowid_tercero = f201_rowid_tercero AND f353_id_sucursal = f201_id_sucursal),0) cliente_saldocxcche' +
				' ,(SELECT count(1) FROM t430_cm_pv_docto WHERE f430_rowid_tercero_fact = t200.f200_rowid AND f430_id_sucursal_fact = t201.f201_id_sucursal) cliente_nro_ped' +
				' ,(SELECT count(1) FROM t350_co_docto_contable WHERE f350_rowid_tercero = t200.f200_rowid AND f350_id_sucursal = t201.f201_id_sucursal) cliente_nro_rec' +
				' ,0 cliente_nro_fac' +
				' ,1 cliente_nro_nov' +
				' FROM t200_mm_terceros t200' +
				' INNER JOIN t201_mm_clientes t201 ON t200.f200_rowid = t201.f201_rowid_tercero' +
				' INNER JOIN t015_mm_contactos t015 ON t201.f201_rowid_contacto = t015.f015_rowid' +
				' INNER JOIN t013_mm_ciudades t013 ON t015.f015_id_pais = t013.f013_id_pais' +
				'  AND t015.f015_id_depto = t013.f013_id_depto AND t015.f015_id_ciudad = t013.f013_id' +
				' WHERE ' +
				filtro;

		} else {
			sql = 'SELECT DISTINCT '
				+ saldoRC+
				' ,t200.f200_rowid cliente_rowid, t200.f200_id cliente_id,t201.f201_id_sucursal cliente_id_suc,' +
				'	t200.f200_nit cliente_nit,t200.f200_dv_nit cliente_dv,t200.f200_razon_social cliente_razon_social,' +
				'	t200.f200_nombre_est cliente_nombre_est,t201.f201_descripcion_sucursal cliente_descripcion_sucursal,' +
				'	t015.f015_id_barrio cliente_barrio,t015.f015_telefono cliente_telefono,' +
				'	t015.f015_direccion1 cliete_direccion,t013.f013_descripcion cliente_ciudad,' +
				'	ifnull((SELECT sum((f353_saldo + f353_total_db_aplicado) - f353_total_cr_aplicado)' +
				'			FROM t353_co_saldo_abierto WHERE f353_rowid_tercero = f201_rowid_tercero AND f353_id_sucursal = f201_id_sucursal),0) cliente_saldocxc,' +
				'	ifnull((SELECT sum(f353_total_ch_postf_orden) FROM t353_co_saldo_abierto ' +
				'      WHERE f353_rowid_tercero = f201_rowid_tercero AND f353_id_sucursal = f201_id_sucursal),0) cliente_saldocxcche' +
				' ,(SELECT count(1) FROM t430_cm_pv_docto WHERE f430_rowid_tercero_fact = t200.f200_rowid AND f430_id_sucursal_fact = t201.f201_id_sucursal) cliente_nro_ped' +
				' ,(SELECT count(1) FROM t350_co_docto_contable WHERE f350_rowid_tercero = t200.f200_rowid AND f350_id_sucursal = t201.f201_id_sucursal) cliente_nro_rec' +
				' ,0 cliente_nro_fac' +
				' ,1 cliente_nro_nov' 
				+ ' FROM t5792_sm_ruta_visita AS t5792'
				+ ' INNER JOIN t5791_sm_ruta_frecuencia AS t5791 ON t5792.f5792_rowid_ruta_frecuencia = t5791.f5791_rowid'
				+ ' INNER JOIN t201_mm_clientes AS t201 ON t5791.f5791_rowid_tercero = t201.f201_rowid_tercero AND t5791.f5791_id_suc_cliente = t201.f201_id_sucursal'
				+ ' INNER JOIN t200_mm_terceros AS t200 ON t201.f201_rowid_tercero = t200.f200_rowid'
				+ ' INNER JOIN t015_mm_contactos AS t015 ON t201.f201_rowid_contacto = t015.f015_rowid'
				+ ' INNER JOIN t013_mm_ciudades AS t013 ON t015.f015_id_pais = t013.f013_id_pais AND t015.f015_id_depto = t013.f013_id_depto AND t015.f015_id_ciudad = t013.f013_id'
				+ ' WHERE  t5792.f5792_fecha = "' + p_dato + '"'
				+ ' ORDER BY t5791.f5791_orden';
		}
		///----Condicional para busqueda y scroll
		if (this.texto != '') {
			this.pag = 0;
			Sqlfull = sql;
		} else {
			Sqlfull = sql + ' LIMIT 10 OFFSET  10 * ' + pg;
		}
		this.db
			.query(Sqlfull)
			.then((respuesta) => {
				this.respuesta = respuesta.length;
				for (let k = 0; k < respuesta.length; k++) {
					if (respuesta[k]['cliente_nro_ped'] === 1) {
						respuesta[k].bordeIzquierdo = 'bordeIzquierdoRojo';
					}
					if (respuesta[k]['cliente_nro_rec'] === 1) {
						respuesta[k].bordeIzquierdo = 'bordeIzquierdoVerde';
					}
					if (respuesta[k]['cliente_nro_fac'] === 1) {
						respuesta[k].bordeIzquierdo = 'bordeIzquierdoNaranja';
					}
					if (respuesta[k]['cliente_nro_nov'] === 1) {
						respuesta[k].bordeIzquierdo = 'bordeIzquierdoAzul';
					}
				}
				console.log('respuesta... ', JSON.stringify(respuesta));
				this.llenarListView(respuesta);
			})
			.catch((error) => {
				console.log(error);
			});
	}

	//Filtrar para consultar.
	filtro(cons) {
		this.filtrar = "abc";
		if (cons == 'abc') {
			this.pag = 0;
			this.items = [];
			this.order = 'f200_razon_social';
			this.setItems(this.texto.toLowerCase(), this.pag, this.order);
		} else if (cons == '123') {
			this.items = [];
			this.pag = 0;
			this.order = 'f200_nit';
			this.setItems(this.texto.toLowerCase(), this.pag, this.order);
		} else {
			this.items = [];
			this.filtrar = "fecha";
			this.setItems(cons+ 'T00:00:00', this.pag, this.order);
		}
		return cons;
	}

	llenarListView(data) {
		let res = this.items.concat(data);
		this.items = res;
	}

	filterItems(ev: any) {
		this.texto = ev.target.value;
		if (this.texto == undefined) {
			this.texto = '';
		} else {
			this.items = [''];
		}
		if (ev.cancelable == true) {
			this.items = [''];
		}
		this.setItems(this.texto.toLowerCase(), this.pag, this.order);
		/*this.items = this.items.filter(function(item) {
      console.log("item",item)
      return item.toLowerCase().includes(this.texto.toLowerCase());
    });*/
	}

	openMenu() {
		this.menu.open('menuPrincipal');
	}

	// Scaner
	Scaner() {
		//this.navCtrl.push('SLstclientesPage');
		this.barcodeScanner.scan().then(barcodeData => {
			alert('Barcode data '+ barcodeData.text);
		}).catch(err => {
			console.log('Error', err);
		})
	}

	//----Refrescar con scroll
	doInfinite(infiniteScroll) {
		if (this.respuesta > 0) {
			this.pag = this.pag + 1;
		}
		setTimeout(() => {
			this.setItems(this.texto.toLowerCase(), this.pag, this.order);
			infiniteScroll.complete();
		}, 500);
	}

	pedido(itemEnv) {
		this.navCtrl.push('SLstclientesPage', { item: itemEnv });
		/*console.log("itemecve.. ",itemEnv)
		this.navCtrl.push('ClientesSeleccionadoPage', { item: itemEnv });*/
	}
}
