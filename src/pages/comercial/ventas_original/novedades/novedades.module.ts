import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovedadesPage } from './novedades';
import { ComponentsModule } from '../../../../components/components.module';
import { PipesModule } from '../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    NovedadesPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(NovedadesPage),
  ],
})
export class NovedadesPageModule {
  
}
