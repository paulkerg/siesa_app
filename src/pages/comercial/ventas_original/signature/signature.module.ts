import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignaturePage } from './signature';

@NgModule({
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    SignaturePage,
    
  ],
  imports: [
    IonicPageModule.forChild(SignaturePage),
  ],
})

export class SignaturePageModule {}
