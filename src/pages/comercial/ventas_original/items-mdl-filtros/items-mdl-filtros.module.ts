import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemsMdlFiltrosPage } from './items-mdl-filtros';
import { ComponentsModule } from '../../../../components/components.module';
import { PipesModule } from '../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    ItemsMdlFiltrosPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(ItemsMdlFiltrosPage),
  ],
})
export class ItemsMdlFiltrosPageModule {}
