import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PedidosProvider } from '../../../../providers/pedidos/pedidos';
@IonicPage()
@Component({
  selector: 'page-items-mdl-filtros',
  templateUrl: 'items-mdl-filtros.html',
})
export class ItemsMdlFiltrosPage {

  selplancriterio: any = '';
  selcriterio: any = '';
  selportafolio: any = '';
  fechaVisita: Date = new Date();
  filtro: any = 'ABC';
  valor: any = '';
  dataPlanCriterio: any = [];
  comboModelo: any;
  objCriterio: any = [];
  dataPortafolio: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController,
    private sPed: PedidosProvider) {
  }

  ionViewDidLoad() {
    this.fnCriterio();
    this.fnPortafolio();
  }

  /* getDate() {
     this.filtro = this.fechaVisita;
   }*/
  fnAceptar() {
    const data = {
      name: 'Diego',
      occupation: this.valor,
      filtrar: this.filtro
    };
    this.view.dismiss(data);
  }

  fnCriterio() {
    this.sPed.getCRI("")
      .then(result => {
        let r = result.rows;
        this.selplancriterio = r[0].f105_descripcion;
        for (let i = 0; i < r.length; i++) {
          this.dataPlanCriterio.push({ id: r[i].f105_id, prop: r[i].f105_id, titulo: r[i].f105_descripcion });
        }
        this.dataPlanCriterio.filter((v) => {
          this.selplancriterio = v.titulo;
          return false;
        });
      })
  }

  //Funcion portafolio...
  fnPortafolio() {
    this.sPed.getPortafolio()
      .then(result => {
        let r = result.rows;
        this.selportafolio = r[0].f136_descripcion;
        for (let i = 0; i < r.length; i++) {
          this.dataPortafolio.push(
            { id: r[i].f136_id, prop: r[i].f136_id, titulo: r[i].f136_descripcion }
          );
        }
        this.dataPortafolio.filter((v) => {
          this.selportafolio = v.titulo;
          return false;
        });
      })
    return false;
  }

  //Consultar por portafolio..
  cbPortafolio(evento){
    this.valor = evento;
  }

  cbPlanes(evento) {
    this.sPed.getCRI(evento).
      then(result => {
        let r = result.rows;
        this.selcriterio = r[0].f106_descripcion;
        for (let i = 0; i < r.length; i++) {
          this.objCriterio.push({ id: r[i].f106_id, prop: r[i].f106_descripcion, titulo: r[i].f106_descripcion });
        }
        this.objCriterio.filter((v) => {
          this.selcriterio = v.titulo;
          return false;
        });
      })
  }
  
  cbCriterio(evento) {
    this.valor = evento;
    this.filtro=evento;
    console.log("que trae evento,... ",evento)
  }
}
