import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecaudoPage } from './recaudo';

@NgModule({
  declarations: [
    RecaudoPage,
  ],
  imports: [
    IonicPageModule.forChild(RecaudoPage),
  ],
})
export class RecaudoPageModule {}
