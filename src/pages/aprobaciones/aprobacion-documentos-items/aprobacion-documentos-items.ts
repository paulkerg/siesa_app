import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AprobacionesProvider } from '../../../providers/aprobaciones/aprobaciones';

@IonicPage()
@Component({
  selector: 'page-aprobacion-documentos-items',
  templateUrl: 'aprobacion-documentos-items.html',
})
export class AprobacionDocumentosItemsPage {
  items: any = [];
  titulo: any = [];
  contador: string = '';
  segmento: string = "1";
  doctos: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private aprobaciones: AprobacionesProvider) {

  }

  ionViewDidLoad() {
    this.doctos = this.navParams.get('itemSel');
    this.titulo = this.doctos.titulo;
    this.segmentoClick();
  }

  segmentoClick() {
    this.items = [];
    this.contador = '0';
    this.cargaDocumentos(this.doctos);
  }

  cargaDocumentos(doctos) {
    this.aprobaciones.leerAprobaciones(doctos['id'], doctos['id_modulo'], this.segmento, '', '')
      .then((respuesta) => {
        this.llenarListView(respuesta);
      })
      .catch((error) => {

      });
  }

  llenarListView(data) {
    this.contador = data.length;
    for (let k = 0; k < data.length; k++) {
      data[k].doctosSplit = data[k].f_desc_docto.split(/\r\n|\n|\r/g);
      if (data[k]['f_ind_antecesor_pend'] == 1) {
        data[k].bordeIzquierdo = 'bordeIzquierdoRojo'
      } else {
        data[k].bordeIzquierdo = 'bordeIzquierdoVerde'
      }
    }
    this.items = data;
  }

  aprobacionDocumento(item) {
    this.navCtrl.push('AprobacionDocumentosPage', { docto: this.doctos, itemSel: item });
  }

}
