import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AprobacionDocumentosItemsPage } from './aprobacion-documentos-items';

@NgModule({
  declarations: [
    AprobacionDocumentosItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(AprobacionDocumentosItemsPage),
  ],
})
export class AprobacionDocumentosItemsPageModule {}
