import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AprobacionPrincipalPage } from './aprobacion-principal';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    AprobacionPrincipalPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AprobacionPrincipalPage),
  ],
})
export class AprobacionPrincipalPageModule {}
