import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { global } from '../../../app/global';
import { AprobacionesProvider } from '../../../providers/aprobaciones/aprobaciones';

@IonicPage()
@Component({
  selector: 'page-aprobacion-principal',
  templateUrl: 'aprobacion-principal.html',
})
export class AprobacionPrincipalPage {
  information: string = "vacio";
  items: any = [];
  contadores: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private aprobaciones: AprobacionesProvider) {
    this.menu = menu;
    this.menu.enable(true, 'menuPrincipal');
    this.items = global.aprobaciones;
  }

  ionViewDidLoad() {
    this.aprobaciones.contarAprobaciones()
      .then((r: any) => {
        for (let k = 0; k < r.length; k++) {
          const e = r[k];
          if (!this.contadores[e['f75957_id_grupo_clase_docto']]) {
            this.contadores[e['f75957_id_grupo_clase_docto']] = e['f75957_contador_0'];
          }
        }

        for (let i = 0; i < this.items.length; i++) {
          let p = this.items[i];
          for (let t = 0; t < p['registros'].length; t++) {
            let h = p['registros'][t];
            if(this.contadores[h['id']]){
              h['badge'] = this.contadores[h['id']];
            }
          }
        }
      })
      .catch((e) => {
        console.log(e);
      });

    //console.log("items ... ",this.items)
    this.information = "vacio";
    console.log('ionViewDidLoad AprobacionPrincipalPage', this.items);
  }

  toggleSection(i, f) {
    if (this.information != i + f) {
      this.information = i + f;
    } else {
      this.information = 'vacio';
    }
  }

  onclikItem(item) {
    this.navCtrl.push('AprobacionDocumentosItemsPage', { itemSel: item });
  }

  openMenu() {
    this.menu.open('menuPrincipal');
  }

}
