import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AprobacionDocumentosPage } from './aprobacion-documentos';

@NgModule({
  declarations: [
    AprobacionDocumentosPage,
  ],
  imports: [
    IonicPageModule.forChild(AprobacionDocumentosPage),
  ],
})
export class AprobacionDocumentosPageModule {}
