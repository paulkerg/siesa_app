import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer } from 'ionic-angular';
import { AprobacionesProvider } from '../../../providers/aprobaciones/aprobaciones';

@IonicPage()
@Component({
	selector: 'page-aprobacion-documentos',
	templateUrl: 'aprobacion-documentos.html'
})
export class AprobacionDocumentosPage {
	items: any = [];
	itemsdocto: any = [];
	itemsdoctohead: any = [];
	blnAprobarBtn: boolean = true;
	blnRechazarBtn: boolean = true;
	checked: boolean = true;
	itemSel: any;
	doctoSel: any;
	fecha: Date;
	doctoid: any = [ '' ];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private aprobaciones: AprobacionesProvider
	) {}

	ionViewDidLoad() {
		this.itemSel = this.navParams.get('itemSel');
		this.doctoSel = this.navParams.get('docto');
		this.doctoid = this.doctoSel['id'];
		this.items = [ this.itemSel ];

		if (this.itemSel.f_ind_antecesor_pend == 1) {
			this.blnAprobarBtn = false;
			this.blnRechazarBtn = false;
			this.checked = false;
		} else {
			this.blnAprobarBtn = true;
			this.blnRechazarBtn = true;
			this.checked = true;
		}

		for (let k = 0; k < this.itemSel.length; k++) {
			this.itemSel[k].doctosSplit = this.itemSel[k].f_desc_docto.split(/\r\n|\n|\r/g);
		}
		this.leerItems();
	}

	seleccion(estado, fab: FabContainer) {
		for (let h = 0; h < this.itemsdocto.length; h++) {
			if (estado == 'a') {
				this.itemsdocto[h]['selcheckbox'] = true;
			} else {
				this.itemsdocto[h]['selcheckbox'] = false;
			}
		}
		fab.close();
	}

	leerItems() {
		this.aprobaciones
			.leerAprobaciones(
				this.doctoSel['id'],
				this.doctoSel['id_modulo'],
				'',
				this.doctoSel['id_detalle'],
				this.itemSel['f_rowid_movto_funcionario']
			)
			.then((respuesta: any) => {
				for (let i = 0; i < respuesta[0].length; i++) {
					respuesta[0][i].selcheckbox = true;
				}

				this.itemsdocto = respuesta[0];
				this.itemsdoctohead = respuesta[1];

				respuesta[1][0].doctosSplit = this.items[0]['doctosSplit'];
        /*
				for (let t = 0; t < 10; t++) {
					let p = this.itemsdocto[0];
					this.itemsdocto.push({
            colPivote:p['colPivote'],
            f_activo:p['f_activo'],
            f_bodega:p['f_bodega'],
            f_cantidad:p['f_cantidad'],
            f_fecha_entrega:p['f_fecha_entrega'],
            f_ind_check:p['f_ind_check'],
            f_indicador:p['f_indicador'],
            f_item_resumen:p['f_item_resumen'],
            f_motivo:p['f_motivo'],
            f_resumen_item_txt:p['f_resumen_item_txt'],
            f_rowid_movto:t,
            f_rowid_movto_entidad:p['f_rowid_movto_entidad'],
            f_texto:p['f_texto'],
            f_valor_neto:p['f_valor_neto'],
            f_vlr_impuestos:p['f_vlr_impuestos'],
            f_vlr_neto_no_format:p['f_vlr_neto_no_format'],
            selcheckbox:p['selcheckbox'],
          });
				}
        */
			})
			.catch((error) => {});
	}
}
