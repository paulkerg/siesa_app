import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { global } from '../../../app/global';
import { AprobacionesProvider } from '../../../providers/aprobaciones/aprobaciones';

@IonicPage()
@Component({
	selector: 'page-aprobacion-grupos',
	templateUrl: 'aprobacion-grupos.html'
})
export class AprobacionGruposPage {
	information: string = 'vacio';
	items: any = [];
	pitems: any = [];
	contadores: any = [];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private menu: MenuController,
		private aprobaciones: AprobacionesProvider
	) {
		this.menu = menu;
		this.menu.enable(true, 'menuPrincipal');
		this.pitems = global.aprobaciones;
	}

	ionViewDidLoad() {
		this.cargarDatos();
	}

	cargarDatos() {
		this.aprobaciones
			.contarAprobaciones()
			.then((r: any) => {
				for (let k = 0; k < r.length; k++) {
					const e = r[k];
					if (!this.contadores[e['f75957_id_grupo_clase_docto']]) {
						this.contadores[e['f75957_id_grupo_clase_docto']] = e['f75957_contador_0'];
					}
				}

				for (let i = 0; i < this.pitems.length; i++) {
					let p = this.pitems[i];
					let nroReg = 0;
					for (let t = 0; t < p['registros'].length; t++) {
						let h = p['registros'][t];
						if (this.contadores[h['id']]) {
							h['badge'] = this.contadores[h['id']];
						}
						nroReg += parseFloat(h['badge']);
					}
					p['badge'] = nroReg;
				}
				console.log(this.pitems)
				this.items = this.pitems;
			})
			.catch((e) => {
				console.log(e);
			});
	}

	openMenu() {
		this.menu.open('menuPrincipal');
	}

	openSubGallery = (e): any => {
		this.navCtrl.push('AprobacionesListaPage', {
			registro: e['item']
		});
	};

}
