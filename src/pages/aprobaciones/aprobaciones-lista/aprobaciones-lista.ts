import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-aprobaciones-lista',
	templateUrl: 'aprobaciones-lista.html'
})
export class AprobacionesListaPage {
	registro: any;
	items: any;
	titulo: string;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.registro = this.navParams.get('registro');
		this.titulo = this.registro.titulo;
		this.items = this.registro.registros;
	}

	ionViewDidLoad() {
		if (this.items.length == 1) {
			this.onclikItem(this.items[0]);
		}
	}

	onclikItem(item) {
		this.navCtrl.push('AprobacionDocumentosItemsPage', { itemSel: item });
	}
}
