import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AprobacionesListaPage } from './aprobaciones-lista';

@NgModule({
  declarations: [
    AprobacionesListaPage,
  ],
  imports: [
    IonicPageModule.forChild(AprobacionesListaPage),
  ],
})
export class AprobacionesListaPageModule {}
