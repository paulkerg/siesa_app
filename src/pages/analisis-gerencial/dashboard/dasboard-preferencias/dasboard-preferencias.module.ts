import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DasboardPreferenciasPage } from './dasboard-preferencias';

@NgModule({
  declarations: [
    DasboardPreferenciasPage,
  ],
  imports: [
    IonicPageModule.forChild(DasboardPreferenciasPage),
  ],
})
export class DasboardPreferenciasPageModule {}
