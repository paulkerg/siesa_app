import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';
import { global } from '../../../../app/global';

@IonicPage()
@Component({
	selector: 'page-dasboard-preferencias',
	templateUrl: 'dasboard-preferencias.html'
})
export class DasboardPreferenciasPage {
	registros: any;
	tablerosServer: any;
	tablerosUsuario: any;

	constructor(public navCtrl: NavController, public navParams: NavParams, private dbSql: SqliteProvider) {}

	ionViewDidLoad() {
		this.cargarOpciones();
	}

	cargarOpciones() {
		let sql = "select t005_resultado from t005_consultas where f005_id_consulta = 'SIESAPPS_TABLEROS'";
		this.dbSql.query(sql).then((res) => {
			if (res.length > 0) {
				let r1 = JSON.parse(res[0]['t005_resultado']);
				this.tablerosServer = r1;
				this.tablerosUsuario = [];
				for (let i = 0; i < r1.length; i++) {
					const e = r1[i];
					if (e['f_id_wb_objeto'] != '') {
						this.tablerosUsuario.push({
							titulo: e['f_descripcion'],
							objeto: e['f_id_wb_objeto'],
							tipo: global.pubEnumStrtipoGrafica[e['f_tipo_grafica']] ,
							orden: e['f_orden'],
							estado: false
						});
					}
				}
				let sql = "select * from configuraciones where id = 'SIESAPPS_TABLEROS'";
				this.dbSql.query(sql).then((res2) => {
					if (res2.length > 0) {
						let r2 = JSON.parse(res2[0]['documento']);
						console.log(r2)
						Object.keys(r2).forEach((key) => {
							for (let t = 0; t < this.tablerosUsuario.length; t++) {
								const e1 = this.tablerosUsuario[t];
								if (e1['objeto'] == r2[key]['objeto']) {
									e1['estado'] = r2[key]['estado'];
								}
							}
						});
					}
				});
			}
		});
	}

	async guardarCambios() {
		return new Promise((resolve) => {
			let sql =
				"insert or replace into configuraciones (id,documento) values('" +
				'SIESAPPS_TABLEROS' +
				"','" +
				JSON.stringify(this.tablerosUsuario) +
				"')";
			this.dbSql.query(sql).then(() => {
				resolve(true);
			});
		});
	}

	guardar() {
		this.guardarCambios().then((_) => {
			this.salir();
		});
	}
	salir() {
		this.navCtrl.pop();
	}
}
