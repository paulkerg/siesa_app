import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, PopoverController, Events } from 'ionic-angular';
import { global } from '../../../../app/global';
import { SincronizacionProvider } from '../../../../providers/sincronizacion/sincronizacion';
import { SqliteProvider } from '../../../../providers/sqlite/sqlite';
import { List } from 'linqts';
import { AnalyticsProvider } from '../../../../providers/analytics/analytics';

@IonicPage()
@Component({
	selector: 'page-principal',
	templateUrl: 'principal.html'
})
export class PrincipalPage {
	intBadges: number = 0;
	tablerosLocales: any = [];
	consultaOnline: boolean = false;
	strUltimaConsulta: string = '';
	grafica: any = {
		datos: [],
		etiquetas: {},
		opciones: {},
		leyendas: {},
		tipo: ''
	};
	overlayHidden: boolean = false;

	constructor(
		public navCtrl: NavController,
		private menu: MenuController,
		private sync: SincronizacionProvider,
		private popoverCtrl: PopoverController,
		private dbSql: SqliteProvider,
		public events: Events,
		public analytics: AnalyticsProvider
	) {
		this.menu = menu;
		this.menu.enable(true, 'menuPrincipal');
		events.subscribe('cambioNotificacion', (n) => {
			this.llenarNotificaciones();
		});
	}

	ionViewWillEnter() {
		this.overlayHidden = false;
		this.menu.enable(true, 'menuPrincipal');
		this.llenarNotificaciones();
		this.leerTablerosSrv();
	}

	leerTablerosSrv() {
		this.consultaOnline = false;
		this.tablerosLocales = [];
		this.events.publish('cargandoOn');

		let parametros = '|';
		let p1 = {
			Parametros: parametros,
			Consulta: 'SIESAPPS_TABLEROS'
		};
		this.sync
			.postParameters(global.api.consulta, p1)
			.then((r: any) => {
				if (r['length'] > 0) {
					let sql =
						"insert or replace into t005_consultas (f005_id_consulta,t005_resultado) values('" +
						'SIESAPPS_TABLEROS' +
						"','" +
						JSON.stringify(r) +
						"')";
					this.dbSql.query(sql).then(() => {
						this.leerDatosServidor();
					});
				} else {
					this.events.publish('cargandoOff');
				}
			})
			.catch((e: any) => {
				this.cargarTablerosLocales();
			});
	}

	cargarTablerosLocales() {
		this.overlayHidden = false;
		let arrTablas = [];
		this.tablerosLocales = [];
		let sql = "select * from configuraciones where id = 'SIESAPPS_TABLEROS'";
		this.dbSql
			.query(sql)
			.then((res) => {
				if (res.length > 0) {
					let rs = JSON.parse(res[0]['documento']);
					for (let i = 0; i < rs.length; i++) {
						const e = rs[i];
						if (e['estado']) {
							let sql =
								"select t005_resultado, f005_sqltime from t005_consultas where f005_id_consulta = '" +
								e['objeto'] +
								"'";
							this.dbSql.query(sql).then((res2) => {
								if (res2.length > 0) {
									this.strUltimaConsulta = 'offline : ' + res2[0]['f005_sqltime'];
									arrTablas.push(JSON.parse(res2[0]['t005_resultado']));
								}
							});
						}
					}
					this.overlayHidden = true;
				} else {
					this.events.publish('cargandoOff');
				}
			})
			.then(() => {
				this.tablerosLocales = arrTablas;
				this.events.publish('cargandoOff');
			});
	}

	leerDatosServidor() {
		this.overlayHidden = false;
		this.consultaOnline = false;
		let sql = "select * from configuraciones where id = 'SIESAPPS_TABLEROS'";
		this.dbSql.query(sql).then((res2) => {
			if (res2.length > 0) {
				let regs = JSON.parse(res2[0]['documento']);
				let canTableros = 0;
				for (let i = 0; i < regs.length; i++) {
					const e = regs[i];
					e['grafica'] = this.grafica;
					e['grafica']['tipo'] = global.pubEnumStrtipoGrafica[e['tipo']];

					if (e['estado']) {
						canTableros = +1;
						//---- extraer datos del servidor
						let p1 = {
							Parametros: '|',
							Consulta: e['objeto']
						};
						this.sync
							.postParameters(global.api.consulta, p1)
							.then((r: any) => {
								if (r['length'] > 0) {
									this.consultaOnline = true;
									//--- EL resultado se convierte en pivot
									var o = r.reduce((a, b) => {
										a[b.f_serie] = a[b.f_serie] || [];
										a[b.f_serie].push({ [b.f_label]: b.f_data });
										return a;
									}, {});
									var a = Object.keys(o).map(function(k) {
										return { label: k, data: Object.assign.apply({}, o[k]) };
									});
									//--- Se arman los labels
									let l = new List<any>(r).GroupBy(
										(key) => key['f_label'],
										(element) => element['f_label']
									);
									//--- Se arma la data definitiva
									let objData = [];
									let labels = Object.keys(l);
									for (let k = 0; k < a.length; k++) {
										let d = [];
										for (let p = 0; p < labels.length; p++) {
											if (!a[k]['data'][labels[p]]) {
												d.push(0);
											} else {
												d.push(a[k]['data'][labels[p]]);
											}
										}
										objData.push({
											label: a[k]['label'],
											data: d
										});
									}
									e['grafica']['datos'] = objData;
									e['grafica']['etiquetas'] = labels;
									e['grafica']['leyendas'] = true;
									e['grafica']['colores'] = global.pubGraficaColoresBase;

									let sql =
										"insert or replace into t005_consultas (f005_id_consulta,t005_resultado) values('" +
										e['objeto'] +
										"','" +
										JSON.stringify(e) +
										"')";
									this.dbSql.query(sql).then(() => {
										this.events.publish('cargandoOff');
									});
								}
							})
							.then(() => {
								this.cargarTablerosLocales();
							})
							.catch((e: any) => {
								this.cargarTablerosLocales();
							});
					}
				}
				if (canTableros == 0) {
					this.events.publish('cargandoOff');
					this.overlayHidden = false;
					this.consultaOnline = true;
				}
			} else {
				this.events.publish('cargandoOff');
			}
		});
	}

	eventoSobre(e, item) {
		console.log(e, item);
	}

	eventoClick(e, item) {
		console.log(e, item);
	}

	abrirPreferencias(ev) {
		let popover = this.popoverCtrl.create('DasboardPreferenciasPage', {}, { enableBackdropDismiss: false });

		popover.onDidDismiss(() => {
			this.leerTablerosSrv();
		});

		popover.present({
			ev: ev
		});
	}

	openMenu() {
		this.menu.open('menuPrincipal');
	}

	/*--- NOTIFICACIONES ---*/
	llenarNotificaciones() {
		let sql = 'select count(1) regs from t231_mm_notificaciones' + " where f231_ind_lectura = '0'";
		this.dbSql
			.query(sql)
			.then((resp) => {
				if (resp.length > 0) {
					this.intBadges = resp[0]['regs'];
				} else {
					this.intBadges = 1;
				}
			})
			.catch((error) => {});
	}

	irListaNotificaciones() {
		this.navCtrl.push('NotificacionesListaPage');
	}

	public hideOverlay() {
		this.overlayHidden = true;
	}
}
