import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrincipalPage } from './principal';
import { ChartsModule } from 'ng2-charts';
import { ComponentsModule } from '../../../../components/components.module';
import { PipesModule } from '../../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PrincipalPage,
  ],
  imports: [
    ChartsModule,
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(PrincipalPage),
  ],
})
export class PrincipalPageModule {}
