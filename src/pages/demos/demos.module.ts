import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemosPage } from './demos';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    DemosPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(DemosPage),
  ],
})
export class DemosPageModule {}
