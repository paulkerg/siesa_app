import { Diagnostic } from '@ionic-native/diagnostic';
import { Component } from '@angular/core';
import { IonicPage, MenuController } from 'ionic-angular';
import { UtilesProvider } from '../../providers/utiles/utiles';
import { ConfiguracionesProvider } from '../../providers/configuraciones/configuraciones';


@IonicPage()
@Component({
  selector: 'page-demos',
  templateUrl: 'demos.html',
})
export class DemosPage {
  items: any = [{
    "titulo": "Compras",
    "badge": 0,
    "registros": [
      {
        "titulo": "Contratos Proveedores",
        "badge": 0,
      }, {
        "titulo": "Ordenes de compra",
        "badge": 0,
      }, {
        "titulo": "Presupuestos de compra",
        "badge": 0,
      }, {
        "titulo": "Solicitudes de compra",
        "badge": 0,
      }]
  }, {
    "titulo": "Cuentas por cobrar",
    "badge": 0,
    "registros": [
      {
        "titulo": "Ordenes de compra servicios",
        "badge": 0,
      }]
  }, {
    "titulo": "Inventarios",
    "badge": 0,
    "registros": [
      {
        "titulo": "Actividades presupuestos de obra",
        "badge": 0,
      }, {
        "titulo": "Presupuestos de consumo",
        "badge": 0,
      }, {
        "titulo": "Presupuestos de obra",
        "badge": 0,
      }]
  }];

  controlInput: any =
    {
      texto: {
        modelo: "prueba Texto",
        nombre: "texto",
        label: "Numero",
        placeholder: "Ingerse solo numeros"
      },
      numero: {
        modelo: 123,
        nombre: "numero",
        label: "Numero",
        placeholder: "Ingerse numero"
      },
      fecha: {
        modelo: 123,
        nombre: "fecha",
        label: "Fecha",
        placeholder: "Ingerse fecha"
      }
    };

  prvOpciones = {
    blnBorrar: true,
    blnCrearDirectorio: true,
  }
  prvCarpetaBase = "mantenimiento";
  prvFiltro = "";

  prvBotonesTitulo = [
    {
      imagen: "paper-plane",
      estado: true
    },
    {
      imagen: "print",
      estado: true
    },
    {
      imagen: "funnel",
      estado: true
    },
    {
      imagen: "options",
      estado: false
    },
  ];

  miNFC = {
    id: "",
    mensaje: ""
  }
  fecha = new Date().toISOString();
  fechados = new Date("2018/07/01").toISOString();

  objEjemplos: any = [
    { id: 'a', prop: 'foo', titulo: 'texto 1' },
    { id: 'c', prop: 'bar', titulo: 'texto 2' },
    { id: 'b', prop: 'bar', titulo: 'texto 3' },
    { id: 'd', prop: 'foo', titulo: 'texto 4' }
  ];

  comboModelo = "a";
  selectSegmento: string = "componentes";

  constructor(
    private menu: MenuController,
    private utiles: UtilesProvider,
    public diagnostic: Diagnostic,
    public diagnosticService: ConfiguracionesProvider,
  ) {
    this.menu = menu;
    this.menu.enable(true, 'menuPrincipal');
    this.utiles.fechaHoy().then((fecha) => {
      this.fecha = fecha;
    });
    /*
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
    */
  }

  ionViewWillEnter() {
    this.prvFiltro = "1";
  }

  enterKey(e) {
    console.log('Tecla Enter')
  }

  clickLista(e) {
    alert(JSON.stringify(e));
  }

  clickCombo(e) {
    this.comboModelo = e;
    console.log(JSON.stringify(e), this.comboModelo);
  }

  clickBotontitulo(e) {
    console.log(JSON.parse(e));
  }

  openMenu() {
    this.menu.open('menuPrincipal');
  }
}
