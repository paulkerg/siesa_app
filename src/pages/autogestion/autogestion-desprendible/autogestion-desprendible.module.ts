import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutogestionDesprendiblePage } from './autogestion-desprendible';
import { PipesModule } from '../../../pipes/pipes.module';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    AutogestionDesprendiblePage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AutogestionDesprendiblePage),
  ],
})
export class AutogestionDesprendiblePageModule {}
