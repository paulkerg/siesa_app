import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { global } from '../../../app/global';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { ManejoArchivosProvider } from '../../../providers/manejo-archivos/manejo-archivos';

@IonicPage()
@Component({
	selector: 'page-autogestion-desprendible',
	templateUrl: 'autogestion-desprendible.html',
})
export class AutogestionDesprendiblePage {
	prvPeriodo: any;
	prvRegistosVolantes: any;
	prvBotonesTitulo = {
		imagen: 'md-cloud-download',
		estado: false
	};
	btCompartir:boolean=false;
	TiposFormatoDescripcion: string;
	hayRegistros = false;
	error: any;
	contHiden = false;
	overlayHidden: boolean = false;
	constructor(
		public events: Events,
		private sync: SincronizacionProvider,
		private mf: ManejoArchivosProvider,
	) {
		this.prvPeriodo = new Date().toISOString();
	}

	ionViewDidLoad() {
		this.prvBotonesTitulo.estado = true;
		this.hayRegistros = false;
		this.sync
			.getParameters(global.api.TiposFormato, {}, 0, 'AG')
			.then((r: any) => {
				if (r.length > 0) {
					this.TiposFormatoDescripcion = r[0].descripcion;
				}
			})
			.catch((e) => {});
	}

	descargarArchivos(fechaSel) {
		this.prvBotonesTitulo.estado = true;
		this.hayRegistros = false;
		this.events.publish('cargandoOn');

		let datos: any = [];
		let datosLista: any = [];
		this.prvRegistosVolantes = [];
		this.prvPeriodo = fechaSel;
		let fechaSeleccionada = new Date(this.prvPeriodo);
		//--- Inicializa volantes

		let p = {
			ano: fechaSeleccionada.getFullYear(),
			mes: fechaSeleccionada.getMonth() + 1
		};

		new Promise((resolve, reject) => {
			this.sync
				.getParameters(global.api.VolantePagoAg, p, 0, 'AG')
				.then((r: any) => {
					//--- Se hace la peticion al servidor para traer los documentos
					datos = r;
				})
				.then((p) => {
					new Promise((resolve2, reject2) => {
						//--- Se descargan los documentos recibidos
						if (datos.length > 0) {
							let p2 = {
								tipoFormato: this.TiposFormatoDescripcion,
								listDoctos: datos
							};
							this.sync
								.postParameters(global.api.ReporteVolantePagoAg, p2, 1, 'AG', 'json')
								.then((r: any) => {
									if (r.length > 0) {
										//--- Se almacenan los documentos descargados
										new Promise((resolve3, reject3) => {
											this.sync
												.procesarDescargasAG(r)
												.then((lista: any) => {
													for (let p = 0; p < r.length; p++) {
														const e = r[p];
														datosLista.push({
															titulo: e['nombre'].trim(),
															estado: false,
															btnEstado: false,
															archivo: '',
															registro: e
														});
														this.hayRegistros = true;
													}
													resolve3();
												})
												.catch(() => {
													reject3();
												});
										});
										this.prvRegistosVolantes = datosLista;											
										this.error = "";
										this.events.publish('cargandoOff');
										resolve();										
									}
								})
								.then((e) => {
									resolve();
								})
								.catch((e) => {
									reject2();
								});
						}
					});
				})
				.catch((e) => {
					this.error = e.Errors[0];
					this.events.publish('cargandoOff');
					reject(e);
				});
		});
		this.contHiden = true;
		this.overlayHidden = true;
	}

	eventoSeleccionItem(regs) {
		this.prvRegistosVolantes = regs;
		this.prvBotonesTitulo.estado = true;
		for (let i = 0; i < this.prvRegistosVolantes.length; i++) {
			const e = this.prvRegistosVolantes[i];
			if (e['estado'] === true) {
				this.prvBotonesTitulo.estado = false;
			}
		}
	}

	bntVerDescargado(item) {
		console.log(item)
		this.mf.getFile('autogestion', item['titulo']).then(() => {}).catch(() => {});
	}

	btnCompartir() {
		let archivos = [];
		for (let i = 0; i < this.prvRegistosVolantes.length; i++) {
			if (this.prvRegistosVolantes[i]['estado'] == true) {
				archivos.push(this.prvRegistosVolantes[i]['titulo']);
			}
		}
		this.mf
			.compartirAdjuntos('autogestion', '', '', archivos)
			.then((r) => {
				console.log(r);
			})
			.catch((e) => {
				console.log(e);
			});
	}
}
