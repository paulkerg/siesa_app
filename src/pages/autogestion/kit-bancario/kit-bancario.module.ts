import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KitBancarioPage } from './kit-bancario';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    KitBancarioPage,
  ],
  imports: [
    IonicPageModule.forChild(KitBancarioPage),
    ComponentsModule
  ],
})
export class KitBancarioPageModule {}
