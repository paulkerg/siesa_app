import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { global } from '../../../app/global';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { ManejoArchivosProvider } from '../../../providers/manejo-archivos/manejo-archivos';

@IonicPage()
@Component({
  selector: 'page-kit-bancario',
  templateUrl: 'kit-bancario.html',
})
export class KitBancarioPage {
  titulo = "";
  listDocumentos: any = [];
  prvRegistos: any = [];
  error: any;
  prvBotonesTitulo = {
    imagen: 'md-cloud-download',
    estado: false
  };
  tipoFormato = "";
  tipoCertificado = "";
  hayRegistros = false;

  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams,
    private sync: SincronizacionProvider,
    private mf: ManejoArchivosProvider) {
  }

  ionViewDidLoad() {
    this.prvBotonesTitulo.estado = true;
    this.llenaFormatoYCertificado();
  }

  btnGenerar() {
    this.prvRegistos = [];
    this.prvBotonesTitulo.estado = true;
    this.hayRegistros = false;
    let parametros = {
      tipoCertificado: this.tipoCertificado,
      tipoFormato: this.tipoFormato,
      titulo: this.titulo
    };
    let registros = [];
    this.events.publish('cargandoOn');
    this.sync.getParameters(global.api.KitBancario,parametros,0,'AG').then((r:any)=>{
      this.sync.procesarDescargasAG(r).then((listaNombres: any) => {
      })
      for (let i = 0; i < r.length; i++) {
        registros.push({ titulo: r[i].nombre, btnEstado: false });
      }
      this.prvRegistos = registros;
      this.events.publish('cargandoOff');
      this.error = "";
      if(this.prvRegistos.length > 0){
        this.hayRegistros = true;
      }
    }).catch((e:any)=>{
      this.prvRegistos = [];
      let mensajeError = e.Errors[0];
        if (typeof (e.Errors) == 'string') {
          mensajeError = e.Errors;
        }
      this.error=mensajeError;
      this.events.publish('cargandoOff');
    });
  }

  llenaFormatoYCertificado() {    
    this.events.publish('cargandoOn'); 
    //trae Formato
    this.sync
      .getParameters(global.api.TiposFormato, {}, 0, 'AG')
      .then((r: any) => {
        let result = r.length;
        for (let i = 0; i < result; i++) {
          if (r[i].indCargarApp == 1) {
            this.tipoFormato = r[i].descripcion;
            break;
          }
        }
      }).catch((e) => {
        console.log(e);
      });
    //trae certificado
      this.sync
      .getParameters(global.api.TiposCertificado, {}, 0, 'AG')
      .then((r: any) => {
        let result = r.length;
        for (let i = 0; i < result; i++) {
          if (r[i].indCargarApp == 1) {
            this.tipoCertificado = r[i].descripcion;
            break;
          }
        }
      }).catch((e) => {
        this.events.publish('cargandoOff');
        this.prvRegistos = [];
        let mensajeError = e.Errors[0];
        if (typeof (e.Errors) == 'string') {
          mensajeError = e.Errors;
        }
        this.error=mensajeError; 
      });
      this.events.publish('cargandoOff');
  }

  eventoSeleccionItem(reg) {
    this.prvRegistos = reg;
		this.prvBotonesTitulo.estado = true;
		for (let i = 0; i < this.prvRegistos.length; i++) {
			const e = this.prvRegistos[i];
			if (e['estado'] === true) {
        this.prvBotonesTitulo.estado = false;
        break;
			}
		}
  }

  eventoVerDescargado(ev) {
    this.mf.getFile('autogestion', ev.titulo).then(() => { }).catch(() => { });    
  }

  btnCompartir() {
    let archivos = [];
		for (let i = 0; i < this.prvRegistos.length; i++) {
			if (this.prvRegistos[i]['estado'] == true) {
				archivos.push(this.prvRegistos[i]['titulo']);
			}
    }
		this.mf
			.compartirAdjuntos('autogestion', '', '', archivos)
			.then((r) => {})
			.catch((e) => {
				console.log(e);
			});
	}
}
