import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartaLaboralPage } from './carta-laboral';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    CartaLaboralPage,
  ],
  imports: [
    IonicPageModule.forChild(CartaLaboralPage),
    ComponentsModule
  ],
})
export class CartaLaboralPageModule {}
