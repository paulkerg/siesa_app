import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { global } from '../../../app/global';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { ManejoArchivosProvider } from '../../../providers/manejo-archivos/manejo-archivos';

@IonicPage()
@Component({
  selector: 'page-carta-laboral',
  templateUrl: 'carta-laboral.html',
})
export class CartaLaboralPage {

  prvTiposCarta: any;
  prvRegistosCartas: any;
  parametrosCartaLaboral = {
    tipoCertificado: "",
    titulo:""
  }
  prvBotonesTitulo = {
		imagen: 'md-cloud-download',
		estado: false
	};
  disabled = true;
  nombrePdf: any;
  error: any;
  hayRegistros = false;

  constructor(public events: Events,public navCtrl: NavController, public navParams: NavParams,private sync: SincronizacionProvider,
    private mf: ManejoArchivosProvider) {
  }

  ionViewDidLoad() {
    this.prvBotonesTitulo.estado = true;    
    let tiposCarta = [];
    this.events.publish('cargandoOn');
    this.sync.getParameters(global.api.TiposCertificado, '', 1, 'AG').then((result: any) => {
      for (let i = 0; i < result.length; i++) {
        let inf = result[i];
        tiposCarta.push({ titulo: inf['descripcion'], id: inf['descripcion'] });
      }
      this.prvTiposCarta = tiposCarta;
      this.events.publish('cargandoOff');
    }).catch(error =>{
      console.log(error);
      this.events.publish('cargandoOff');
    });
  }
  
  eventoSeleccionAnio(ev) {
    this.disabled = true;
    this.parametrosCartaLaboral.tipoCertificado = ev;
    if(this.parametrosCartaLaboral.tipoCertificado!=""){
      this.disabled = false;
    }
  }

  btnGenerar(){
    this.prvBotonesTitulo.estado = true;
    this.hayRegistros = false;
    this.events.publish('cargandoOn');
    let registros = [];
    this.sync.getParameters(global.api.CertificadoLaboralAg, this.parametrosCartaLaboral, 1, 'AG').then((result: any) => {
      this.sync.procesarDescargasAG(result).then((listaNombres: any) => {})
      for (let i = 0; i < result.length; i++) {
        registros.push({ titulo: result[i].nombre, btnEstado: false });
      }
      this.nombrePdf = registros;
      if(this.nombrePdf.length > 0){
        this.hayRegistros = true;
      }
      this.events.publish('cargandoOff');
    }).catch(e => {
      this.events.publish('cargandoOff');
      this.nombrePdf = [];
      let mensajeError = e.Errors[0];
        if (typeof (e.Errors) == 'string') {
          mensajeError = e.Errors;
        }
        this.error=mensajeError;
    });
  }

  eventoSeleccionItem(regs) {
		this.prvRegistosCartas = regs;
		this.prvBotonesTitulo.estado = true;
		for (let i = 0; i < this.prvRegistosCartas.length; i++) {
			const e = this.prvRegistosCartas[i];
			if (e['estado'] === true) {
				this.prvBotonesTitulo.estado = false;
			}
		}		
	}

  eventoVerDescargado(ev) {
      this.mf.getFile('autogestion', ev.titulo).then(() => { }).catch(() => { });
  }

  btnCompartir() {
		let archivos = [];
		for (let i = 0; i < this.prvRegistosCartas.length; i++) {
			if (this.prvRegistosCartas[i]['estado'] == true) {
				archivos.push(this.prvRegistosCartas[i]['titulo']);
			}
		}
		this.mf
			.compartirAdjuntos('autogestion', '', '', archivos)
			.then((r) => {})
			.catch((e) => {
				console.log(e);
			});
	}
}

