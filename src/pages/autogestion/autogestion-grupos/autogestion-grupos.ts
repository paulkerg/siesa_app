import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { global } from '../../../app/global';

@IonicPage()
@Component({
  selector: 'page-autogestion-grupos',
  templateUrl: 'autogestion-grupos.html',
})
export class AutogestionGruposPage {

	information: string = 'vacio';
	items: any = [];
	pitems: any = [];
	contadores: any = [];

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private menu: MenuController,
	) {
		this.menu = menu;
		this.menu.enable(true, 'menuPrincipal');
		this.items = global.autogestion;
	}

  ionViewDidLoad() {
  }

	openMenu() {
		this.menu.open('menuPrincipal');
	}

  openSubGallery = (e): any => {
		this.navCtrl.push(e['item']['pagina']);
	};

}
