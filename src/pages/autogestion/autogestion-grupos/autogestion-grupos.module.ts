import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutogestionGruposPage } from './autogestion-grupos';
import { PipesModule } from '../../../pipes/pipes.module';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    AutogestionGruposPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(AutogestionGruposPage),
  ],
})
export class AutogestionGruposPageModule {}
