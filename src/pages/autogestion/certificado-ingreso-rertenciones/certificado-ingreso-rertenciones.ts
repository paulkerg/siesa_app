import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert } from 'ionic-angular';
import { global } from '../../../app/global';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { Events } from 'ionic-angular';
import { ManejoArchivosProvider } from '../../../providers/manejo-archivos/manejo-archivos';

@IonicPage()
@Component({
  selector: 'page-certificado-ingreso-rertenciones',
  templateUrl: 'certificado-ingreso-rertenciones.html',
})
export class CertificadoIngresoRertencionesPage {
  prvAnos: any;
  nombrePdf: any;
  error: any;
  prvBotonesTitulo = {
    imagen: 'icon icon-share-variant',
    estado: false
  };
  overlayHidden: boolean = false;
  contHiden = false;
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams,
    private sync: SincronizacionProvider,
    private mf: ManejoArchivosProvider
  ) {}

  ionViewDidLoad() {
    this.prvBotonesTitulo.estado = true;
    let anios = [];
    this.events.publish('cargandoOn');
    this.sync.getParameters(global.api.ListAniosIngresosRetenAg, '', 1, 'AG').then((result: any) => {
      for (let i = 0; i < result.length; i++) {
        let inf = result[i];
        anios.push({ titulo: inf['ano'], id: inf['ano'] });
      }
      this.prvAnos = anios;
      this.events.publish('cargandoOff');
    }).catch(error => {
      this.events.publish('cargandoOff'); 
      console.log("error en fechas.. ", error);
    });
  }

  eventoSeleccionAnio(ev) {
    this.prvBotonesTitulo.estado = true;
    this.events.publish('cargandoOn');
    let parametros = {
      anio: ev
    };
    let registros = [];
    this.sync.getParameters(global.api.ReporteIngresosRetencionAg, parametros, 0, 'AG').then((result: any) => {
      this.sync.procesarDescargasAG(result).then((listaNombres: any) => {
      })
      for (let i = 0; i < result.length; i++) {
        registros.push({ titulo: result[i].nombre, btnEstado: false });
      }
      this.nombrePdf = registros;
      this.events.publish('cargandoOff');
      this.error = "";
    }).catch(e => {
      this.events.publish('cargandoOff');
      this.nombrePdf = [];
      this.error = 'No existen registros en la consulta.';
    });

    this.contHiden = true;
    this.overlayHidden = true;
  }

  eventoClickItem(ev) {
    for (let i = 0; i < ev.length; i++) {
      if(ev[i].estado==true){
        this.prvBotonesTitulo.estado = false;
        break;
      }else{
        this.prvBotonesTitulo.estado = true;
      }
    }
  }

  eventoClickVer() {
    for (let i = 0; i < this.nombrePdf.length; i++) {
      this.mf.getFile('autogestion', this.nombrePdf[i].titulo).then(() => { }).catch(() => { });
    }
  }

  btnCompartir() {
    let archivos = [];
    for (let i = 0; i < this.nombrePdf.length; i++) {
      archivos.push(this.nombrePdf[i]['titulo']);
    }
    this.mf
      .compartirAdjuntos('autogestion', '', '', archivos)
      .then((r) => {})
      .catch((e) => {});
  }


}
