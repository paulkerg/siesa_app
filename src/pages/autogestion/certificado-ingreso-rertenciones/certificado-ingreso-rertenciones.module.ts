import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CertificadoIngresoRertencionesPage } from './certificado-ingreso-rertenciones';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    CertificadoIngresoRertencionesPage,
  ],
  imports: [
    IonicPageModule.forChild(CertificadoIngresoRertencionesPage),
    ComponentsModule
  ],
})
export class CertificadoIngresoRertencionesPageModule {}
