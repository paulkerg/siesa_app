import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SincronizacionProvider } from '../../../providers/sincronizacion/sincronizacion';
import { global } from '../../../app/global';
import { PipeFechaDiferenciaPipe } from '../../../pipes/pipe-fecha-diferencia/pipe-fecha-diferencia';
import { PipeFechaFormatearAPipe } from '../../../pipes/pipe-fecha-formatear-a/pipe-fecha-formatear-a';



@IonicPage()
@Component({
  selector: 'page-vacaciones',
  templateUrl: 'vacaciones.html',
  providers: [PipeFechaDiferenciaPipe,PipeFechaFormatearAPipe]
})
export class VacacionesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private sync: SincronizacionProvider, private pipeFecha: PipeFechaDiferenciaPipe, 
  private pipeFechaFormato: PipeFechaFormatearAPipe,
  public alertController: AlertController) {}

  items: any;
  fechaHoy = new Date();
  fechaMin = this.pipeFechaFormato.transform(this.fechaHoy,'YYYY-MM-DD');
  parametrosVacaciones = {
    fechaInicio: this.fechaMin,
    dias: 0,
    correoJefe: 'false'
  }; 
  disabledButton = true;
  transaccionExitosa = false;
  inicioVacaciones = '';
  duracion = '';
  fechaReintegro = '';
  error:any;

  ionViewDidLoad() {
    this.sync.getParameters(global.api.ConsultarVacacionesPendientesAg, {}, 1, 'AG').then((result: any) => {      
      let diasHastaVacaciones = this.calculaDiasHasta(result[0].VencimientoProximoPeriodo);
      let items = [];
      items.push({
        titulo: diasHastaVacaciones,
        subtitulo: 'Días hasta mis vacaciones',
        texto1: result[0].VencimientoProximoPeriodo,
        colorbaldosa: 'columnas-azul'
      });
      items.push({
        titulo: Math.round(result[0].TotalDiasPendientes),
        subtitulo: 'Días dispónibles',
        colorbaldosa: 'columnas-verde'
      });
      items.push({
        titulo: Math.round(result[0].PeriodosPendientes),
        subtitulo: 'Periodos de vacaciones',
        colorbaldosa: 'columnas-blanco',
        texto1: "Venció \n" + result[0].VencimientoUltimoPeriodo
      })
      this.items = items;
    }).catch((error:any)=>{
      let items = [];
      items.push({
        titulo: 0,
        subtitulo: 'Días hasta mis vacaciones',
        texto1: 0,
        colorbaldosa: 'columnas-azul'
      });
      items.push({
        titulo: 0,
        subtitulo: 'Días dispónibles',
        colorbaldosa: 'columnas-verde'
      });
      items.push({
        titulo: 0,
        subtitulo: 'Periodos de vacaciones',
        colorbaldosa: 'columnas-blanco'
      })
      this.items = items;
    })
  }

  calculaDiasHasta(VencimientoProximoPeriodo) {
    let fechaInicio = new Date();
    let fechaFin = new Date(VencimientoProximoPeriodo);
    let diasHasta = this.pipeFecha.transform(fechaFin, fechaInicio, 'd');
    return diasHasta;
  }

  onclikCamposObligatorios() {
    this.disabledButton = true;
    if (this.parametrosVacaciones.dias > 0 && this.parametrosVacaciones.fechaInicio != '') {
      this.disabledButton = false;
    }
  }

  async presentAlertConfirm() {
    const alert = this.alertController.create({
      title: 'Solicitar Vacaciones',
      message: '<p>Inicio vacaciones: <b>'+this.parametrosVacaciones.fechaInicio+'</b></p>'+
               '<p>Duración: <b>'+this.parametrosVacaciones.dias+' días</b></p>',
      enableBackdropDismiss: false,
      cssClass:'alertCustomCss',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'buttonCancelar',
          handler: (blah) => {}
        }, {
          text: 'Solicitar',
          cssClass: 'buttonAceptar',
          handler: () => {
            this.btnSolicitar();
          }
        }
      ]
    });
    alert.present();
  }

  btnSolicitar() {    
    this.sync.postParameters(global.api.ProgramacionVacacionesAg, this.parametrosVacaciones, 0, 'AG').then(
      (result: any) => {
        this.transaccionExitosa = true;
        this.inicioVacaciones = result.c0616FechaInicial;
        this.duracion = result.c0616DiasLiqVacac;
        this.fechaReintegro = result.c0616FechaFinal;
        this.error = "";
      }).catch((error: any) => {
        this.transaccionExitosa = false;
        let mensajeError = error.Errors[0];
        if (typeof (error.Errors) == 'string') {
          mensajeError = error.Errors;
        }
        this.error=mensajeError;
      })
  }
}
