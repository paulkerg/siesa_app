import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VacacionesPage } from './vacaciones';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    VacacionesPage,
  ],
  imports: [
    IonicPageModule.forChild(VacacionesPage),
    ComponentsModule
  ],
})
export class VacacionesPageModule {}
