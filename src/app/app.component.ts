import { Component, ViewChild } from '@angular/core';
import {
	Platform,
	MenuController,
	Nav,
	Events,
	App,
	AlertController,
	Content,
	LoadingController,
	Loading
} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuService } from '../services/menu-service';
import { ConfiguracionesProvider } from '../providers/configuraciones/configuraciones';
import { SocketServiceProvider } from './../providers/socket-service/socket-service';
import { ManejoArchivosProvider } from '../providers/manejo-archivos/manejo-archivos';
import { global } from './global';
import { Device } from '../../node_modules/@ionic-native/device';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AppVersion } from '@ionic-native/app-version';
import { AuthProvider } from '../providers/auth/auth';
import { APP_CONFIG } from '../config/app.config';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AnalyticsProvider } from '../providers/analytics/analytics';

@Component({
	templateUrl: 'app.html',
	providers: [ MenuService ]
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
	@ViewChild(Content) content: Content;

	loading: Loading;
	rootPage: any = '';
	pages: any;
	leftMenuTitle: string;
	fotoUsuario: string;
	selectedTheme: any;
	blnCargando: boolean = false;
	urlDescargas: string;
	options: InAppBrowserOptions = {
		location: 'yes', //Or 'no'
		hidden: 'no', //Or  'yes'
		clearcache: 'yes',
		clearsessioncache: 'yes',
		zoom: 'yes', //Android only ,shows browser zoom controls
		hardwareback: 'no',
		mediaPlaybackRequiresUserAction: 'no',
		shouldPauseOnSuspend: 'no', //Android only
		closebuttoncaption: 'Cerrar', //iOS only
		disallowoverscroll: 'no', //iOS only
		toolbar: 'yes', //iOS only
		enableViewportScale: 'no', //iOS only
		allowInlineMediaPlayback: 'no', //iOS only
		presentationstyle: 'pagesheet', //iOS only
		fullscreen: 'yes' //Windows only
	};
	constructor(
		public platform: Platform,
		statusBar: StatusBar,
		splashScreen: SplashScreen,
		private loadingCtrl: LoadingController,
		public menu: MenuController,
		private menuService: MenuService,
		public events: Events,
		public app: App,
		public alertCtrl: AlertController,
		private conf: ConfiguracionesProvider,
		public socket: SocketServiceProvider,
		private device: Device,
		public fs: ManejoArchivosProvider,
		private auth: AuthProvider,
		private iab: InAppBrowser,
		private appVersion: AppVersion,
		private ga: GoogleAnalytics,
		public analyticsProvider: AnalyticsProvider
	) {
		platform.ready().then(() => {
			statusBar.styleDefault();

			this.auth.validaUsuarioLocal().then((nuevoUsuario: boolean) => {
				if (nuevoUsuario) {
					this.rootPage = 'OnboardingPage';
				} else {
					this.rootPage = 'LoginPage';
				}
				splashScreen.hide();
			});

			if (this.device.platform) {
				global.dispositivo.plataforma = this.device.platform.toLowerCase();
				if (global.dispositivo.plataforma.match(/win/)) {
					global.dispositivo.plataforma = 'windows';
				}
				let body = document.getElementsByTagName('body')[0];
				body.classList.add(global.dispositivo.plataforma);
			}

			// Configuraciones del Sistema
			this.conf.eventosGenerales();
			if (this.platform.is('core')) {
				console.log('browser');
			} else {
				//this.conf.validacionNatives();
				this.ga
					.startTrackerWithId('UA-134638627-1')
					.then(() => {
						this.nav.viewDidEnter.subscribe(
							(view) => {
							  this.analyticsProvider.trackView(view.instance.constructor.name);
							}
						  );
					  
					})
					.catch((e) => console.log('Error starting GoogleAnalytics == ' + e));
			}

			this.conf.getActiveTheme().subscribe((val) => (this.selectedTheme = val));
			//this.conf.setActiveTheme('online-theme');

			platform.registerBackButtonAction(() => {
				let nav = app.getActiveNavs()[0];
				let activeView = nav.getActive();
				if (activeView.name === 'PrincipalPage') {
					if (nav.canGoBack()) {
						nav.pop();
					} else {
						const alerta = this.alertCtrl.create({
							cssClass: 'custom-alert-confirm',
							title: 'Alerta  SIESAPPS',
							message: 'Realmente desea salir de SIESAPPS?',
							buttons: [
								{
									text: 'Cancelar',
									role: 'cancel',
									handler: () => {
										console.log('Application exit prevented!');
									}
								},
								{
									text: 'Cerrar',
									handler: () => {
										this.platform.exitApp(); // Close this application
									}
								}
							]
						});
						alerta.present();
					}
				}
			});

			this.fs.inicializa();
		});
		/*
		this.socket.initialize();
		this.socket.socketService.subscribe(event => {
			console.log('message received from server... ', event);
		});
		*/
		this.llenaPaginas();

		/*
			Definicion de eventos genericos del APP
		*/
		events.subscribe('cambioMenu', (direction) => {
			this.llenaPaginas();
		});

		events.subscribe('validaVersionERP', (datos) => {
			this.validaVersionERP(datos);
			this.auth.buscarMenuUNOEE().then(() => {});
		});

		events.subscribe('cambioNFC', (mensaje) => {
			let token = localStorage.getItem('token');
			if (token) {
				mensaje = JSON.parse(mensaje);
				if (mensaje['modulo'] === 'mantenimiento') {
					global.params_mnto.id_equipo = mensaje['codigo'];
					if (!global.params_mnto.flag_detalle) {
						global.params_mnto.actividad_id = null;
					}
					this.nav.setRoot('ActividadesListaPage');
				}
			}
		});

		events.subscribe('cargandoOn', () => {
			if (!this.blnCargando) {
				this.blnCargando = true;
				this.loading = this.loadingCtrl.create({
					content: 'Cargando...',
					dismissOnPageChange: true
				});
				this.loading.present();
			}
		});

		events.subscribe('cargandoOff', () => {
			if (this.blnCargando) {
				this.blnCargando = false;
				this.loading.dismiss().catch(() => {});
			}
		});
	}

	fnComparaVersion(a, b, options: {}) {
		//-- 1.19.0131.0
		return new Promise((resolve, reject) => {
			if (a === b) {
				resolve(0);
			}

			var a_components = a.split('.');
			var b_components = b.split('.');

			var len = Math.min(a_components.length, b_components.length);

			// loop while the components are equal
			for (var i = 0; i < len; i++) {
				// A bigger than B
				if (parseInt(a_components[i]) > parseInt(b_components[i])) {
					resolve(1);
				}

				// B bigger than A
				if (parseInt(a_components[i]) < parseInt(b_components[i])) {
					resolve(-1);
				}
			}

			// If one's a prefix of the other, the longer one is greater.
			if (a_components.length > b_components.length) {
				resolve(1);
			}

			if (a_components.length < b_components.length) {
				resolve(-1);
			}

			// Otherwise they are the same.
			resolve(0);
		});
	}

	validaVersionERP(datos) {
		if (datos && this.device.platform.toUpperCase() != 'BROWSER') {
			let vesionERP: string = datos[0]['f5798_id_version'] + '.' + datos[0]['f5798_id_service_pack'];
			this.appVersion.getVersionNumber().then((version: string) => {
				this.fnComparaVersion(version, vesionERP, {})
					.then((r) => {
						if (r === -1) {
							if (this.device.platform.toUpperCase() != 'BROWSER') {
								if (this.device.platform.toUpperCase() === 'ANDROID') {
									this.urlDescargas =
										APP_CONFIG.urlDescargas +
										'android/' +
										datos[0]['f5798_id_version'] +
										'/' +
										datos[0]['f5798_id_service_pack'] +
										'/siesapps.apk';
								} else {
									this.urlDescargas =
										'itms-services://?action=download-manifest&url=' +
										APP_CONFIG.urlDescargas +
										'/ios/' +
										datos[0]['f5798_id_version'] +
										'/' +
										datos[0]['f5798_id_service_pack'] +
										'/app.plist';
								}
								const alerta = this.alertCtrl.create({
									cssClass: 'custom-alert-general',
									title: 'Actualizacion SIESAPPS',
									message: 'Descargar nueva version ' + vesionERP + '?',
									enableBackdropDismiss: false,
									buttons: [
										{
											text: 'Cancelar',
											cssClass:
												'disable-hover button button-outline button-outline custom-alert-btn-cancelar',
											handler: () => {}
										},
										{
											text: 'Aceptar',
											cssClass:
												'disable-hover button button-outline button-outline custom-alert-btn-aceptar',
											handler: () => {
												let target = '_system';
												this.iab
													.create(this.urlDescargas, target, this.options)
													.on('loadstart')
													.subscribe(
														() => {
															this.platform.exitApp();
														},
														(err) => {
															alert('Error cargando archivo: ' + err);
														}
													);
											}
										}
									]
								});
								alerta.present();
							}
						}
					})
					.then(() => {
						this.auth.buscarMenuUNOEE().then(() => {}).catch(() => {});
					});
			});
		}
	}

	llenaPaginas() {
		return new Promise((resolve, reject) => {
			this.menuService.getUsuarioMenu().then((r) => {
				if (r.length > 0) {
					let usuario = JSON.parse(r[0]['documento'] ? r[0]['documento'] : '[]');
					let menu = JSON.parse(r[1] ? r[1]['documento'] : '[]');
					if (usuario['f_tercero_foto']) {
						this.fotoUsuario = 'data:image/png;base64,' + usuario['f_tercero_foto'];
					} else {
						this.fotoUsuario = 'assets/images/logo/login-3.png';
					}
					this.leftMenuTitle = usuario['f_tercero_nombre'];
					setTimeout(() => {
						this.pages = menu;
						this.content.resize();
					}, 10);
				}

				resolve(r);
			});

			this.menuService.getTitle().then((nombre) => {
				this.leftMenuTitle = nombre;
			});

			this.menuService.getFoto().then((foto) => {
				if (foto) {
					this.fotoUsuario = 'data:image/png;base64,' + foto;
				} else {
					this.fotoUsuario = 'assets/images/logo/login-3.png';
				}
			});

			/*
			this.menuService.getAllMenu().then((paginas) => {
				console.log(paginas)
				this.pages = paginas;
				resolve(paginas);
			});
			*/
		});
	}

	openPage(page) {
		this.menu.enable(false, 'menuPrincipal');
		this.menu.close();
		// navigate to the new page if it is not the current page
		if (page['page'] != '') {
			if (page['page'] === 'salir') {
				this.platform.exitApp();
			} else {
				this.nav.setRoot(page['page']);
			}
			/*
			this.nav.root(page['page'], {
				componentName: page.theme
			});
			*/
		}
	}

	menuClosed() {
		this.events.publish('menu:closed', '');
	}

	menuOpened() {
		this.llenaPaginas().then((pg) => {
			this.events.publish('menu:opened', '');
		});
	}

	descargarActualizacion(e) {
		e.preventDefault();
		alert('descargarActualizacion');
	}
}
