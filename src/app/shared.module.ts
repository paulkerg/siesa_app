import { PipesModule } from '../pipes/pipes.module';
import { ComponentsModule } from '../components/components.module';
import { DIRECTIVES } from './app.imports';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TextMaskModule } from 'angular2-text-mask';
import { LongPressModule } from 'ionic-long-press';
import { DirectivesModule } from '../directives/directives.module';

@NgModule({
  declarations: [
    DIRECTIVES,
  ],
  imports: [
    IonicModule,
    PipesModule,
    ComponentsModule,
    DirectivesModule,
    TextMaskModule,
    LongPressModule,
  ],
  exports: [
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    TextMaskModule,
    LongPressModule,
  ]
})

export class SharedModule { }
