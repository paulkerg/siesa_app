//import { APP_CONFIG } from '../config/app.config';
export var global = {
	sincronizando: [],
	sincronizando_toast: [],
	timeoutPost: 18000,
	timeoutSync: 9000,
	production: true,
	//--- Parametros Mantenimiento
	seguridad_mnto: {
		MNTOPREV_ORDENES_ACTUALIZACION_OT_ACTIVID_REQ_PARO: 0,
		MNTOPREV_ORDENES_ACT_FALLA_TRAB_CORRECTIVOS: 0
	},
	params_mnto: {
		id_equipo: null, //Variable donde se guarda el ID del equipo leido por NFC
		actividad_id: null, //actividad que se encontrava en la vista detalle
		flag_detalle: false, //valiadar que se encuentre en la vista Detalle de la actividad
		finalizar: false // validar que se desea terminar la actividad y exige NFC
	},
	actividadSel: {
		c0820_consec_docto: null,
		c0820_id_co: null,
		c0820_id_tipo_docto: null,
		id: '',
		descripcion: '',
		trabajo: '',
		detalle_trabajo: '',
		f_foto: '',
		c0822_fecha_programada: '',
		c0822_notas: '',
		CO: '', //---
		tipo_docto: '', // --
		numero_docto: '', // --
		notas: '', // --
		procedimiento: '',
		paroEnCadena: false, // --
		c0822_fecha_ejecucion_ini: null, // -
		c0819_rowid_w0822: null,
		c0822_rowid_docto_ot: null,
		c0822_rowid: null,
		lecturas: null, // -
		motivos: [], // --
		ayudantes: [ { c0827_rowid_tercero: 0 } ], // --
		c0819_ind_dano: 0, // --
		c0819_detalle_dano: ' ', // --
		c0819_ind_tipo_dano: 0, // -
		c0846_rowid_causal_falla: null, // -
		c0846_rowid_tipo_falla: null, // --
		c0846_notas: ' ', // --
		c0846_ind_estado: 0, // --
		c0822_id_equipo: null,
		c0808_referencia: null,
		bordeIzquierdo: null, // --
		prvIdEquipo: 0, // --
		c0806_ind_regimen: null,
		c0808_ind_requiere_paro: null,
		c0819_ind_requiere_paro: null,
		c0819_ind_tipo_trabajo: null,
		c0822_duracion_horas_prog: 0,
		tareas: [], // -
		movimiento: null, // -
		tiempo_movimientos: 0, //-
		tareaSeleccionada: null, //--
		c0822_rowid_actividad: null,
		c0822_rowid_responsable: null,
		c0822_ind_enviado: 0,
		habilitar_error: true // -
	},
	//--- Parametros Generales
	usuario: {},
	dispositivo: {
		plataforma: '',
		idUsuario: '',
		idVendedor: '',
		idFuncionario: '',
		idDispositivo: '',
		idCia: '1',
		idConexion: '1',
		nombre: '',
		perfil: '',
		isOnLine: false,
		indUsuarioUNOEE: 0,
		indUsuarioAutogestion: 0,
		indUsuarioMantenimiento: 0,
		apps: []
	},

	api: {
		token: 'Token',
		tokenAG: 'Token',
		//"consulta": "api/mobile/consulta",
		//"sincronizar": "api/mobile/sincronizar",
		salir: 'api/sesion/salir',
		//--- Nuevo API
		GetConexiones: 'api/auth/GetConexiones',
		GetToken: 'api/auth/GetToken',
		GetTokenAG: 'api/auth/GetTokenAG',
		GetMenusAG: 'api/auth/GetMenusAG',
		consulta: 'api/Mobile/consulta',
		sincronizar: 'api/Mobile/sincronizar',
		EnviarActividades: 'api/Mobile/EnviarActividades',
		TiposFormato: 'api/Nomina/Reportes/Liquidacion/Certificados/TiposFormato',
		VolantePagoAg: 'api/Nomina/Reportes/Liquidacion/Certificados/VolantePagoAg',
		ReporteVolantePagoAg: 'api/Nomina/Reportes/Liquidacion/Certificados/ReporteVolantePagoAg',
		ListAniosIngresosRetenAg:'api/Nomina/Reportes/Liquidacion/Certificados/ListAniosIngresosRetenAg',
		ReporteIngresosRetencionAg:'api/Nomina/Reportes/Liquidacion/Certificados/ReporteIngresosRetencionAg',		
		ConsultarVacacionesPendientesAg:'api/Nomina/Procesos/Liquidacion/Vacaciones/ConsultarVacacionesPendientesAg',
		ProgramacionVacacionesAg: 'api/Nomina/Procesos/Liquidacion/Vacaciones/ProgramacionVacacionesAg',
		TiposCertificado: 'api/Nomina/Reportes/Liquidacion/Certificados/TiposCertificado',
		CertificadoLaboralAg: 'api/Nomina/Reportes/Liquidacion/Certificados/CertificadoLaboralAg',
		KitBancario: 'api/Nomina/Reportes/Liquidacion/Certificados/kitBancario'				
	},
	pubGraficaColoresBase: [
		{
			// azul claro
			backgroundColor: 'rgba(48, 183, 210, 0.73)',
			pointBackgroundColor: '#233238',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: '#233238'
		},
		{
			// azul oscuro
			backgroundColor: 'rgba(0, 75, 150, 0.73)',
			pointBackgroundColor: '#233238',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: '#233238'
		},
		{
			// amarillo
			backgroundColor: 'rgba(255, 189, 71, 0.73)',
			pointBackgroundColor: '#233238',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: '#233238'
		},
		{
			// naranja
			backgroundColor: 'rgba(247, 147, 30, 0.73)',
			pointBackgroundColor: '#233238',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: '#233238'
		},
		{
			// verde oscuro
			backgroundColor: 'rgba(34, 181, 115, 0.73)',
			pointBackgroundColor: '#233238',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: '#233238'
		},
		{
			// verde claro
			backgroundColor: 'rgba(140, 198, 63, 0.73)',
			pointBackgroundColor: '#233238',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: '#233238'
		}
	],
	pubGraficaTipoBarras: 'bar',
	pubGraficaTipoLineas: 'line',
	pubGraficaTipoTorta: 'pie',
	pubGraficaTipoDonas: 'doughnut',
	tipoGraficas: {
		torta: 'md-pie',
		lineas: 'md-trending-up',
		barras: 'md-stats',
		grilla: 'md-grid'
	},
	pubEnumStrtipoGrafica: {
		torta: 'pie',
		lineas: 'line',
		barras: 'bar',
		donas: 'doughnut'
	},
	autogestion: [
		{
			titulo: 'Cert. Ingresos y Retenciones',
			imagen: 'assets/icon/certificadoir.png',
			pagina: 'CertificadoIngresoRertencionesPage'
		},
		{
			titulo: 'Desprendible Pago',
			imagen: 'assets/icon/desprendible.png',
			pagina: 'AutogestionDesprendiblePage'
		},
		{
			titulo: 'Vacaciones',
			imagen: 'assets/icon/vacaciones.png',
			pagina: 'VacacionesPage'
		},
		{
			titulo: 'Kit Bancario',
			imagen: 'assets/icon/kit.png',
			pagina: 'KitBancarioPage'
		},
		{
			titulo: 'Carta Laboral',
			imagen: 'assets/icon/carta.png',
			pagina: 'CartaLaboralPage'
		}
	],
	aprobaciones: [
		{
			titulo: 'Compras',
			badge: 0,
			imagen: 'assets/icon/compras.png',
			registros: [
				{
					titulo: 'Contratos Proveedores',
					badge: 0,
					id: 409,
					id_modulo: 11574,
					id_detalle: 11571
				},
				{
					titulo: 'Ordenes de compra',
					badge: 0,
					id: 402,
					id_modulo: 11568,
					id_detalle: 11581
				},
				{
					titulo: 'Presupuestos de compra',
					badge: 0,
					id: 420,
					id_modulo: 11569,
					id_detalle: 99981
				},
				{
					titulo: 'Solicitudes de compra',
					badge: 0,
					id: 401,
					id_modulo: 11567,
					id_detalle: 11580
				}
			]
		},
		{
			titulo: 'Cuentas por pagar',
			badge: 0,
			imagen: 'assets/icon/cxcobrar.png',
			registros: [
				{
					titulo: 'Ordenes de compra servicios',
					badge: 0,
					id: 305,
					id_modulo: 11569,
					id_detalle: 11571
				}
			]
		},
		{
			titulo: 'Inventarios',
			badge: 0,
			imagen: 'assets/icon/inventarios.png',
			registros: [
				{
					titulo: 'Actividades presupuestos de obra',
					badge: 0,
					id: 643,
					id_modulo: 11569,
					id_detalle: 99981
				},
				{
					titulo: 'Presupuestos de consumo',
					badge: 0,
					id: 642,
					id_modulo: 11569,
					id_detalle: 99981
				},
				{
					titulo: 'Presupuestos de obra',
					badge: 0,
					id: 641,
					id_modulo: 11569,
					id_detalle: 99981
				}
			]
		}
	],
	//--- Parametros Comercial

	//--- Parametros Autogestion
	//--- Parametros Financiero
	menu: [
		{
			estado: true,
			icono: '',
			icon: 'icon-file-document',
			page: 'InventariosPrincipalPage',
			title: 'Inventarios'
		},
		{
			estado: true,
			icono: '',
			icon: 'icon-file-document',
			page: 'ActividadesListaPage',
			title: 'Mantenimiento'
		},
		{
			estado: true,
			icono: '',
			icon: 'icon-file-document',
			page: 'TableauPage',
			title: 'Tableau'
		}
		/*
		{
			estado: true,
			icono:
				'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAG+AAABvgEXVoYUAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAkxQTFRF////qv+qmeazldWqjuOqlN6tkd2qkN2rkN6qj+Cpj96rkd+pj9+rkd6pkOCqkN+rkd6pj9+qkN+qkN+qj9+qkOCqkN+qkN+qkN+qkN+qkN+qkN+qkN+qkN+qMkpeOFNiPVRnPlVnTHNzVYJ6WYd9XpCBZnmHZpyIa3yLcKyQca2RfL+ahMyghNv/iZmjidOki93/jJqpj97/kJ6skN+qkZ6pl96wl+H/mN6xoKy5oNy4puX/rLm+rLm/rOHBrOf/ruf/r+j/sbrGsuLFtOn/tur/uMDHuOXKuuv/vtjSv9fTwe3/xc3Wxe7/yvD/zfD/ztXg0eDg0/L/1d/f1efi1+fj2PT/2d7m3+Pp4Pb/4+bs4+fs5Ofr5Pf/5unu5+ru5+rv6Ovw6Pj/6fn/6u3x6+7y7Ovu7O7y7PPy7PTy7Pn/7e/z7fTz7vDz79rZ7/H08MG78N/f8PH18PL18Pv/8fHz8fP18fP28fv/8vP38vT287638/T38/z/9PX49fb49ff59vf595qM9/b39/j5+Pn6+P3/+ZeH+fb3+fn7+fr7+uLg+vHw+vr7+vv8+8W8++jm/IBr/P7//X1o/ZuK/f39/f7+/f7//v7+/v7//v///3Jb/3pj/31o/35o/35p/4Bq/4Fs/4Nu/4Vx/457/5OB/5yL/56O/6CQ/6GR/6KS/7er/7+0/8a8/8i//8rB/83E/87F/8/H/9HJ/9nT/+bi/+fj/+nl/+vo/+zp/+7r/+/s//Lv//Xz//z7//z8//38//39//7+////kAOCugAAAB50Uk5TAAMKDBsfPExOYmRocHSKjqSwtr7Ay9XY5ery+Pn+bfME6gAAA1RJREFUWMOtl/lDEkEUgJe8UMADRPA+8ta08mlqamWlHXZqZaUpFdlKZSd00kmlpZWF2X1q2WH3XWbOP9aIC7I7uzCI34/DvI/dmbdv3jCMBAFyZXikRhcXp9NEhivlAYxPBCrU8cAjXq0IpI2eodImggiJWtUMinBZmB4k0YfJvMWHxIBHYkI8hgdFg1eig6Tjg2OBgthgqfjQBKAiIVR89SKAmgiRtZRFgQ9EkQbn/+elAxTOLARIzwPIygXIzRIMTjwD8f5Od04yQH5SPkByDkBaBkBGmmBwAsE6BHPrV9JQkcKy9Un1LJtSybKZBSxbkMmyla7BioYSbiV5exHk3L8qVJOKUFNSE0KpNQhlFyNUnI14g1XO3XTPB1f+1KK6IgtqnNOILEV1qKusGqHqsi7eYK0ro9zyFyYFXnEJwJXVspipCWKcexkGUxNAGPf966cq0E/UBxWICb4//+tVACqHQCsm+PFk2PsTgNZR/xL5gu42o9F4QDKeJ0gcr5MK4Ass5qvd129cw1MXzeXRSwpAgQVqgcBsffbu6YlOPHXHZh53RQRqXP/jCcHne8NtnTS7gKt9ACMHQoD+IIdg/iyO7ZICkDNKoeBCa0tLi8GOpx5t5+iVFiiZcKHAh0TChDOREoLRN6M0gkhGIy4YfTk4RiPQMDqhoO+01Wo9PzQ4QvUKOiZOKDjeZjYfOXcLx69ZxuO2mCCOFJitQ79fXbqMp65eykNCoCMFr+8P7qJMJPwKGlLw7+2II5FWLeQ47GkRiW08aezo6DD04KnrV3Kc8rSNRCL1n8XY6BNJKZWJP6nyQEl+TBzvH1Blopz8nFGf3W7v//DwC+XnTBSUM60Gg2H/o68ItTfzQOIFhShpZuunsY83r+CpC2bzkCppwqJqtg68eLyPLpEcRVVY1s3WXwPfHInUvJHjoseyLjxYLIdsNtvOLjx1yTyOds8Hi+Bo69lrMpkO3hF7+mP4F9Ni4mijPlxbt2E2lROHK/XxvmU8fm05ebzTNhhbx+PdBCGiLY5HAY6fFESLN1meBOPxLgGvyXK1eaUbdu+RZN0KzPJSsTbPrdGkJVSq1aVtt6e/2fa73ff/wjENVx7/L13+X/um4eLp/9V3Gi7fvl3//wNz/M/J2H1BvAAAAABJRU5ErkJggg==',
			icon: 'icon-file-document',
			page: 'ConsultasFlexPage',
			title: 'Consultas Flex'
		},
		{
			estado: true,
			icono:
				'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAC/AAAAvwBpCekMAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAd8SURBVHja5ZtrUFNHFMdT7ThTx37RdqxVa50KKsGqgFOpig8eKhKSQCJF8YESilRbh4rKoMUHD6FTUBDUmYoiwlBQEUEeKY+gVEABeYhilWHsVJSH2ocfZNpyupsAJmQ3uRdIuMQP/4HJvefuOb9779nds3t5AMAzpPhS/3E2rjJXG5HPThuhb5S1UHbeRiwrthH5NtmIZH+phP5HvymPoXOU5yIbbGto/wxyUVsXv8nWYpkfCi4X6RUSDFDYNhdfC1+T0wCsBb5jFwhlu9Ddu4Wc7h5E0DR142vjNnBbnAEglUpHWwt9fZCDjw0QNE2PcZu47WEFYCWWuViLZI1GDFxDuG3sg9EBzHfbOg05oBiuwAlSYJ+MAsBa7LsYNdjOoeB71Y59MygA1FVtQQ11cTD4XnVhH4ccAE426MLRHA5cU8hXpgmSWfAi2eURE/xrXWYCgcljr33nxb5g6R8AFt8EckLYF+wT6UkYFICed17rwub7gmFiRhynND0QQRD5kCBsGRCAnmxPTHifhB/gHICpIehpsBeTIHTp6h109fPUro4fcRjMMxK0ZJYaA2ZJEVqakXSEfH56AvF8LPP042SblOjX1z2LFa5S0NdgvmQ1DUI7bZxAfvT1DHIKb1RB5x9/aikrUAgNEp6WbviaEc9vfdJKPB/rXk050eZKyAbi+amuE5QAdEBQMAKAh5a6gl8o8YPW9k6ic9lbLInOFe1yYA2gIieVaFOUGEk8v9p9NFjYrdQJgTRs1p7Y6Bnbb9odTnQMS+ExnuhcQehm1gCKTkcSbWrL5FQb4YqFfQBIEHBs/btGzcSnmtXp7F+jE9PJwTxthTrJW0TH8mKDWQO4GrWDaNPe2Qk1klFEm+BVZhoA+iAI1SCgGIkA8BybyZS2sLya6FhVYRY1mPxzcawBZAVJqE+afO0EvXlAB4TH6vWEPgCqYobu4G0l26jvv/xECDWYwqx09gC+sqMCyPS2pOYBvloeoEHAsWoB6Knk6ATgvSeC6hS+Y7RgrpcWswaQ482ntnVpt4hqJ7L/jAhACWFFLwSfWxoAcL2NSRkr5kw61alsn3lUp2pq61gDkHtOoraVH7+fakfKAwQI3b01xp5Rn7KAqXeCUVxRQ3Wq6Iv3qU792vKINYDrkrHUtqqK6PkmzXW8TgC9EKxEW7f3Aeip3up//zueER1q6+iAWkpmrke9TtuzF6wB1KIepf35c6Ld07Y25XHi0+Y+ipoHNCGI2pQAlHV7BqXrjXvD4c7DFqJK5dlQ4j6OqIJNM+BucwtR9XfvQank3T4psI3b2D7dLC+DOw+aibqybqrGueoSr1ysFwAW30H6EU+1aKH/8ReGRUJdXR1R8fkZULLybaISN1vAbYpdeX0t5HlOJdoVO78DJfU11DYvBK2itum9zYsRgDmO0u94ylUYBgDmBuyFvKoKojPS/DQ4tdGM6MzOYAnk3SEHcqmxGs4f8SLaZexxgNTGKqJdScNtSEiLINrlrZ8GqwMCmD0B9u7ZPNVSlH4AuPCw6MoZkFdV9jlSi7RPcVU5HZ17OgSSPaZoOHNguy3MS4mHyKZKpdPqQfzcUANh9ysgrEEBl3Yu0rDL8psPEdX5cBgdz+kHrww9NT+g6x1Cx5KjNkLJqjF9dvmeUyC68Bw4fRvICMDs5cImnnI9jiEAHOi0iwkgyE0Br4J0sMr6UWNOPiUtBhyO7gCPsA2w4FSQCkxqgtJZHEz8vZtw9u4tiEN/8W/qiik4DaeS9sOx3JNwqOmGxrGjTSq7E8gutJ/d92UX4GTyATieGaOEiX9jCmDWMkE7T7VQyRwAW/UCMKaYA3B5yetZpX0zASxd8w+vZ4n6jQRgvsQZ3mgAM+2cu4f8FZiUEgsfx0bDxPSR8AoIuoY8CdqsDwVbl4MwKzhKBQB1g1wFgLrB56y7Qb0APFUAZgepAHyaHMthAKIWVgMhJgA+TD4G02Oi4YOf4jgPwMLerZLxUHigSZDLACzt3TMZT4ZMEoCDNJTxdNj0ADiDpavnRMYFEVMDgBJgO6uSmKkB4Du4p7IqipoagDlO66xZlcVNCcDs5YKXrBdGTAkArgSxXhozFQAzl7r8a77M8z1Wi6Nu/vsgpbQMih81s9b1lmZo7HhqVGVeKwOndT6U5CdJYr08TtsQwWXll/5CKoG94kulY1hvkJCXVZoEAL6j9OCAtsiIvtwLyfJCKHl4n7WuPbgPdb//ZlRdLFZovQJo6vtiUJukrATeOIEwLDVxSzPtXP6zdJJ8PuhtclaCzSMQgjPwnTz8h2yj5EiDYOkoSRzyrbIjBYKFvbjaYJuluQ4BJb0n/bu8Id8ur4KwhpN3nknwQ/LBBLcgOOt95w3yyQwXIOCuTle2N/hHU8MJAQ9yaP28UT+bMzYE5dieMLwd1g8njQEBT2nxrI5pojP6p7MIQrchIOBKDi5mqM/nOfvx9Nw1XodmLRX8Pdisjqu3uIDZW8MbEV+Pq8vcRTp5juPa/Xx7txy8J2fWctc2vDMDb07AAeIlarxKixcq8VodXq7CKzZ40QLX7Q3t3/+GIj8bmCHe+wAAAABJRU5ErkJggg==',
			icon: 'icon-cube-outline',
			page: 'InventariosPage',
			title: 'Inventarios'
		},
		{
			estado: true,
			icono:
				'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB7QAAAe0BDNry2gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAovSURBVHja1Zt7VBNXHsezj+4f3Z7dra+1aNtzqq26bqurkEFk67tWVLSCWx+oETSCRkziY8/KWQ+KVtEW32B1tVVReSyK+KSigQCxKOKLYBJf5AGuWEhA5Ug95rf3DgEyJDOZmUyyLud8DwcOZOb7mXt/93d/vzsiABD5Uovr6t5SmqzhSqNVrjDaNqPv6ej7JSQdUpPCZHuKZEAqRDqmMDWmKMy2lXKTdSr+X1/fn08+VGH+qRcyE4t0FukFEvDUC8dnxK4yPQl4rQFIa2vflJtsK9DNXkWye2GaTnalyXZFYW6UJWrhN68NgOkAv5KbbQvkpsYaH5h2L7OtWmG0zksE+OX/FIDS2DRJYbJq/WbcRVYtjhV+B7D8gfV9R9DidMPL7j4BaaEWJMc1MOeICmbuOwdj12fDhG2nYcq+Aph95josNdRxBoFGXzGOO34BsNzYEIouWsflBuOu3Ieo7y5AZHImRG7K6BD6WbwolSIiLg0+23QcoksMXEE8UlY3hfgUABpu0ehCLWxvSlZpgaiDBVTTTopA6gzAWeO35ELsDTMXCC1oaZUKDgAHOnJ95vBEYjUGmP51Nq15EsBGZgBYw+L3gUR1h+toSJOWwxuCAHCYz+VyAwsLbroOd54AyGkRmwbTcsq4QshBBn7hNQA+T96Tca4ASCEI4eeuc10uN3kFwDHnWV9QdtvicdjzBoC1dC9EaAwcVwhrNC8AjmjfwuViUd9fYG2eFwA8HdZnwYyqWi4QflZarKM4AXCs89yWurJ7nMzzBYAVnHuFK4SGlSZbH9YA+CQ5sw/84DcAeBQEa/QwQ8sFQmMeKwCt6S3XDK+OVdQXCoB48R4IVldxh2C0jWUE0Lrkcc/tpUVazua9AoBHwdFiEgBHCLewR1oAeFfHZ1OCc3t+AI7xB7DjdDsArC/ZQuiUKXbaz/Pb0s5JV/l/BGzKoQBgDcFse7z0bv3vXAA4ihm8tqWz0K7O3wDEa466AOAwEmJdADgqOf/3AFhCUFEAOGp4vMtYc9Iv+X8KbMyhBcACwquVD5/17ADQWsDkXZmR5LALgl8kpXsEEBizkx2A7acZAbRCqGGIBY0yZwBnvQGAqzyMT/qroxAYvhq6/3EyfCrZRAtgwPh/QLfuk+HDEUoIku72sAyqPQJggoCrSCQAXHv3snTtMREKnJIAXd6eQKprlzAImpoAYav2tgMYPDMZ+g5f1v43WB+NWkEPIA4lQkVVrAAwQLDjUrsINy2EKFDO3p/POAI++ERCMYj1zrsR0L3nVJff9x4wF8QMI4BIymRtnhGCufFzEdmxEQBA7OW7jNNg2oYjMGic0sVsZ/UJlkHQwl3Mm6ETZZwBuIOgNNniRLhdJVSZms2GaMKKPSSI9z6aBT16ToFuPcLhnQ9mkMYHz9rsOfity+Rl3h0EBGCLiOzVCQRAdsvs24LIkm8h+FyFVwCw/tYGwWj7t4hsVArYrFhUovNZSYzILPXafCcI10Rkl1bgjk3UmXLBARD7CwQz36bp2ppmkaNFLXjbKvLCDYjYnOU9ADTsicwSwc23CSdBz3zVu4tEF5i85zRvAMTaDAg+W+Ez80hNGMB9XzYwIystMBJNicl7zrgkS247Q3iuI+NEzo++NE6K0Oh1eApofN3FxRDwBUOKtDD6xGX4/FABTEQ7yPCdJ0GckA7EhmwgtuYBcbgIglFa7Wvj7bqsv4RHwHF/tLLbILxWKjWkYwBp/urnv24QCI1uM06EEgVJha8ZQarSkjtDLFmpHo7WN7tol6UBvtqwF5LWpsL6NTtAlrCdVPzGfSAzPHKr4ceKgThU2KqDKgjMKxMEwDCNTi5SmhvDvDEuf9gAU3bnw8CxiZQAF7XrJNB9PRs0DuD3/eDl2wNALepFqmDIBFC/BLcKUeynBMpuAVOh39/3oiGs8zIG6MLJYijf7bCkQAuBM7ZCv+Grof9fEygAJLvzaAHYZy0hAbxCagOgnqugBRCqPEAB0LZxChBL4S+HL/IF8GKkSvtWW0H0By7G4w11MG5NJvQLXU2adwcghgEAJG11BZCcRgvg0+XuAZDqOhH6RCcDUVjJFcBZp4qQVcHW/FLdYyAku9qN0wGQpjIAyD5NArD/oX8HgFOXaAGMWvEdPYC2GsKoZSC+dJvD8DfEtgOQVz8dwMb8kqpHII7a7mLeHYC41FP0AG7cApg4DeyfhUPDb/vCkx6fQHl5JS2AMSwAYPUKWQziCzfZALAP0+h7UcviZtsdJvOLb9eg+Z7i1jxnABcvAsyfDzBvHjkSsHTH8rwGQMaFICkE5Vd4iP76q659AaN1Hu2Tr6yFIdO/pjXPGcDhwy4ATIkpggAgS22DoyHofAXD+m9Y4QIgEeDXyOwDt0XPB/VAzN0pHIDkZBcAP81cIhyAwIVAFNGm1DVDy2vfdNscVZptC2kTnQoT/Hn8OmEALFvmACBpB9A8ZLwgALq/GwFDT2iY8v8FtN1hfAgZmTXRFjryKihLHy8AT5+2mu8EwN7lT1DyvMU7AF3C4ONtx5nmv3Z6FkN73BEMY5iCYdiWPO8A6PUdACQdALCu3dR7BaDvom88BD/DJJZHZOjPBcqrrRAi3cMfgErlBGA+BcCdrDO8AfQeGQ/BJXcYAOgKWZ8RWmJp6orMWmiXRO0jGBSTyg/AkSMdAOZTARjX7+AFoPcYBVr6rjM9/bqQ0qr3OR2Tk5utI3EXlbYE/rAegpKy22MCawBbttACeDInnhsANOc/XLzN06aohfhRH8rzoKRtPWNaXN0Aww8VQf9xa9kDUCicAERTADwXT2QNoPt7kfDxzlw2W15+ByWdzgl/7wnCyPybMHBmimcAz59Tn340FYC920Aobn7pEUAAsQgCT7KqGaZ4fVYYHzhGK8O3niCMvmyAsVnF8EVKDv1m6N69TgBiKACwyrX36DdD8n8BkXYeCHUViyevz+285PE+Lo8hKI3WHZ4gjLl6H4aV6GBMTinMP5DvCkCtpgKIcQVQdTzfBcDoxAwgDqB9f/Edtru9FDbmOb8woTTaktlAICvASLGVJjhoqQf9sxdgxwAyMjwCqN64G4qQ6YzGF7DOVA9RlWb0WawrPy2e5rzXr8zgl5SQ2f+wgeCsieh3JzJzWwOfA4A9ZgHF/CuUDe7dfhjGXr3Hp8JTxxTtBX1pCucJaIXIYIwJbiBgrTp6CpoXSkkAL51GwNOAIRC/i2/rW1dIt8779LU5udEWSXeqnAnC3FNqeCyLh58dI8DSfwR8mcmrtqd1l9769cVJfOrSUVJ7yAXCpIJrcHtVAlSERsD48+VcjdfgXR3bQOeXV2dxzoC20xEKs7WULYQRai2EqlkfdsJlrKu4mOG8n39tAFDfNrENlZsa/4nyhwIE4TkTBE+la1y9xQXMthqe0PLpq+nkIexyeENuaRq26EF9EoJQFqwxqJCpm0gWpGbcosZdWtyoxL063K7CHRvctMB1e1/f338Bdp2S6D5tgOYAAAAASUVORK5CYII=',
			icon: 'icon-library-books',
			page: 'AnaliticaPage',
			title: 'Analitica'
		},
		{
			estado: true,
			icono:
				'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAL8AAAC/AGkJ6QwAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAddQTFRF////AAAAAFVVVVVVMzNmOTlVM01mQFVqN0lbKENRNlFeJkBZOVVoOlprMEVTL05eL0xaLUhVPFVmLENRPFdoO1hpPFZoPVdpO1ZnLkZTO1dpO1hnMElYLkRTL0lWL0hVO1dnO1ZoLkZUOFJjPVdoLkZTMElXLERTLERTLEVTLUZVO1doLkZVLERRLUVTPFdoPFdnPFhoPFdoLUVTLERSPFdoK0JQPFdoLEVTKkNPPFdoLEVSPFdoKkJPKkFOK0JPO1ZnPFdoKkJOLERQKkJOKEBNPFdoPFdoKUBNJz9LKEBMJz9MKD9MPFdoJj1JJz5KJ05OJ09OKD9KKFxSKUFNKUFOKWpWKkFOKkJOKkJPKoVdKoZdK0RQK2BWLERMLERSLZhkLsdwLslxLstxLsxxL8NwL8xxMaZtMbFvMbJvMbNvM59uM6JuNJFsNoxsN1FRN31rN39rOG9oOHhrOXFqOlRlOlVlOlVmOlxnOmhpOmppO1VmO1ZnO2BpO9B0PFdnPFdoPFhoPFpoU3NwVHRgVHRkVNd7VXdhVndhWXtjad6AbJJsbpVtcJducKh8cZh7cuCCe6V0fKZ0jbp9mMiFmc2EmuyNm+yNnM2EqdyLqvGRvfSVv/eWQITduQAAAE50Uk5TAAEDAwUJCgwOExMUGx8lMTY5PExMTmJkaG5wdHp7goeKjo+WpKWlrK2trrCys7a2vsDLzM3V2Njg5eXq6uzv8PLy9PT19vj5+vz8/f3+YN+7zwAAApJJREFUWMOll/d/EjEYh6OnxVGsYikIDlC0alUoWlHEgVVwlFPEVVcduGe1rVtf995b+8fK3aU04S6T5xfG8X0+XC55kxchBr5gJJZIZfL5TCoRiwR9SInWcLIAFIVkuFU23RJNl8CDUjraIhE3Qllgkg0Zonx7D3DpaefG/d0gpNvPzgdyIEEuwMp3FEGKYof36MVBmrjHWBpdoECX2xAHJeKu+wdFGsYhUFQVFKln4c+BMjlyPuD5c+vKxUtsLlweomcUMX+db65VBZwZpAz1WW048//haZGgevYJtS7GnmUI30BVzD3qL4Tw+sfrd1BCQI9C1qkPUdAVQNQWpPUFabv+lfQFJatOhkFfAOGaINmMIFmr/wWu4NzLtyQfPzk8G6v2PhQEnuDR31Fvvj92QkEU4Qo+j7J48cAORVCMK/jJFHwZsA0xlOAKfjEFX03bkEApbYFtSKGMvsAyZFC+CUHNkG9OYA5saeoWavQ2M4gW24nHeENHsJ6YSHd1BGuIqQznNQTLicUEd06eUhbMJ5YzwP1je8vlg070aNnih0gwgygolqHfNCt2/ohz/RtT8MG+voksadhQIfLmc1b+33X7+jKyqGJDhcib+9798cz/fuNcn0WWdWw4TuRdvLfSr+sft1EbC+YmJ+8YxgUrqK0NM3S13+Qb6oK+6dTminkKtwWGumAxvb2PIzC8wm92TG44YMgaTuDXha4jDmE4bArZPJF3yBIb+mbyj3kjhwSCOaKDpsDQKT7qcg1rJ0gctjmGdZOkjvtMw2pDsuEYOeCZXyrf8ngZds9TabqGXYYNbWptX4Nh5wLlxpM07OqcqtH6Du/H8a1Lpug135Zhz8aVs9UaeLL9X7Rq7jTW7/4DocqUpeBpTkkAAAAASUVORK5CYII=',
			icon: 'icon-comment-text-outline',
			page: 'FormulariosPage',
			title: 'Formularios'
		}
		*/
	]
};

export enum enumAccionNotificacion {
	INSERT_UPDATE = 1,
	SINCRONIZAR = 2,
	SINCRONIZAR_RESTRICCION = 21,
	ALERTA = 3,
	MENSAJE = 4
}

export enum enumModuloSincronizar {
	TODOS = 0,
	CONTABILIDAD = 1,
	CUENTASxCOBRAR = 2,
	CUENTASxPAGAR = 3,
	COMPRAS = 4,
	VENTAS = 5,
	INVENTARIOS = 6,
	MANUFACTURA = 7,
	ACTIVOSFIJOS = 9,
	VEHICULOS = 10,
	PLANEACIONMPR = 11,
	POS = 12,
	CONTROLDECALIDAD = 13,
	CONTROLDEPROYECTOS = 14,
	NOMINABASICA = 20,
	CENTROSCOMERCIALES = 21,
	MANTENIMIENTOPREVENTIVO = 22,
	GESTIONHUMANA = 23,
	NOMINAHORARIOS = 24,
	SALUDOCUPACIONAL = 25,
	ESTRUCTURACION = 26
}
