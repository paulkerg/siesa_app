// Global state (used for theming)
//import { AppState } from './app.global';

// Directives
//import { SlidingDrawer } from '../components/sliding-drawer/sliding-drawer';
//import { Autosize } from '../components/autosize/autosize';

// Modules
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ChartsModule } from 'ng2-charts';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { SignaturePadModule } from 'angular2-signaturepad';

const config: SocketIoConfig = { 
    url: 'http://172.16.8.31:3001', 
    options: {} 
};

export const MODULES = [
    BrowserModule,
    HttpModule,
    ChartsModule,
    IonicImageViewerModule,
    SignaturePadModule,
    SocketIoModule.forRoot(config),
];

export const DIRECTIVES = [
  //SlidingDrawer,
  //Autosize,
];
