import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';

import { AppProviders } from './app.providers';
import { MODULES } from './app.imports';
import { SharedModule } from './shared.module';
import { APP_CONFIG } from './../config/app.config';

import { MyApp } from './app.component';

@NgModule({
	declarations: [
		MyApp,
	],
	imports: [
		MODULES,
		IonicModule.forRoot(MyApp, APP_CONFIG.module_default),
		SharedModule,
	],
	bootstrap: [IonicApp],
	entryComponents: [MyApp],
	providers: AppProviders.getProviders()
})
export class AppModule { }
