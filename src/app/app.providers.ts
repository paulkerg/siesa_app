import { MantenimientoServiceProvider } from './../providers/mantenimiento-service/mantenimiento-service';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { XHRBackend, RequestOptions, Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpProvider } from '../providers/http/interceptor-http';
import { ErrorHandler } from '@angular/core';
import { IonicErrorHandler } from 'ionic-angular';
import { MyErrorHandlerProvider } from '../providers/my-error-handler/my-error-handler';
import { ActionSheet } from '@ionic-native/action-sheet';

//--- Natives
import { AppVersion } from '@ionic-native/app-version';
//import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Badge } from '@ionic-native/badge';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { BatteryStatus } from '@ionic-native/battery-status';
import { BLE } from '@ionic-native/ble';
import { Calendar } from '@ionic-native/calendar';
import { Camera } from '@ionic-native/camera';
import { DatePicker } from '@ionic-native/date-picker';
import { Device } from '@ionic-native/device';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Dialogs } from '@ionic-native/dialogs';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { EmailComposer } from '@ionic-native/email-composer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { NFC, Ndef } from '@ionic-native/nfc';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { Keyboard } from '@ionic-native/keyboard';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//--- Mocks
import { AppVersionMock } from './../app_mocks/app.version.mock';
//import { BackgroundGeolocatioMock } from './../app_mocks/background.geolocation.mock';
import { BadgeMock } from './../app_mocks/badge.mock';
import { BarcodeScannerMock } from './../app_mocks/barcode.scanner.mock';
import { BatteryStatusMock } from './../app_mocks/battery.status.mock';
import { BLEMock } from './../app_mocks/ble.mock';
import { CalendarMock } from './../app_mocks/calendar.mock';
import { CameraMock } from './../app_mocks/camera.mock';
import { DeviceMock } from './../app_mocks/device.mock';
import { DialogsMock } from './../app_mocks/dialogs.mock';
import { DocumentViewerMock } from './../app_mocks/document.viewer.mock';
import { EmailComposerMock } from './../app_mocks/email.composer.mock';
import { GeolocationMock } from './../app_mocks/geolocation.mock';
import { FileMock } from './../app_mocks/file.mock';
import { FileOpenerMock } from './../app_mocks/file.opener';
import { FilePathMock } from '@ionic-native-mocks/file-path';
import { FileTransferMock } from '@ionic-native-mocks/file-transfer';
import { InAppBrowserMock } from './../app_mocks/in.app.browser.mock';
import { NFCMock } from './../app_mocks/nfc.mock';
//import { NetworkMock } from './../app_mocks/network.mock';
import { OpenNativeSettingsMock } from '../app_mocks/open.native.settings.mock';
import { SocialSharingMock } from './../app_mocks/social.sharing.mock';
import { SQLiteMock } from '../app_mocks/sqlite.mock';
import { StatusBarMock } from '../app_mocks/status.bar.mock';
import { SplashScreenMock } from '../app_mocks/splash.screen.mock';
import { ToastMock } from '../app_mocks/toast.mock';
import { FingerprintAIOMock } from '@ionic-native-mocks/fingerprint-aio';
import { SpeechRecognitionMock } from '../app_mocks/speech.recognition';
import { KeyboardMock } from '../app_mocks/keyboard.mock';
import { GoogleAnalyticsMock } from '../app_mocks/google.analytics.mock';

//--- Mis providers
import { ConsultasFlexProvider } from '../providers/consultas-flex/consultas-flex';
import { SincronizacionProvider } from '../providers/sincronizacion/sincronizacion';
import { AuthProvider } from '../providers/auth/auth';
import { SqliteProvider } from '../providers/sqlite/sqlite';
import { ManejoArchivosProvider } from '../providers/manejo-archivos/manejo-archivos';
import { AlertasProvider } from '../providers/alertas/alertas';
import { AprobacionesProvider } from '../providers/aprobaciones/aprobaciones';
import { EmojiProvider } from '../providers/emoji/emoji';
import { ConfiguracionesProvider } from '../providers/configuraciones/configuraciones';
import { SocketServiceProvider } from '../providers/socket-service/socket-service';
import { SignalrServiceProvider } from '../providers/signalr-service/signalr-service';
import { global } from './global';

import { UtilesProvider } from '../providers/utiles/utiles';
import { PedidosProvider } from '../providers/pedidos/pedidos';
import { ClienteServiceProvider } from '../providers/clientes/cliente-service';
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
import { NativeAudio } from '@ionic-native/native-audio';
import { TablasSincronizacionServiceProvider } from '../providers/tablas-sincronizacion-service/tablas-sincronizacion-service';
import { ParametrosProvider } from '../providers/parametros/parametros';
import { ProvPedidosProvider } from '../providers/comercial/prov-pedidos/prov-pedidos';
import { ProvRecaudosProvider } from '../providers/comercial/prov-recaudos/prov-recaudos';
import { ProvNovedadesProvider } from '../providers/comercial/prov-novedades/prov-novedades';
import { ProvFacturasProvider } from '../providers/comercial/prov-facturas/prov-facturas';
import { AnalyticsProvider } from '../providers/analytics/analytics';
import { ProvClientesProvider } from '../providers/prov-clientes/prov-clientes';

//-- Mantenimiento Providers

const ERROR_HANDLER = global.production ? MyErrorHandlerProvider : IonicErrorHandler;

export class AppProviders {
	public static getProviders() {
		let providers;
		if (document.URL.includes('https://') || document.URL.includes('http://')) {
			// Provider para cuando se trabaja con navegadores
			providers = [
				Diagnostic,
				ActionSheet,
				DatePicker,
				NativeGeocoder,
				//--- Mis providers
				AuthProvider,
				AlertasProvider,
				ProvPedidosProvider,
				ProvRecaudosProvider,
				ProvNovedadesProvider,
				ProvFacturasProvider,
				PedidosProvider,
				ProvClientesProvider,
				ClienteServiceProvider,
				AprobacionesProvider,
				ParametrosProvider,
				SincronizacionProvider,
				SqliteProvider,
				ManejoArchivosProvider,
				NotificacionesProvider,
				HttpClient,NativeAudio,
				{
					provide: Http,
					useFactory: (backend: XHRBackend, defaultOptions: RequestOptions) => {
						return new HttpProvider(backend, defaultOptions);
					},
					deps: [XHRBackend, RequestOptions]
				},
				ConsultasFlexProvider,
				EmojiProvider,
				ConfiguracionesProvider,
				SocketServiceProvider,
				SignalrServiceProvider,
				UtilesProvider,
				AnalyticsProvider,
				MantenimientoServiceProvider,
				TablasSincronizacionServiceProvider,
				//--- Models
				{ provide: ErrorHandler, useClass: ERROR_HANDLER }
			];
			if (document.defaultView['chrome']) {
				providers.push({ provide: AppVersion, useClass: AppVersionMock });
				//providers.push({ provide: BackgroundGeolocation, useClass: BackgroundGeolocatioMock });
				providers.push({ provide: Badge, useClass: BadgeMock });
				providers.push({ provide: BarcodeScanner, useClass: BarcodeScannerMock });
				providers.push({ provide: BatteryStatus, useClass: BatteryStatusMock });
				providers.push({ provide: BLE, useClass: BLEMock });
				providers.push({ provide: Calendar, useClass: CalendarMock });
				providers.push({ provide: Camera, useClass: CameraMock });
				providers.push({ provide: Device, useClass: DeviceMock });
				providers.push({ provide: Dialogs, useClass: DialogsMock });
				providers.push({ provide: DocumentViewer, useClass: DocumentViewerMock });
				providers.push({ provide: EmailComposer, useClass: EmailComposerMock });
				providers.push({ provide: File, useClass: FileMock });
				providers.push({ provide: FileOpener, useClass: FileOpenerMock });
				providers.push({ provide: FilePath, useClass: FilePathMock });
				providers.push({ provide: FileTransfer, useClass: FileTransferMock });
				providers.push({ provide: Geolocation, useClass: GeolocationMock });
				providers.push({ provide: InAppBrowser, useClass: InAppBrowserMock });
				providers.push({ provide: NFC, useClass: NFCMock });
				providers.push(Ndef);
				//providers.push({ provide: Network, useClass: NetworkMock });
				providers.push(Network);
				providers.push({ provide: OpenNativeSettings, useClass: OpenNativeSettingsMock });
				providers.push({ provide: SocialSharing, useClass: SocialSharingMock });
				providers.push({ provide: SQLite, useClass: SQLiteMock });
				providers.push({ provide: StatusBar, useClass: StatusBarMock });
				providers.push({ provide: SplashScreen, useClass: SplashScreenMock });
				providers.push({ provide: Toast, useClass: ToastMock });
				providers.push({ provide: FingerprintAIO, useClass: FingerprintAIOMock });
				providers.push({ provide: SpeechRecognition, useClass: SpeechRecognitionMock });
				providers.push({ provide: Keyboard, useClass: KeyboardMock });
				providers.push({ provide: GoogleAnalytics, useClass: GoogleAnalyticsMock });
			} else {
				providers.push(AppVersion);
				//providers.push(BackgroundGeolocation);
				providers.push(Badge);
				providers.push(BarcodeScanner);
				providers.push(BatteryStatus);
				providers.push(BLE);
				providers.push(Calendar);
				providers.push(Camera);
				providers.push(Device);
				providers.push(Dialogs);
				providers.push(DocumentViewer);
				providers.push(EmailComposer);
				providers.push(File);
				providers.push(FileOpener);
				providers.push(FilePath);
				providers.push(FileTransfer);
				providers.push(Geolocation);
				providers.push(InAppBrowser);
				providers.push(NFC);
				providers.push(Ndef);
				providers.push(Network);
				providers.push(OpenNativeSettings);
				providers.push(SocialSharing);
				providers.push(SQLite);
				providers.push(StatusBar);
				providers.push(SplashScreen);
				providers.push(Toast);
				providers.push(FingerprintAIO);
				providers.push(SpeechRecognition);
				providers.push(Keyboard);
				providers.push(GoogleAnalytics);
			}
		} else {
			// Provider para cuando se trabaja con navegadores
			providers = [
				//--- Natives
				StatusBar,
				SplashScreen,
				AppVersion,
				//BackgroundGeolocation,
				Badge,
				BarcodeScanner,
				BatteryStatus,
				BLE,
				Calendar,
				Camera,
				Device,
				Diagnostic,
				ActionSheet,
				DatePicker,
				Dialogs,
				DocumentViewer,
				EmailComposer,
				File,
				FileOpener,
				FilePath,
				FileTransfer,
				Geolocation,
				InAppBrowser,
				NFC,
				Ndef,
				NativeGeocoder,
				Network,
				OpenNativeSettings,
				SocialSharing,
				SQLite,
				SqliteProvider,
				ManejoArchivosProvider,
				Toast,
				FingerprintAIO,
				SpeechRecognition,
				Keyboard,
				GoogleAnalytics,
				NativeAudio,
				//--- Mis providers
				AuthProvider,
				AlertasProvider,
				AprobacionesProvider,
				SincronizacionProvider,
				HttpClient,
				{
					provide: Http,
					useFactory: (backend: XHRBackend, defaultOptions: RequestOptions) => {
						return new HttpProvider(backend, defaultOptions);
					},
					deps: [XHRBackend, RequestOptions]
				},
				ConsultasFlexProvider,
				EmojiProvider,
				ConfiguracionesProvider,
				ProvPedidosProvider,
				ProvRecaudosProvider,
				ProvNovedadesProvider,
				ProvFacturasProvider,
				PedidosProvider,
				ProvClientesProvider,
				ParametrosProvider,
				ClienteServiceProvider,
				SocketServiceProvider,
				SignalrServiceProvider,
				UtilesProvider,
				AnalyticsProvider,
				MantenimientoServiceProvider,
				TablasSincronizacionServiceProvider,
				NotificacionesProvider,
				//--- Models
				{ provide: ErrorHandler, useClass: ERROR_HANDLER }
			];
		}

		return providers;
	}
}
