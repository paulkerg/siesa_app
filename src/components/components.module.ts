import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../pipes/pipes.module';
import { CompListaAcordionComponent } from './comp-lista-acordion/comp-lista-acordion';
import { CompListaCompletaComponent } from './comp-lista-completa/comp-lista-completa';
import { CompGraficasContenedorComponent } from './comp-graficas-contenedor/comp-graficas-contenedor';
import { CompGraficasVisualizarComponent } from './comp-graficas-visualizar/comp-graficas-visualizar';
import { CompBarraProgresoComponent } from './comp-barra-progreso/comp-barra-progreso';
import { CompInputComponent } from './comp-input/comp-input';
import { CompExploradorArchivosComponent } from './comp-explorador-archivos/comp-explorador-archivos';
//--- Mantenimiento
import { TareasComponent } from './mantenimiento/tareas/tareas';
import { AyudantesComponent } from './mantenimiento/ayudantes/ayudantes';
import { MotivoParoComponent } from './mantenimiento/motivo-paro/motivo-paro';
import { RepuestosComponent } from './mantenimiento/repuestos/repuestos';
import { CameraComponent } from "./mantenimiento/camera/camera";
import { NoRutinarioComponent } from './mantenimiento/no-rutinario/no-rutinario';
import { CompEncabezadoMenuPrincipalComponent } from './comp-encabezado-menu-principal/comp-encabezado-menu-principal';
import { CompComboSelectComponent } from './comp-combo-select/comp-combo-select';
import { CompInputFechasComponent } from './comp-input-fechas/comp-input-fechas';
import { CompCalendarioComponent } from './comp-calendario/comp-calendario';
import { CompInputFechasBotonComponent } from './comp-input-fechas-boton/comp-input-fechas-boton';
import { CompListaCheckBoxComponent } from './comp-lista-check-box/comp-lista-check-box';
import { CompBotonEncabezadoMenuComponent } from './comp-boton-encabezado-menu/comp-boton-encabezado-menu';
import { CompItemCompletoComponent } from './comp-item-completo/comp-item-completo';
import { CompListaComponent } from './comp-lista/comp-lista';
import { CompBaldosasComponent } from './comp-baldosas/comp-baldosas';
import { CompItemEncabezadoComponent } from './comp-item-encabezado/comp-item-encabezado';
import { CompTabComponent } from './comp-tab/comp-tab';
import { CompListaGenericaComponent } from './comp-lista-generica/comp-lista-generica';
import { CompBuscarComponent } from './comp-buscar/comp-buscar';
import { CompSubheaderComponent } from './comp-subheader/comp-subheader';
import { CompActionsheetComponent } from './comp-actionsheet/comp-actionsheet';
import { CompBaldosasAutoGestionComponent } from './comp-baldosas-auto-gestion/comp-baldosas-auto-gestion';

/*import { CmptegraficaComponent } from './cmptegrafica/cmptegrafica';
import { CmptegraficaContenedorComponent } from './cmptegrafica-contenedor/cmptegrafica-contenedor';
import { EmojiPickerComponent } from './emoji-picker/emoji-picker';
import { CmptearchivosComponent } from './cmptearchivos/cmptearchivos';
*/
@NgModule({
  declarations: [
    //--- Generales
    CompListaAcordionComponent,
    CompListaCompletaComponent,
    CompGraficasContenedorComponent,
    CompGraficasVisualizarComponent,
    CompBarraProgresoComponent,
    CompInputComponent,
    CompExploradorArchivosComponent,
    CompBuscarComponent,
    //--- Mantenimiento
    TareasComponent,
    AyudantesComponent,
    MotivoParoComponent,
    RepuestosComponent,
    CameraComponent,
    NoRutinarioComponent,
    CompListaGenericaComponent,
    CompEncabezadoMenuPrincipalComponent,
    CompComboSelectComponent,
    CompInputFechasComponent,
    CompCalendarioComponent,
    CompInputFechasBotonComponent,
    CompListaCheckBoxComponent,
    CompBotonEncabezadoMenuComponent,
    CompItemCompletoComponent,
    CompListaComponent,
    CompBaldosasComponent,
    CompItemEncabezadoComponent,
    CompTabComponent,
    CompSubheaderComponent,
    CompActionsheetComponent,
    CompBaldosasAutoGestionComponent,
        /*
        CmptegraficaComponent,
        CmptegraficaContenedorComponent,
        EmojiPickerComponent,
        CmptearchivosComponent,
        */
  ],
  imports: [
    PipesModule,
    IonicModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    //--- Generales
    CompListaAcordionComponent,
    CompListaCompletaComponent,
    CompGraficasContenedorComponent,
    CompGraficasVisualizarComponent,
    CompBarraProgresoComponent,
    CompInputComponent,
    CompExploradorArchivosComponent,
    CompBuscarComponent,
    //--- Mantenimiento
    TareasComponent,
    AyudantesComponent,
    MotivoParoComponent,
    RepuestosComponent,
    CameraComponent,
    NoRutinarioComponent,
    CompListaGenericaComponent,
    CompEncabezadoMenuPrincipalComponent,
    CompComboSelectComponent,
    CompInputFechasComponent,
    CompCalendarioComponent,
    CompInputFechasBotonComponent,
    CompListaCheckBoxComponent,
    CompBotonEncabezadoMenuComponent,
    CompItemCompletoComponent,
    CompListaComponent,
    CompBaldosasComponent,
    CompItemEncabezadoComponent,
    CompTabComponent,
    CompSubheaderComponent,
    CompActionsheetComponent,
    CompBaldosasAutoGestionComponent,
    /*
      CmptegraficaComponent,
      CmptegraficaContenedorComponent,
      EmojiPickerComponent,
      CmptearchivosComponent,
      */
  ]
})
export class ComponentsModule { }
