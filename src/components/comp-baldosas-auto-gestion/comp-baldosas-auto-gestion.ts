import { Component, Input, ViewChild, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Content } from 'ionic-angular';

@Component({
  selector: 'comp-baldosas-auto-gestion',
  templateUrl: 'comp-baldosas-auto-gestion.html'
})
export class CompBaldosasAutoGestionComponent {

  @Input() datos: any;
	@Input() eventos: any;
	@Input() columnas: any;
	@ViewChild(Content) contenedor: Content;
	@Output('eventoClick') eventoClick: EventEmitter<any> = new EventEmitter();
	columnasBladosas: string = 'columnas-azul';
	constructor() {}
	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges) {
	}

	ngAfterViewInit() {}

	onEvent(event: string, item: any, e: any) {
		if (e) {
			e.stopPropagation();
		}
		this.eventoClick.emit({
			evento: event,
			item: item,
			e: e
		});
	}

}
