import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'comp-combo-select',
	templateUrl: 'comp-combo-select.html'
})
export class CompComboSelectComponent {
	@Input('pCmptTitulo') pCmptTitulo: string = '';
	@Input('pCmptOrdenarPor') pCmptOrdenarPor: string = '';
	@Input('pCmptModelo') pCmptModelo: any;
	@Input('pCmptMultiseleccion') pCmptMultiseleccion: boolean = false;
	@Input('pCmptRegistros') pCmptRegistros: any = [];
	// popover
	// action-sheet
	@Input('pCmptInterface') pCmptInterface: string = '';

	/**
  * @{ eventoClick } class utility methods
  * for use with
  */
	/**
   * Convert byte array to string
   * @param e {any[]}
   * @returns {event}
   */
	@Output('eventoClick') eventoClick: EventEmitter<any> = new EventEmitter();

	registros: any = [];
	vInterface: any = {
		normal: 'popover',
		accion: 'action-sheet',
		popup: ''
	};

	constructor() {}

	ngOnInit() {
		this.inicializar();
	}

	ngOnChanges() {
		this.inicializar();
	}

	inicializar() {
		this.registros = this.pCmptRegistros;
		console.log(this.pCmptRegistros);
	}

	/**
  * @{ CompComboSelectComponent } class utility methods
  * for use with
  */
	/**
   * Convert byte array to string
   * @param e {any[]}
   * @returns {event}
   */
	clickBoton(e: any): void {
		this.eventoClick.emit(e);
	}
}
