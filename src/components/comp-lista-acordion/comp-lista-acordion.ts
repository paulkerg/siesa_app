import { Component, Input, ViewChild, ElementRef, Renderer } from '@angular/core';

/**
 * Generated class for the CompListaCompletaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'comp-lista-acordion',
  templateUrl: 'comp-lista-acordion.html'
})
export class CompListaAcordionComponent {
  /**
   * Define color del encabezado
   */
  @Input('pCompEncabezadoColor') pCompEncabezadoColor: string = "#69B3E7";//"linear-gradient(to left, #00bee4 0%, #0091d2 100%)";
  @Input('pCompTextoColor') pCompTextoColor: string = '#FFF';
  @Input('pCompContenedorColor') pCompContenedorColor: string = '#F9F9F9';
  @Input('pCompTitulo') pCompTitulo: string;
  @Input('pCompBlnMargen') pCompBlnMargen: boolean = true;
  @Input('pCompBlnExpandir') pCompBlnExpandir: boolean = false;
  @Input('pCompBlnContador') pCompBlnContador: boolean = false;
  @Input('pCompNumContador') pCompNumContador: number = 0;

  @ViewChild('accordionContent') elementView: ElementRef;
  /**
   * viewHeight: number;
   */
  viewHeight: number;

  constructor(public renderer: Renderer) { }

  ngAfterViewInit() {
    this.viewHeight = this.elementView.nativeElement.offsetHeight;

    if (!this.pCompBlnExpandir) {
      this.renderer.setElementStyle(this.elementView.nativeElement, 'height', 0 + 'px');
    }
  }

  toggleAccordion() {
    this.pCompBlnExpandir = !this.pCompBlnExpandir;
    const newHeight = this.pCompBlnExpandir ? '100%' : '0px';
    this.renderer.setElementStyle(this.elementView.nativeElement, 'height', newHeight);
  }
}
