import { Component, Input, Output, EventEmitter } from '@angular/core';
/**
 * @name CompListaCompletaComponent
 * @description
 * The essential purpose of badge numbers is to enable an application to inform its users that it has something for them — for example, unread messages — when the application isn’t running in the foreground.
 *
 * Requires Cordova plugin: cordova-plugin-badge. For more info, please see the [Badge plugin docs](https://github.com/katzer/cordova-plugin-badge).
 *
 * @usage
 * ```HTML
 * <comp-lista-completa [blnImagen]="true" (eventoClick)="accion($event)" [registros]="regLista"></comp-lista-completa>
 *
 * constructor() { }
 *
 * ...
 * @param pCompTextoColor color del texto
 * @param pCompContenedorColor Color del contenedor
 * @param pCompBlnBadge boolean para visualizar Badge
 * @param pCompBlnImagen boolean para visaualizar imagen a la izquierda
 * @param pCompBlnScrollEvento boolean para activar el scroll infinito
 * @param pCompRegistros arreglo con los registros {imagen:string, titulo:string, subTitulo:string, texto1:string, texto2:string, texto3:string}
 * ```
 */

@Component({
  selector: 'comp-lista-generica',
  templateUrl: 'comp-lista-generica.html'
})
export class CompListaGenericaComponent {
  @Input('pCompTextoColor') pCompTextoColor: string = '#FFF';
  @Input('pCompContenedorColor') pCompContenedorColor: string = '#F9F9F9';
  @Input('pCompBlnBadge') pCompBlnBadge: boolean = false;
  @Input('pCompBlnImagen') pCompBlnImagen: boolean = false;
  @Input('pCompBlnScrollEvento') pCompBlnScrollEvento: boolean = false;
  @Input('pCompRegistros') pCompRegistros: any[];

  @Output('eventoClick') eventoClick: EventEmitter<any> = new EventEmitter();
  @Output('eventoScroll') eventoScroll: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  // Eventos
  public onclikItem(item: any, idx: number): void {
    this.eventoClick.emit({
      indice: idx,
      item: item
    });
  }

  public onScroll(e: any): void {
    this.eventoScroll.emit(e);
  }


}
