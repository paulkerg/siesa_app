import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SincronizacionProvider } from '../../providers/sincronizacion/sincronizacion';
import { List } from 'linqts';
import { global } from '../../app/global';
import { SqliteProvider } from '../../providers/sqlite/sqlite';

@Component({
  selector: 'comp-graficas-contenedor',
  templateUrl: 'comp-graficas-contenedor.html'
})
export class CompGraficasContenedorComponent {
  @Input() pCmptChartContIdWbObjeto: string;
  @Input() pCmptChartContTipoGrafica: string;
  @Input() pCmptChartContTitulo: string;
  @Input() pCmptChartContIndLeyenda: boolean;
  @Input() pCmptChartContFiltro: any;

  @Output() pCmptChartEventClick: EventEmitter<any> = new EventEmitter();
  @Output() pCmptChartEventHovered: EventEmitter<any> = new EventEmitter();

  prvTitulo: string;
  prvChartData: number[] | any[];
  prvChartLabels: Array<any>;
  prvChartLegend: boolean;
  prvChartType: string;
  pvrArrPrivFiltro: Array<any>;
  prvChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  constructor(private sync: SincronizacionProvider, private dbSql: SqliteProvider) { }

  ngOnChanges() {
    this.cargaConsulta();
  }

  ngOnInit() {
    //this.cargaConsulta();
  }

  cargaConsulta() {
    if (this.pCmptChartContIdWbObjeto != undefined) {
      this.prvTitulo = this.pCmptChartContTitulo;

      let parametros = '|';
      let p1 = {
        'Parametros': parametros,
        'Consulta': this.pCmptChartContIdWbObjeto
      };
      this.sync.postParameters(global.api.consulta, p1)
        .then((r: any) => {
          if (r['length'] > 0) {
            console.log('cargaConsulta', r)
            let sql =
              "insert or replace into t005_consultas (f005_id_consulta,t005_resultado) values('" +
              this.pCmptChartContIdWbObjeto +
              "','" +
              JSON.stringify(r) +
              "')";
            this.dbSql.query(sql).then(() => {
              this.mueveData();
            });
          }
        })
        .catch((e: any) => {
          console.log(e)
        });
    }
  }

  mueveData() {
    let sql =
      "select t005_resultado from t005_consultas where f005_id_consulta = '" +
      this.pCmptChartContIdWbObjeto +
      "'";
    this.dbSql.query(sql)
      .then((res) => {
        if (res.length > 0) {
          let r = JSON.parse(res[0]['t005_resultado']);
          console.log('mueveData', r)

          //--- EL resultado se convierte en pivot
          var o = r.reduce((a, b) => {
            a[b.f_serie] = a[b.f_serie] || [];
            a[b.f_serie].push({ [b.f_label]: b.f_data });
            return a;
          }, {});
          var a = Object.keys(o).map(function (k) {
            return { label: k, data: Object.assign.apply({}, o[k]) };
          });
          //--- Se arman los labels
          let l = new List<any>(r).GroupBy((key) => key['f_label'], (element) => element['f_label']);
          //--- Se arma la data definitiva
          let objData = [];
          let labels = Object.keys(l);
          for (let k = 0; k < a.length; k++) {
            let d = [];
            for (let p = 0; p < labels.length; p++) {
              if (!a[k]['data'][labels[p]]) {
                d.push(0);
              } else {
                d.push(a[k]['data'][labels[p]]);
              }
            }
            objData.push({
              label: a[k]['label'],
              data: d
            });
          }
          //--- Se asignan los resultadoss
          this.prvChartType = global.pubEnumStrtipoGrafica[this.pCmptChartContTipoGrafica];

          this.pvrArrPrivFiltro = this.pCmptChartContFiltro;
          this.prvChartLegend = this.pCmptChartContIndLeyenda === true;
          if (this.prvChartType == global.pubGraficaTipoBarras) {
            this.prvChartData = objData;
          }
          if (this.prvChartType == global.pubGraficaTipoLineas) {
            this.prvChartData = objData;
          }
          if (this.prvChartType == global.pubGraficaTipoTorta) {
            this.prvChartData = objData[0]['data'];
          }
          if (this.prvChartType == global.pubGraficaTipoDonas) {
            this.prvChartData = objData[0]['data'];
          }
          this.prvChartLabels = labels;
        }
      })
      .catch((e: any) => {
        console.log(e);
      });

  }

  // events
  public chartClicked(e: any): void {
    console.log('PageChartClicked', e);
    this.pCmptChartEventClick.emit(e);
  }

  public chartHovered(e: any): void {
    console.log('PageChartHovered', e);
    this.pCmptChartEventHovered.emit(e);
  }

}
