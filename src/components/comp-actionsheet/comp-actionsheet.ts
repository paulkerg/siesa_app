import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'comp-actionsheet',
  templateUrl: 'comp-actionsheet.html'
})
export class CompActionsheetComponent {
  @Input('pCompRegistros') pCompRegistros: any[];
  text: string;
  sheet:any='false';
  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  constructor() {
    console.log('Hello CompActionsheetComponent Component... ',this.sheet);
    this.text = 'Hello World';
  }
  presentActionSheet(evento){
    console.log("this.sheet... ",this.sheet)
    this.sheet=evento;
  }

  // Eventos Buscar
  public onclikItem(e: any): void {
    this.eventoClick.emit({
      opt: 'texto', value: e.target.value
    });
  }

}
