import { Component, OnChanges, OnInit, Input, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { Content } from 'ionic-angular';

@Component({
	selector: 'comp-tab',
	templateUrl: 'comp-tab.html'
})
export class CompTabComponent implements OnChanges, OnInit {
	@Input() datos: any;
	@Input() eventos: any;
	@Input('pCmptModelo') pCmptModelo: any;
	@ViewChild(Content) contenedor: Content;
	@Output('eventoClick') eventoClick: EventEmitter<any> = new EventEmitter();
	@Output('eventoTabSeleccionado') eventoTabSeleccionado: EventEmitter<any> = new EventEmitter();
	seleccionado: any;

	constructor() {
	}
	ngOnInit() {
	}
	
	ngOnChanges(changes: SimpleChanges) {
		this.llenaDatos();
	}

	ngAfterViewInit() {
	}

	llenaDatos() {
		setTimeout(() => {
			this.seleccionado = this.datos[0]['titulo'];
		}, 1);
	}

	onclikItem(item: any) {
		this.seleccionado = item['titulo'];

		this.eventoClick.emit({
			seleccionado: item
		});
	}

}
