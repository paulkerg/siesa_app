import { Component, Input, Output, EventEmitter } from '@angular/core';
/**
 * @name CompListaCompletaComponent
 * @description
 * The essential purpose of badge numbers is to enable an application to inform its users that it has something for them — for example, unread messages — when the application isn’t running in the foreground.
 *
 * Requires Cordova plugin: cordova-plugin-badge. For more info, please see the [Badge plugin docs](https://github.com/katzer/cordova-plugin-badge).
 *
 * @usage
 * ```HTML
 * <comp-lista-completa [blnImagen]="true" (eventoClick)="accion($event)" [registros]="regLista"></comp-lista-completa>
 *
 * constructor() { }
 *
 * ...
 * @param pCompTextoColor color del texto
 * @param pCompContenedorColor Color del contenedor
 * @param pCompBlnBadge boolean para visualizar Badge
 * @param pCompBlnImagen boolean para visaualizar imagen a la izquierda
 * @param pCompRegistros arreglo con los registros {imagen:string, titulo:string, subTitulo:string, texto1:string, texto2:string, texto3:string}
 * ```
 */

@Component({
  selector: 'comp-lista-completa',
  templateUrl: 'comp-lista-completa.html'
})
export class CompListaCompletaComponent {
  @Input('pCompTextoColor') pCompTextoColor: string = '#FFF';
  @Input('pCompContenedorColor') pCompContenedorColor: string = '#F9F9F9';
  @Input('pCompBlnBadge') pCompBlnBadge: boolean = false;
  @Input('pCompBlnImagen') pCompBlnImagen: boolean = false;
  @Input('pCompRegistros') pCompRegistros: any[];

  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  // Eventos
  public onclikItem(e: any): void {
    this.eventoClick.emit(e);
  }


}
