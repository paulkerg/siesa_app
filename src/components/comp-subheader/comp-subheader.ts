import { Component, Input } from '@angular/core';

/**
 * Generated class for the CompSubheaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'comp-subheader',
  templateUrl: 'comp-subheader.html'
})
export class CompSubheaderComponent {
  @Input('pCompRegistros') pCompRegistros: any[];
  text: string;

  constructor() {
    console.log('Hello CompSubheaderComponent Component');
    this.text = 'Hello World';
  }

}
