import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MenuController, Slides } from 'ionic-angular';

export class CategoryModel {
  id: string;
  name: string;
}

@Component({
  selector: 'comp-encabezado-menu-principal',
  templateUrl: 'comp-encabezado-menu-principal.html'
})
export class CompEncabezadoMenuPrincipalComponent {
  @Input('pCmptTitulo') pCmptTitulo: string = "";
  @Input('pCmptBotonesDerecha') pCmptBotonesDerecha: any = []

  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  @ViewChild(Slides) slides: Slides;
  @ViewChild('subHeader') elementView: ElementRef;

  public selectedCategory: CategoryModel;
  public categories: Array<CategoryModel>;
  public showLeftButton: boolean;
  public showRightButton: boolean;

  prvBotonesDerecha: any = [];

  constructor(
    private menu: MenuController,
  ) {
    this.menu = menu;
    this.menu.enable(true, 'menuPrincipal');

    this.categories = [
      {
        id: "1", name: "Prueba"
      }, {
        id: "2", name: "Segunda"
      }, {
        id: "3", name: "Prueba tres"
      }, {
        id: "4", name: "Continuacion"
      }, {
        id: "5", name: "Documentos"
      }, {
        id: "6", name: "primeras"
      },
    ]
  }

  ngOnInit() {
    this.inicializar();
  }

  ngOnChanges() {
    this.inicializar();
  }

  inicializar() {
    this.prvBotonesDerecha = this.pCmptBotonesDerecha;
    this.initializeCategories();
  }

  openMenu() {
    this.menu.open('menuPrincipal');
  }

  // Eventos
  clickBoton(e: any): void {
    this.eventoClick.emit(e);
  }


  private initializeCategories(): void {
    // Select it by defaut
    this.selectedCategory = this.categories[0];
    // Check which arrows should be shown
    this.showLeftButton = false;
    this.showRightButton = this.categories.length > 3;
  }

  public filterData(categoryId: number): void {
    this.selectedCategory = this.categories[categoryId - 1];
  }

  // Method executed when the slides are changed
  public slideChanged(): void {
    let currentIndex = this.slides.getActiveIndex();
    this.showLeftButton = currentIndex !== 0;
    this.showRightButton = currentIndex !== Math.ceil(this.slides.length() / 3);
  }

  // Method that shows the next slide
  public slideNext(): void {
    this.slides.slideNext();
  }

  // Method that shows the previous slide
  public slidePrev(): void {
    this.slides.slidePrev();
  }

}
