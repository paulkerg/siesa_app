import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'comp-input-fechas-boton',
  templateUrl: 'comp-input-fechas-boton.html'
})
export class CompInputFechasBotonComponent {
  @Input('pCompLabel') pCompLabel: string;
  @Input('pCompFormatoFecha') pCompFormatoFecha: string;
  @Input('pCompFechaMinima') pCompFechaMinima: Date;
  @Input('pCompFechaMaxima') pCompFechaMaxima: Date;
  @Input('pCompModelo') pCompModelo: any;

  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  prvFechaModelo: Date = new Date();
  prvFormatoSel: any;
  prvTipoFormato: any = {
    ano: "YYYY",
    periodo: "YYYY/MM",
    completa: "YYYY/MM/DD",
    hora24: "HH:mm:ss",
    hora12: "hh:mm:ss a",
    timestamp: "YYYY-MM-DD HH:mm:ssTZD",
  }

  constructor() { }

  ngOnInit(): void {
    if (this.pCompModelo) {
      this.prvFechaModelo = this.pCompModelo;
    }
    this.prvFormatoSel = this.prvTipoFormato['completa'];
    if (this.prvTipoFormato) {
      this.prvFormatoSel = this.prvTipoFormato[this.pCompFormatoFecha];
    }
  }

  changeDate(fecha){
    this.pCompModelo = this.prvFechaModelo;
  }

  fnBtnBuscar() {
    this.pCompModelo = this.prvFechaModelo;
    this.eventoClick.emit(this.prvFechaModelo);
  }
}
