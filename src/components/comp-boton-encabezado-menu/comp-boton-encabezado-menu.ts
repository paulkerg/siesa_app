import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MenuController } from 'ionic-angular';

@Component({
  selector: 'comp-boton-encabezado-menu',
  templateUrl: 'comp-boton-encabezado-menu.html'
})
export class CompBotonEncabezadoMenuComponent {
  @Input('pCmptTitulo') pCmptTitulo: string = "";
  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  constructor(
    private menu: MenuController,
  ) {
    this.menu = menu;
    this.menu.enable(true, 'menuPrincipal');
  }

  openMenu() {
    this.menu.open('menuPrincipal');
  }
}
