import { Component, Input, EventEmitter, ElementRef, Renderer, Output } from '@angular/core';
import { ManejoArchivosProvider } from '../../providers/manejo-archivos/manejo-archivos';

@Component({
  selector: 'comp-explorador-archivos',
  templateUrl: 'comp-explorador-archivos.html'
})
export class CompExploradorArchivosComponent {
  @Input() pCmptArchivosCarpeta: string;
  @Input() pCmptArchivosOpciones: Array<any>;
  @Input() pCmptArchivosLegendBln: boolean;
  @Input() pCmptArchivosFiltro: string = "";

  @Output() pCmptArchivosEventClick: EventEmitter<any> = new EventEmitter();
  @Output() pCmptArchivosEventHovered: EventEmitter<any> = new EventEmitter();

  dirBase = 'mantenimiento';
  rutaCarpetas = "";
  dirs: any;
  dirPath = [];
  textoFiltro: string = "";

  constructor(
    public element: ElementRef,
    public renderer: Renderer,
    private mf: ManejoArchivosProvider
  ) {

  }

  ngOnInit() {
    this.inicializar();
  }

  ngOnChanges() {
    this.inicializar();
  }

  private inicializar() {
    this.dirBase = this.pCmptArchivosCarpeta;
    this.dirPath = [this.dirBase];
    this.textoFiltro = this.pCmptArchivosFiltro;
    this.goToDir(this.dirBase);
  }

  atras() {
    this.dirPath.splice(this.dirPath.length - 1, 1);
    this.rutaMem().then((ruta) => {
      this.goToDir(ruta);
    })
  }

  rutaMem(): Promise<any> {
    return new Promise((resolve, reject) => {
      let miRuta = "";
      for (let t = 0; t < this.dirPath.length; t++) {
        if (t > 0) {
          miRuta += "/";
        }
        miRuta += this.dirPath[t];
      }
      resolve(miRuta);
    });

  }

  public open(r) {
    if (r.isDirectory) {
      this.dirPath.push(r['name']);
      this.rutaMem().then((ruta) => {
        this.goToDir(ruta);
      })
    } else {
      this.rutaMem().then((ruta) => {
        this.mf.getFile(ruta, r['name'])
          .then((r2) => {
            console.log(JSON.stringify(r2))
          })
          .catch((e) => {
            console.log(JSON.stringify(e))
          });
      })
    }
  }

  public goToDir(directorio) {
    this.rutaCarpetas = directorio;
    this.mf.listDir(directorio)
      .then((lstDirArc: any) => {
        this.dirs = lstDirArc;
      })
      .catch((error) => {

      })
  }

}
