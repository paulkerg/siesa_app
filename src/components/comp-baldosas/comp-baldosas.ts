import { Component, Input, ViewChild, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Content } from 'ionic-angular';

/*
 * @name CompBaldosasComponent
 * @description
 * Componente creacion de baldosas 
 * @parametros
 * @param {datos} objeto [
							{
								id: '',
								titulo: '',
								badge: 0,
								imagen: 'assets/icon/comprar.png',
								registros: []
							}
						]
 * @returns {eventoClick} registro seleccionado.
 * @Modo de uso
 * <comp-baldosas [(datos)]="datosModelo" (eventoClick)="eventoNombre($event)"></comp-baldosas>
 */
@Component({
	selector: 'comp-baldosas',
	templateUrl: 'comp-baldosas.html'
})
export class CompBaldosasComponent implements OnChanges, OnInit {
	@Input() datos: any;
	@Input() eventos: any;
	@Input() columnas: any;
	@ViewChild(Content) contenedor: Content;
	@Output('eventoClick') eventoClick: EventEmitter<any> = new EventEmitter();
	columnasBladosas: string = 'columnas-2';
	constructor() {}
	ngOnInit() {
		this.setColumnas();
	}

	ngOnChanges(changes: SimpleChanges) {
		this.setColumnas();
	}

	ngAfterViewInit() {}

	onEvent(event: string, item: any, e: any) {
		if (e) {
			e.stopPropagation();
		}
		this.eventoClick.emit({
			evento: event,
			item: item,
			e: e
		});
	}

	setColumnas() {
		this.columnasBladosas = 'columnas-2';

		if (this.columnas) {
			this.columnasBladosas = 'columnas-' + this.columnas;
		}
	}
}
