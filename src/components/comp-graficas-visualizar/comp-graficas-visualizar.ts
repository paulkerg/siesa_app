import { Component, Input, Output, EventEmitter, ElementRef, Renderer, ViewChild } from '@angular/core';

@Component({
  selector: 'comp-graficas-visualizar',
  templateUrl: 'comp-graficas-visualizar.html'
})
export class CompGraficasVisualizarComponent {
  @ViewChild('barCanvas') barCanvas;

	@Input() pCmptChartFiltroAny: any;
	@Input() pCmptChartTituloStr: string;
	@Input() pCmptChartTipoGraficaStr: string;
	@Input() pCmptChartDataAny: number[] | any[];
	@Input() pCmptChartLabelsArr: Array<any>;
	@Input() pCmptChartOptionsAny: any;
	@Input() pCmptChartColorsArr: Array<any>;
	@Input() pCmptChartLegendBln: boolean;

	@Output() pCmptChartEventClick: EventEmitter<any> = new EventEmitter();
	@Output() pCmptChartEventHovered: EventEmitter<any> = new EventEmitter();

	vPrivTipoGrafica: string = '';
	vPrivblnFiltro: boolean = false;

	constructor(public element: ElementRef, public renderer: Renderer) { }

	ngOnInit() {
		/*
		console.log('ngOnInit');
		console.log('pCmptChartFiltroAny', this.pCmptChartFiltroAny);
		console.log('pCmptChartTituloStr', this.pCmptChartTituloStr);
		console.log('pCmptChartTipoGraficaStr', this.pCmptChartTipoGraficaStr);
		console.log('pCmptChartDataAny', this.pCmptChartDataAny);
		console.log('pCmptChartLabelsArr', this.pCmptChartLabelsArr);
		console.log('pCmptChartOptionsAny', this.pCmptChartOptionsAny);
		console.log('pCmptChartColorsArr', this.pCmptChartColorsArr);
		console.log('pCmptChartLegendBln', this.pCmptChartLegendBln);
		*/
	}

	ngOnChanges() {
		//console.log(this.pCmptChartDataAny)
		this.vPrivblnFiltro = false;
		if (this.pCmptChartFiltroAny != undefined && this.pCmptChartFiltroAny !='') {
			this.vPrivblnFiltro = true;
		}
		this.vPrivTipoGrafica = this.pCmptChartTipoGraficaStr;
	}

	// events
	public chartClicked(e: any): void {
		console.log('ComponentChartClicked', e);
		this.pCmptChartEventClick.emit(e);
	}

	public chartHovered(e: any): void {
		console.log('ComponentChartHovered', e);
		this.pCmptChartEventHovered.emit(e);
	}
}
