import {
  Component,
  Input,
  ViewChild,
  AfterViewInit,
  EventEmitter,
  Output,
  OnInit
} from "@angular/core";
import { Content, FabButton, ItemSliding } from "ionic-angular";
import { SqliteProvider } from "../../../providers/sqlite/sqlite";
import { global } from "../../../app/global";
import { UtilesProvider } from "../../../providers/utiles/utiles";

@Component({
  selector: "motivo-paro",
  templateUrl: "motivo-paro.html"
})
export class MotivoParoComponent implements AfterViewInit, OnInit {
  @Input()
  actividad: any;
  // @Input() events: any;
  // @ViewChild(content)
  // @ViewChild(Content) content: Content;
  @ViewChild(FabButton)
  fabButton: FabButton;
  @Output()
  actualizarMotivos: EventEmitter<any> = new EventEmitter();
  motivosParo: any[];
  ngOnInit(): void {
    this.cargarMotivosParo();
  }
  constructor(
    private content: Content,
    private db: SqliteProvider,
    private msg: UtilesProvider
  ) {
    this.validar();
  }
  validar() {
    if (this.actividad == null) {
      // console.log("La actividad es nula");
      this.actividad = global.actividadSel;
    }

    if (this.actividad.motivos == null) {
      this.actividad.motivos = [];
      // console.log('los motivos son nulos');
    }
    // console.log('###########Actividad para los motivos');
    // console.log(this.actividad.motivos);
    if (this.actividad.motivos.length == 0) {
      // console.log('El largo de los motivos es 0')
      this.actividad.motivos.push({
        c0836_rowid_motivo_paro: null,
        razon: "Motivo Paro 1",
        c0836_minutos_paro: "",
        c0836_horas_paro: "",
        c0836_dias_paro: ""
      });
    }
  }
  diabledActividad() {
    return (
      (this.actividad.c0822_fecha_ejecucion_ini == null ||
        this.actividad["c0846_ind_estado"] != 1 ||
        this.actividad.c0822_rowid_responsable !=
          global.usuario["f_tercero_rowid"]) &&
      this.actividad.habilitar_error
    );
  }
  actualizarTipoParo(index) {
    if (this.actividad.motivos[index].c0836_rowid_motivo_paro != null) {
      let c0836_rowid_motivo_paro = this.actividad.motivos[index]
        .c0836_rowid_motivo_paro;
      this.actividad.motivos[index].c0835_ind_tipo = this.motivosParo.filter(
        motivo => motivo.c0835_rowid == c0836_rowid_motivo_paro
      )[0]["c0835_ind_tipo"];
    }
  }
  ionViewWillEnter() {
    this.validar();
  }
  aumentar(tipo: string, motivo: Motivo, index: number) {
    // console.log("cambio algo: " + tipo);
    // console.log(motivo);
    switch (tipo) {
      case "c0836_horas_paro":
        // console.log("va a mostar c0836_horas_paro");
        if (motivo.c0836_minutos_paro) {
          if (Number(motivo.c0836_minutos_paro) == 60) {
            motivo.c0836_horas_paro = Number(motivo.c0836_horas_paro) + 1;
            motivo.c0836_minutos_paro = 0;
          }
        }
        break;
      case "c0836_dias_paro":
        if (motivo.c0836_dias_paro) {
          if (Number(motivo.c0836_horas_paro) == 24) {
            motivo.c0836_dias_paro = Number(motivo.c0836_dias_paro) + 1;
            motivo.c0836_horas_paro = 0;
          }
        }
        break;
    }
  }

  agregarMotivo() {
    if (this.diabledActividad()) return;
    // console.log('Entror  a agregar un motivo');
    let validar_tiempos =
      this.actividad.motivos.filter(
        motivo =>
          (motivo.c0836_dias_paro == "" ||
            motivo.c0836_dias_paro == 0 ||
            motivo.c0836_dias_paro < 0) &&
          (motivo.c0836_horas_paro == "" ||
            motivo.c0836_horas_paro == 0 ||
            motivo.c0836_horas_paro < 0) &&
          (motivo.c0836_minutos_paro == "" ||
            motivo.c0836_minutos_paro == 0 ||
            motivo.c0836_minutos_paro < 0)
      ).length > 0;
    let validar_motivos =
      this.actividad.motivos.filter(
        motivo => motivo.c0836_rowid_motivo_paro == null
      ).length > 0;

    validar_tiempos =
      validar_tiempos || this.actividad.c0822_fecha_ejecucion_ini == null;
    // console.log('________Actividad ')
    // console.log(this.actividad.motivos);
    if (validar_motivos) {
      this.msg.msgAlerta({
        titulo: "Error en la validacion",
        message:
          "Para agregar otro motivo de paro se debe seleccionar un motivo"
      });
      return;
    }
    if (validar_tiempos) {
      this.msg.msgAlerta({
        titulo: "Error en la validacion",
        message:
          "Para agregar otro motivo de paro se debe de ingresar valores de tiempo validos"
      });
      return;
    }

    // console.log('Se agrega el motivo a la actividad');
    this.actividad.motivos.push({
      c0836_rowid_motivo_paro: null,
      descripcion: "",
      c0836_minutos_paro: "",
      c0836_horas_paro: "",
      c0836_dias_paro: ""
    });
  }

  cargarMotivosParo() {
    let filtro = "";
    // console.log("this.actividad");
    // console.log(this.actividad);
    if (
      this.actividad.c0808_ind_requiere_paro != null &&
      this.actividad.c0808_ind_requiere_paro == 0 &&
      this.actividad.c0819_ind_requiere_paro != null &&
      this.actividad.c0819_ind_requiere_paro == 0
    ) {
      filtro = "where c0835_ind_tipo != 0";
    }

    let sql = `select 
                  c0835_rowid,
                  c0835_id,
                  c0835_ind_tipo 
                from w0835_motivos_paro
                ${filtro}
              `;
    this.db
      .query(sql)
      .then(respuesta => {
        this.motivosParo = respuesta;
      })
      .catch(error => {
        console.error("hubo algun error");
        console.error(error);
      });
  }

  undo = (slidingItem: ItemSliding) => {
    slidingItem.close();
  };

  delete = (motivo: any): void => {
    let index = this.actividad.motivos.indexOf(motivo);
    if (index > -1) {
      this.actividad.motivos.splice(index, 1);
    }
  };

  ngAfterViewInit() {
    this.content.ionScroll.subscribe(d => {
      this.fabButton.setElementClass("fab-button-out", d.directionY == "down");
    });
  }
}
export interface Motivo {
  id: number;
  razon: string;
  c0836_minutos_paro: number;
  c0836_horas_paro: number;
  c0836_dias_paro: number;
}
