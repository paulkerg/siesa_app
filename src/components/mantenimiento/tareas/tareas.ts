import { Component, AfterViewInit, OnInit, Input } from "@angular/core";
import { SqliteProvider } from "../../../providers/sqlite/sqlite";
import {
  ModalController,
  ModalOptions,
  Modal,
  ItemSliding
} from "ionic-angular";
import { global } from "../../../app/global";
/**
 * Generated class for the TareasComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "tareas",
  templateUrl: "tareas.html"
})
export class TareasComponent implements AfterViewInit, OnInit {
  @Input()
  actividad: any;
  tareaSeleccionada;
  ngOnInit(): void {
    //Aqui se va a cargar las tareas
    // this.getTareas();
  }
  ngAfterViewInit(): void {}
  constructor(private db: SqliteProvider, public modal: ModalController) {}
  ionViewWillEnter() {
    //Aqui vuelve y se consulta las tareas
    // console.log("Entro a ionViewWillEnter");
    // console.log("actividad antes: ")
    // console.log(this.actividad)
    this.actividad = global.actividadSel;
    // console.log("this.actividad despues:")
    // console.log(this.actividad)
    // this.getTareas();
  }
  disabledActividad() {
    return (
      (this.actividad.c0822_fecha_ejecucion_ini == null ||
        this.actividad["c0846_ind_estado"] != 1 ||
        this.actividad.c0822_rowid_responsable !=
          global.usuario["f_tercero_rowid"]
          ) && this.actividad.habilitar_error
    );
  }

  getTareas() {
    // console.warn("Consultado las tareas");
    let sql = `
     select 
				c0847_rowid,
        c0847_id_cia,
        c0847_rowid_movto_activ_ot,
        c0847_orden_tarea,
        c0847_descripcion_tarea,
        c0847_notas_tarea,
        c0847_ind_ejecucion,
        c0847_observaciones,
        c0847_fecha_creacion,
        c0847_usuario_creacion,
        c0847_fecha_actualizacion,
        c0847_usuario_actualizacion
      
      from w0847_movto_actividades_tareas 
      where c0847_rowid_movto_activ_ot = ${this.actividad.c0846_rowid}
    `;
    if (!this.actividad["c0846_ind_estado"]) {
      sql = `
     select 
				c0848_rowid,
        c0848_rowid_actividad,
        c0848_orden,
        c0848_descripcion c0847_descripcion_tarea,
        c0848_notas,
        c0848_fecha_creacion,
        c0848_usuario_creacion,
        c0848_fecha_actualizacion,
        c0848_usuario_actualizacion,
        0 c0847_ind_ejecucion
      
      from w0848_actividades_plan_tareas 
      where c0848_rowid_actividad = ${this.actividad.c0822_rowid_actividad}
    `;
    }

    this.db
      .query(sql)
      .then(res => {
        // console.log("Se consultan las tareas");
        // console.log(res);
        this.actividad.tareas = res;
      })
      .catch(error => {
        console.error("ERROR ha ocurrido un error al consultar las tareas");
        console.error(error);
      });
  }

  openModal(tareaSel, slidingItem: ItemSliding) {
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false
    };

    const myModalData = {
      tarea: tareaSel
    };
    global.actividadSel["tareaSeleccionada"] = tareaSel;

    const myModal: Modal = this.modal.create(
      "TareasModalPage",
      { data: myModalData },
      myModalOptions
    );

    myModal.present();

    myModal.onDidDismiss(data => {
      this.getTareas();
    });

    myModal.onWillDismiss(data => {});
    this.undo(slidingItem);
  }

  undo = (slidingItem: ItemSliding) => {
    slidingItem.close();
  };
}
