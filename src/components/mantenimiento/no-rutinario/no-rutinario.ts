import { SqliteProvider } from "./../../../providers/sqlite/sqlite";
import { Component, Input, OnInit, AfterViewInit } from "@angular/core";
import { global } from "../../../app/global";

/**
 * Generated class for the NoRutinarioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "no-rutinario",
  templateUrl: "no-rutinario.html"
})
export class NoRutinarioComponent implements AfterViewInit, OnInit {
  ngAfterViewInit(): void {}
  @Input() actividad: any;
  text: string;
  tipoDanos: any[] = [
    { rowid: 0, descripcion: "No aplica" },
    { rowid: 1, descripcion: "Alto impacto" },
    { rowid: 2, descripcion: "Medio impacto" },
    { rowid: 3, descripcion: "Bajo impacto" }
  ];
  tipoDano: any;
  causalFallas: any[];
  causalFalla: any;
  tipoFallas: any[];
  tipoFalla: any;
  detalleDano: string;
  ngOnInit(): void {
    this.cargarDatos(this.actividad);
  }
  constructor(
    // private content: Content,
    private db: SqliteProvider
  ) {
    this.validar();
  }
  ionViewWillEnter() {
    this.validar();
  }

  validar() {
    if (this.actividad == null) {
      // console.log("La actividad es nula");
      this.actividad = global.actividadSel;
      if (!this.actividad.c0846_rowid_tipo_falla){
        this.actividad.c0846_rowid_tipo_falla=0;
      }
      if (!this.actividad.c0846_rowid_causal_falla){
        this.actividad.c0846_rowid_causal_falla=0;
      }
    }
  }

  handlChangeParoEnCadena() {
    //  console.log("paron en cadena");
    //  console.log(this.actividad.paroEnCadena);
    //  console.log("paron en cadena Global");
    //  console.log(global.actividadSel.paroEnCadena);
  }

  setearTipoDano() {
    // this.actividad.c0819_ind_tipo_dano = this.tipoDano;
    // console.log("this.actividad.c0819_ind_tipo_dano");
    // console.log(this.actividad.c0819_ind_tipo_dano);
    if (this.actividad.c0819_ind_tipo_dano != 0) {
      this.actividad.c0819_ind_dano = 1;
    } else {
      this.actividad.c0819_detalle_dano = "";
    }
  }
  disabledParoEnCadena() {
    let validar = this.disabledActividad();
    if (global.actividadSel.motivos) {
      validar = validar || global.actividadSel.motivos.length == 0;
    }
    if (validar) {
      this.actividad.paroEnCadena = false;
    }
    return validar && this.actividad.habilitar_error;
  }
  disabledTipoFalla() {
    let validar = this.disabledActividad();
    if (this.actividad) {
      validar =
        validar ||
        isNaN(Number(this.actividad.c0822_id_equipo)) ||
        this.actividad.c0822_id_equipo == null;
    }
    return validar && this.actividad.habilitar_error;
  }
  disabledActividad() {
    return (
      (
      this.actividad.c0822_fecha_ejecucion_ini == null ||
      this.actividad["c0846_ind_estado"] != 1 ||
      this.actividad.c0822_rowid_responsable != global.usuario['f_tercero_rowid']
      ) && this.actividad.habilitar_error
    );
  }
  disabledDetalleDano() {
    let validar = this.disabledActividad();
    validar =
      validar ||
      this.actividad.c0819_ind_tipo_dano == 0 ||
      this.actividad.c0819_ind_tipo_dano == null;

    return validar && this.actividad.habilitar_error;
  }

  cargarDatos(actividad) {
    this.cargarTipoDanos();
    this.cargarCausalFallas();
    this.cargarTipoFallas(actividad);
  }
  cargarTipoDanos() {
    // let sql = ``;
    // this.db
    //   .query(sql)
    //   .then(respuesta => {
    //     this.tipoDanos = respuesta;
    //   })
    //   .catch(error => {
    //     console.error("Error al consultar los tipos daños");
    //     console.error(error);
    //   });
  }
  cargarCausalFallas() {
    let sql = `
        select 
          c0805_rowid,
          c0805_id,
          c0805_notas
        from w0805_causales_fallas`;
    this.db
      .query(sql)
      .then(respuesta => {
        this.causalFallas = respuesta;
        this.causalFallas.unshift({ c0805_rowid: 0, c0805_id:'Ninguno'});
      })
      .catch(error => {
        console.error("Error al consultar los tipos daños");
        console.error(error);
      });
  }
  cargarTipoFallas(actividad) {
    if (
      actividad == null ||
      actividad.c0822_id_equipo == null ||
      actividad.c0822_id_equipo == ""
    ) {
      this.tipoFallas = [];
      // console.log("la actividad es nula o no corresponde a un equipo");

      return;
    }

    let sql = `
          select 
            c0804_rowid_tipo_equipo,
            c0804_rowid,
            c0804_id
          from w0804_fallas_tipos_equipos
          inner join w0800_equipos on c0800_rowid_tipo_equipo = c0804_rowid_tipo_equipo 
 														AND					c0800_id = ${actividad.c0822_id_equipo}
    `;
    this.db
      .query(sql)
      .then(respuesta => {
        this.tipoFallas = respuesta;
        this.tipoFallas.unshift({ c0804_rowid: 0, c0804_id:'Ninguno'});
      })
      .catch(error => {
        console.error("Error al consultar los tipos daños");
        console.error(error);
      });
  }
}
