import { MantenimientoServiceProvider } from "./../../../providers/mantenimiento-service/mantenimiento-service";
import { Component, OnInit, Input } from "@angular/core";
import { global } from "../../../app/global";
import { UtilesProvider } from "../../../providers/utiles/utiles";
import { Events } from "ionic-angular";

/**
 * Generated class for the AyudantesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "ayudantes",
  templateUrl: "ayudantes.html"
})
export class AyudantesComponent implements OnInit {
  @Input() actividad: any;
  // actividad: any={
  //   lecturas:"",
  //   c0846_notas:""
  // };
  ngOnInit(): void {
    this.mantenimiento.cargarTeceros().then((res: any) => {
      this.terceros = res;
    });
    // console.log("actividad ayudantes",this.actividad)
    this.actividad = global.actividadSel;
  }

  terceros: any[] = [];
  terceros_temp: any[] = [];
  ayudantes: any[] = [{ c0827_rowid_tercero: 0, nombre: "ayudante1" }];

  constructor(
    private msg: UtilesProvider,
    private mantenimiento: MantenimientoServiceProvider,
    private events: Events
  ) {
    if (
      !global.actividadSel.ayudantes ||
      global.actividadSel.ayudantes.length == 0
    ) {
      this.mantenimiento.cargarAyudantes(global.actividadSel);
    }
    // console.log("Desde Ayudantes global.actividadSel :");
    // console.log(global.actividadSel);
    if (global.actividadSel != null ){
      // if (global.actividadSel.lecturas){
      //   this.actividad.lecturas = global.actividadSel.lecturas;
      // }
      // if (global.actividadSel.c0846_notas){
      //   this.actividad.c0846_notas = global.actividadSel.c0846_notas;
      // }
    if (
      global.actividadSel.ayudantes != null &&
      global.actividadSel.ayudantes.length > 0
    ) {
      this.ayudantes = global.actividadSel.ayudantes;
      this.agergarAyudante();
    }
  }
    this.events.subscribe("actividadSel_change", actividad => {
      this.actividad = actividad;
      // console.log("Cmpte Ayudantes recibe la actividad por evento",this.actividad);
      
    });
  }

  validarAyudantes(index) {
    if (this.ayudantes[index].c0827_rowid_tercero == 0) return true;
    let ayudantes = this.ayudantes.filter(
      (ayudante, current_index) => current_index != index
    );
    let ayudante_repetido = ayudantes.find(
      ayudante =>
        ayudante.c0827_rowid_tercero ==
        this.ayudantes[index].c0827_rowid_tercero
    );
    return !ayudante_repetido;

    //Alerta sobre que no se puede seleccionar ese ayudante
  }
  ionViewWillEnter() {
    // console.log("Ayudantes before:");
    // console.log(this.ayudantes);
    if (global.actividadSel != null) {
      this.actividad.lecturas = global.actividadSel.lecturas;
      this.actividad.c0846_notas = global.actividadSel.c0846_notas;
      if (global.actividadSel.ayudantes.length > 0) {
        this.ayudantes = global.actividadSel.ayudantes;
      }
    }

    // console.log("Ayudantes after:");
    // console.log(this.ayudantes);
  }

  disableActividadLecturas() {
    return global.actividadSel.c0806_ind_regimen == 2;
    // return this.actividad.c0806_ind_regimen == 2;
    //Esto quiere decir que no maneja lecturas
    //si el valor es 2 entonces se puede ingresar valores en el campo lecturas
  }

  disableActividad() {
    return (
      (global.actividadSel.c0822_fecha_ejecucion_ini == null ||
        global.actividadSel.c0846_ind_estado != 1 ||
        global.actividadSel.c0822_rowid_responsable !=
          global.usuario["f_tercero_rowid"]) &&
      global.actividadSel.habilitar_error
    );
    // return (
    //   (this.actividad.c0822_fecha_ejecucion_ini == null ||
    //     this.actividad.c0846_ind_estado != 1 ||
    //     this.actividad.c0822_rowid_responsable !=
    //       global.usuario["f_tercero_rowid"]) &&
    //   this.actividad.habilitar_error
    // );
  }
  handleChangeLectura() {
    if (
      isNaN(Number(this.actividad.lecturas)) ||
      this.actividad.lecturas < 0 ||
      this.actividad.lecturas > 99999999999999999999
    ) {
      // this.actividad.lecturas=0;
      this.actividad.lecturas = Number(
        this.actividad.lecturas.replace(/[^\d]/g, "").replace(",", ".")
      );
      global.actividadSel.lecturas = this.actividad.lecturas;
    }
    // global.actividadSel.lecturas = this.actividad.lecturas;
    // console.log("lecturas: " + global.actividadSel.lecturas);
  }
  handleChangeNotas() {
    global.actividadSel.c0846_notas = this.actividad.c0846_notas;
    // console.log("notas: " + global.actividadSel.c0846_notas);
  }
  handleChangeAyudante(index) {
    if (this.validarAyudantes(index)) {
      this.actividad.ayudantes = this.ayudantes;
      this.agergarAyudante();
    } else {
      //Alerta sobre ayudante repetido por favor valide
      // this.ayudantes[index].c0827_rowid_tercero = 0;
      this.ayudantes.splice(index, 1);
      this.agergarAyudante(index);
      this.msg.msgAlerta(
        "El Ayudante no puede estar repetido, por favor seleccione otro"
      );
    }
    // global.actividadSel.ayudantes = this.ayudantes;
  }
  agergarAyudante(index = null) {
    // console.log("vamos a agregar un ayudante");
    if (this.ayudantes.length < 5) {
      // console.log("Largo correto de: "+this.ayudantes.length);
      if (
        this.ayudantes.filter(ayudante => ayudante.c0827_rowid_tercero == 0)
          .length > 0 &&
        index == null
      ) {
        // console.log("Esto quiere decir que hay alguno sin seleccionar un ayudante");
        return;
      }
      let new_ayudante = {
        c0827_rowid_tercero: 0,
        descripcion: "otro ayudante"
      };
      if (index != null) {
        this.ayudantes.splice(index, 0, new_ayudante);
      } else {
        this.ayudantes.push(new_ayudante);
        // console.log("se agrego Ayudante");
      }
    } else {
      console.log("ya no se pueden agregar mas");
    }
    // console.log("ayudantes models:");
    // console.log(this.ayudantes);
  }

  eliminarDeLista(index: number) {
    this.ayudantes.splice(index, 1);
    // console.log("Se elimina de la lista de ayudantes");
    this.actividad.ayudantes = this.ayudantes;
    // global.actividadSel.ayudantes = this.ayudantes;
  }
}
