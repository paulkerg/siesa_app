import { Component, OnInit, Input } from "@angular/core";
import { SqliteProvider } from "../../../providers/sqlite/sqlite";
import { global } from "../../../app/global";


/**
 * Generated class for the RepuestosComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "repuestos",
  templateUrl: "repuestos.html"
})
export class RepuestosComponent implements OnInit {
  repuestos: any[];
  @Input() c0822_rowid: any;
  ngOnInit(): void {
    // console.log("c0822_rowid: " + this.c0822_rowid);
    // console.warn("global.actividadSel")
    // console.warn(global.actividadSel)
    if (!this.c0822_rowid) {
      this.c0822_rowid = global.actividadSel.id;
    }
    let sql = `
     select 
        w0841.c0841_item_resumen repuesto,
        w0841.c0841_cantidad cantidad_requerida,
        w0841.c0841_rq_cant1_pedida cantidad_pedida,
        w0841.c0841_id_unidad_medida unidad_medida,
        case  
					when w0841.c0841_rq_consec_docto is null then ' ' 
					when w0841.c0841_rq_consec_docto = 'null' then ' ' 
					else w0841.c0841_rq_id_tipo_docto||'-'||w0841.c0841_rq_consec_docto
				end requisicion_numero,
        case when w0841.c0841_rq_estado = 'null' then ' '
        when w0841.c0841_rq_estado = 0 then 'En elaboracion'
        when w0841.c0841_rq_estado = 1 then 'Aprobado'
        when w0841.c0841_rq_estado = 2 then 'Comprometido'
        when w0841.c0841_rq_estado = 3 then 'Cumplido'
        when w0841.c0841_rq_estado = 9 then 'Anulado'
        end estado,
        case when w0841.c0841_ind_critico = 0 then 'NO'
        else 'SI' 
        end critico
      from w0822_actividades_ot w0822 
      left join  w0819_mnto_no_rutinario w0819 on w0819.c0819_rowid_w0822 = w0822.c0822_rowid
      inner join w0841_repuestos_actividades_ot w0841 on w0841.c0841_rowid_actividad_ot = w0822.c0822_rowid or 
                                                      w0841.c0841_rowid_trabajo_ot = w0819.c0819_rowid
      
      where w0822.c0822_rowid = ${this.c0822_rowid}
    `;
    this.db
      .query(sql)
      .then(respuesta => {
        this.repuestos = respuesta;
      })
      .catch(error => {
        console.error("Hubo un error al ejecutar la consulta de repuestos");
        console.error(error);
      });
  }
  constructor(
    // private contentido: Content,
     private db: SqliteProvider) {
       

  }
}
