import { Camera, CameraOptions } from "@ionic-native/camera";
import { Component, OnInit, Input } from "@angular/core";
import { SqliteProvider } from "../../../providers/sqlite/sqlite";

/**
 * Generated class for the CameraComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "camera",
  templateUrl: "camera.html"
})
export class CameraComponent implements OnInit {
  @Input() c0822_rowid: any;
  ngOnInit(): void {
    // console.log("Se inicio el componente de Camara");
    this.cargarImagenes();
  }

  image: string;
  data_image: string;
  imagenes: any[] = [];

  constructor(
    private camera: Camera,
    private db: SqliteProvider //  private content:Content
  ) {
    this.image = "";
  }
  delete(c08221_rowid, index) {
    if(c08221_rowid){

      // console.log("Se eliminara la foto c08221_rowid: "+c08221_rowid);
      let sql = `delete from w08221_fotos where c08221_rowid = ${c08221_rowid}`;
      this.db.queryExec(sql).then(res=>{
        // console.log("Se elimino la foto correctamente");
        console.log(res);
        this.imagenes.splice(index,1);
      }).catch(error=>{
        console.error("Error al intentar elimianr la foto con c08221_rowid: "+c08221_rowid);
        console.error(error);
      })
    }else{
      // console.log("LLega un dato invalido para eliminar");
    }
  }
  cargarImagenes() {
    let sql = `select 
                c08221_foto,
                c08221_rowid 
              from
                w08221_fotos
              where c08221_rowid_w0822 = ${this.c0822_rowid}`;
    this.db
      .query(sql)
      .then(res => {
        this.imagenes = [];
        res.forEach(result => {
          this.imagenes.push({
            image: `data:image/jpeg;base64,${result.c08221_foto}`,
            value: "",
            c08221_rowid:result.c08221_rowid,
          });
        });
      })
      .catch(error => {
        console.error("ERROR no se pudo consultar la imagenes guardadas");
        console.error(error);
      });
  }
  getPicture() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    };
    this.camera
      .getPicture(options)
      .then(imageData => {
        if (imageData) {
          this.image = `data:image/jpeg;base64,${imageData}`;
          this.data_image = imageData;
        }
      })
      .catch(error => {
        console.error("ERROR al obtener la foto");
        console.error(error);
      });
  }

  guardarImagen() {
    // console.log("Se guarda la imagen");
    this.imagenes.push({ image: this.image, value: "path" });
    let sql = `insert into w08221_fotos (c08221_foto,c08221_rowid_w0822) values ('${
      this.data_image
    }',${this.c0822_rowid})`;
    let self = this;
    this.db
      .queryExec(sql)
      .then(res => {
        // console.log("Ya se guardo la imagen");
        self.cargarImagenes();
      })
      .catch(error => {
        console.error("ocurrio algun error al guardar la imagen en la bd");
        console.error(error);
      });
    this.image = "";
    this.data_image = "";
  }
}
