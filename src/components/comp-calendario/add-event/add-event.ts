import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';

@IonicPage()
@Component({
  selector: 'page-add-event',
  templateUrl: 'add-event.html',
})
export class AddEventPage {

  event = {
    title: "",
    location: "",
    message: "",
    startDate: "",
    endDate: ""
  };

  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private calendar: Calendar,
    public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventPage');
  }

  save() {
    this.calendar.createEvent(this.event.title, this.event.location, this.event.message, new Date(this.event.startDate), new Date(this.event.endDate)).then(
      (msg) => {
        /*
        let alert = this.alertCtrl.create({
          title: 'Guardar!',
          subTitle: 'Actualizado con exito',
          buttons: ['Aceptar']
        });
        alert.present();
        this.navCtrl.pop();
        */
        this.dismiss();
      },
      (err) => {
        let alert = this.alertCtrl.create({
          title: 'Failed!',
          subTitle: err,
          buttons: ['Aceptar']
        });
        alert.present();
      }
    );
  }

  dismiss() {
    let data = {
      titulo: this.event.title,
      ubicacion: this.event.location,
      notas: this.event.message,
      fechaInicio: this.event.startDate,
      fechaFin: this.event.endDate
    };
    this.viewCtrl.dismiss(data);
  }

}
