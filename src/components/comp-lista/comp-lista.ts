import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the CompListaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'comp-lista',
  templateUrl: 'comp-lista.html'
})
export class CompListaComponent {
  @Input('pCompTextoColor') pCompTextoColor: string = '#FFF';
  @Input('pCompContenedorColor') pCompContenedorColor: string = '#F9F9F9';
  @Input('pCompBlnBadge') pCompBlnBadge: boolean = false;
  @Input('pCompBlnboton') pCompBlnboton: boolean = false;
  @Input('pCompBlnImagen') pCompBlnImagen: boolean = false;
  @Input('pCompRegistros') pCompRegistros: any[];

  
  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  constructor() {
    
  }

   // Eventos
   public onclikItem(e: any): void {
    this.eventoClick.emit(e);
  }
}
