import { Component, Input } from "@angular/core";
/**
 * @name CompBarraProgresoComponent
 * @description
 * Componente de barra de progreso
 *
 * @Modo de uso
 * ```typescript
 * <comp-barra-progreso [progreso]="porcentaje"></comp-barra-progreso>
 *
 * ```
 */
@Component({
  selector: "comp-barra-progreso",
  templateUrl: "comp-barra-progreso.html"
})
export class CompBarraProgresoComponent {
  /**
   * Seteo del valor del progreso
   * @param {number} pCompProgreso  nuevo numero de progreso.
   *
   */
  @Input("pCompProgreso") pCompProgreso;
  @Input("pTextoProgreso") pTextoProgreso;
  @Input("pRangosColores") pRangosColores;
  texto: string = "0";
  constructor() {}

  textoMostrar() {
    if (this.pTextoProgreso && this.pTextoProgreso!="") {
      this.texto = this.pTextoProgreso;
    } else {
      this.texto = this.pCompProgreso + "%";
    }
    return this.texto;
  }

  validarColores() {
    if (this.pRangosColores) {
      let rango = this.pRangosColores.find(rango =>rango.porcentaje(this.pCompProgreso));
      if(rango){
        return rango.color;
      }else{
        return '';
      }
    } else {
      return "";
    }
  }
}
