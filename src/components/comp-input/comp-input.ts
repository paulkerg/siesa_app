import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'comp-input',
  templateUrl: 'comp-input.html'
})
export class CompInputComponent {
  @Input('pCompProgreso') pCompProgreso;
  @Input('pCompTipoInput') pCompTipoInput: string;
  @Input('pCompLabel') pCompLabel: string;
  @Input('pCompModelo') pCompModelo: string;
  @Input('pCompNombre') pCompNombre: string;
  @Input('pCompPlaceHolder') pCompPlaceHolder: string;
  @Input('pCompMascara') pCompMascara: string;
  @Input('pCompMinimo') pCompMinimo: number;
  @Input('pCompMaximo') pCompMaximo: number;
  @Input('pCompSoloLectura') pCompSoloLectura: boolean = false;
  @Input('pCompBotonLimpiar') pCompBotonLimpiar: boolean = false;
  @Input('pCompLimpiarAlEditar') pCompLimpiarAlEditar: boolean = false;

  @Output() eventoEnter: EventEmitter<any> = new EventEmitter();
  mascaras = {
    telefono: ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    numeroTarjeta: [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    expiracionTarjeta: [/[0-1]/, /\d/, '/', /[1-2]/, /\d/],
    codigoOrden: [/[a-zA-z]/, ':', /\d/, /\d/, /\d/, /\d/]
  };

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.mascaras[this.pCompMascara])
  }


  teclaPresionada(e) {
    if (e == 13) {
      this.eventoEnter.emit(e);
    }
  }

}
