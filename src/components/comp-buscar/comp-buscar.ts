import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'comp-buscar',
  templateUrl: 'comp-buscar.html'
})
export class CompBuscarComponent {
  @Input('pCmptFecha') pCmptFecha: any;
  @Input('pCmptBuscar') pCmptBuscar: any;
  @Input('pCompBlnBadge') pCompBlnBadge: boolean = false;
  @Input('pCompBlnImagen') pCompBlnImagen: boolean = false;
  @Input('pCompRegistros') pCompRegistros: any[];
  @Input('pCmptBotonesDerecha') pCmptBotonesDerecha: any = [];
  @Input() columnas: any;
  columnasInicial: string = 'columnas-8';
  columnasFinal: string = 'columnas-4';
  @Output() eventoClick: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  // Eventos Buscar
  public onclikItem(e: any): void {
    this.columnasInicial = 'columnas-8';
    this.columnasFinal = 'columnas-4';
    this.eventoClick.emit({
      opt: 'texto', value: e.target.value
    });
  }

  // Eventos Buscar por fecha
  public onclikFecha(): void {
    this.columnasInicial = 'columnas-6';
    this.columnasFinal = 'columnas-6';
    this.pCmptBuscar = '';
    this.eventoClick.emit({
      opt: 'fecha', value: this.pCmptFecha
    });
  }

}
