import { Component, Input, Output, EventEmitter } from '@angular/core';
/**
 * @name CompListaCompletaComponent
 * @description
 * The essential purpose of badge numbers is to enable an application to inform its users that it has something for them — for example, unread messages — when the application isn’t running in the foreground.
 *
 * Requires Cordova plugin: cordova-plugin-badge. For more info, please see the [Badge plugin docs](https://github.com/katzer/cordova-plugin-badge).
 *
 * @usage
 * ```HTML
 * <comp-lista-completa [blnImagen]="true" (eventoClick)="accion($event)" [registros]="regLista"></comp-lista-completa>
 *
 * constructor() { }
 *
 * ...
 * @param pCompTextoColor color del texto
 * @param pCompContenedorColor Color del contenedor
 * @param pCompBlnBadge boolean para visualizar Badge
 * @param pCompBlnImagen boolean para visaualizar imagen a la izquierda
 * @param pCompRegistros arreglo con los registros {imagen:string, titulo:string, subTitulo:string, texto1:string, texto2:string, texto3:string}
 * ```
 */

@Component({
  selector: 'comp-lista-check-box',
  templateUrl: 'comp-lista-check-box.html'
})
export class CompListaCheckBoxComponent {
  @Input('pCompTituloLista') pCompTituloLista: string = '';
  @Input('pCompTextoColor') pCompTextoColor: string = '#FFF';
  @Input('pCompContenedorColor') pCompContenedorColor: string = '#F9F9F9';
  @Input('pCompBlnBtn') pCompBlnBtn: boolean = false;
  @Input('pCompBtnImagen') pCompBtnImagen: string = "";
  @Input('pCompRegistros') pCompRegistros: any[];

  @Output('eventoClick') eventoClick: EventEmitter<any> = new EventEmitter();
  @Output('eventoClickBtn') eventoClickBtn: EventEmitter<any> = new EventEmitter();
  @Output('eventoClickCheck') eventoClickCheck: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  // Eventos
  public onclikItem(e: any): void {
    this.eventoClick.emit(e);
  }

  public onclikItemBtn(e: any): void {
    this.eventoClickBtn.emit(e);
  }

  public actualizaEstados(){
    this.eventoClickCheck.emit(this.pCompRegistros);
  }

}
