import { Component,Input,Output,EventEmitter } from '@angular/core';
/**
 * @param pCompRegistros arreglo con los registros {imagen:string, titulo:string, subTitulo:string, texto1:string, texto2:string, texto3:string}
 * ```
 */
@Component({
  selector: 'comp-item-completo',
  templateUrl: 'comp-item-completo.html'
})
export class CompItemCompletoComponent {
  @Input('pCompRegistros') pCompRegistros: any[];
  @Input('pCompRegistros2') pCompRegistros2: any[];
  @Output() eventoClick: EventEmitter<any> = new EventEmitter();
  constructor() {
    console.log('Hello CompItemCompletoComponent Component ',this.pCompRegistros2);
  }

  public onclikItem(e: any): void {
    this.eventoClick.emit(e);
  }
  public onclikItem1(e: any): void {
    this.eventoClick.emit(e);
  }
}
